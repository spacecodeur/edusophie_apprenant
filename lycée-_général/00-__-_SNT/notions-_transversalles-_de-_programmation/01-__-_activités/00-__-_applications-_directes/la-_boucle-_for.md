# la boucle for

1) Créez à l'aide d'une boucle for un code qui affiche tout les nombres compris entre 0 compris et 25 compris

widget python

widget

En fouillant sur Internet, nous découvrons que la fonction `range` peut utiliser plusieurs paramètres.

![https://i.ibb.co/cCy1MFP/Screenshot-from-2021-09-02-21-43-41.png](https://i.ibb.co/cCy1MFP/Screenshot-from-2021-09-02-21-43-41.png)

2) Modifiez la fonction range afin de n'afficher que les nombres pairs (0 2 4 6 ...) de 0 compris à 25 compris

widget python

widget

+++spoil "indice"
  Par défaut, la fonction <code>print()</code> ajoute un retour à la ligne après son appel. Pour éviter ce retour à la ligne automatique, vous pouvez renseignez le paramètre <code>end</code> de print comme suit : <code>print('bonjours', end="")</code>
spoil+++

3) Modifiez la fonction range afin de n'afficher que les nombres multiples de 5 entre 5 compris et 25 compris (résultat attendu : 5 10 15 20 25)

widget python

widget

## Réponses

<ol>
  <li>+++spoil

widget python
for i in range(25):
  print(i, end=" ")
widget

  spoil+++
  </li>
  <li>+++spoil

widget python
for i in range(0, 26, 2):
  print(i, end=" ")
widget

  spoil+++
  </li>
  <li>+++spoil

widget python
for i in range(5, 26, 5):
  print(i, end=" ")
widget

  spoil+++
  </li>
</ol>