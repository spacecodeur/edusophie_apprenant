# Manipulation de variables

## Analyse de code

1. Deviner le résultat qui sera affiché après l'exécution du code

```python
# Python 3
# les commentaires commencent par un # en python. Après un #, le code n'est pas exécuté par l'ordinateur. Les commentaires permettent de décrire un programme.

print('Bienvenue !') 

a = 6 # je déclare la variable 'a' et lui affecte la valeur 6
b = 20 # je déclare la variable 'b' et lui affecte la valeur 20
c = "hello !" # je déclare la variable 'c' et lui affecte la chaine de caractère "hello !"

d = a + b
print(d)

d = b + a
print(d)

d = b - a
print(d)

d = a - b
print(d)

d = b / a
print(d)

d = a / b
print(d)

d = a * b
print(d)

d = b * a
print(d)

d = a + c
print(d)
```

Lorsqu'on exécute le code, une erreur s'affiche : 

<span class="text-red">[Error] line 31, TypeError: unsupported operand type(s) for + : 'int' and 'str'</span><br>

2. La ligne 31 est la ligne qui contient `d = a + c`. À quoi cela est dû ? proposez une correction du code (plusieurs solutions sont possibles)

## Réponses

<ol>
    <li>
        +++spoil
            widget python 
print('Bienvenue !') 

a = 6 # je déclare la variable 'a' et lui affecte la valeur 5
b = 20 # je déclare la variable 'b' et lui affecte la valeur 25
c = "hello !" # je déclare la variable 'c' et lui affecte la chaîne de caractère "hello !"

d = a + b
print(d)

d = b + a
print(d)

d = b - a
print(d)

d = a - b
print(d)

d = b / a
print(d)

d = a / b
print(d)

d = a * b
print(d)

d = b * a
print(d)

d = a + c
print(d)
            widget      
        spoil+++    
    </li>
    <li>
        +++spoil
            L'ordinateur ne comprend pas comment additionner une chaîne de caractères (<code>string</code>) et un nombre (<code>integer</code>). Pour ce genre d'opération, on souhaite en général concaténer le mot au nombre. Si c'est ce comportement que nous souhaitons, nous aurons alors comme résultat '<code>6hello !</code>'<br><br>

            Pour le faire comprendre à l'ordinateur, nous changeons le type de notre nombre pour en faire une chaîne de caractère. Ainsi, l'ordinateur comprendra naturellement que si nous souhaitons additionner deux chaînes de caractères alors il faut simplement les coller l'une à l'autre.<br><br> 
            
            La plupart des langages informatiques mettent à disposition des fonctions prêtes à l'emploi et qui permettent de changer le type d'une donnée contenue dans une variable.
            
            widget python
print('Bienvenue !') 

a = 6 # je déclare la variable 'a' et lui affecte la valeur 5
b = 20 # je déclare la variable 'b' et lui affecte la valeur 25
c = "hello !" # je déclare la variable 'c' et lui affecte la chaîne de caractère "hello !"

d = a + b
print(d)

d = b + a
print(d)

d = b - a
print(d)

d = a - b
print(d)

d = b / a
print(d)

d = a / b
print(d)

d = a * b
print(d)

d = b * a
print(d)

d = str(a) + c
print(d)
            widget 
            +++bulle matthieu
                Une des pires situations en informatique est de se retrouver avec un programme dont on n'est pas certain qu'il fonctionne et qui ne retourne aucune erreur
            +++
            +++bulle matthieu droite
                Introduire des erreurs dans un ordinateur est le lot de tout informaticien. Elles sont en général d'une grande aide, il ne faut pas prendre peur face à du <span class="text-red"><strong>texte rouge</strong></span> et s'appliquer à analyser et comprendre les erreurs
            +++
        spoil+++
    </li>
<ol></ol></ol>