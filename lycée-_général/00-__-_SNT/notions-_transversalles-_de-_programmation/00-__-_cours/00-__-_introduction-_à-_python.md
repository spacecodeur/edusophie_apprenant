# Introduction à python

+++ sommaire 1

## Introduction

Le programme de seconde aborde un ensemble de thème : 
- la photographie numérique
- les données structurées et leur traitement
- Localisation, cartographie et mobilité
- Internet
- Le Web
- Les réseaux sociaux
- Informatique embarquée et objets connectés

Et également le thème 'Notions transversales de programmation'. Cela signifie qu'à travers l'ensemble des thèmes, vous serez amener à coder des programmes. Le principal langage de programmation utilisé au lycée est python (dans sa version 3).

+++bulle matthieu
    <b class="text-red">Attention !</b> Python met à disposition un grand nombre d’instructions dont des instructions propres à ce langage. Or Le but au lycée n’est pas de faire de vous des programmeurs python
+++

+++bulle matthieu droite
    Nous omettrons donc volontairement des instructions propres au langage pour nous concentrer sur l’essentiel et cela vous permettra aussi de vous adapter plus facilement à d'autres langages de programmation
+++

## Les variables

### Des variables dans notre quotidien

Lors de  l'élaboration d'une tâche complexe, nous manipulons un certain nombre d'**éléments**. Nous avons besoin parfois de les **stocker** temporairement dans un **conteneur**, de les **modifier**, de les **combiner**, etc.. 

<br>
<img class="rounded-pill" src="https://i.ibb.co/4f2YbMK/CREATOR-gd-jpeg-v1-0-using-IJG-JPEG-v80-quality-90.jpg" height="180">

Une peintre utilise comme **élément** des peintures **stockées** dans des **conteneurs** (des pots de peinture). Parfois, elle aura par exemple besoin de **combiner** deux couleurs pour en créer une nouvelle qu'elle **stockera** dans un nouveau pot de peinture. 

<br>
<img class="rounded-pill" src="https://i.ibb.co/hDMMkvZ/Cooking-Food-Boat-Pots-River-Boat-Pans-Kitchen.jpg" height="180">

Un cuisinier lui utilise comme **éléments** des ingrédients, des assaisonnements, etc... Il **stocke** dans un **container** (un bol) du riz qu'il fait cuire ensuite dans une casserole. Le cuisinier remettra ensuite le riz, cette fois cuit, dans son bol initial. L'état du riz dans ce bol a donc été **modifié** de cru à cuit.

### Quid de l'informatique ? 
<img class="rounded-pill" src="https://i.ibb.co/XCcGmxX/SSUCv3-H4s-IAAAAAAAACpx-VSW7c-MBC8-B8gf-DJ5-Dg-Pu-Srw-Q5-UFw8gi-Vx-IFFJDMN-D7-Vw-TMk-TJ8ht-VF1sdleze.jpg" style="max-height:250px">

En informatique, nous avons aussi besoin de manipuler des **éléments** ; les **stocker** dans des **conteneurs**, les **modifier**, les **combiner**, etc... 

- Un **conteneur** en programmation informatique est un emplacement dans la mémoire de l'ordinateur. Le **conteneur** le plus couramment utilisé est une **variable**.

+++diagramme
    flowchart TD
    A1[\bol de cuisine/]
    B1[\pot de peinture/]
    C1[\ <b>variable</b> a/]
    C2[\ <b>variable</b> b/]
    C3[\ <b>variable</b> c/]
+++

<br>
- Un **élément** en programmation informatique correspond au **type** de la **variable**. Ce **type** peut être entre autres : 
    - soit un nombre entier (+++flag **`integer`** ou **`int`**) ou décimal (+++flag **`float`**)
    - soit un caractère (+++flag **`char`**) ou une suite de caractères (+++flag **`string`**), par exemple : `'a'`, `'bonjour'`
    - soit un booléen (+++flag **`boolean`** ou +++flag **`bool`**) de valeur soit Vrai (+++flag **`True`**), soit Fausse (+++flag **`False`**)

+++diagramme
    flowchart TD
    A[<img src="https://i.ibb.co/bRG2dQZ/curcuma.jpg" width="100"><div>Curcuma</div>]-->|stocké dans|A1[\bol : pour le riz/]
    C[<img src="https://i.ibb.co/N9zFv8B/peinture-bleu.jpg" width="100"><div>Gouache bleu</div>]-->|stocké dans|B1[\pot : pour la gouache/]
    E[<code>15.17</code>]==>|stocké dans|C1[\ <b>variable</b> a : <br>pour un nombre décimal/]
    F[<code>'hello !'</code>]==>|stocké dans|C2[\ <b>variable</b> b : <br>pour une chaîne de caractères/]
    G[<code>True</code>]==>|stocké dans|C3[\ <b>variable</b> c : <br>pour un booléen booléen/]
+++
<br>

#### Exemple de manipulation de variables

+++bulle matthieu
Pendant le cours, pour des questions pratiques, je vous présente du code directement intégré au cours
+++

+++bulle matthieu droite
Lorsque vous écrirez du code, vous utiliserez un IDE (logiciel spécialisé dans l'écriture de code). Cela facilitera le débogage et l'organisation du code (séparé en plusieurs fichiers par exemple)
+++

widget python 
# Par convention, les variables sont déclarées et (idéalement) affectées au début du programme

a = 10 # je déclare la variable a et lui affecte la valeur 10
b = 5 # je déclare la variable b et lui affecte la valeur 5

print(a) # print est une fonction
print(b,a)
print(a + a)

a = b + 1 + a
print(a)

widget

widget python
c = "hello" # je déclare la variable c et lui affecte la chaîne de caractère "hello !"
d = "!" # je déclare la variable d et lui affecte le char '!'

print(c,d)

widget

#### Rapide détour sur la notion de fonction

Vous utilisez des fonctions depuis plusieurs années en mathématique. Qu'est-ce qui a pu pousser les mathématiciens à créer ce type "d'objet" ?

+++spoil
Pour une question pratique ! Afin de ne pas réécrire chaque fois des mêmes calculs réutilisés de multiples fois, les calculs sont enfermés dans une fonction. Et il ne reste plus qu'à utiliser ladite fonction.

Ici, <code>print</code> regroupe un une séquence de code qui permet d'afficher ce qui est mis en paramètre de <code>print</code>
spoil+++

### Résumons +++emoji-writing_hand 
- Une variable permet de stocker dans la mémoire de l'ordinateur une donnée 
- Le type d'une variable correspond au type de la donnée qu'elle contient. Le type peut être : 
    - soit `integer` ou `int` : la donnée est un nombre entier (ex : `-7`)
    - soit `float` : la donnée est un nombre décimal (ex : `10.5`)
    - soit `char` : la variable est un caractère (ex : `"k"`)
    - soit `string` : la variable est une chaîne de caractères (ex : `"hello !"`)
    - soit `boolean` ou `bool` : la variable contient la valeur `True` ou `False`
- Le signe '`=`' <u>**seul**</u> permet d'affecter à une variable une valeur, ex :
    - `a = 17 # on affecte dans la case mémoire nommée 'a' la valeur 17`
    - `a = 10 + a # on affecte denouveau dans la case mémoire nommée 'a' une donnée qui est le résultat de l'addition de 10 et du cnotenu de la variable  'a'. 'a' devient donc égal à 27 (10 + 17)`
- Il faut toujours s'assurer que les variables qu'on manipule soient du même type. Sinon une erreur apparaîtra (dans le meilleur des cas) et stoppera le programme.

+++activité individuel 10 ../01-__-_activites/00-__-_applications-_directes/debranche-_--_-_manipulation-_de-_variables.md "Manipulation de variables"
    <ul>
        <li>Faire l'exercice mis en lien</li>
        <li>Après relecture du cours, si vous rencontrez des difficultés, appelez l'enseignant</li>
        <li>Si l'exercice est terminé en avance, aider vos camarades. <b class="text-red"></b></li><b class="text-red">
        </b><ul><b class="text-red"></b><li><b class="text-red">Attention :</b> respectez bien <b>la charte d'entraide</b></li></ul>
    </ul>
+++

## Les structures itératives

Les langages de programmation fournissent des structures permettant de répéter une séquence de code. Le comportement d'une boucle se comporte **toujours** de la manière suivante : 

+++diagramme col-8 mx-auto
flowchart TB
  A["<b>[1]</b> Initialisation des variables nécessaires"]-->B
  B{{"<b>[2]</b> Condition de la boucle"}}-->|True|C
  C["<b>[3]</b> Exécution du contenu de la boucle"]-->B
  B-->|False|D["<b>[4]</b> Sortie de boucle"]
+++

Les deux instructions les plus communes entre les langages de programmation sont les instructions `for` et `while`.

Nous nous intéresserons ici pour l'instant qu'à l'instruction `for`, en voici un exemple d'utilisation : 

widget python
count = 10
sum_count = 0

for i in range(count): # [1 et 2] Initialisation et condition de terminaison de la boucle

    # [3] Contenu de la boucle
    print(i)
    sum_count = sum_count + i
    
# [4] Sortie de la boucle

print('Résultat du calcul : ',sum_count)
print('Nombre d\'additions : ',count)
widget

La spécialité de l'instruction `for` est de créer rapidement une boucle qui va itérer un nombre entier de fois. Attardons nous sur la ligne 4 du code ci-dessus : `for i in range(count)`

`range()` est une fonction qui génère un ensemble de nombres entiers. `count` est égal à 10, donc `range(count)` génère la suite de nombre : 0 1 2 3 4`5 6 7 8 9

`for i in range(count)` signifie : 
<div style="padding-left:1rem">
    <div>1. déclare une variable <code>i</code></div>
    <div>2. affecte à la variable <code>i</code> la première valeur générée par la fonction `range` (la première valeur est 0)</div>
    <div><span class="text-blue"><b>3</b></span>. exécute le contenu de la boucle</div>
    <div>4. incrémente la variable <code>i</code> de 1</div>
    <div>5. Est-ce que <code>i</code> est inférieur ou égal au dernier nombre généré par la fonction `range` ? (qui est 9 ici)</div>
    <ul>
        <li>Si oui (`True`) : revenir à l'étape <span class="text-blue"><b>3</b></span></li>
        <li>Si non (`False`) : sortir de la boucle</li>
    <ul>
</ul></ul></div>

+++activité individuel 15 ../01-__-_activites/00-__-_applications-_directes/la-_boucle-_for.md "La boucle for"
<ul>
  <li>Faire l'exercice mis en lien</li>
  <li>Après relecture du cours, si vous rencontrez des difficultés, appelez l'enseignant</li>
  <li>Si l'exercice est terminé en avance, aider vos camarades. <b class="text-red"></b></li><b class="text-red">
  </b><ul><b class="text-red"></b><li><b class="text-red">Attention :</b> respectez bien <b>la charte d'entraide</b></li></ul>
</ul>
+++

### Résumons +++emoji-writing_hand 
 
- La plupart des langages de programmation fournissent au moins ces deux instructions pour répéter une séquence de code : la boucle `while` et la boucle `for`
- Lorsqu'on souhaite répéter un nombre entier de fois une séquence de code, l'instruction `for` est en général la plus pratique 

<div class="d-none">
## Les fonctions

Les fonctions sont un concept que vous connaissez et manipulez déjà depuis plusieurs années en mathématique. 

widget python 
personnage_pointsDeVie = 20
personnage_prenom = "Matthieu"

ennemi_type = 'souris'

print('Nom de votre personnage : ', personnage_prenom)
print('Nombre de vos points de vie : ', personnage_pointsDeVie)

print()
print(personnage_prenom, ' se fait attaquer par un ', ennemi_type , ' !!')

personnage_pointsDeVie = personnage_pointsDeVie - 3
print('Nombre de vos points de vie : ', personnage_pointsDeVie)
    
widget

widget python 
a = 20
b = "Matthieu"
c = 'souris'

print(b)
print(a)

print()
print(personnage_prenom, ' se fait attaquer par un ', ennemi_type , ' !!')

personnage_pointsDeVie = personnage_pointsDeVie - 3
print('Nombre de vos points de vie : ', personnage_pointsDeVie)
    
widget


</div>