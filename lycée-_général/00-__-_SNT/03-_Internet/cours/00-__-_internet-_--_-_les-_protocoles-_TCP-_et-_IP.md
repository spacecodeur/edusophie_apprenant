# Internet : les protocoles TCP et IP

+++programme
  Distinguer le rôle des protocoles IP et TCP. 
  Caractériser les principes du routage et ses limites. 
  Distinguer la fiabilité de transmission et l’absence de garantie temporelle. 
  
  Comprendre comment fonctionne le réseau Internet
  Différencier Internet du Web
+++

+++sommaire 2

## Introduction

### Qu'est-ce qu'Internet ?

+++bulle r3do
  comment définiriez vous Internet ?
+++

+++bulle r3do droite
  depuis quand Internet existe d'après vous ?
+++

+++video "https://www.youtube.com/watch?v=hrzXdKvG1CY" "L'histoire d'Internet" 00:05

[Complément de la vidéo] Les premiers réseaux créés étaient des réseaux centralisés. Paul Baran et Donald Davies ont conçu le premier réseau décentralisé. Imaginons le réseau fictif suivant :

<div class="d-flex col-12 justify-content-between">
  <div class="col-5">
    +++image "/images/md/G36779DDE472GC8D4H7252D5D3GHB4"
    <p class="text-center">réseau <b>centralisé</b></p>
  </div>
  <div class="col-5">
    +++image "/images/md/H97BAHD62E7A9H2DG83GD5A8DDD84G"
    <p class="text-center">réseau <b>décentralisé</b></p>
  </div>
</div>

+++question write "<b>[00:24]</b> Pourquoi un réseau décentralisé est plus résistant aux pannes techniques qu'un réseau centralisé ?"

+++spoil
  Dans le réseau centralisé, une panne technique sur le nœud central peut mettre hors de service l'ensemble du réseau. Alors que dans un réseau décentralisé, si un nœud est hors service, l'échange d'information entre les autres nœuds n'est pas impacté
spoil+++

+++question write "<b>[01:32]</b> Rappelez ce qu'est un protocole"

+++spoil
  Un protocole informatique est un ensemble de règles sur lesquelles s'accordent différents appareils pour pouvoir communiquer entre eux
spoil+++

+++question write "<b>[01:32]</b> À quoi sert le protocole TCP ? à quoi sert le protocole IP ?"

+++spoil
  Le protocole TCP se charge du découpage d'une données et paquets et assure leur transmission dans un réseau. Le protocole IP permet à chaque ordinateur d'un même réseau de posséder une adresse unique. 
spoil+++

+++question write "<b>[2:27]</b> En dehors du Web, quelles autres applications utilisant Internet connaissez vous ?"

+++spoil
<ul>
  <li>Logiciels de messagerie instantanée / vidéo-conférence : Skype, Discord, ...</li>
  <li>Client mail "lourds" : thunderbird, outlook, apple mail, ...</li>
  <li>Jeux vidéos en ligne</li>
  <li>...</li>
</ul>
spoil+++

---

+++bulle r3do
  est-ce que des personnes parmi vous possèdent des objets connectés à la maison ? 
+++

### Résumons

- Internet est la contraction « inter-Networks » c’est à dire « entre réseaux ». Internet est donc  l’interconnexion de réseaux situés un peu partout sur la planète
- Pour qu'une information puisse circuler d'une machine à une autre via Internet, les protocoles TCP et IP sont utilisés
- Le protocole TCP assure la transmission des données (une photo, un fichier texte, ...) à travers le réseau
- Le protocole IP permet à chaque machine d'obtenir un identifiant (l'ip) au sein d'un réseau

## Simulation d'un envoi de donnée

Le protocole IP permet simplement d'identifier les machines connectées dans un réseau. Afin de comprendre les règles utilisées dans les protocoles TCP, nous allons simuler dans la classe l'envoi d'une donnée d'un émetteur à un destinataire. 

Une donnée peut prendre de multiples formes, cela peut être une image, une musique ou même un film. Imaginons ici que la donnée que nous souhaitons envoyer soit un message textuel.

+++todo pas simple de canaliser la classe, je pense que la configuration de la salle n'aide pas. Partie à retravailler peut être

+++consignes
  <b>Initialisation : </b>
  <ol start="0">
    <li>Déplacez vos chaises afin de vous retrouver à porté de bras d'au moins deux autres élèves</li>
  </ol>
  <b>Déroulement : </b>
  <ol start="0">
    <li>L'élève le plus proche de l'enseignant écrit un message sur un support destiné à un autre élève situé de l'autre côté de la classe</li>
    <li>Le message ne peut être confié que de voisins en voisins</li>
    <li>Lorsqu'une personne a fait passer un message, elle se lève et reste debout</li>
  </ol>
+++

### C'est parti !

+++bulle r3do
  après le premier essai, est-ce que le message a bien été reçu ?
+++

+++bulle r3do droite
  imaginons que l'émetteur et le destinataire du message soient à une distance de 100km l'un de l'autre
+++

+++question write "Comment l'émetteur peut-il s'assurer que son message a bien été reçu ?"

+++spoil
  le destinataire doit renvoyer un message de confirmation à l'émetteur. Pour pouvoir envoyer un accuser de réception, le destinataire doit connaître l'identifiant de l'émetteur. Donc le message initial doit contenir l'identifiant de l'émetteur
spoil+++

---

+++bulle r3do
  en classe, nous nous identifions avec nos prénoms (plus nos noms si par exemple deux personnes portent le même prénom)
+++

+++bulle r3do droite
  en informatique, les différents nœuds d'un réseau sont identifiés par leur adresse IP, voici un exemple d'adresse IP "192.168.1.29"
+++

---

+++question write "Comment gérer la transmission d'une données volumineuses ?"

+++spoil
  Les données sont découpées en plusieurs paquets afin d'éviter de perdre entièrement la donnée initiale si un nœud du réseau tombe en panne. Chaque paquet est numéroté afin que le destinataire puisse recomposer la donnée dans le bon ordre
spoil+++

+++question write "Dû à une panne sur le réseau, un paquet peut être définitivement perdu. Comment gérer ce cas de figure ?"

+++spoil
  L'émetteur envoie périodiquement le paquet sur le réseau tant qu'il n'a pas reçu d'accuser de réception
spoil+++



<hr>

+++bulle r3do
  en cas de panne, une paquet peut être amené à circuler indéfiniment dans le réseau
+++

+++bulle r3do droite
  ce qui peut poser des problèmes de surcharge de réseau, en effet sa capacité physique n'est pas infinie
+++

+++question write "En prenant en compte le fait qu'un paquet peut circuler en boucle sans jamais arriver à destination, comment éviter de surcharger le réseau ?"

+++spoil
  La paquet doit avoir une date de fin de vie. Ã chaque passage à travers un nœud, la vie du paquet du réduite de 1.
spoil+++

+++question "(Aller plus loin) Comment éviter qu'une information sensible soit lue par une autre personne que le destinataire ?"

+++pagebreak

### Résumons

- Un réseau informatique permet à un émetteur d'envoyer des données de toute sorte à un destinataire : des fichiers textes, des images, des musiques, des films, ...
- **Un réseau informatique est composé d'un ensemble de nœuds**, chaque nœud est un ordinateur qui est capable de recevoir des données et de les retransmettre
- Le **protocole TCP découpe une donnée en milliers, voir millions, de paquets** (en fonction de la taille de la donnée à envoyer). Cela permet de ne pas perdre l'entièreté d'une donnée en cas de panne d'une des machines dans le réseau. 
- Un **paquet TCP** contient, entre autres, les informations suivantes : 
    - l'**adresse IP**, bien entendu, du destinataire
    - l'**adresse IP** de l'émetteur, ceci afin que le destinataire puisse envoyer un accuser de réception une fois le paquet n°x reçu
    - une **durée de vie** (fixée à 255 avec le protocole TCP) et qui est réduit de 1 chaque fois qu'un paquet passe par un noeud du réseau. Si la vie tombe à 0, la paquet est automatiquement détruit. Ceci permet d'éviter de surcharger le réseau de paquets qui ne trouvent pas leur destinataire

<div class="d-none">

À dispatcher


- Sur des exemples réels, retrouver une adresse IP à partir d’une adresse symbolique et inversement.

- Caractériser quelques types de réseaux physiques : obsolètes ou actuels, rapides ou lents, filaires ou non.
- Caractériser l’ordre de grandeur du trafic de données sur internet et son évolution.

- Décrire l’intérêt des réseaux pair-à-pair ainsi que les usages illicites qu’on peut en faire.
 

</div>