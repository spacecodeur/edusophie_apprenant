# Le réseau Internet dans sa globalité

+++programme /images/md/888556C5D3GG8HA2D2E3A3AAE8865D
  Caractériser quelques types de réseaux physiques : obsolètes ou actuels, rapides ou lents, filaires ou non.
  Caractériser l’ordre de grandeur du trafic de données sur internet et son évolution.
  
  Comparer les différents types de réseaux utilisés par Internet
  Convertir des bits en octets
+++

+++sommaire

## Précédemment...


+++bulle r3do
  qu'est-ce que TCP/IP ?
+++

+++spoil
  TCP et IP sont deux protocoles utilisés pour échanger des données (fichier, musique, films, ...) entre deux ordinateurs à travers Internet
spoil+++

+++bulle r3do
  sous quelle forme est envoyée, par exemple, une musique dans un réseau utilisant le protocole TCP ?
+++

+++spoil
  La musique est découpée en petits paquets numérotés de données. Ces paquets sont envoyés au destinataire à travers le réseau.
spoil+++

+++bulle r3do
  cette musique circule à travers le réseau mondial Internet. Nous nous intéressons dans ce cours à l'architecture de ce réseau mondial 
+++

## Internet dans le monde

+++video "https://www.youtube.com/watch?v=9UMvyfT4V_Y" "Comment fonctionne Internet ?" 00:00

+++question write "<b>[1:16]</b> Pour quelle raison transmettre des données liées à Internet par satellite n'est pas privilégiée ?"

+++spoil
  Car par satellite le débit est limité par les distances physiques entre émetteur et destinataire 
spoil+++

--- 

+++image "/images/md/5H9C46HBE73F2BA88359AC9EAGABAC" col-8 mx-auto

Un satellite artificiel se trouve en moyenne à <b class="text-red">36 000 km</b> d'altitude. 

La distance à vol d'oiseau entre Paris et New York est environ de <b class="text-green">5 836 km</b>. Supposons qu'un satellite soit positionné dans l'espace **exactement entre** Paris et New York : 

+++question write "Quelle distance doit parcourir une donnée entre Paris et le satellite?"

+++spoil
  <p>En s'aidant du théorème de Pythagore : </p>
  <object data="/images/md/2FBC72H23C5AB53BH6D2GHG688BF3H" type="image/svg+xml"></object>
spoil+++

+++question write "En déduire la distance totale parcourue d'une donnée, par satellite, entre Paris et New York"

+++spoil
  <p>Il suffit de multiplier le résultat Précédent par 2 : </p>
  <object data="/images/md/E4B45F9AED89GGGFF5BAF7ADFGAA75" type="image/svg+xml"></object>
spoil+++

---


Les communications effectuées avec les satellites se font par <b class="text-blue">ondes radio</b>. La communication terrestre passe par des <b class="text-green">câbles sous-marin fibrés</b>. La fibre consiste à faire passer des signaux sous forme de faisceaux lumineux. Que cela soit par fibre ou par satellite, la vitesse d'un signal est d'<b>environ 300 000 km par seconde</b>. 

Supposons que le câble sous-marin soit de même longueur que la distance Paris <=> New York à vol d'oiseau.

+++question write "Combien de temps met un signal pour aller de Paris à New York par satellite ? et par câble fibré ?"

+++spoil
  <ul>
    <li>Par satellite : 72236 / 300 000, soit environ 0,24 secondes </li>
    <li>Par câble fibré : 5836 / 300 000, soit environ 0,019 secondes </li>
  </ul>
  <p> Par voix terrestre, un signal voyage plus de 10 fois rapidement que par satellite
spoil+++

--- 

+++bulle r3do
  Continuons la video !
+++

+++video "https://www.youtube.com/watch?v=9UMvyfT4V_Y" "Comment fonctionne Internet ?" 01:16

+++question write "<b>[4:38]</b> Quel est le format d'une adresse IP ?"

+++spoil
  </p><p>Une adresse IP (version 4) est composé de 4 groupes de nombres décimaux séparés par un point : xxx.xxx.xxx.xxx</p>
  <p>chaque groupe de nombre décimal xxx est un nombre compris entre 0 et 255. Voici par exemple l'IP du serveur qui héberge le site wikipédia (fr) : 208.80.154.224</p>
spoil+++

+++bulle r3do
  dans un navigateur Web, si je saisis et valide 208.80.154.224 dans la barre d'url, j'arrive bien sur le site wikipedia
+++

+++bulle r3do droite
  si je saisie l'url "https://fr.wikipedia.org", comment mon navigateur sait que le serveur se trouve à l'adresse 208.80.154.224 ?
+++

---

+++question write "<b>[5:23]</b> Quel service fournit un serveur <b>DNS</b> ? (+++flag-us <b>D</b>omain <b>N</b>ame <b>S</b>ystem |  +++flag-fr Système de noms de domaine)"

+++spoil
  <p>Un serveur <b>DNS</b> agit comme un annuaire. Il permet à un navigateur Web de trouver l'adresse IP d'un ordinateur sur le réseau à partir de son adresse url.</p>
spoil+++

+++question write "<b>[6:44]</b> Dans votre domicile, quel équipement vous relie à Internet ?"

+++spoil
  <p>La plupart des domiciles utilisent une box, c'est un appareil qui est à la fois</p><p>
  </p><ul>
    <li><b>un routeur</b> : elle traduit le signal du domicile à l'extérieur</li>
    <li><b>un commutateur (+++flag-us switch)</b> : elle répartie le réseau entre les différents appareils connectés dans le domicile</li>
  </ul>
spoil+++

### Résumons 

- Internet est un regroupement d'un ensemble de réseaux parsemés dans le monde
- Une machine peut être reliée à Internet via différents réseaux : 
    - réseau filaire : fibre optique, ADSL, câble, ...
    - réseau sans fil : 4G, 5G, wifi, bluetooth, satellite, ...
- Internet est indépendant du réseau physique grâce aux protocoles de communications utilisés qui permettent de passer d'un type de connexion à un autre tout en assurant la continuité des communications
- Internet utilise le protocole TCP pour s'assurer que les données d'un émetteur arrivent bien au destinataire prévu
- Internet utilise le protocole IP qui permet d'identifier physiquement chaque ordinateur (au sens large) appartenant au réseau global
- Un serveur DNS est un ordinateur qui contient un annuaire permettant de faire la correspondance entre une url et une adresse IP

## Le trafic Internet

La vitesse de transmission de données (appelé aussi débit) à travers Internet et plus généralement à travers un réseau correspond au nombre de bits transmis en un laps de temps. 

+++question write "Qu'est-ce qu'un bit ?"

+++spoil
  Un bit est représenté par le chiffre 0 ou le chiffre 1
spoil+++

---

Le débit peut s'écrire : 
- en bit par secondes (bit/s, b/s ou bps)
- en kilo bits par seconde (kb/s)
- en mégabits par seconde (Mb/s)
...

Le débit est parfois exprimé en octets par seconde. **Un octet équivaut à 8 bits**, un débit peut s'exprimer alors en ko/s (kilo octet par seconde), Mo/s (Mega octet par secondes), ...

+++question write "Compléter le tableau de conversion ci-dessous (vous pouvez vous aider d'Internet si vous le souhaitez)"

<table class="table table-sm">
  <thead class="text-center">
    <tr>
      <th colspan="3">Débit</th>
    </tr>
    <tr>
      <th>Unité</th>
      <th>Notation</th>
      <th>Valeur</th>
    </tr>
  </thead>
  <tbody class="text-center">
    <tr>
      <td>bit par seconde</td>
      <td>bit/s ou b/s ou bps</td>
      <td>1 bit/s</td>
    </tr>
    <tr>
      <td>kilo bit par seconde</td>
      <td>kbit/s ou kb/s</td>
      <td>
+++spoil text-center nolabel
        10<sup>3</sup> bit/s
spoil+++
      </td>
    </tr>
    <tr>
      <td>méga bit par seconde</td>
      <td>Mbit/s ou Mb/s</td>
      <td>
+++spoil text-center nolabel
        10<sup>6</sup> bit/s
spoil+++
      </td>
    </tr>
    <tr>
      <td>gigabit bit par seconde</td>
      <td>Gbit/s ou Gb/s</td>
      <td>
+++spoil text-center nolabel
        10<sup>9</sup> bit/s
spoil+++
      </td>
    </tr>
    <tr>
      <td>térabit bit par seconde</td>
      <td>Tbit/s ou Tb/s</td>
      <td>
+++spoil text-center nolabel
        10<sup>12</sup> bit/s
spoil+++
      </td>
    </tr>
  </tbody>
</table>

+++question write "Combien de temps faut il au minimum pour transférer une photo de 5 Mo, à la vitesse de 10 Mbit/s ?"

+++todo fin de séance avec la classe, a déterminer : soit je part du principe que ce cours dure plus d'une séance, soit je le réorganise

+++spoil
  5Mo équivaut à (5 * 8) 40 méga bits. Avec un débit de 10 Mbit/s, il faut alors <b>au minimum</b> 4 secondes pour transférer la photo
spoil+++

+++question write "Combien de temps faut il au minimum pour transférer un film de 3Go, à la vitesse de 200 Mbit/s ?"

+++spoil
  3Go équivaut à (3 * 8) 24 giga bits, soit 24 000 mega bits. Avec un débit de 200 Mbit/s, il faut alors <b>au minimum</b> 120 secondes pour transférer le film
spoil+++

---

De nombreux sites Web proposent de tester le débit de la connexion du réseau sur lequel est connecté notre ordinateur, tablette, smartphone, ... 

+++bulle r3do
  recherchez en un et essayez le ! 
+++

<br>

+++bulle matthieu
  si je fais le test chez moi...voila ce que j'obtiens !
+++

+++image "/images/md/77D699FC35G39BBF2H8DG4FD69G6GB" col-10 mx-auto

<b style="color:#0c66ac">Réception</b> : cela correspond au **débit descendant**, il s'agit de la quantité de données que la machine qu'on a entre les mains peut recevoir grâce au réseau

<table class="text-center">
  <thead>
    <tr>
      <th colspan="6" class="px-2">Type de connexion relié au domicile par débit théorique ( sens : descendant )</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="px-2">12 Mbps</td>
      <td class="px-2">24 Mbps</td>
      <td class="px-2">200 Mbps</td>
      <td class="px-2">500 Mbps</td>
    </tr>
    <tr>
      <td class="px-2">ADSL</td>
      <td class="px-2">ADSL2+</td>
      <td class="px-2">VDSL2</td>
      <td class="px-2">Fibre optique</td>
    </tr>
  </tbody>
</table>
  

<b style="color:#94c11c">Envoi</b> : cela correspond au **débit montant**, il s'agit de la quantité de données que la machine qu'on a entre les mains envoyer à travers le réseau


<table class="text-center">
  <thead>
    <tr>
      <th colspan="6" class="px-2">Type de connexion relié au domicile par débit théorique ( sens : montant )</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="px-2">1,8 Mbps</td>
      <td class="px-2">3,3 Mbps</td>
      <td class="px-2">100 Mbps</td>
      <td class="px-2">200 Mbps</td>
    </tr>
    <tr>
      <td class="px-2">ADSL</td>
      <td class="px-2">ADSL2+</td>
      <td class="px-2">VDSL2</td>
      <td class="px-2">Fibre optique</td>
    </tr>
  </tbody>
</table>

+++question write "D'après les informations ci-dessus, quel est le type de connexion utilisée chez Matthieu dans le sens descendant ?"

+++spoil
  Le type de connexion est 'ADSL2+' car Matthieu a une vitesse de téléchargement juste en dessous des 24 Mbps théoriques pour une connexion de type ADSL2+. 
spoil+++

+++question write "D'après les informations ci-dessus, quel est le type de connexion utilisée chez Matthieu dans le sens montant ?"

+++spoil
  Le test indique une connexion montante avec une vitesse proche du niveau théorique ADSL
spoil+++

---

Le type de réseau utilisé en montant ou en descendant semblent différents, pourtant le test a été effectué sur le même réseau

+++question write " Comment expliquer cette différence ?"

+++spoil
  Plusieurs facteurs peuvent expliquer cela :
  <ul>
    <li>La qualité générale de la ligne</li>
    <li>L'utilisation du réseau par les habitants du même quartier</li>
  </ul>
spoil+++

+++question write " Qu'est-ce que la latence ?"

+++spoil
  La latence est le délai qui s'écoule entre le départ d'une information de l'ordinateur et l'arrivée sur un serveur
spoil+++

--- 

Pour les personnes qui ont terminé ou avec l'ensemble de la classe 

+++video "https://www.youtube.com/watch?v=-aYat9357mE" "la taille d'Internet"

+++pagebreak

### Résumons

- Les données sont découpées en paquets de bits avant d'être envoyée sur le réseau
- Le débit est une vitesse qui mesure le nombre de bits (ou d'octets) transmis en un laps de temps
- Un bit correspond au chiffre 0 ou au chiffre 1
- Un octet correspond à 8 bits, voici quelques exemples d'octets :
    - 10010011
    - 00101101
    - 10101110
    - ...
- le débit montant correspond à la vitesse d'envoi de données vers le réseau
- le débit descendant correspond à la vitesse d'envoi de données reçues par le réseau
