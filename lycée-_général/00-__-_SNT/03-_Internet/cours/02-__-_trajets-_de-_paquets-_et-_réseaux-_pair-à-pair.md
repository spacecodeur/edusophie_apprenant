# Trajets de paquets et réseaux pair-à-pair

+++programme
  Sur des exemples réels, retrouver une adresse IP à partir d’une adresse symbolique et inversement
  Décrire l’intérêt des réseaux pair-à-pair ainsi que les usages illicites qu’on peut en faire
  
  Trouver l'adresse IP correspondant à une URL
  Suivre le trajet d'un paquet TCP à travers Internet
  
+++

+++sommaire

## Résumé des séances précédentes

+++bulle julie
  Je suis chez moi et je souhaite faire une recherche sur le Web
+++

+++bulle r3do droite
  que se passe-t-il lorsque Julie ouvre son navigateur Web et accède à la page Web du moteur de recherche duckduckgo ?
+++

Les étapes ci-dessous, mélangées, décrivent la communication qui se passe entre l'ordinateur de Julie et le serveur qui héberge le site Web du moteur de recherche.

<table class="table table-sm">
  <tbody>
    <tr>
      <td class="text-center align-middle"><b style="font-size:2rem">0</b></td>
      <td>Julie ouvre son navigateur Web, saisit et valide l'url '<span class="text-blue">https://duckduckgo.com</span>'. Le navigateur de Julie crée une requête HTTPS, puis découpe la requête en plusieurs paquets TCP</td>
    </tr>
    <tr>
      <td class="text-center align-middle">
+++spoil text-center nolabel
        <b style="font-size:2rem">6</b>
spoil+++
      </td>
      <td>Les paquets TCP voyagent de routeurs en routeurs à travers le monde en direction de la maison de Julie</td>
    </tr>
    <tr>
      <td class="text-center align-middle">
+++spoil text-center nolabel
        <b style="font-size:2rem">5</b>
spoil+++
      </td>
      <td>En réponse à la requête de Julie, le serveur génère un document HTML ayant comme contenu la page '<span class="text-blue">https://duckduckgo.com</span>'. Le document est ensuite découpé et réparti dans des paquets TCP. Les paquets TCP sont ensuite envoyés sur le réseau Internet et ont comme destinataire la maison de Julie</td>
    </tr>
    <tr>
      <td class="text-center align-middle">
+++spoil text-center nolabel
        <b style="font-size:2rem">7</b>
spoil+++
      </td>
      <td>Les paquets TCP arrivent au niveau de la box de Julie. À chaque paquet reçu, la box envoie un accusé de réception au serveur hébergeant la page Web '<span class="text-blue">https://duckduckgo.com</span>'. Une fois tous les paquets TCP reçu, la box les recomposent pour obtenir la page HTML générée par le serveur duckduckgo. Pour terminer, la box envoie la page HTML à l'ordinateur de Julie</td>
    </tr>
    <tr>
      <td class="text-center align-middle">
+++spoil text-center nolabel
        <b style="font-size:2rem">3</b>
spoil+++
      </td>
      <td>Les paquets TCP voyagent de routeur en routeur à travers le monde en direction du serveur duckduckgo</td>
    </tr>
    <tr>
      <td class="text-center align-middle">
+++spoil text-center nolabel
        <b style="font-size:2rem">1</b>
spoil+++
      </td>
      <td>L'ordinateur de Julie communique avec un serveur DNS afin de connaître l'adresse IP du serveur qui héberge la page Web '<span class="text-blue">https://duckduckgo.com</span>'. Le serveur DNS indique que l'url '<span class="text-blue">https://duckduckgo.com</span>' correspond à l'adresse IP "40.114.177.156". L'adresse IP est marquée sur chacun des paquets TCP</td>
    </tr>
    <tr>
      <td class="text-center align-middle">
+++spoil text-center nolabel
        <b style="font-size:2rem">8</b>
spoil+++
      </td>
      <td>L'ordinateur de Julie reçoit la page HTML correspondant à l'url '<span class="text-blue">https://duckduckgo.com</span>'. Le navigateur web interprète le code contenu dans la page HTML et affiche sur l'écran de Julie le page '<span class="text-blue">https://duckduckgo.com</span>'</td>
    </tr>
    <tr>
      <td class="text-center align-middle">
+++spoil text-center nolabel
        <b style="font-size:2rem">4</b>
spoil+++
      </td>
      <td>Le serveur hébergeant la page '<span class="text-blue">https://duckduckgo.com</span>' reçoit les paquets TCP. À chaque paquet reçu, il envoie un accusé de réception au navigateur de Julie. Une fois tous les paquets TCP reçu, le serveur les recomposent pour obtenir la requête HTTPS de Julie.</td>
    </tr>
    <tr>
      <td class="text-center align-middle">
+++spoil text-center nolabel
        <b style="font-size:2rem">2</b>
spoil+++
      </td>
      <td>Les paquets TCP sont envoyés sur le réseau mondial Internet</td>
    </tr>
  </tbody>
</table>  

+++question write "Dans quel ordre doivent se dérouler les étapes ci-dessus ? Complétez le tableau en ajoutant des numéros dans la colonne de gauche"

+++pagebreak

+++question write "Complétez le schéma ci-dessous en y ajoutant les numéros des étapes notées dans le tableau"
  
+++image "/images/md/CC7D7B44H969CB9EHGB7373938B7DF" col-11 mx-auto
  
+++spoil nolabel d-print-none-2

+++image "/images/md/F87HBA8GGEFE7B9FF43FD355H87GF8" col-11 mx-auto

<hr>

+++image "/images/md/97E3866B675H2D879A76G98DD77E2B" col-11 mx-auto

spoil+++

## L'invite de commande windows 

L'invite de commande windows est un terminal qui permet d'exécuter des commandes. Dans le cadre du cours sur Internet, deux commandes sont intéressantes, c'est une bonne occasion donc pour découvrir l'invite de commande.

Pour ouvrir l'invite de commande, il faut appuyer sur les touches 'windows + r'. Une fenêtre apparaît, il faut saisir dedans cmd et valider : 

+++image "/images/md/E59CB5573H62C9HCC59F36BGBH9CBF" col-6 mx-auto

L'invite de commande s'ouvre, il s'agit d'un terminal :

+++image "/images/md/F398AEC6AC4CCGE22E3623F6HE27AD" col-6 mx-auto

### Trouver l'adresse IP d'un serveur hébergeant un site Internet

Pour trouver l'adresse IP du serveur qui héberge un site que vous connaissez, vous pouvez utiliser la commande ping dans l'invite de commande windows.

Par exemple, pour connaître l'IP de la page Web du moteur de recherche google (adresse : www.google.com), il suffit de taper dans l'invite de commande 'ping www.google.com -4' (sans les apostrophes) puis de valider en appuyant sur la touche entrée.

+++question write "En analysant ce qui s'affiche sur le terminal après avoir validé la commande ping, quelle est l'adresse IP du serveur qui héberge le site google ?"

+++spoil
  L'adresse IP du serveur google qui héberge la page Web du moteur de recherche est <input class="" type="text" value="">
spoil+++

+++question write "Toujours en analysant ce qui s'est affiché dans le terminal, que fait exactement la commande ping ?"

+++spoil
  La commande ping envoie 4 paquets sur le réseau à destination de l'url saisie avec la commande ping. Ensuite, un rapport est fait et nous indique si les 4 paquets envoyés sur le réseau ont bien été reçus.
spoil+++

+++question write "En utilisant la commande ping, remplissez le tableau ci-dessous"

<table class="table table-sm text-center">
  <thead>
    <tr>
      <th>domaine</th>
      <th>adresse IP du serveur</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>www.youtube.com</td>
      <td><input class="w-100 text-center" type="text" value=""></td>
    </tr>
    <tr>
      <td>www.ecoledirecte.com</td>
      <td><input class="w-100 text-center" type="text" value=""></td>
    </tr>
    <tr>
      <td>9gag.com</td>
      <td><input class="w-100 text-center" type="text" value=""></td>
    </tr>
    <tr>
      <td>fr.wikipedia.org</td>
      <td><input class="w-100 text-center" type="text" value=""></td>
    </tr>
  </tbody>
</table>
  
### Suivre le trajet d'un paquet à travers le réseau

Toujours dans l'invite de commande, il est possible de découvrir par quels routeurs passe un paquet envoyé sur le réseau Internet. Pour cela, il faut utiliser la commande tracert. 

Par exemple, nous pouvons observer le trajet emprunté dans le réseau qui va du lycée au serveur qui héberge le site Web wikipédia français. Il suffit de taper dans le terminal 'tracert -4 fr.wikipedia.org' (sans les apostrophes) puis de valider en appuyant sur la touche entrée.

la commande tracert affiche une liste de lignes qui représentent les ordinateurs par lesquels passe la paquet envoyé par la commande. La première ligne correspond à l'ordinateur de départ et la dernière ligne au serveur qui héberge le site wikipédia

+++question write "D'après le résultat affiché par la commande tracert, combien de routeurs se trouve entre le lycée et le serveur fr.wikipedia.org d'après la commande tracert ?"

+++spoil
  Il suffit de compter les lignes entre la première et la dernière pour savoir combien de routeur sont traversés entre le lycée et le serveur wikipédia. Il y en a au total <input class="" type="text" value="">
spoil+++

<hr>

La commande tracert bien que pratique génère un résultat difficilement lisible. Il existe des outils sur Internet qui permettent d'exécuter la commande tracert et d'afficher visuellement par où passe le paquet envoyé par la commande. Le site Web +++lien "https://geotraceroute.com" "geotraceroute". 

+++question "Ouvrez un navigateur et accédez au site Web 'https://geotraceroute.com'"

+++question "Effectuez un tracert en partant de l'ordinateur FR-Toulouse avec comme destination le serveur hébergeant le site 'www.kyoto-sjn.jp'"

+++question write "Par quels pays passe un paquet partant de Toulouse à destination du serveur 'www.kyoto-sjn.jp' ?"

+++spoil
  Le paquet passe par les pays : <input class="w-100" type="text" value="">
spoil+++

### Résumons

- L'invite de commande sur windows est un terminal qui permet d'exécuter des commandes. Pour l'ouvrir, il suffit d'utiliser le raccourcis clavier 'touche windows + x' et de cliquer sur "Invite de commandes"
- Pour connaître l'IP (version 4) rattachée à une url, il suffit d'utiliser la commande 'ping' dans l'invite de commande windows.
- La commande tracert sur Windows permet de suivre le chemin que prend un paquet envoyé dans un réseau depuis un expéditeur vers un destinataire
- chaque lignes de la commande tracert représente l'adresse de ordinateur par lequel passe le paquet généré par la commande : la première ligne correspond à l'adresse IP de l'ordinateur depuis lequel la commande est exécutée et la dernière ligne correspond à l'adresse IP du destinataire

## Les réseaux peer-to-peer

Lorsque nous naviguons sur le Web avec un navigateur Web, nous utilisons un réseau dont l'architecture se compose d'ordinateurs clients (les internautes) et d'ordinateurs serveurs (les serveurs qui hébergent les sites Web)

+++image "/images/md/9B8D8B4C9E3B52HHHF33AG7DCEED2A" col-4 mx-auto

Il existe un autre type d'architecture réseau nomme réseau pair-à-pair ( +++flag-us peer-to-peer). Le principe ? Tous les ordinateurs dans le réseau sont à la fois client et serveur

+++image "/images/md/7F9D2942DFC726BE2AB2AB8D3CAF3F" col-4 mx-auto

+++video "https://www.youtube.com/watch?v=cSH8p54w6y0" "Qu'est-ce que le Peer to Peer (pair à pair) ?"

## Résumons

- Dans un réseau pair-à-pair ( +++flag-us peer-to-peer ), les ordinateurs sont interconnectés entre eux. Un ordinateur dans un tel réseau est à la fois client et serveur car il reçoit mais envoie aussi des données dans le réseau
- Un réseau pair-à-pair n'est en soit pas illégal, c'est l'utilisation qui en est fait avec qui peut devenir illégale
- Tant que les fichiers qui sont échangés nous appartiennent ou sont libres de droits, l'utilisation d'un réseau peer-to-peer n'est pas illégale
