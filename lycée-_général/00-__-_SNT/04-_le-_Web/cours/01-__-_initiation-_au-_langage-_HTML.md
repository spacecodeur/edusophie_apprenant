# Initiation au langage HTML

+++programme
  Distinguer ce qui relève du contenu d’une page et de son style de présentation
  Étudier et modifier une page HTML simple
  Maîtriser les renvois d’un texte à différents contenus
  Inspecter le code d’une page hébergée par un serveur et distinguer ce qui est exécuté par le client et par le serveur
  
  Créer une page Web simple avec du code HTML
+++

+++sommaire

+++pagebreak

Lors de cette séance, et durant la suivante, vous allez apprendre à créer une page Web. Cette page Web devra présenter un sujet de votre choix. La page devra également intégrer une vidéo de moins de 4 minutes consacrée au sujet que vous aurez choisis. 

+++bulle r3do
  voici +++lien "/d/snt/lindyhop-version2.html" "un exemple de résultat final !"
+++

Des personnes pourront choisir de présenter après les vacances leur page Web ainsi que la vidéo qu'elles ont choisie.

## Quelques rappels

+++bulle r3do
  que se passe-t-il lorsque, depuis mon navigateur Web, je saisis une URL ou lorsque je clique sur un lien depuis un site Web ?
+++

+++bulle r3do droite
  qu'elles principales informations contient la requête qu'envoie le navigateur Web (client) vers un serveur hébergeant un site Web ?
+++

## Le fonctionnement d'un navigateur Web

Nous l'avons vu la précédente séance, <b class="text-red">à travers le réseau Internet</b>, le navigateur Web est capable d'<b class="text-green">envoyer des requêtes à des serveurs</b>. En réponse, les serveurs <b class="text-blue">envoient des documents de divers types</b>.

+++image "/images/md/HG8HE5HCAFFDF24A864B42G6C54G9G" col-8 mx-auto

Un navigateur est capable de lire les documents envoyés par les serveurs. Dans ces documents se trouvent les documents **.html** qui contiennent généralement du code **HTML**, **CSS** et **Javascript**. Les navigateurs Web savent interpréter ces 3 langages. Ils peuvent donc à partir du document **.html** envoyé par un serveur, afficher sur votre écran le contenu d'une page Web.

+++bulle r3do
  en seconde, nous nous focalisons sur les langages <b>HTML</b> et <b>CSS</b>
+++

- Grâce au langage **HTML**, nous pouvons <b>remplir et structurer le contenu</b> d'une page Web
- Grâce au langage **CSS**, nous pouvons <b>décorer d'une page Web</b>

+++bulle r3do
  autrement dit, le langage <b>HTML</b> s'occupe du fond et le langage <b>CSS</b> s'occupe de la forme d'une page Web
+++

## Créer sa toute première page Web !

Dans cette séance, nous ne nous concentrons que sur le code **HTML** (le langage **CSS** sera pour la séance suivante)

Les navigateurs Web savent interpréter les langages **HTML** (et **CSS**). Vous allez donc à travers plusieurs étapes télécharger un document **.html**, le modifier et visualiser vos modifications sur le navigateur Web de votre choix.

+++consignes
<ul>
  <li>Tous ensemble et à travers plusieurs étapes, nous allons créer une page Web</li>
  <li>À la fin de ces étapes, vous aurez le reste de l'heure pour améliorer le contenu de votre page Web <b class="text-blue">en approfondissant</b> les étapes qui vous intéressent</li>
  <li>Avant la fin de l'heure, n'oubliez pas de copier votre travail sur votre clé usb (ou envoyez le vous par mail), pour être sur de ne pas le perdre</li>
</ul>
+++

### Téléchargement et ouverture d'un squelette de page Web

- **Télécharger** +++fichier "/d/snt/mapageweb.html" "le fichier mapageweb.html" sur votre ordinateur
- **Ouvrez le fichier** (clic droit puis "ouvrir avec") avec l'éditeur de texte Windows "**Bloc-notes**", gardez la fenêtre de l'éditeur de texte ouverte
- **Ouvrez le fichier** (clic droit puis "ouvrir avec") avec le navigateur Web de votre choix, privilégiez Firefox ou Chrome

+++bulle r3do
  le navigateur affiche une page entièrement blanche, c'est tout à fait normal !
+++

#### <b class="text-blue">Approfondissement</b>

Le **Bloc-notes** fourni par défaut par Windows est très rudimentaire. Vous pouvez télécharger et utiliser des éditeurs de textes plus pratiques pour coder en HTML comme par exemple **notepad++** qui colorise le code et gère correctement les lettres accentuées (contrairement au **Bloc-notes**) :

+++image "/images/md/2385295GGAD9578G3C28EE4EFA6E2F" col-6 mx-auto

**notepad++** est un éditeur de texte gratuit, open source : 

- +++lien "https://notepad-plus-plus.org/downloads" "téléchargez la dernière version" de **notepad++** et installez la
- **Ouvrez votre fichier** "mapageweb.html" avec **notepad++** (clic droit puis "ouvrir avec")

### Ajouter un titre dans la page Web

Le langage **HTML** est ce qu'on appelle un langage de balisage. Nous ajoutons du contenu dans notre page Web sous la forme de balises **HTML**.

La balise <code>body</code> qui commence à la **ligne 7** (balise **ouvrante**) et qui se termine à la **ligne 9** (balise **fermante**) doit contenir le contenu principal de la page Web. **Jusqu'à la fin de cette séance, vous travaillerez toujours dans la balise <code>body</code>, le reste du code n'est pas à modifier.**

Pour ajouter un titre principal en **HTML**, nous utilisons la balise <code>h1</code> : 

- Depuis l'éditeur de texte, **ajoutez dans la balise <code>body</code> une balise <code>h1</code>** :

+++image "/images/md/C7BCFH4E8AB5DA7B5D3247CH88CCHA" col-6 mx-auto

- **Sauvegardez votre fichier** et **rechargez votre page** depuis le navigateur Web, le contenu de votre paragraphe devrait s'afficher !

#### <b class="text-blue">Approfondissement</b>

La balise <code>h1</code> permet d'ajouter le titre principal de la page (c'est à dire, le titre le plus important). Il est possible d'ajouter des titres moins importants en utilisant d'autres balises : 

<table class="text-center">
  <thead>
    <tr>
      <th class="px-1">balise</th>
      <th class="px-1">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="px-1"><code>h2</code></td>
      <td class="px-1">affiche un titre moins important que la balise <code>h1</code></td>
    </tr>
    <tr>
      <td class="px-1"><code>h3</code></td>
      <td class="px-1">affiche un titre moins important que la balise <code>h2</code></td>
    </tr>
    <tr>
      <td class="px-1"><code>h4</code></td>
      <td class="px-1">affiche un titre moins important que la balise <code>h3</code></td>
    </tr>
    <tr>
      <td class="px-1"><code>h5</code></td>
      <td class="px-1">affiche un titre moins important que la balise <code>h4</code></td>
    </tr>
    <tr>
      <td class="px-1"><code>h6</code></td>
      <td class="px-1">affiche un titre moins important que la balise <code>h5</code></td>
    </tr>
  </tbody>
</table>

- Ajoutez des titres de différentes tailles dans votre page Web. Pour rappel, il faut ajouter les balises dans la balise body

+++bulle r3do
  si vous rencontrez des difficultés pour utiliser ces balises, cherchez des exemples d'utilisation de ces balises sur le Web
+++

### Ajouter un paragraphe dans la page Web

Pour ajouter un paragraphe, il faut utiliser la balise <code>p</code>.

- Depuis l'éditeur de texte, **ajoutez dans la balise <code>body</code> une balise <code>p</code>** :

+++image "/images/md/F95HHD6EA745ACC2GF2CEFDA994GG8" col-6 mx-auto

- **Sauvegardez votre fichier** et **rechargez votre page** depuis le navigateur Web, le titre Hello devrait s'afficher !

#### <b class="text-blue">Approfondissement</b>

Dans un paragraphe nous souhaitons parfois mettre en avant des mots par rapport à d'autres, nous pouvons par exemple : 
- mettre un à plusieurs mots en **gras**
- <u>surligner</u> un à plusieurs mots
- mettre un à plusieurs mots en <i>italique</i>

Il faut alors utiliser les balises suivantes : 

<table class="text-center">
  <thead>
    <tr>
      <th class="px-1">balise</th>
      <th class="px-1">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="px-1"><code>b</code></td>
      <td class="px-1">affiche un ensemble de mots en <b>gras</b> (<code>b</code> comme <b>b</b>old +++flag-us )</td>
    </tr>
    <tr>
      <td class="px-1"><code>u</code></td>
      <td class="px-1"><u>surligne</u> un ensemble de mots (<code>u</code> comme <b>u</b>nderline +++flag-us )</td>
    </tr>
    <tr>
      <td class="px-1"><code>i</code></td>
      <td class="px-1">affiche un ensemble de mots en <i>italique</i> (<code>i</code> comme <b>i</b>talic +++flag-us )</td>
    </tr>
  </tbody>
</table>

- Mettez des mots en valeurs dans votre page en utilisant au choix les balises <code>b</code>, <code>u</code> ou encore <code>i</code>

+++bulle r3do
  si vous rencontrez des difficultés pour utiliser ces balises, cherchez des exemples d'utilisation de ces balises sur le Web
+++

### Ajouter un lien vers un autre site Web

Certaines balises doivent être configurées pour pouvoir fonctionner correctement. C'est le cas de la balise <code>a</code> qui permet d'ajouter un lien dans sa page.

- commencer par ajouter la balise <code>a</code> dans votre fichier mapageweb.html

+++image "/images/md/6A49F9HCGFGB76D3948A37G6898HBA" col-6 mx-auto

+++bulle r3do
  si vous rechargez votre page Web, vous verrez que le texte dans la balise <code>a</code> ne réagit pas lorsqu'on clique dessus
+++

+++bulle r3do droite
  votre navigateur ne connaît pas l'url du site à utiliser. Donc ce dernier affiche un simple texte à la place du lien. 
+++

Vous devez préciser l'url vers laquelle votre navigateur doit vous rediriger si vous cliquez sur le lien. Il faut alors ajouter dans la balise ouvrante <code>a</code> l'attribut <code>href</code> avec comme valeur l'url du site cible.

- modifiez votre code comme ceci : 

+++image "/images/md/3839EG4ACD722G63H82EEAB434EHB9" col-10 mx-auto

- **Sauvegardez votre fichier** et **rechargez votre page** depuis le navigateur Web, lien hypertexte devrait maintenant fonctionner !

#### <b class="text-blue">Approfondissement</b>

Lorsque vous cliquez sur votre lien, il est possible de charger la nouvelle page **dans un nouvel onglet** de votre navigateur. Il faut pour cela ajouter l'attribut <code>target</code> dans la balise <code>a</code>.

- Recherchez sur Internet comment utiliser l'attribut <code>target</code> dans une balise <code>a</code> afin de charger la page du lien dans un nouvel onglet
- Modifiez le code **HTML** de votre lien afin qu'un nouvel onglet de votre navigateur s'ouvre lorsqu'on clique sur votre lien

### Ajouter une image dans la page Web

La balise **HTML** <code>img</code> permet d'afficher une image sur la page Web. Il est possible d'utiliser des images de différents types : png, jpeg, gif, ...

Pour être utilisée, la balise <code>img</code> n'a pas besoin de balise fermante contrairement aux autres balises que nous avons vu jusqu'à maintenant. On dit que la balise <code>img</code> est une **balise orpheline**.

- Recherchez l'url d'une image sur le Web
- Modifiez votre code comme ceci : 

+++image "/images/md/H927A6BAGBC4EACC6B697E8E8EAEFA"

- **Sauvegardez votre fichier** et **rechargez votre page** depuis le navigateur Web, votre image devrait s'afficher !

#### <b class="text-blue">Approfondissement</b>

Il est possible de modifier les dimensions de l'image qu'on ajoute dans notre page Web. Pour cela, il vous faut utiliser dans la balise <code>img</code> les attributs <code>width</code> (largeur) et <code>height</code> (longueur)

- Recherchez sur Internet comment utiliser les attributs <code>width</code> et <code>height</code> dans une balise <code>img</code>
- Modifiez le code **HTML** de votre image afin de modifier ses dimensions

### Intégrer une vidéo dans sa page Web

Pour cette partie, nous utiliserons en exemple une vidéo stockée dans un des serveurs de youtube. Mais cela peut très bien être adapté pour d'autres plateformes : daylimotion, tiktok, twitch, ...

La plupart des sites d'hébergement de vidéos permettent d'intégrer une vidéo directement sur sa page Web. Sur youtube, lorsque vous vous trouvez sur la page d'une vidéo, il suffit de cliquer sur "partager" puis sur "Intégrer" (voir image ci-dessous)

+++image "/images/md/33E5FABD27G6H638D98E926H2H3HB5" col-8 mx-auto

- Après avoir cliqué sur "Intégrer", copiez le code qui s'affiche à l'écran dans votre page Web

+++image "/images/md/2DF89227F4BB7C484EFB268E5BG7F6"

- **Sauvegardez votre fichier** et **rechargez votre page** depuis le navigateur Web, la vidéo est directement intégrée tout en bas de votre page Web !

## Ensuite ?

Le reste du cours vous appartient ! 
- Continuez à remplir votre page Web, sur un sujet de votre choix, en utilisant les balises et attributs que nous avons vu ensemble où qui se trouvent dans les parties <b class="text-blue">Approfondissement</b>. **Votre page Web doit au moins contenir** : 
    - un titre
    - un texte explicatif (contenu dans un paragraphe)
    - une image
    - un lien 
    - une vidéo de moins de 4 minutes
- La fenêtre "<b>Outils de développement</b>" (disponible en appuyant sur F12 depuis votre navigateur) permet d'inspecter le code d'une page Web. N'hésitez donc pas à vous inspirer du code de la page Web de présentation +++lien "/d/snt/lindyhop-version1.html" "'Lindy Hop'" pour construire votre propre page.
- N'oubliez pas d'enregistrer votre fichier 'mapageweb.html' dans votre clé usb avant la fin de la séance. Vous continuerez à travailler dessus la séance suivante.

## Résumons

- Depuis un navigateur Web, lorsqu'on saisit l'url d'un site ou encore lorsqu'on clique sur un lien, une requête est construite par notre navigateur Web et est envoyée au serveur qui héberge le site Web
- En réponse, le serveur peut transmettre des documents au navigateur Web : un document html, une musique, une vidéo, etc..
- Les pages Web que nous consultons sont écrites avec du code **HTML** et **CSS**.
    - le langage **HTML** permet d'ajouter du contenu dans la page Web et de le structurer 
    - le langage **CSS** permet de décorer la page Web
- Les pages Web, d'un même site Web ou de différents sites Web, sont liées entre elles grâce aux liens hypertexte. Pour créer un lien en langage **HTML**, il faut utiliser la balise <code>a</code>