# Initiation au langage CSS

+++programme
 Distinguer ce qui relève du contenu d’une page et de son style de présentation
Étudier et modifier une page HTML simple
Maîtriser les renvois d’un texte à différents contenus
Inspecter le code d’une page hébergée par un serveur et distinguer ce qui est exécuté par le client et par le serveur

Décorer une page Web
+++

+++sommaire

+++pagebreak

Lors du cours précédent, nous avons créé une page Web simple ne contenant que du code HTML. Nous utilisons le Web tel qu'il était avant 1996 ! 

- Voici un exemple de page Web n'utilisant que du code **HTML** : (le Web avant 1996)
    - +++lien "/d/snt/lindyhop-version1.html"
- Voici un exemple de page Web utilisant du code **HTML** et <b class="text-red">C</b><b class="text-green">S</b><b class="text-blue">S</b> : (le Web après 1996)
    - +++lien "/d/snt/lindyhop-version2.html"

Le but de ce cours est de découvrir le langage **CSS** en décorant votre page Web. 

## Décorer sa page web

Tout comme pour le langage **HTML**, les navigateurs Web savent interpréter le langage **CSS**. À travers plusieurs étapes, vous allez modifier le code de votre page Web et visualiser les changements depuis le navigateur Web de votre choix.  

+++consignes
<ul>
  <li>Tous ensemble et à travers plusieurs étapes, nous allons modifier votre page Web</li>
  <li>À la fin de ces étapes, vous aurez le reste de l'heure pour améliorer le contenu de votre page Web <b class="text-blue">en approfondissant</b> les étapes qui vous intéressent</li>
  <li>Avant la fin de l'heure, n'oubliez pas de copier votre travail sur votre clé usb (ou envoyez le vous par mail), pour être sur de ne pas le perdre</li>
</ul>
+++

### Ouvrir son fichier 

- Si vous n´avez pas votre fichier construit le cours précédent, **Télécharger** +++fichier "/d/snt/lindyhop-version1.html" "ce fichier html servant de base" sur votre ordinateur
- **Ouvrez votre fichier** (clic droit puis "ouvrir avec") avec l'éditeur de texte Windows "**Bloc-notes**", gardez la fenêtre de l'éditeur de texte ouverte
- **Ouvrez le fichier** (clic droit puis "ouvrir avec") avec le navigateur Web de votre choix, privilégiez Firefox ou Chrome

+++pagebreak

#### <b class="text-blue">Approfondissement</b>

Le **Bloc-notes** fourni par défaut par Windows est très rudimentaire. Vous pouvez télécharger et utiliser des éditeurs de textes plus pratiques pour coder en HTML comme par exemple **notepad++** qui colorise le code et gère correctement les lettres accentuées (contrairement au **Bloc-notes**) :

+++image "/images/md/3H6ED8HEBD53D29G6266HDEAEFGADF" col-6 mx-auto

**notepad++** est un éditeur de texte gratuit, open source : 

- +++lien "https://notepad-plus-plus.org/downloads" "téléchargez la dernière version" de **notepad++** et installez la
- **Ouvrez votre fichier** "mapageweb.html" avec **notepad++** (clic droit puis "ouvrir avec")

### Ajouter une couleur de fond

Il existe plusieurs manières d'ajouter du code **CSS** dans une page Web. Dans ce cours d'introduction au langage **CSS**, nous utiliserons l'approche la plus simple : écrire le code **CSS** directement dans la balise **HTML** que nous souhaitons "décorer".

La balise **HTML** <code>body</code> est la balise qui contient tout le contenu visible d'une page Web. Pour ajouter une couleur de fond au contenu de cette balise, nous utilisons la règle css  <code>background-color</code> : 

- Dans la balise ouvrante <code>body</code>, ajoutez l'attribut **HTML** <code>style</code>
- Dans l'attribut <code>style</code>, ajoutez la règle **CSS** <code>background-color</code> avec comme valeur <code>rgb(201, 175, 149);</code>

+++image "/images/md/728CB34F56E8B3FA38CD29782A27G2" col-7 mx-auto

- **Sauvegardez votre fichier** et **rechargez votre page** depuis le navigateur Web pour visualiser le résultat de votre modification

#### <b class="text-blue">Approfondissement</b>

Modifier les valeurs contenues dans <code>rgb(201, 175, 149);</code> change la couleur affichée sur votre page Web. Vous pouvez vous aider d'une pipette à couleurs ( +++flag-us 'color picker' ) trouvable sur le Web pour obtenir les valeurs correspondantes à la couleur que vous souhaitez utiliser.

Voici +++lien "https://www.w3schools.com/colors/colors_picker.asp" "un exemple de color picker" interactif. 

+++question write "À quoi correspond les trois valeurs utilisées dans <code>rgb(201, 175, 149);</code> ?"

+++spoil
  <ul>
    <li>La première valeur (ici <code>201</code>), correspond à l'intensité de la couleur <b class="text-red">rouge</b></li>
    <li>La deuxième valeur (ici <code>175</code>), correspond à l'intensité de la couleur <b class="text-greend">verte</b></li>
    <li>La première valeur (ici <code>149</code>), correspond à l'intensité de la couleur <b class="text-blue">bleu</b></li>
  </ul>
spoil+++

### Colorer du texte

Pour colorer du texte, nous utilisons la règle **CSS** <code>color</code>. Colorons le texte du titre principal de la page Web : 

- Dans la balise ouvrante <code>h1</code>, ajouter l'attribut **HTML** <code>style</code>
- Dans l'attribut <code>style</code>, ajouter la règle **CSS** <code>color</code> avec comme valeur <code>rgb(210, 89, 0);</code>

+++image "/images/md/5EDHF5EGC38E3EA9F4DH2AA42H977A" col-7 mx-auto

- **Sauvegardez votre fichier** et **rechargez votre page** depuis le navigateur Web pour visualiser le résultat de votre modification

#### <b class="text-blue">Approfondissement</b>

Colorez le texte (contenu dans les balises **HTML** <code>p</code>) qui se trouve dans votre page Web en utilisant la règle **CSS** <code>color</code> 

### Modifier la taille du titre

Pour modifier la taille du texte contenu dans une balise (par exemples, les balises : <code>h1</code>, <code>p</code>, ...), nous utilisons la règle **CSS** <code>font-size</code>. 

Modifions la taille du titre principal de la page : 

- Dans l'attribut **HTML** <code>style</code> de la balise ouvrante <code>h1</code>, ajoutez la règle **CSS** <code>font-sie</code> avec comme valeur <code>3rem;</code>

+++bulle r3do
  <code>3rem</code> signifie "<code>3</code> fois plus grand que la taille par défaut"
+++

+++image "/images/md/59D6DFHBDB56BEE793B497A3F52689" col-7 mx-auto

- **Sauvegardez votre fichier** et **rechargez votre page** depuis le navigateur Web pour visualiser le résultat de votre modification

#### <b class="text-blue">Approfondissement</b>

Modifiez la taille du texte (contenu dans les balises **HTML** <code>p</code>) qui se trouve dans votre page Web en utilisant la règle **CSS** <code>font-size</code> 

## Ensuite ?

Le reste du cours vous appartient ! vous pouvez continuer à ajouter du contenu dans votre page Web (illustrant le sujet de votre choix) ou continuer à la décorer. N'hésitez pas à inspecter (raccourcis touche F12) le code final de +++lien "/d/snt/lindyhop-version2.html" "la page de présentation du lindy hop" pour vous en inspirer pour votre page. 

Pour rappel, **votre page Web doit au moins contenir** : 
- un titre
- un texte explicatif (contenu dans un paragraphe)
- une image
- un lien 
- une vidéo de moins de 4 minutes

Ci-dessous se trouvent quelques règles **CSS** supplémentaires si vous souhaitez allez plus loin dans la décoration de votre page Web : 

- +++lien "https://developer.mozilla.org/fr/docs/Web/CSS/box-shadow" "Ajouter une ombre en dessous d'un texte ou d'une image"
- +++lien "https://developer.mozilla.org/fr/docs/Web/CSS/border" "Ajouter une bordure autour d'un texte ou d'une image"