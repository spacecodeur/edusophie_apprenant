# Les données structurées

+++programme
Identifier les différents descripteurs d’un objet
Retrouver les méta-données d’un fichier personnel
Distinguer la valeur d’une donnée de son descripteur

Définir ce qu'est un format de fichier et une donnée
Trouver comment accéder aux données descriptives (méta-données) d'un fichier
Analyser et structurer des données
+++

+++sommaire 1

## Introduction

Les ordinateurs, tablettes et même les smartphones utilisent des fichiers qui contiennent des données. 

+++question write "Il existe des fichiers de différents formats, savez vous à quels types de fichiers appartiennent les formats suivants ? remplissez le tableau avec les exemples de format listés ci-dessous :"

Liste de formats connus :
<div class="d-flex">
  <b class="p-1 mx-2 border border-dark rounded">txt</b>
  <span class="p-1 mx-2 border border-dark rounded">png</span>
  <span class="p-1 mx-2 border border-dark rounded">pdf</span>
  <span class="p-1 mx-2 border border-dark rounded">odt</span>
  <span class="p-1 mx-2 border border-dark rounded">zip</span>
  <span class="p-1 mx-2 border border-dark rounded">avi</span>
  <span class="p-1 mx-2 border border-dark rounded">jpeg</span>
  <span class="p-1 mx-2 border border-dark rounded">mkv</span>
  <span class="p-1 mx-2 border border-dark rounded">doc</span>
  <span class="p-1 mx-2 border border-dark rounded">mp3</span>
  <span class="p-1 mx-2 border border-dark rounded">csv</span>
  <span class="p-1 mx-2 border border-dark rounded">rar</span>
  <span class="p-1 mx-2 border border-dark rounded">mp4</span>
</div><br>

<table class="table">
  <thead>
    <tr>
      <th>description du type de fichier</th>
      <th>exemple(s) de format(s) utilisé(s)</th>
    </tr>
  </thead>
  <tbody>
    <tr> 
      <td>Un fichier qui contient du texte (brut ou enrichi)</td>
      <td>
        <b class="p-1 mx-2 border border-dark rounded">txt</b>
      </td>
    </tr>
    <tr> 
      <td>Un fichier qui contient une musique</td>
      <td>
      </td>
    </tr>
    <tr> 
      <td>Un fichier qui contient une image ou une photo</td>
      <td>
      </td>
    </tr>
    <tr> 
      <td>Un fichier qui contient une vidéo</td>
      <td>
      </td>
    </tr>
    <tr> 
      <td>Un fichier qui compresse d'autres fichiers</td>
      <td>
      </td>
    </tr>
  </tbody>
</table>

+++spoil d-print-none
<br><br>
<table class="table">
  <thead>
    <tr>
      <th>description du type de fichier</th>
      <th>exemple(s) de format(s) utilisé(s)</th>
    </tr>
  </thead>
  <tbody>
    <tr> 
      <td>Un fichier qui contient du texte (brut ou enrichi)</td>
      <td>
        <b class="p-1 mx-2 border border-dark rounded">txt</b>
        <span class="p-1 mx-2 border border-dark rounded">pdf</span>
        <span class="p-1 mx-2 border border-dark rounded">odt</span>
        <span class="p-1 mx-2 border border-dark rounded">doc</span>
        <span class="p-1 mx-2 border border-dark rounded">csv</span>
      </td>
    </tr>
    <tr> 
      <td>Un fichier qui contient une musique</td>
      <td>
        <span class="p-1 mx-2 border border-dark rounded">mp3</span>
      </td>
    </tr>
    <tr> 
      <td>Un fichier qui contient une image ou une photo</td>
      <td>
        <span class="p-1 mx-2 border border-dark rounded">png</span>
        <span class="p-1 mx-2 border border-dark rounded">jpeg</span>
      </td>
    </tr>
    <tr> 
      <td>Un fichier qui contient une vidéo</td>
      <td>
        <span class="p-1 mx-2 border border-dark rounded">avi</span>
        <span class="p-1 mx-2 border border-dark rounded">mkv</span>
        <span class="p-1 mx-2 border border-dark rounded">mp4</span>
      </td>
    </tr>
    <tr> 
      <td>Un fichier qui compresse d'autres fichiers</td>
      <td>
        <span class="p-1 mx-2 border border-dark rounded">zip</span>
        <span class="p-1 mx-2 border border-dark rounded">rar</span>
      </td>
    </tr>
  </tbody>
</table><br><br>
spoil+++

+++bulle r3do
  Savez vous comment afficher le format d'un fichier depuis un ordinateur ?
+++
<br>

### Les données

<div class="text-center d-print-none">
  <br>
  <iframe src="https://www.youtube-nocookie.com/embed/IJJgcZ2DEs0" title="YouTube video player" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" class="rounded" width="560" height="315" frameborder="0"></iframe>
  <div><b>00:00 -> 01:10</b></div>
  <br>
</div>
<div class="d-none d-print-block">Lien vers la vidéo <a href="https://youtu.be/IJJgcZ2DEs0" target="_blank">"SNT / Données, comment les manipuler ?"</a> ( https://youtu.be/IJJgcZ2DEs0 )<br><br></div>

+++question write "D'après la vidéo, qu'est-ce qu'une donnée ?"

+++spoil
  Une donnée est une valeur décrivant un objet informatique. Un objet en informatique peut être un fichier. Une donnée peut être de différents types : 
  <ul>
    <li>texte</li>
    <li>nombre</li>
    <li>date</li>
    <li>intervalle de temps (en minutes et en secondes par exemple)</li>
  </ul>
spoil+++

<div class="text-center d-print-none">
  <br>
  <iframe src="https://www.youtube-nocookie.com/embed/IJJgcZ2DEs0?start=49" title="YouTube video player" class="rounded" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" width="560" height="315" frameborder="0"></iframe>
  <div><b>00:50 -> 02:21</b></div>
  <br>
</div>

+++question write "D'après la vidéo qu'est-ce qu'un descripteur ?"

+++spoil
  Un objet informatique contient des <u>données</u> : une année, du texte, un intervalle de temps, ...<br>
  Un <b>descripteur</b> est une catégorie de <u>données</u> : 
  <ul>
    <li><u>donnée de type 'intervalle de temps' :</u><ul>
      <li><b>la durée de la musique</b></li>
    </ul></li>
    <li><u>donnée de type 'texte' :</u><ul>
      <li><b>le nom du titre de la musique</b></li>
      <li><b>le nom du style de musique</b></li>
      <li><b>le nom de l'interprète</b></li>
    </ul></li>
    <li><u>donnée de type 'année' :</u><ul>
      <li><b>l'année de création de la musique</b></li>
    </ul></li>
  </ul>
spoil+++

## Déterminer les données structurées d'un fichier

Vous allez apprendre à analyser puis structurer les données d'un fichier au format jpg.

Depuis un ordinateur, pour afficher les informations d'un fichier au format jpg, il me suffit de cliquer sur le fichier et d'accéder à son détaillé. Sur windows par exemple, il suffit de faire un clic droit sur le fichier et de cliquer sur "Propriétés" dans le menu qui apparaît. Cette opération est sensiblement la même quelque soit le système d'exploitation de l'ordinateur.

+++bulle r3do
  Quels autres systèmes d'exploitations connaissez vous ? et comment faites vous pour afficher le détail d'un fichier ?
+++

+++bulle r3do droite
  <b>remarque, </b>il est difficile sur smartphone et tablette de consulter le détaillé de n'importe quel fichier. Il est généralement nécessaire d'installer des applications spécifiques depuis le store
+++

Voici donc les informations détaillées que je peux trouver sur une image nommée 'paysage.jpg' stockée sur un ordinateur : (qui utilise un système d'exploitation Linux)

<div class="d-print-avoid-break mb-3">
  <img class="rounded-3 offset-2 col-8" src="https://i.ibb.co/5R13L4q/Capture-d-cran-de-2021-09-29-21-04-19.png">
</div>

Les données affichées ici sont ce qu'on appelle les **méta-données** du fichier. Il s'agit des données qui décrivent le fichier (qui est ici une photo). 

+++question write "Dans l'image ci-dessus, entourez au crayon gris l'ensemble des éléments qui sont les descripteurs de la photo"

+++spoil d-print-none
  Les éléments situés à gauche de l'image (Type d'image, Largeur, Hauteur, ...) sont des descripteurs des données que contient la photo
spoil+++

+++question write "Imaginez que vous possédez un dossier 'mes photos' contenant des photos prises avec différents appareils photo. Vous aimeriez dans ce dossier effectuer diverses recherches comme par exemple : rechercher les photos prises avec un appareil photo Nikon. D'après l'image ci-dessus, quels seraient selon vous les 5 descripteurs les plus intéressants pour vos recherches ?"

<table class="table">
  <thead class="text-center">
    <tr><th colspan="2">Descripteurs</th></tr>
  </thead>
  <tbody>
    <tr>
      <td class="text-center">0</td>
      <td>la <b>largeur (en pixel)</b> car cela permettra de rechercher les photos selon leur largeur</td>
    </tr>
    <tr>
      <td class="text-center">1</td>
      <td></td>
    </tr>
    <tr>
      <td class="text-center">2</td>
      <td></td>
    </tr>
    <tr>
      <td class="text-center">3</td>
      <td></td>
    </tr>
    <tr>
      <td class="text-center">4</td>
      <td></td>
    </tr>
  </tbody>
</table>

+++spoil d-print-none
<table class="table">
  <thead class="text-center">
    <tr><th colspan="2">Descripteurs</th></tr>
  </thead>
  <tbody>
    <tr>
      <td class="text-center">0</td>
      <td>la <b>largeur (en pixel)</b> car cela permettra de rechercher les photos selon leur largeur</td>
    </tr>
    <tr>
      <td class="text-center">1</td>
      <td>l'<b>hauteur (en pixel)</b> car cela permettra de rechercher les photos selon leur hauteur</td>
    </tr>
    <tr>
      <td class="text-center">2</td>
      <td>la <b>marque</b> car cela permettra de rechercher les photos selon la marque de l'appareil photo utilisé</td>
    </tr>
    <tr>
      <td class="text-center">3</td>
      <td>le <b>modèle</b> car cela permettra de rechercher les photos selon le modèle de l'appareil photo utilisé</td>
    </tr>
    <tr>
      <td class="text-center">4</td>
      <td>la <b>date</b> car cela permettra de rechercher les photos selon la date où elles ont été prises</td>
    </tr>
  </tbody>
</table>
spoil+++

+++todo fin de séance d'une heure ici

+++question write "Complétez le tableau ci-dessous en ajoutant dans la première ligne les descripteurs et dans la deuxième les valeurs des descripteurs de l'image 'paysage.jpg'"

+++bulle r3do
  <b>attention !</b> par convention nous ne mettons pas les unités dans les valeurs et les valeurs doivent être écrites en minuscules ! 
+++

<style>
  .tableq5{
    table-layout: fixed !important;
  }
</style>
<table class="table tableq5">
  <thead>
    <tr>
      <th>largeur (en pixel)</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>640</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>

+++spoil d-print-none
<table class="table tableq5">
  <thead>
    <tr>
      <th>largeur (en pixel)</th>
      <th>hauteur (en pixel)</th>
      <th>marque</th>
      <th>modèle</th>
      <th>date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>640</td>
      <td>480</td>
      <td>nikon</td>
      <td>coolpix p6000</td>
      <td>2008:10:22 16:28:37</td>
    </tr>
  </tbody>
</table>
spoil+++

+++question write "Ajouter dans le tableau des données provenant d'autres images du dossier 'mes photos'. Inventez des valeurs qui soient cohérentes vis à vis des descripteurs (par exemple dans la colonne largeur, je ne dois trouver que des nombres représentant une largeur en pixel)"

<table class="table tableq5">
  <thead>
    <tr>
      <th>largeur (en pixel)</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>640</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>1024</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>1280</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>1024</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>1600</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>

+++spoil d-print-none
<table class="table tableq5">
  <thead>
    <tr>
      <th>largeur (en pixel)</th>
      <th>hauteur (en pixel)</th>
      <th>marque</th>
      <th>modèle</th>
      <th>date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>640</td>
      <td>480</td>
      <td>nikon</td>
      <td>coolpix p6000</td>
      <td>2008:10:22 16:28:37</td>
    </tr>
        <tr>
      <td>1024</td>
      <td>768</td>
      <td>olympus</td>
      <td>alios 2</td>
      <td>2018:05:01 11:11:54</td>
    </tr>
    <tr>
      <td>1280</td>
      <td>720</td>
      <td>sony</td>
      <td>oyaa a200</td>
      <td>2011:11:15 06:02:44</td>
    </tr>
    <tr>
      <td>1024</td>
      <td>768</td>
      <td>nikon</td>
      <td>nimbus 2000</td>
      <td>2011:11:15 15:48:10</td>
    </tr>
    <tr>
      <td>1600</td>
      <td>900</td>
      <td>sony</td>
      <td>oyaa a500</td>
      <td>2021:01:07 17:45:01</td>
    </tr>
  </tbody>
</table>
spoil+++

+++bulle r3do
  Vous êtes en avance ? faîtes l'activité 3 du livre page 18 :]
+++

## Résumons

- Un fichier informatique est composé de données de types variés, les principaux types sont : texte, nombre, date, intervalle de temps
- Les méta-données d'un fichier sont des catégories de données qui décrivent le fichier : son poids (nombre), sa date de création (date), sa dernière date de modification (date), ...
- Les descripteurs sont des catégories de données pour un objet informatique donné. Ils permettent de structurer les données d'un objet informatique
- Dans le cas où l'objet informatique est un fichier, les méta-données peuvent être utilisées comme descripteur

<style>
  .table-resumons .s{
    border-left: 2px solid blue !important;
    border-top: 2px solid blue !important;
    border-bottom: 2px solid blue !important;
  }
  .table-resumons .m{
    border-top: 2px solid blue !important;
    border-bottom: 2px solid blue !important;
  }
  .table-resumons .e{
    border-right: 2px solid blue !important;
    border-top: 2px solid blue !important;
    border-bottom: 2px solid blue !important;
  }
  .table-resumons{
    border-collapse:separate !important;
    border-spacing: 0px 1px !important;
  }
</style>

<table class="table table-resumons">
  <thead>
    <tr class="text-red">
      <th>nom</th>
      <th>capitale</th>
      <th>hymne</th>
      <th>superficie (km²)</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="text-green s">france</td>
      <td class="text-green m">paris</td>
      <td class="text-green m">la marseillaise</td>
      <td class="text-green e">632734</td>
    </tr>
    <tr>
      <td class="text-green s">chine</td>
      <td class="text-green m">pékin</td>
      <td class="text-green m">la marche des volontaires</td>
      <td class="text-green e">9596961</td>
    </tr>
    <tr>
      <td class="text-green s">états-unis</td>
      <td class="text-green m">washington</td>
      <td class="text-green m">the star-spangled banner</td>
      <td class="text-green e">9833517</td>
    </tr>
    <tr>
      <td class="text-green s">argentine</td>
      <td class="text-green m">buenos Aires</td>
      <td class="text-green m">himno nacional argentino</td>
      <td class="text-green e">2791810</td>
    </tr>
  </tbody>
</table>

<b>Légende : </b>
<ul>
  <li><b class="text-red">Nom du descripteur</b></li>
  <li><span class="text-green">Valeur du descripteur</span></li>
  <li><span class="p-1 border border-2" style="border-color:blue !important">Objet informatique</span></li>
</ul>
