# Le traitement des données structurées

+++programme
Identifier les principaux formats et représentations de données
Utiliser un site de données ouvertes, pour sélectionner et récupérer des données
Réaliser des opérations de recherche, filtre, tri ou calcul sur une ou plusieurs tables

Différencier les principaux formats de données utilisés aujourd'hui
Récupérer des données librement mises à disposition sur Internet
Manipuler des données structurées à l'aide d'un logiciel ou d'un programme Python
+++

+++sommaire 1

## L'open Data

+++video "https://www.youtube.com/watch?v=ejDKCywfYmQ" "Qu'est ce que l'Open Data ?"

+++question write "Qu'est-ce que l'open data ?"

+++spoil "Réponse"
  L'open data, qui se traduit par donnée ouverte, désigne les données qui sont librement partagées et facilement réutilisable
spoil+++

+++question write "À quelles personnes les données ouvertes sont elles destinées ?"

+++spoil "Réponse"
  À toutes les personnes sans distinction. Les chercheurs, les journalistes et les développeurs d'applications font partie des principaux utilisateurs.
spoil+++

+++question write "Que permettent de faire les données ouvertes ?"

+++spoil "Réponse"
  L'addition de données ouvertes provenant de différentes ressources permet, entre autres,  de créer de nouvelles applications informatiques innovantes
spoil+++

+++bulle r3do
  dans cette séance, vous allez apprendre à utiliser des données fournies par un site de partage de données ouvertes
+++

- ouvrez un navigateur Internet depuis votre ordinateur
- saisissez dans un moteur de recherche la recherche "données ouvertes france"

+++question write "Quels sont les trois premiers sites qui apparaissent dans les premiers résultats du moteur de recherche ?"

+++spoil "Réponse"
  <input class="w-100" placeholder="Réponse variable">
spoil+++

<br>

Le site [www.data.gouv.fr](https://www.data.gouv.fr) regroupe des données ouvertes en lien avec la France.

### Le concours "ma thèse en 180 secondes"

Nous allons apprendre à manipuler des données relatives aux concours "ma thèse en 180 secondes". Dans ce concours, des doctorants présentent le sujet de recherche sur lequel ils travaillent en 180 secondes. Le but est de promouvoir le secteur de la recherche en France et d'inciter les élèves à se lancer dans une carrière de chercheur.

Voici par exemple la présentation de thèse du 1er lauréat du concours de 2021 : 

+++video "https://www.youtube.com/watch?v=NK6Gbqcm4rU" "Paul Dequidt - Finale nationale MT180 édition 2021"

Il est possible de récupérer dans un fichier de données structurées toutes les informations des participants au concours depuis 2017 : 

- dirigez vous sur le site [www.data.gouv.fr](https://www.data.gouv.fr) avec votre navigateur
- effectuez une recherche dans le site avec les mots "thèse 180 secondes"
- dans les résultats de la recherche, sélectionnez l'élément qui possède le plus de ressources
- vous devriez arriver sur une page portant le titre "Finalistes et lauréats du concours Ma Thèse en 180 secondes France"

<div class="d-print-avoid-break mb-3">
  <img class="rounded-3 offset-3 col-6" src="https://i.ibb.co/K5fTV11/Screenshot-from-2021-09-30-23-52-03.png">
</div>

+++question write "Combien de ressources sont disponibles sur cette page ?"

+++spoil "Réponse"
  <input class="w-100" placeholder="Réponse variable">
spoil+++

+++question write "Quels sont les différents formats de ressources qui sont proposés ?"

+++spoil "Réponse"
  <input class="w-100" placeholder="Réponse variable">
spoil+++

## Les formats de fichiers de données structurées

Les données structurées peuvent être enregistrées dans des fichiers. Il existe différentes manières de structurer des données, et donc, il existe différents type de format de données structurées. Dans cette séance, nous allons étudier en détail un format de fichier très utilisé pour stocker des données structurées : le format csv

### Le format csv

Dans un fichier au format csv, la première ligne liste les descripteurs des données qui nous intéresse. Ces descripteurs doivent être séparés par un séparateur. Toutes les lignes suivantes correspondent aux données des objets informatiques que l'on souhaite stocker. 

Voici un exemple de contenu de fichier csv : 

```text
animal;age;durée de la vidéo (en secondes)
oiseau;14;124
poisson;4;99
cheval;17;64
```

+++bulle r3do
  remarquez que dans l'exemple du contenu du fichier csv, les éléments sont tous séparés par un ';'
+++

+++bulle r3do droite
  c'est le séparateur utilisé par défaut en général
+++

<br>
Dans la séance précédente, vous aviez créé un tableau contenant des données de plusieurs photos : 

<style>
  .tableq5{
    table-layout: fixed !important;
  }
</style>
<table class="table tableq5">
  <thead>
    <tr>
      <th>largeur (en pixel)</th>
      <th>hauteur (en pixel)</th>
      <th>marque</th>
      <th>modèle</th>
      <th>date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>640</td>
      <td>480</td>
      <td>nikon</td>
      <td>coolpix p6000</td>
      <td>2008:10:22 16:28:37</td>
    </tr>
        <tr>
      <td>1024</td>
      <td>768</td>
      <td>olympus</td>
      <td>alios 2</td>
      <td>2018:05:01 11:11:54</td>
    </tr>
    <tr>
      <td>1280</td>
      <td>720</td>
      <td>sony</td>
      <td>oyaa a200</td>
      <td>2011:11:15 06:02:44</td>
    </tr>
    <tr>
      <td>1024</td>
      <td>768</td>
      <td>nikon</td>
      <td>nimbus 2000</td>
      <td>2011:11:15 15:48:10</td>
    </tr>
    <tr>
      <td>1600</td>
      <td>900</td>
      <td>sony</td>
      <td>oyaa a500</td>
      <td>2021:01:07 17:45:01</td>
    </tr>
  </tbody>
</table>

+++question write "Rappelez ce qui correspond, dans ce tableau, aux descripteurs"

+++spoil "Réponse"
  Un objet informatique contient des données : une année, du texte, un intervalle de temps, ... <br>
  Pour rappel, un descripteur est une catégorie de données. Ici les descripteurs sont : 
  <ul>
    <li>largeur (en pixel)</li>
    <li>hauteur (en pixel)</li>
    <li>marque</li>
    <li>modèle</li>
    <li>date</li>
  </ul>
spoil+++

+++question write "Écrivez ci-dessous le contenu d'un fichier csv contenant les données des photos prises après 2015"

+++spoil "Réponse"
<pre class="col-lg-12 mx-auto col-print-12"><code class="hljs text language-text">largeur (en pixel);hauteur (en pixel);marque;modèle;date
1024;768;olympus;alios 2;2018:05:01 11:11:54
1600;900;sony;oyaa a500;2021:01:07 17:45:01
</code></pre>
spoil+++

Il existe un autre format de fichier de données structurées très connu et très utilisé notamment en programmation web : le format json. Voici à quoi ressemble les données de vos photos mais sous format json : 

<pre class="col-lg-12 mx-auto col-print-12"><code class="hljs text language-json">[
  {
    "largeur (en pixel)": 1024,
    "hauteur (en pixel)": 768,
    "marque": "olympus",
    "modèle": "alios 2",
    "date": "2018:05:01 11:11:54"
  },
  {
    "largeur (en pixel)": 1600,
    "hauteur (en pixel)": 900,
    "marque": "sony",
    "modèle": "oyaa a500",
    "date": "2021:01:07 17:45:01"
  }
]
</code></pre>

+++bulle r3do
  les formats de fichiers de données structurées ont tous leurs avantages et leurs inconvénients 
+++

+++bulle r3do droite
  par exemple, un fichier au format csv sera plus léger en poids que le format json. Mais le format json permet d'insérer les données d'un nouvel objet sans renseigner forcement tous les descripteurs
+++

+++todo séance terminée ici

## Manipuler un fichier de données csv  

Revenons en sur les données ouvertes. Dans la page Web "Finalistes et lauréats du concours Ma Thèse en 180 secondes France" : 

- téléchargez une ressource au format csv

<div class="d-print-avoid-break mb-3">
  <img class="rounded-3 col-12" src="https://i.ibb.co/YPRx6MP/Capture-d-cran-de-2021-10-01-10-03-43.png">
</div>

- une fois le fichier téléchargé, ouvrez le fichier avec un logiciel de type tableur

+++bulle r3do
  les deux logiciels tableurs les plus connus aujourd'hui sont le logiciel propriétaire Excel (de Microsoft) et le logiciel libre Calc (de Open ou Libre office)
+++

Les données s'affichent proprement dans un tableau sur votre logiciel tableur

<div class="d-print-avoid-break mb-3">
  <img class="rounded-3 col-8 offset-2" src="https://i.ibb.co/FnPtvxm/Screenshot-from-2021-10-01-15-03-18.png">
</div>

### Supprimer les données superflues

Certaines colonnes contiennent des données qui ne sont pas utiles pour nous aujourd'hui. Le format csv permet de supprimer facilement les descripteurs (et leurs données) dont nous n'avons pas besoin. 

Par exemple, nous souhaitons supprimer la première colonne "type\_de\_concours" et les données qu'elle contient. Pour ce faire : 

- depuis le logiciel tableur, cliquez sur la lettre qui se trouve juste au dessus de la colonne, cela va avoir pour effet de sélectionner toute la colonne

<div class="d-print-avoid-break mb-3">
  <img class="rounded-3 col-4 offset-4" src="https://i.ibb.co/v1X8TFw/Screenshot-from-2021-10-01-15-05-24.png">
</div>

- faire un clic gauche sur la lettre au dessus de 'type\_de\_concours', un menu s'ouvre. Cliquez sur "supprimer" (ou "supprimer la colonne" selon le logiciel). La colonne 'type\_de\_concours' disparaît.

Supprimez toutes les colonnes sauf les colonnes :

- millesime
- prix
- sexe_code
- prenom
- nom
- linkedin
- video\_ma\_these\_en\_180\_secondes
- titre\_de\_la\_these
- lien\_vers\_theses\_fr
- laboratoire
- etablissement

### Effectuer un tri

Avant de commencer, sauvegardez votre fichier en le nommant "concours_theses" (toujours au format csv bien sur). Une mauvaise manipulation vous permettra de recharger le fichier comme il était. 

- sélectionnez la colonne qui contient les données "millesime"
- en explorant les outils mis à disposition dans votre logiciel tableur, trouver le moyen de trier toutes les donnés par années. Ce qui permettra de voir tous les passages dans l'ordre chronologique.

+++bulle r3do
  Le format csv permet de trier rapidement les données que contient le fichier de données
+++

### Les limites d'un logiciel tableur

#### Les fonctionnalités mises à disposition

La plupart des logiciels tableur disposent d'un outil de recherche pour trouver une information dans les données que nous visualisons. Cependant, l'outil de recherche mis à disposition ne permet pas d'effectuer des recherches très avancées. Pire encore, chaque logiciel tableur peut avoir sa manière de faire pour rechercher une information, ce qui implique qu'il faille comprendre le fonctionnement de chaque logiciel et ceci chaque fois qu'il est mis à jour.

#### La performance

Les logiciels tableur embarquent énormément de fonctionnalités, ceci plus l'interface graphique, la mémoire de l'ordinateur devient vite saturée si le fichier de données chargé devient volumineux. (ce qui, par exemple, arrive très facilement en machine learning)

### Python à la rescousse ! 

Pour contourner les limitations et problèmes qui peuvent survenir lors de l'utilisation d'un logiciel tableur, nous pouvons utiliser un langage de programmation pour manipuler un fichier de données csv.

Le format csv facilite grandement le traitement de données par un langage informatique. Et l'utilisation d'un langage informatique permet d'effectuer des manipulations complexes sur de très larges données contrairement aux logiciels type tableur.

[Dans ce lien](/s/9H4HE4) (https://www.edusophie.com/s/9H4HE4) se trouve un script python qui permet de lister les vidéos selon certains critères. Copiez/collez le code dans l'IDE thonny, sauvegardez le sur votre ordinateur, et exécutez le !

+++bulle r3do
  <b>attention !</b> assurez vous bien que le fichier qui contient le code python se trouve dans le même dossier que le fichier csv 'concours_theses'
+++

+++question write "En analysant le résultat affiché après avoir exécuté le programme python, expliquez en quelques mots ce que fait le programme python"

+++spoil "Réponse"
  Ce programme python effectue une recherche dans les données enregistrées dans le fichier csv 'concours_theses'. À la fin de l'exécution, le programme python affiche les résultats qu'il a trouvé
spoil+++

+++question write "En lisant les commentaires dans le code (les textes après un # dans python), quelles sont les deux lignes que l'on peut modifier rapidement pour changer les critères utilisés dans la recherche ?"

+++spoil "Réponse"
  Les critères de recherches sont configurés à la ligne 34 (le sexe) et la ligne 35 (un mot clé) du code. Il suffit de modifier les valeurs de ces variables pour effectuer une nouvelle recherche.
spoil+++

+++bulle r3do
  Vous êtes en avance ? faîtes l'exercice 5 du livre page 29 :]
+++

## Résumons

<ul>
    <li>Il existe différentes manières de structurer des données dans un fichier. Deux des formats de fichiers les plus connus sont : <ul>
            <li><b>le fichier csv</b> : peut être facilement manipulable à travers un logiciel de type 'tableur'

                <pre class="col-lg-8 mx-auto col-print-10"><code>joueur;niveau;classe
1;3;guerrier
2;7;mage
3;1;mage
4;3;rodeur
</code></pre>

            </li>
            <li><b>le fichier json</b> : ne contenant pas les noms des descripteurs, les fichiers json sont très légers et en général utilisés pour partager rapidement des données entre ordinateurs

                <pre class="col-lg-8 mx-auto col-print-10"><code>[
  {
    "joueur": 1,
    "niveau": 3,
    "classe": "guerrier"
  },
  {
    "joueur": 2,
    "niveau": 7,
    "classe": "mage"
  },
  {
    "joueur": 3,
    "niveau": 1,
    "classe": "mage"
  },
  {
    "joueur": 4,
    "niveau": 3,
    "classe": "rodeur"
  }
]
</code></pre>

    </li>
    </ul></li>
</ul>

- L'open data désigne des données librement partagées et facilement réutilisables : 
    - ces données proviennent de différents services publiques ; transport, éducation, ...
    - ces données peuvent permettre de créer de nouvelles applications et garantissent une plus grande transparence entre citoyens et services publiques
- Lorsque les données deviennent volumineuses et/ou que les manipulations que nous souhaitons faire deviennent complexes, un logiciel type 'tableur' va rapidement saturer la mémoire de l'ordinateur. il faut alors utiliser un langage de programmation à la place pour manipuler les données