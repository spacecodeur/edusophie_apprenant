# Présentation et utilisation d'outils de cartographie numérique

+++programme
  Contribuer à OpenStreetMap de façon collaborative.
  Identifier les différentes couches d’information de GeoPortail pour extraire différents types de données
  
  Utiliser un service de cartographie
  Contribuer à un service de cartographie
  Créer et partager votre carte numérique personnalisée
+++

+++sommaire

## Introduction 

Il existe de nombreux services de cartographie numérique accessibles en ligne :

<div class="d-flex justify-content-around">
+++image "/images/md/8H653AED5G3F496F339GH8E2BDAGE8" col-2
+++image "/images/md/D4CF7DB6G7834D952CCGB2A2D7B2A8" col-2
+++image "/images/md/3FECD983DEC5DED52435735H4HF8A4" col-2
+++image "/images/md/3E95994C3ADE3GBE4C8B5E63GH7752" col-2
+++image "/images/md/A22CH46G2CBFHGE3C32934756BBA86" col-2
</div>

+++bulle r3do
  lesquels reconnaissez vous ?
+++

L'un des plus connu est google map. L'entreprise Google met à disposition sur son application des fonctionnalités gratuites ; rechercher une adresse, établir un itinéraire, etc ... 

Comme pour la plupart des applications mises à disposition par Google, l'entreprise propose également des services payant pour les autres entreprises :
- calculer automatiquement le temps avant de recevoir une livraison
- intégrer une carte directement sur une application mobile
- intégrer une "street view" sur un site Web
- ... 

+++bulle r3do
  depuis combien de temps google map existe ?
+++

En 2018, les prix des services proposés par google map ont été revus à la hausse, ce qui a poussé un certain nombre d'entreprise à se tourner vers d'autres alternatives.

+++pagebreak

## OpenStreetMap 

+++citation /images/md/A22CH46G2CBFHGE3C32934756BBA86 gauche
  OpenStreetMap (<b>OSM</b>) est un projet collaboratif de cartographie en ligne qui vise à constituer une base de données géographiques libre du monde (permettant par exemple de créer des cartes sous licence libre), en utilisant le système GPS et d'autres données libres. Il est mis en route en juillet 2004 par Steve Coast à l'University College de Londres
+++wikipedia https://fr.wikipedia.org/wiki/OpenStreetMap

### Utiliser et contribuer au projet OSM

+++bulle r3do
  accordons nous ensemble sur une ville et un lieu à analyser
+++

+++question write "Notez le nom de la ville"

+++spoil
  Narbonne
spoil+++

+++question write "Notez le nom du lieu"

+++spoil
  Lycée Beauséjour
spoil+++

+++question "Ouvrez un navigateur Web et accédez à l'+++lien "https://www.openstreetmap.org" "url du site OpenStreetMap""

+++question write "Déplacez vous vers le lieu que nous souhaitons analyser"

<hr>

Il est possible de contribuer facilement à l'amélioration d'openstreetmap en soumettant par exemple l'ajout d'un nouveau lieu. Le plus simple est d'ajouter une note en étant anonyme (c'est à dire non connecté) ou de confirmer une note déjà existante. Si la note est nouvelle, vous écrirez dedans les informations suivantes : 

- le nom du lieu et son type (stade, bar, piscine, école, lycée, ...)
- l'adresse exacte du lieu 
- une source sur Internet qui permet de vérifier facilement que votre note est valide

Si la note existe déjà, un simple message "Je confirme cette note" suffira.

Pour savoir comment ajouter/modifier une note, vous pouvez vous aider du +++lien "https://www.teritorio.fr/aide/osm/ajouter-une-note-dans-openstreetmap/" "tutoriel suivant"

+++question write "Quel est le contenu de la note à ajouter si elle est nouvelle ?"

+++spoil
  <pre>Établissement Beauséjour, c'est un lycée privé
Adresse : 2 Rue Girard, 11100 Narbonne
Le lien vers le site officiel du lycée : https://lycee-beausejour.com/</pre>
spoil+++

+++question write "Quel est le numéro de la note que vous avez créé ou modifié ?"

+++spoil
 (Réponse variable)
spoil+++

<hr>

En ajoutant une note, d'autres contributeurs pourront venir la lire et valider sa véracité. Au bout de quelques temps, le nouveau lieu sera officiellement intégré à openstreetmap.

### résumons

- OpenStreetMap est un service de cartographie libre et collaboratif qui permet de visualiser, de modifier et d'utiliser des données géographiques
- OpenStreetMap propose également de calculer un itinéraire

## Geoportail

Géoportail est un site français qui permet de créer sa propre carte numérique. Cela peut être utile pour un particulier comme à une entreprise qui souhaite visualiser l'état d'une région en fonction d'un ensemble de critères.

### Créez votre propre carte numérique

Pour l'activité qui va suivre, nous supposons le scénario suivant : 
- un nouvel élève, Téo, intègre la classe en cours d'année et est tout nouveau dans notre région. Il ne connaît donc pas du tout notre ville
- pour accueillir cet élève et l'aider à se repérer dans la ville, vous vous proposez de composer une carte sur +++lien "https://www.geoportail.gouv.fr" "le site géoportail" affichant des informations susceptibles de l'intéresser 

Voici les centres d'intérêt de Téo : 
0. Téo souhaite poursuivre plus tard des études en histoire, il aime bien **visiter des musées**
1. La famille de Téo possède un drone qu'ils utilisent pour leur loisir. Il faudrait **afficher sur la carte les zones interdites à la prise de vue aérienne**
2. Téo aime randonner dans la foret avec sa famille et ses amis, la carte doit **afficher les forêts publiques proches de la ville**
3. Téo vous demande d'**indiquer votre lieu préféré dans la ville**

Vous pouvez vous aider +++lien "https://www.youtube.com/watch?v=Jg7SWI3GCF4" "de cette courte vidéo" pour comprendre comment créer votre carte sur-mesure sur le site geoportail. 

+++question write "Quelle première couche de données avez vous utilisé pour répondre au premier centre d'intérêt de Téo ?"

+++spoil
  Il faut utiliser la couche de données 'Musées' qui se trouve dans la catégorie 'CULTURE ET PATRIMOINE'
spoil+++

+++question write "Dans les données mises à disposition par le site, dans quelle catégorie et sous catégorie se trouve la couche de données 'zones interdites à la prise de vue aérienne' ?"

+++spoil
  La couche de données 'zones interdites à la prise de vue aérienne' se trouve dans la catégorie 'TERRITOIRES ET TRANSPORTS' puis la sous-catégorie 'DESCRIPTION DU TERRITOIRE'
spoil+++

+++question write "Concernant le troisième centre d'intérêt de Téo, quelle couche de donnée avez vous utilisé ?"

+++spoil
  La couche de données 'zones interdites à la prise de vue aérienne' ( se trouve dans la catégorie 'DÉVELOPPEMENT DURABLE, ÉNERGIE' puis la sous-catégorie 'Forêts'
spoil+++

+++question "En vous aidant +++lien "https://www.geoportail.gouv.fr/tutoriels/ajoutez-vos-informations" "de ce lien", ajoutez un lieu de votre choix dans la carte numérique que vous êtes en train de composer"

+++question write "Quelles options de partage de votre carte sont proposées par le site geoportail ?"

+++spoil
  Le site propose de partager la carte par mail, sur facebook ou sur twitter. Il est également possible de générer un lien permanent de la carte ou d'intégrer la carte dans une page Web html. 
spoil+++

+++question write "Quels peut être les intérêts de créer un lien permanent de votre carte ?"

+++spoil
  Il est possible de partager votre carte d'un simple copié collé et dans la quasi totalité des réseaux sociaux
spoil+++

+++question "Partagez votre carte à votre adresse email, elle servira dans un prochain thème de l'année"


### résumons

- Géoportail est un site public français permettant l'accès à des données géographiques ou géolocalisées
- L'utilisateur peut superposer sur un fond de carte différentes couches de données de manière à créer une carte numérique personnalisée 
- Geoportail permet aussi la localisation, le calcul de distances, de surfaces et d'itinéraires 
