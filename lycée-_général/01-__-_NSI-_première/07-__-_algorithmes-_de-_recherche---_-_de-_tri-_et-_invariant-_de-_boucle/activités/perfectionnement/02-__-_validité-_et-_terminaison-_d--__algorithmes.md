# Validité et terminaison d'algorithmes

Dans cette activité, plusieurs fonctions simples sont exposées : 
- Nous allons vérifier si ces fonctions sont valides, c'est à dire que nous allons prouver qu'elles effectuent le travail que l'on attends d'elles quelques soient les données qu'on leur fournis (**validité**)
- Nous allons justifier également que ces fonctions ne contiennent pas de boucle infinie (**terminaison**)

<hr>

<b>Algorithme #0</b>

```python
def foisX(nbr, x):
  resultat = nbr
  for i in range(x - 1):
    resultat = resultat + nbr
  return resultat
  
print(foisX(9, 10))
```

+++question write "Prouvez que 'resultat = nbr * x' est un invariant de boucle"

+++spoil "Aide"
  revoyez la méthode employée <a href="/apprenant/01-__-_NSI-_premiere/07-__-_algorithmes-_de-_recherche---_-_de-_tri-_et-_invariant-_de-_boucle/cours/02-__-_invariant-_et-_terminaison-_de-_boucle.md" target="_blank">présentée en cours</a>
spoil+++

+++question write "Justifiez, si elle a lieu, la terminaison de ce programme"

<hr>

<b>Algorithme #1</b>

```python
def puissance(x, n):
  assert n > 0
  assert isinstance(n, int)
  
  resultat = x
  while(n > 1):
    resultat = resultat * x
    n = n - 1
  return resultat

print(puissance(2,10))
```

+++question write "Quelle formule semble calculer cette fonction ?"

+++question write "Prouvez que 'resultat = x<sup>n</sup>' est un invariant de boucle"

+++question write "Justifiez, si elle a lieu, la terminaison de ce programme"

<hr>

<b>Algorithme #2</b>

```python
def fonctionMystere(n):
  resultat = 0
  for i in range(1, n + 1):
    resultat = resultat + i
  return resultat

print(fonctionMystere(6))
print(fonctionMystere(10))
```

+++question write "Quel résultat vont afficher les print de la ligne 7 et de la ligne 8 ?"

+++question write "Prouver que 'resultat = 0 + 1 + 2 + 3 + ... + n' est un invariant de boucle"

<hr>

<b>Algorithme #3</b>

```python
def mafonction3(a, b):
  while(a < 0 and b == True):
    a += 2
    print(a)
```

+++question write "Justifiez si ce programme s'exécute indéfiniment (présence d'une boucle infinie) ou non"

<b>Algorithme #4</b>

```python
def mafonction4(a, b):
  while(a < 0 or b == True):
    a += 2
    print(a)
```

+++question write "Justifiez si ce programme s'exécute indéfiniment (présence d'une boucle infinie) ou non"

<b>Algorithme #5</b>

```python
def mafonction5(a):
  if(a > 0):
    while(a != 0):
      a = a - 2
      print(a)
```

+++question write "Justifiez si ce programme s'exécute indéfiniment (présence d'une boucle infinie) ou non"

<b>Algorithme #6</b>

```python
def mafonction6(a, b, c):
  if(a > 0 and b > 10):
    while(a != 0 and b > 10 and c < 30):
      a = a - 2
      b = a + b 
    print(a)
```

+++question write "Justifiez si ce programme s'exécute indéfiniment (présence d'une boucle infinie) ou non"

## Réponses

<ol start="0">
  <li>
  +++spoil
<br>
<p>Tentons de démontrer que la propriété 'resultat = nbr * x' est un invariant de boucle</p>
<p><b>Initialisation :</b></p>
<ul>
  <li>Avant la boucle for, resultat est initialisé à nbr et x est initialisé à 1</li>
  <li>'resultat = nbr * x' est donc vraie car 'nbr = nbr * 1'</li>
  <li>Donc la propriété 'resultat = nbr * x' est vraie avant le premier tour de boucle</li>
</ul>

<br>
<p><b>Hérédité :</b></p>
Supposons que "resultat = nbr * x" est vraie. Il faut démontrer que "resultat' = nbr * ( x + 1 )" est vraie
<ul>
  <li>Au tour de boucle suivant, on ajoute à résultat la valeur nbr</li>
  <li>resultat' = ( nbr * x ) + nbr</li>
  <li>On factorise '( nbr * x ) + nbr' <ul>
      <li>( nbr * x ) + nbr = ( nbr * x ) + ( nbr * 1) = nbr * ( x + 1 )</li>
  </ul></li>
  <li>Et donc : resultat' = nbr * ( x + 1 ) </li>
</ul>

<br>
<p><b>Conclusion :</b></p>
La propriété est vraie dans l'étape d'initialisation. En supposant que la propriété est vraie, nous avons démontré qu'elle est vraie au tour de boucle suivant. Nous pouvons en conclure que la propriété est vraie quelque soient les valeurs de nbr et de x. Donc 'resultat = nbr * x' est un invariant de boucle et donc, la fonction foisX produira toujours le résultat attendu.
  spoil+++
  </li>
  <li>
  +++spoil
La fonction comporte une boucle, qui est une boucle for. La boucle for va être répétée x-1 fois et se terminera ensuite. Composée seulement d'un boucle for qui se terminera tôt ou tard, nous sommes assuré que la fonction foisX terminera toujours son exécution
  spoil+++
  </li>
  <li>
  +++spoil
La fonction semble calculer la formule : resultat = x<sup>n</sup>.  
  spoil+++
  </li>
  <li>
  +++spoil
<br>
<p>Tentons de démontrer que la propriété 'resultat = x<sup>n</sup> est un invariant de boucle</p>
<p><b>Initialisation :</b></p>
<ul>
  <li>Avant la boucle while, resultat est initialisé à x</li>
  <li>Comme nous sommes avant la boucle while, la formule x<sup>n</sup> n'a pas encore commencé à s'appliquer, cela revient donc à calculer : resultat = x<sup>1</sup> et x<sup>1</sup> est égal à x</li>
  <li>Donc resultat = x</li>
</ul>

<br>
<p><b>Hérédité :</b></p>
Supposons que "resultat = x<sup>n</sup>" est vraie. Il faut démontrer au tour de boucle suivant que "resultat' = x<sup>n+1</sup>" est vraie
<ul>
  <li>Au tour de boucle suivant, on multiplie par x le résultat</li>
  <li>resultat' = x<sup>n</sup> * x</li>
  <li>resultat' = x<sup>n+1</sup></li>
  <li>Donc la propriété reste vraie au tour de boucle suivant</li>
</ul>

<br>
<p><b>Conclusion :</b></p>
La propriété est vraie dans l'étape d'initialisation. En supposant que la propriété est vraie, nous avons démontré qu'elle est vraie au tour de boucle suivant. Nous pouvons en conclure que la propriété est vraie quelque soit la valeur de x et de n. Donc 'resultat = x<sup>n</sup>' est un invariant de boucle et donc, la fonction puissance produira toujours le résultat attendu.
  spoil+++
  </li>
  <li>
    +++spoil
      La fonction contient une boucle while.<ul>
        <li>La condition de la boucle while contient un variant qui est n</li>
        <li>Nous voyons que dans le contenu de la boucle while, la variable n est décrémentée de 1 à chaque tour de boucle (ligne 5)</li>
        <li>Puisque n doit être supérieure à 0 (étant donné l'assertion à la ligne 2), tôt ou tard, n aura pour valeur 1</li>
        <li>Donc la condition de la boucle while deviendra fausse tôt ou tard et donc nous sommes assurés que la boucle while ne va pas s'exécuter indéfiniment</li>
      </ul>
    spoil+++
  </li>
  <li>
    +++spoil
      <p>la ligne 7 affichera 21 (1 + 2 + 3 + 4 + 5 + 6)</p>
      <p>la ligne 8 affichera 55 (1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10)</p>
    spoil+++
  </li>
  <li>
  +++spoil
<br>
<p>Tentons de démontrer que la propriété 'resultat = 0 + 1 + 2 + 3 + ... + n' est un invariant de boucle</p>
<p><b>Initialisation :</b></p>
<ul>
  <li>Avant la boucle for, resultat est initialisé à 0</li>
  <li>Comme nous ne sommes pas entré encore dans la boucle for, le calcul '0 + 1' n'a pas été encore effectué</li>
  <li>Donc la propriété 'resultat = 0 + 1 + 2 + 3 + ... + n' avant le premier tour de boucle est équivalente à 'resultat = 0`</li>
  <li>Donc dans cette étape d'initialisation, la propriété est vraie</li>
</ul>

<br>
<p><b>Hérédité :</b></p>
Supposons que "resultat = 0 + 1 + 2 + 3 + ... + n" est vraie. Il faut démontrer que "resultat' = 0 + 1 + 2 + 3 + ... + n + ( n + 1 )" est vraie
<ul>
  <li>Au tour de boucle suivant, on ajoute à résultat i</li>
  <li>resultat' = 0 + 1 + 2 + 3 + ... + n + i</li>
  <li>Or i correspond à l'élément suivant qui suit n, donc i a pour valeur n + 1, donc : <ul>
      <li>resultat' = 0 + 1 + 2 + 3 + ... + n + (n + 1)</li>
  </ul></li>
</ul>

<br>
<p><b>Conclusion :</b></p>
La propriété est vraie dans l'étape d'initialisation. En supposant que la propriété est vraie, nous avons démontré qu'elle est vraie au tour de boucle suivant. Nous pouvons en conclure que la propriété est vraie quelque soit la valeur de n. Donc 'resultat = 0 + 1 + 2 + 3 + ... + n' est un invariant de boucle et donc, la fonction fonctionMystere produira toujours le résultat attendu.
  spoil+++    
  </li>
  <li>
    +++spoil
<ul>
  <li>Dans le programme existe une seule boucle while, et la condition de cette boucle contient deux variables : a et b</li>
  <li>Tant que a est supérieur à 0 <b>et</b> que b est égal à True, la boucle continuera à s'exécuter. Il faut que les deux sous-conditions (séparées par un and) soient vraies toutes les deux pour que la condition de la boucle soit vraie</li>
  <li>La variable a est modifiée à chaque tour de boucle (ligne 3), a est donc un variant de boucle</li>
  <li>Comme à chaque tour de boucle, a est incrémenté de 2, tôt ou tard, a deviendra supérieure à 0. Donc la condition de la boucle "a < 0" deviendra fausse</li>
  <li>Nous sommes donc assurés que la boucle while ne s'exécutera pas indéfiniment</li>
</ul>
    spoil+++
  </li>
    <li>
    +++spoil
<ul>
  <li>Tout comme le programme précédent, celui-ci contient une seule boucle while, et la condition de cette boucle contient deux variables : a et b</li>
  <li>Tant que a est supérieur à 0 <b>ou</b> que b est égal à True, la boucle continuera à s'exécuter. Il suffit qu'une des deux conditions (séparées par un or) soit vraie pour que la condition de la boucle soit vraie</li>
  <li>La variable b n'est pas modifiée à chaque tour de boucle, et donc si elle est initialement égale à True, la boucle va alors s'exécuter indéfiniment</li>
</ul>
    spoil+++
  </li>
      <li>
    +++spoil
<ul>
  <li>Ce programme contient une seule boucle while, et la condition de cette boucle contient une variable qui est la variable a</li>
  <li>Tant que a est différent de 0 <b>ou</b>, la boucle continuera à s'exécuter</li>
  <li>La variable a est modifiée à chaque tour de boucle (ligne 4), a est donc un variant de boucle</li>
  <li>À chaque tour de boucle, a est décrémentée de 2. Cela ne nous assure pas que a sera un moment donné égal à 0 (par exemple, a peut être un nombre impair ou un nombre négatif)</li>
  <li>La boucle peut donc, en fonction de la valeur initiale de a, s'exécuter indéfiniment</li>
</ul>
    spoil+++
  </li>
</ol>