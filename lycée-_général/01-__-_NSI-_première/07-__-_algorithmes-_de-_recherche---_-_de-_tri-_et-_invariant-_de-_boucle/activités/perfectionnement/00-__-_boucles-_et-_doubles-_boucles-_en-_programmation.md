# Boucles et doubles boucles en programmation


Les algorithmes de tri par sélection et par insertion, qui sont le cœur de ce module, utilisent deux boucles imbriquées l'une dans l'autre. Le but de cet exercice est de faire un rappel sur les boucles en python et de s'entraîner sur les imbrications de boucles. 

## Rappels sur les boucles

Ci-dessous, un programme python permettant d'afficher les entiers allant de 0 (inclus) à 10 (inclus) : 

widget python
for i in range(11):
  print(i)
widget

+++question write "Quelle(s) modification(s) faut il faire dans le code ci-dessus pour afficher les nombres allant de 5 (inclus) à 15 (inclus) ?"

<hr>

Ci-dessous, un programme python permettant d'afficher les entiers allant de 0 (inclus) à 10 (inclus) mais utilisant cette fois-ci l'instruction <code>while</code> au lieu de l'instruction <code>for</code>: 

widget python
i = 0
while (i < 11):
  print(i)
  i = i + 1
widget

+++question write "Quelle(s) modification(s) faut il faire dans le code ci-dessus pour afficher les nombres allant de 5 (inclus) à 15 (inclus) ?"

<hr>

+++question write "Écrivez la fonction python 'superieur10' qui prend en paramètre une liste de nombre et qui affiche tous les nombres supérieurs à 10"

+++question write "Améliorez la fonction pour qu'elle retourne à la fin de son traitement le nombre de chiffres supérieurs à 10"

+++question write "Donnez un exemple d'utilisation de la fonction superieur10"

## Imbrication de boucles

Étant donné le programme suivant : 

widget python
a = 4
b = 3
for i in range(a):
  print(i)
  for j in range(b):
    print(j)
widget

+++question write "En exécutant le code et en analysant le résultat de son exécution, expliquez en quelques mots ce que fait le programme ci-dessus"

<hr>

Étant donné le fonction <code>repeter</code> définie ci-dessous : 

```python

def repeter(nombres):
  for nombre in nombres:
    for i in range(nombre):
      if(i < 3):
        print(nombre)
```

Si nous utilisons cette fonction avec comme paramètre la liste [4,0,2], nous obtenons dans la console : 

<pre class="p-2">4
4
4
2
2
</pre>

<hr>

Nous souhaitons utiliser la fonction avec en paramètre la liste [5,0,2,2]

+++question write "Remplissez la tableau ci-dessous qui suit l'évolution des valeurs des variables <code>nombre</code> et <code>i</code> lorsque la fonction est exécutée"

<table class="text-center">
  <thead>
    <tr>
      <th class="px-2"><code>nombre</code></th>
      <th class="px-2"><code>i</code></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>5</td>
      <td>0</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </tbody>
</table>

+++question write "Quel résultat s'affichera dans la console si nous exécutons cette fonction avec comme paramètre [5,0,2,2] ?"