# Analyse d'algorithmes simples

+++consigne
  <ul class="text-start my-0">
    <li>Sortez votre crayon et une feuille et laissez vous guider par l'activité !</li>
    <li>Comme vu en cours concernant la complexité d'un algorithme, nous ne considérons que les actions suivantes : <b>affectation</b>, <b>calcul</b> et <b>comparaison</b></li>
  </ul>
+++

+++sommaire

## Instructions basiques et conditionnelle

+++question write "Quel(s) type(s) d'étape est (sont) effectuée(s) dans cet algorithme ? Combien y a-t-il d'étapes au total ?"

```markdown
a ← 10
b ← a * a * 17
c ← b - a
```

+++question write "Quel(s) type(s) d'étape est (sont) effectuée(s) dans cet algorithme ? Combien y a-t-il d'étapes au total ?"

```markdown
a ← 10
b ← 4 + 6
Si a = b
  Afficher "a est égal à b"
Sinon
  Afficher "a n'est pas égal à b"
Fin Si
```

+++question write "Quel(s) type(s) d'étape est (sont) effectuée(s) dans cet algorithme ? Combien y a-t-il d'étapes au total ?"

```markdown
Fonction produit(a, b)
  resultat ← a x b
  Afficher resultat
Fin Fonction

produit(3, 4)
produit(7, 5)
produit(8, 4)
```

+++question write "Quel(s) type(s) d'étape est (sont) effectuée(s) dans cet algorithme ? Combien y a-t-il d'étapes au total ?"

**note** : nous considérons que l'instruction "<code>est un nombre pair</code>" compte pour trois actions

```markdown
Fonction estPaire(nombre)
  Si nombre est un nombre pair
    Retourner Vrai
  Sinon 
    Retourner Faux
  FIn Si
Fin Fonction

Si estPaire(17 + 9)
  Afficher "le résultat de la somme est pair"
Fin Si
```

---


## Instructions basiques, conditionnelles et itératives

Pour rappel, en programmation, les instructions itératives permettent de répéter un certain nombre de fois un bloc d'instructions. La plupart des langages de programmation fournissent les instructions <code>for</code> et <code>while</code>

L'instruction <code>for</code> est utilisée pour répéter un nombre entier de fois un bloc d'instructions. Voici ci-dessous un exemple d'utilisation de l'instruction <code>for</code> :

widget python
prixDesArticles = [10, 3, 7]
prixTotal = 0

for prix in prixDesArticles:
  prixTotal = prixTotal + prix
  
print(prixTotal)
widget

+++question write "Combien de fois la ligne 5 est exécutée ?"

+++question write "Réécrivez le programme ci-dessus en pseudo-code"

+++spoil "Aide"
  L'instruction python <code>for</code> peut se traduire par "Pour chaque"
spoil+++

+++question write "Combien d'étapes sont effectuées au total dans votre algorithme ?"

<hr>

Tout comme l'instruction <code>for</code>, l'instruction <code>while</code> permet de répéter un bloc d'instruction(s) plusieurs fois. Ce qui différencie ces deux instructions et que l'instruction <code>While</code> permet de répéter en boucle un bloc d'instruction en fonction d'une **condition**. 

Voici ci-dessous un exemple d'utilisation de l'instruction <code>while</code> :

widget python
nombre = 2
puissance = 8
resultat = 1

while(puissance > 0):
  resultat = resultat * nombre
  puissance = puissance - 1
  
print(resultat)
widget

+++question write "À quelle condition les lignes 6 et 7 sont exécutées ?"

+++question write "Combien de fois les lignes 6 et 7 sont exécutées ?"

+++question write "Réécrivez le programme ci-dessus en pseudo-code"

+++spoil "Aide"
  L'instruction python <code>while</code> peut se traduire par "Tant que"
spoil+++

+++question write "Combien d'actions sont effectuées au total dans votre algorithme ?"

## Entraînement python

Dans cette partie, vous pouvez utiliser utiliser votre IDE sur votre ordinateur

+++question "Codez la fonction estPair en vous aidant de l'opération % (modulo)"

+++spoil "Aide"
  Cette opération a été vue en contrôle et vous pouvez vous aider d'Internet pour voir comment utiliser le module en python
spoil+++

+++question "Codez la fonction estPair sans utiliser l'opération % (modulo)"

+++spoil "Aide"
  Deux approches sont possibles : une première dans laquelle vous devez utiliser une instruction itérative, et une seconde où vous pouvez comparer si la division entière par deux est égale à la division par 2 du nombre à tester (quelques recherches sur Internet sont nécessaire pour savoir comment faire une division 'entière' en python)
spoil+++