# Tri par sélection

+++consignes
  <ol start="0">
    <li>Composez des groupes de 3 à 4 personnes</li>
    <li>Mélangez les cartes du paquet de carte qui est fournis à votre groupe</li>
    <li>Répondez avec votre groupe à la première question de l'activité en vous aidant des ressources fournies</li>
    <li>Répondez aux autres questions à l'aide d'un ordinateur. L'entre aide entre membres du groupe est bien sur toujours encouragée</li>
  </ol>
+++

## Prise en main de l'algorithme

Le tri par sélection peut être implémenté dans la plupart des langages de programmation. Voici le pseudo code de cet algorithme mis à disposition +++lien "https://fr.wikipedia.org/wiki/Tri_par_s%C3%A9lection" "par la page wikipédia 'Tri par sélection'" (et très légèrement amélioré) : 

<pre class="p-2" style="background-color:#eeeeee">0.  procédure tri_selection(tableau t)
1.      n ← longueur(t) 
2.      POUR i allant de 0 à n - 2 (compris)
3.          min ← i       
4.          POUR j allant de i + 1 à n - 1 (compris)
5.              si t[j] < t[min], alors min ← j
6.          fin POUR
7.          SI min ≠ i
8.              alors échanger t[i] et t[min]
9.          fin SI
10.     fin POUR
11.     retourner t
10. fin procédure
</pre>

Voici quelques ressources Web qui peuvent vous aider à comprendre comment appliquer cet algorithme 

<b>Une animation :</b>

+++image "/images/md/2GE328HCGCA7B7FGF23G5BGF9426DB" col-4 mx-auto

<b>Une vidéo de 'danse algorithmique' (si si):</b> 

+++video "https://www.youtube.com/watch?v=Ns4TPTC8whw" "Insert-sort with Romanian folk dance"

+++question "Reproduisez l'algorithme du tri par sélection avec un paquet de cartes mélangées"

<hr>

À la ligne 7 de l'algorithme, un échange de donnée est effectué entre deux éléments du tableau, ce qui correspond à échanger deux cartes dans votre paquet.

+++question write "Rédigez la fonction echanger qui prend trois paramètres en entrée : un tableau, un entier a et un entier b. La fonction doit échanger l'élément du tableau d'indice a avec l'élément du tableau d'indice b puis la fonction doit retourner le tableau avec les deux éléments inversés"

+++spoil "Aide"
  Pour vous aider à créer la fonction, voici deux exemples d'utilisation de la fonction 'echange' : 
  
  ```python

liste = [5, 1, 10, 12]

liste = echanger(liste, 0, 2)
print(liste) # affichera : [10, 1, 5, 12]

liste = echanger(liste, 3, 1)
print(liste) # affichera : [10, 12, 5, 11]
  ```
spoil+++

<hr>

Pour rappel la fonction len, fournie de base par python, permet d'obtenir le nombre d'élément dans un tableau.

+++question "Implémentez en python l'algorithme du tri par sélection. Vérifiez que le résultat du programme python donne le même résultat que l'application de l'algorithme avec un jeu de cartes"

## Étude de la complexité

La difficulté dans l'étude de la complexité de cet algorithme est qu'il contient deux boucles imbriquées l'une dans l'autre. Pour rappel, ce n'est pas le nombre d'étape exacte qui nous intéresse en algorithmique mais l'ordre de grandeur du nombre total d'étape. 

Donc pour simplifier l'étude de cet algorithme de tri par sélection, nous allons seulement nous concentrer sur le nombre de tour de boucle total effectué en faisant abstraction des étapes intermédiaire. L'ordre de grandeur de la complexité de cet algorithme est en majeure partie conditionné par le nombre total de tours de boucle effectués.

Dans le pseudo-code, <b class="text-red">la première boucle</b> démarre à la ligne 2 et <b class="text-green">la seconde boucle</b> à la ligne 4.

<pre class="p-2" style="background-color:#eeeeee">0.  procédure tri_selection(tableau t)
1.     n ← longueur(t) 
2.     <b class="text-red">pour i de 0 à n - 2 (compris)</b>
3.         min ← i       
4.         <b class="text-green">pour j de i + 1 à n - 1 (compris)</b>
5.             si t[j] < t[min], alors min ← j
6.         fin pour
7.         si min ≠ i, alors échanger t[i] et t[min]
8.     fin pour
9.     retourner t
10.  fin procédure
</pre>

Dans l'algorithme, n correspond au nombre total de cartes qu'il y a dans votre paquet.

+++question write "Supposons que le paquet contient 5 cartes au total, combien de tour va effectuer <b class="text-red">la première boucle</b> ?"

+++question write "En supposant que le paquet contient toujours 5 cartes au total, pour chaque tour de <b class="text-red">la première boucle</b>, combien de tour effectue <b class="text-green">la seconde boucle</b> en fonction de n ? Complétez le tableau ci-dessous :"

<table class="table table-sm text-center">
  <thead>
    <tr>
      <th>Tour de <b class="text-red">la première boucle</b>, valeur de <b class="text-red">i</b></th>
      <th>Nombre de tours effectués par <b class="text-green">la seconde boucle</b></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td></td>
    </tr>
    <tr>
      <td>1</td>
      <td></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td></td>
    </tr>
  </tbody>
</table>
  

+++question write "Soit n le nombre de cartes dans le paquet, combien de tour va effectuer <b class="text-red">la première boucle</b> en fonction de n ?"

+++question write "En supposant que le paquet contient toujours n cartes au total, <b>pour chaque tour</b> de <b class="text-red"> la première boucle</b>, combien de tour effectue <b class="text-green">la seconde boucle</b> ? Complétez le tableau ci-dessous :"

<table class="table table-sm text-center">
  <thead>
    <tr>
      <th>Tour de <b class="text-red">la première boucle</b>, valeur de <b class="text-red">i</b></th>
      <th>Nombre de tours effectués par <b class="text-green">la seconde boucle</b></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>n - 1</td>
    </tr>
    <tr>
      <td>1</td>
      <td>n - 2</td>
    </tr>
    <tr>
      <td>2</td>
      <td>n - 3</td>
    </tr>
    <tr>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <td>n - 4</td>
      <td></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td> </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td> </td>
    </tr>
  </tbody>
</table>

+++question write "En déduire que le nombre <b>total</b> de tours de la seconde boucle est égal à : ( ( n - 1 ) * n ) /2"

+++question write "En déduire que la complexité de l'algorithme est en O(n²)"

### Représentation graphique de la complexité

+++question "Améliorer le programme python afin de chronométrer le temps d'exécution de la fonction de tri"

+++bulle matthieu
  pour vous aider, +++lien "preparation-_python-_pour-_les-_algorithmes-_de-_tri.md" "voici le lien" vers l'activité dans laquelle nous nous sommes intéressés au chronométrage du temps d'exécution de code python
+++

<hr>

La fonction ci-dessous, qui va servir par la suite, permet de générer une liste de n entiers aléatoires compris entre 0 et 100

```python
import random

def genererListeEntiers(n):
  liste = []
  for i in range(n):
    liste.append(random.randint(0, 100))
  return liste
```

+++question "Ajouter la fonction genererListeEntiers dans votre programme afin de pouvoir générer par la suite des listes contenant n éléments aléatoires"

+++question "Complétez le tableau ci-dessous"

+++bulle matthieu
  <b>attention !</b> si dans votre programme vous affichiez toutes les cartes, enlevez de votre code ces affichages (les fonctions print) car la nombre de cartes va être très important et l'affichage va alors saturer votre IDE et par conséquent votre ordinateur
+++

<div class="col-8 mx-auto my-3">
<table class="table table-sm text-center">
  <thead>
    <tr>
      <th>n</th>
      <th>temps d'exécution de la fonction de tri (en ms)</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>300</td>
      <td></td>
    </tr>
    <tr>
      <td>3000</td>
      <td></td>
    </tr>
    <tr>
      <td>30000</td>
      <td></td>
    </tr>
  </tbody>
</table>
</div>

+++question write "Tracez grossièrement dans un graphe une courbe passant par les points décrits dans le tableau de la question précédente (l'abscisse du graphe est n et l'ordonnée est le temps d'exécution)"

+++question write "Est-ce que l'évolution de la courbe de votre graphe correspond bien à la complexité algorithmique donnée à la question #8 ?"

## Réponses

<ol start="1">
  <li>
  +++spoil
widget python
def echanger(tableau, a, b):
  tmp = tableau[a]
  tableau[a] = tableau[b]
  tableau[b] = tmp
  return tableau
widget
  spoil+++
  </li>
  <li>
  +++spoil
widget python
def echanger(tableau, a, b):
  tmp = tableau[a]
  tableau[a] = tableau[b]
  tableau[b] = tmp
  return tableau
  
def tri_selection(cartes):
  n = len(cartes)
  for i in range(n-1):
    min = i
    for j in range(i+1, n):
      if(cartes[j] < cartes[min]):
        min = j
    if(min != i):
      cartes = echanger(cartes, i, min)
  return cartes

cartes = [9, 6, 1, 4, 8]
cartes = tri_selection(cartes)
print(cartes)

widget
  spoil+++
  </li>
  <li>
  +++spoil
La première boucle démarre à i = 0 et se termine à i = n - 2, où n est égal à 5. Donc i prendra les valeurs 0 1 2 et 3. La première boucle effectue donc 4 tours
  spoil+++
  </li>
  <li>
  +++spoil
<table class="table table-sm text-center">
  <thead>
    <tr>
      <th>Tour de <b class="text-red">la première boucle</b>, valeur de <b class="text-red">i</b></th>
      <th>Nombre de tours effectués par <b class="text-green">la seconde boucle</b></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>4</td>
    </tr>
    <tr>
      <td>1</td>
      <td>3</td>
    </tr>
    <tr>
      <td>2</td>
      <td>2</td>
    </tr>
    <tr>
      <td>3</td>
      <td>1</td>
    </tr>
  </tbody>
</table>
  spoil+++
  </li>
  <li>
  +++spoil
La première boucle démarre à i = 0 et se termine à i = n - 2. Donc i prendra les valeurs 0 1 2 ... n-4 n-3 n-2. La première boucle effectue donc n-1 tours
  spoil+++
  </li>
  <li>
    +++spoil
<table class="table table-sm text-center">
  <thead>
    <tr>
      <th>Tour de <b class="text-red">la première boucle</b>, valeur de <b class="text-red">i</b></th>
      <th>Nombre de tours effectués par <b class="text-green">la seconde boucle</b></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>n - 1</td>
    </tr>
    <tr>
      <td>1</td>
      <td>n - 2</td>
    </tr>
    <tr>
      <td>2</td>
      <td>n - 3</td>
    </tr>
    <tr>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <td>n - 4</td>
      <td>3</td>
    </tr>
    <tr>
      <td>n - 3</td>
      <td>2</td>
    </tr>
    <tr>
      <td>n - 2</td>
      <td>1</td>
    </tr>
  </tbody>
</table>      
    spoil+++
  </li>
  <li>
    +++spoil
+++image "/images/md/EGD5C74GD5AF6578GH9EECBB99A89C" col-10 mx-auto
    spoil+++
  </li>
  <li>
    +++spoil
<p>Développons la formule : ( ( n - 1 ) * n ) /2</p>
<p>Nous obtenons alors :  n² / 2 - n / 2</p>

<p>En algorithmique, nous ne nous intéressons qu'aux ordres de grandeur. Si n atteint une valeur infiniment grande, la partie la plus importante dans la formule est n² (le reste devient négligeable).</p>

<p>L'algorithme du tri par sélection est donc de complexité <b>O(n²)</b></p>

+++bulle matthieu
  on dit que l'algorithme est de complexité <b>quadratique</b>
+++
    spoil+++
  </li>
  <li>
    +++spoil
widget python
import timeit

def echanger(tableau, a, b):
  tmp = tableau[a]
  tableau[a] = tableau[b]
  tableau[b] = tmp
  return tableau

def tri_selection(cartes):
  n = len(cartes)
  for i in range(n-1):
    min = i
    for j in range(i+1, n):
      if(cartes[j] < cartes[min]):
        min = j
    if(min != i):
      cartes = echanger(cartes, i, min)
  return cartes


start = timeit.default_timer()

cartes = [9, 6, 1, 4, 8]
cartes = tri_selection(cartes)

end = timeit.default_timer()
difference = end - start
tempsEnMilliSeconde = int(round(1000 * difference))
print("Le temps total d'exécution de la fonction de tri est de " + str(tempsEnMilliSeconde) + " ms")
widget
    spoil+++
  </li>
  <li>
    +++spoil
widget python
import timeit
import random

def genererListeEntiers(n):
  liste = []
  for i in range(n):
    liste.append(random.randint(0, 100))
  return liste

def echanger(tableau, a, b):
  tmp = tableau[a]
  tableau[a] = tableau[b]
  tableau[b] = tmp
  return tableau

def tri_selection(cartes):
  n = len(cartes)
  for i in range(n-1):
    min = i
    for j in range(i+1, n):
      if(cartes[j] < cartes[min]):
        min = j
    if(min != i):
      cartes = echanger(cartes, i, min)
  return cartes


start = timeit.default_timer()

cartes = genererListeEntiers(10)
cartes = tri_selection(cartes)

end = timeit.default_timer()
difference = end - start
tempsEnMilliSeconde = int(round(1000 * difference))
print("Le temps total d'exécution de la fonction de tri est de " + str(tempsEnMilliSeconde) + " ms")
widget
    spoil+++
  </li>
  <li>
    +++spoil
Voici un exemple de temps d'exécutions moyens relevés depuis un ordinateur fixe

<div class="col-8 mx-auto my-3">
<table class="table table-sm text-center">
  <thead>
    <tr>
      <th>n</th>
      <th>temps d'exécution de la fonction de tri (en ms)</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>300</td>
      <td>5</td>
    </tr>
    <tr>
      <td>3000</td>
      <td>472</td>
    </tr>
    <tr>
      <td>30000</td>
      <td>44500</td>
    </tr>
  </tbody>
</table>
</div>
    spoil+++
  </li>
    <li>
  +++spoil
En utilisant +++lien "https://www.geogebra.org/graphing" "l'outil suivant", voici à quoi ressemble la courbe que j'obtiens : 

+++image "/images/md/37G9HBHAF8BBEH2DHHBD9CDBG3DEFH"

  spoil+++
  </li>
  <li>
  +++spoil
La courbe décrite par les différentes mesures évolue de manière très semblable à la fonction x². Ce comportement correspond bien à la complexité de l'algorithme de la fonction taille établit à la question #8
  spoil+++
  </li>
</ol>