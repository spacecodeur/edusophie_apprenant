# Validité d'un algorithme

+++consigne
  Sortez votre crayon et une feuille et laissez vous guider par l'activité !
+++

La division euclidienne consiste à diviser a par b en effectuant une série d'opérations. Il s'agit de déterminer deux entiers q et r tels que a = b * q + r avec r compris entre 0 (inclus) et b (exclu).

Par exemple si nous souhaitons diviser 49 par 5 nous obtenons : 49 = 5 * 9 + 4 

Voici un algorithme déterminant q et r à partir de deux nombres a et b: 

```text
a ← un nombre entier
b ← un nombre entier

q ← 0
r ← a 

tant que r >= b
  q ← q + 1
  r ← r - b
  
afficher q et r
```

Nous cherchons à déterminer si cet algorithme retournera toujours les bonnes valeurs de q et de r (tel que r >= 0 et r < b ). Pour cela, il faut prouver que la propriété 'a = b * q + r' est un invariant de la boucle utilisée dans l'algorithme

+++question write "Prouver que la propriété ' a = b * q + r ' est un invariant de boucle"

+++bulle matthieu
  il faut donc suivre l'étape d'initialisation et l'étape d'hérédité comme vu en cours
+++

+++question write "Si ' a = b * q + r ' est un invariant de la boucle utilisée dans l'algorithme, que peut on en déduire ?"

<hr>

Étant donné la fonction python ci-dessous qui permet de calculer un nombre x à la puissance n

```python
def puissance(x, n):
  r = 1
  for i in range(n):
    r = r * x
  return r
```

+++question write "Donner un exemple d'utilisation de la fonction puissance"

<hr>

Considérons que la variable i est égale à 0 avant le premier tour de la boucle for.

+++question write "Prouver que la propriété ' r = x<sup>i</sup> ' est un invariant de la boucle utilisée dans la fonction puissance"

+++question write "En déduire que le programme de la fonction puissance utilise un algorithme valide"

+++spoil "Aide"
  dans la question précédente, nous avons démontré que ' r = x<sup>i</sup> ' est un invariant de boucle. Il faut réussir à démontrer le lien entre cet invariant de boucle et la formule x<sup>n</sup> 
spoil+++

+++question write "Quelle est la complexité de la fonction puissance ?"

## Réponses

<ol start="0">
  <li>
  +++spoil
<p><b>Étape 'Initialisation' : </b></p>
<p>Comme on peut le voir, le programme initialise q à 0 et r avec la valeur de a. Donc avant que la boucle ne démarre : </p><ul>
  <li>a = b * q + r <=> a = b * 0 + a <=> a = a</li>
</ul>
Donc la propriété ' a = b * q + r ' est vraie avant que la ligne 4 de l'algorithme
<p></p>

<p><b>Étape 'Hérédité' : </b></p>
<p>Avant une itération arbitraire, supposons que la propriété ' a = b * q + r ' est vraie lors d'un tour de boucle, montrons que la propriété reste vraie au tour de boucle suivant. Nous devons alors montrer que a = bq′ + r′ : </p><ul>
  <li>q' = q + 1</li>
  <li>r' = r - b</li>
  <li><u>b * q' +  r'</u> = b * (q + 1) + (r − b) = b * q + r = <u>a</u></li>
  <li>donc nous avons bien <u>a = b * q' +  r'</u></li>
</ul>
<p></p>

<p><b>Conclusion : </b></p>
<p>Puisque ' a = b * q + r ' est vraie avant et pendant la boucle, alors ' a = b * q + r ' est un invariant, toujours vrai, de la boucle utilisée dans l'algorithme</p>

  spoil+++
  </li>
  <li>
  +++spoil

Puisque que ' a = b * q + r ' est vraie avant et pendant la boucle, alors en fin de boucle, ' a = b * q + r ' est toujours vraie. Et donc, le programme nous affiche à la fin les bonnes valeurs q et r 

  spoil+++
  </li>
  <li>
  +++spoil
widget python
def puissance(x, n):
  r = 1
  for i in range(n):
    r = r * x
  return r

# exemple d'utilisation en utilisant une variable
resultat = puissance(2, 8)
print(resultat)

# exemple d'utilisation sans utiliser de variable
print(puissance(2, 8))
widget
  spoil+++
  </li>
  <li>
  +++spoil
<p>Prouvons que ' r = x<sup>i</sup> ' est un invariant de la boucle for</p>
<p><b>Étape 'Initialisation' : </b></p>
<p>Avant le premier tour de la boucle for, r est initialisé à 1. La variable i n'est pas définie avant la boucle for, nous considérons que cette variable est alors égale à 0. Nous avons donc : </p>
<ul>
  <li>x<sup>i</sup> = x<sup>0</sup></li>
  <li>et x<sup>0</sup> = 1, quelque soit x</li>
  <li>r est initialisé à 1 avant la boucle for</li>
  <li>Donc nous avons bien r = x<sup>i</sup> car 1 = x<sup>0</sup></li>
</ul>
Donc la propriété ' r = x<sup>i</sup> ' est vraie avant la boucle for
<p></p>

<p><b>Étape 'Hérédité' : </b></p>
<p>Avant une itération arbitraire, supposons que la propriété ' r = x<sup>i</sup> ' est vraie lors d'un tour de boucle, montrons que la propriété reste vraie au tour de boucle suivant. Nous devons alors montrer que r' = x<sup>i+1</sup> : </p><ul>
  <li>r = x<sup>i</sup> (notre supposition de départ)</li>
  <li>le calcul effectué au tour de boucle suivant est r' = r * x</li>
  <li>donc au tour suivant r * x = x<sup>i</sup> * x = x<sup>i+1</sup></li>
  <li>donc nous avons bien r' = x<sup>i+1</sup></li>
</ul>
<p></p>

<p><b>Conclusion : </b></p>
<p>Puisque ' r = x<sup>i</sup> ' est vraie avant et pendant la boucle, alors '  r = x<sup>i</sup> ' est un invariant, toujours vrai, de la boucle utilisée dans l'algorithme</p>
  spoil+++
  </li>
  <li>
  +++spoil
Lors de la dernière itération, i à comme valeur n - 1. Donc r = x<sup>n-1</sup> avant d'exécuter une dernière fois la ligne 4 du programme. Après avoir exécuté la ligne 4 lors du dernier tour de boucle, r = x<sup>n-1</sup> * n et donc r = x<sup>n</sup>. Comme nous avons démontré que ' r = x<sup>i</sup> ' est un invariant de boucle, alors à la sortie de la boucle for nous avons bien r = x<sup>n</sup>. Ce programme est donc valide.
  spoil+++
  </li>
  <li>
  +++spoil
Dans le programme : 
<ul>
  <li>la ligne 2 compte pour une étape (affectation)</li>
  <li>la boucle for est répétée n fois</li>
  <li>dans la boucle for sont effectuées deux étapes : un calcul puis une affectation</li>
</ul>
Donc la fonction puissance contient au total : 1 + n * 2 étapes. Étant donné qu'en complexité, nous ne nous  intéressons qu'aux ordres de grandeurs lorsque les variables prennent des valeurs très grandes, nous en déduisons que 1 + n * 2 = O(n). La fonction puissance est donc de complexité linéaire.
  spoil+++
  </li>
</ol>