# Présentation de la spécialité NSI

<p class="h3 text-center"><b style="color:#cd5c5c;font-size:2.5rem">N</b>umérique et <b style="color:#cd5c5c;font-size:2.5rem">S</b>ciences <b style="color:#cd5c5c;font-size:2.5rem">I</b>nformatiques</p>

<p class="h4 text-center d-none">Lien vers cette présentation : <a href="https://www.edusophie.com/s/2HG728">https://www.edusophie.com/s/2HG728</a></p>

<hr>


<p class="h4 text-center">L'Informatique a bouleversée nos habitudes depuis les années 1990~2000</p>
<p class="h4 text-center">Ses applications dans notre quotidien sont aujourd'hui omniprésentes</p>

+++image "/images/md/2E62G2C75G695H32CE2DG6GEAE556E" col-8 mx-auto

<hr>

<style>
.bag{
  -webkit-border-top-left-radius: 50px;
  -webkit-border-top-right-radius: 50px;
  -moz-border-radius-topleft: 50px;
  -moz-border-radius-topright: 50px;
  border-top-left-radius: 50px;
  border-top-right-radius: 50px;
}
.cadre{
  -webkit-border-bottom-left-radius: 1rem;
  -webkit-border-bottom-right-radius: 1rem;
  -moz-border-radius-bottomleft: 50px;
  -moz-border-radius-bottomright: 1rem;
  border-top-bottom-radius: 1rem;
  border-top-bottom-radius: 1rem;
}
@font-face { font-family: pixel; src: url('/font/md/square/Square One.ttf'); } 
.cadre-subtitle{
   font-family: pixel;
   color:white !important;
}
.cadre-subtitle-r{
   border-left:#f94144 solid 1rem;
   border-right:#f94144 solid 1rem;
}
.cadre-subtitle-g{
   border-left:#90be6d solid 1rem;
   border-right:#90be6d solid 1rem;
}
.cadre-subtitle-b{
   border-left:#577590 solid 1rem;
   border-right:#577590 solid 1rem;
}
.tr{
  color:#f94144;
  font-weight: bold;
}
.tg{
  color:#90be6d;
  font-weight: bold;
}
.tb{
  color:#577590;
  font-weight: bold;
}
</style>

<p class="h4 text-center">La spécialité NSI fournit un premier bagage informatique</p>
<p class="h3 text-center text-dark"><span class="tr">pratique</span>, <span class="tg">théorique</span> et <span class="tb">réflexif</span> </p>

<div class="col-3 mx-auto bg-dark text-center bag mt-3"><i style="font-size:6rem" class="fas fa-luggage-cart fa-inverse mt-4"></i></div>

<div class="border-dark pb-5 cadre col-10 mx-auto" style="border-top:solid 4px !important; border:2px solid; border-bottom:10px solid;">
  
  <div>
    
    <div class="h1 bg-dark text-center cadre-subtitle py-2 cadre-subtitle-r mb-4"># PRATIQUE</div>
    
      <div class="h5 text-center mb-3"><b><span class="tr"><</span> Développer des applications simples <span class="tr">></span></b></div>
      
      <div class="d-flex justify-content-evenly">
        <div class="zoomOnHoverNoShadow">
          <span class="fa-stack fa-lg tr" style="font-size:3rem">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fas fa-laptop-code fa-stack-1x fa-inverse"></i>
          </span><span class="d-block text-center text-dark">python</span>
        </div>
        <div class="zoomOnHoverNoShadow">
          <span class="fa-stack fa-lg tr" style="font-size:3rem">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fas fa-at fa-stack-1x fa-inverse"></i>
          </span><span class="d-block text-center text-dark">site Web</span>
        </div>
        <div class="zoomOnHoverNoShadow">
          <span class="fa-stack fa-lg tr" style="font-size:3rem">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fas fa-gamepad fa-stack-1x fa-inverse"></i>
          </span><span class="d-block text-center text-dark">jeux vidéo</span>
        </div>
      </div>
      
      <hr>
      
      <div class="h5 text-center my-3"><b><span class="tr"><</span> Créer et déployer des serveurs  <span class="tr">></span></b></div>
      
      <div class="d-flex justify-content-evenly">
        <div class="zoomOnHoverNoShadow">
          <span class="fa-stack fa-lg tr" style="font-size:3rem">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fab fa-linux fa-stack-1x fa-inverse"></i>
          </span><span class="d-block text-center text-dark">linux</span>
        </div>
        <div class="zoomOnHoverNoShadow">
          <span class="fa-stack fa-lg tr" style="font-size:3rem">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fas fa-network-wired fa-stack-1x fa-inverse"></i>
          </span><span class="d-block text-center text-dark">réseaux</span>
        </div>
        <div class="zoomOnHoverNoShadow">
          <span class="fa-stack fa-lg tr" style="font-size:3rem">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fas fa-shield-alt fa-stack-1x fa-inverse"></i>
          </span><span class="d-block text-center text-dark">sécurité</span>
        </div>
        <div class="zoomOnHoverNoShadow">
          <span class="fa-stack fa-lg tr" style="font-size:3rem">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fas fa-database fa-stack-1x fa-inverse"></i>
          </span><span class="d-block text-center text-dark">base de données</span>
        </div>
      </div>
    
    <style>
      .theo-bloc{
        border-top: 4px solid #90be6d !important;
        border-bottom-left-radius: 10px;
        border-bottom-right-radius: 10px;
      } 
    </style>
    
    <div class="h1 bg-dark text-center cadre-subtitle py-2 cadre-subtitle-g my-4"># THEORIQUE</div>
      
      <div class="col-11 mx-auto mb-4">
        <div class="text-center">
          <div><div class="h5 text-center my-1"><b><span class="tg"><</span> Les données <span class="tg">></span></b></div></div>
        </div>
        <div class="text-center zoomOnHoverNoShadow">
          <div class="fa-stack fa-lg tg" style="font-size:2.5rem">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fas fa-database fa-stack-1x fa-inverse"></i>
          </div>
          <div class="px-2 pb-2 col-11 theo-bloc border border-dark mx-auto bg-white pt-4" style="margin-top:-20px">
            <div class="text-center">
              <p class="mb-0">Représentations informatiques de données de haut niveau : textes, image, sons, mesures physiques, ...</p>
              <hr>
              <p class="mb-0">Création et manipulation de données fondamentales de base ou structurées</p>
              <hr>
              <p class="mb-0">Conception, création et utilisation de bases de données</p>
            </div>
          </div>
        </div>
      </div>
      
      <div class="col-11 mx-auto mb-4">
        <div class="text-center">
          <div><div class="h5 text-center my-1"><b><span class="tg"><</span> Les algorithmes <span class="tg">></span></b></div></div>
        </div>
        <div class="text-center zoomOnHoverNoShadow">
          <div class="fa-stack fa-lg tg" style="font-size:2.5rem">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fas fa-cogs fa-stack-1x fa-inverse"></i>
          </div>
          <div class="px-2 pb-2 col-11 theo-bloc border border-dark mx-auto bg-white pt-4" style="margin-top:-20px">
            <div class="text-center">
              <p class="mb-0">Concepts algorithmiques fondamentaux</p>
              <hr>
              <p class="mb-0">Conception et rédaction d'algorithmes optimaux</p>
              <hr>
              <p class="mb-0">Étude d'algorithmes connus</p>
            </div>
          </div>
        </div>
      </div>
      
      <div class="col-11 mx-auto mb-4">
        <div class="text-center">
          <div><div class="h5 text-center my-1"><b><span class="tg"><</span> Les langages <span class="tg">></span></b></div></div>
        </div>
        <div class="text-center zoomOnHoverNoShadow">
          <div class="fa-stack fa-lg tg" style="font-size:2.5rem">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fas fa-file-alt fa-stack-1x fa-inverse"></i>
          </div>
          <div class="px-2 pb-2 col-11 theo-bloc border border-dark mx-auto bg-white pt-4" style="margin-top:-20px">
            <div class="text-center">
              <p class="mb-0">Concepts généraux sur la programmation en informatique</p>
              <hr>
              <p class="mb-0">Paradygmes de programmation : procédurale ou orientée objet</p>
              <hr>
              <p class="mb-0">Utilisation de bibliothèques de fonctions prêtes à l'emploi</p>
            </div>
          </div>
        </div>
      </div>
      
      <div class="col-11 mx-auto mb-4">
        <div class="text-center">
          <div><div class="h5 text-center my-1"><b><span class="tg"><</span> Les machines <span class="tg">></span></b></div></div>
        </div>
        <div class="text-center zoomOnHoverNoShadow">
          <div class="fa-stack fa-lg tg" style="font-size:2.5rem">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fas fa-robot fa-stack-1x fa-inverse"></i>
          </div>
          <div class="px-2 pb-2 col-11 theo-bloc border border-dark mx-auto bg-white pt-4" style="margin-top:-20px">
            <div class="text-center">
              <p class="mb-0">Concepts généraux sur les réseaux informatiques : architectures, modèles et protocoles</p>
              <hr>
              <p class="mb-0">Systèmes d'exploitations : généralités, processus et lignes de commandes </p>
              <hr>
              <p class="mb-0">Architecture matérielle informatique</p>
            </div>
          </div>
        </div>
      </div>
    
    <div class="h1 bg-dark text-center cadre-subtitle py-2 cadre-subtitle-b mt-4"># REFLEXIF</div>
    
    <div class="d-flex flex-wrap justify-content-evenly mt-4">

      <div class="bulleGenerator kevin mb-3 col-8 d-print-avoid-break zoomOnHoverNoShadow" style="break-inside: avoid;">
        <div class="rounded p-2 in d-flex justify-content-center align-items-center" style="border-color:#577590 !important">
          <div class="d-none d-lg-block d-print-block"><i style="color:#577590" class="fas fa-user-circle fa-3x"></i></div>
          <p class="mb-0 text-center">Quels sont les impacts écologiques de l'informatique et comment les limiter ?</p>
        </div>
      </div>
      
      <div class="bulleGenerator kevin mb-3 col-8 d-print-avoid-break zoomOnHoverNoShadow" style="break-inside: avoid;">
        <div class="rounded p-2 in d-flex justify-content-center align-items-center" style="border-color:#577590 !important">
          <p class="mb-0 text-center">Qu'est-ce que l'open source et sa philosophie ?</p>
          <div class="d-none d-lg-block d-print-block"><i style="color:#577590" class="fas fa-user-circle fa-3x"></i></div>
        </div>
      </div>
      
      <div class="bulleGenerator kevin mb-3 col-5 d-print-avoid-break zoomOnHoverNoShadow" style="break-inside: avoid;">
        <div class="rounded p-2 in d-flex justify-content-center align-items-center" style="border-color:#577590 !important">
          <p class="mb-0 text-center">Comment travailler et partager en équipe ?</p>
          <div class="d-none d-lg-block d-print-block"><i style="color:#577590" class="fas fa-user-circle fa-3x"></i></div>
        </div>
      </div>
      
      <div class="bulleGenerator kevin mb-3 col-5 d-print-avoid-break zoomOnHoverNoShadow" style="break-inside: avoid;">
        <div class="rounded p-2 in d-flex justify-content-center align-items-center" style="border-color:#577590 !important">
          <div class="d-none d-lg-block d-print-block"><i style="color:#577590" class="fas fa-user-circle fa-3x"></i></div>
          <p class="mb-0 text-center">Comment exposer et mettre en valeur mon travail ?</p>
        </div>
      </div>
      
      <div class="bulleGenerator kevin mb-3 col-8 d-print-avoid-break zoomOnHoverNoShadow" style="break-inside: avoid;">
        <div class="rounded p-2 in d-flex justify-content-center align-items-center" style="border-color:#577590 !important">
          <div class="d-none d-lg-block d-print-block"><i style="color:#577590" class="fas fa-user-circle fa-3x"></i></div>
          <p class="mb-0 text-center">Quels sont les modèles économiques des outils informatiques gratuits que nous utilisons au quotidien ?</p>
        </div>
      </div>
      
      <div class="bulleGenerator kevin mb-3 col-8 d-print-avoid-break zoomOnHoverNoShadow" style="break-inside: avoid;">
        <div class="rounded p-2 in d-flex justify-content-center align-items-center" style="border-color:#577590 !important">
          <p class="mb-0 text-center">Informatisation : quelles menaces pour nos vie privées et nos libertés ?</p>
          <div class="d-none d-lg-block d-print-block"><i style="color:#577590" class="fas fa-user-circle fa-3x"></i></div>
        </div>
      </div>
      
      <div class="bulleGenerator kevin col-8 d-print-avoid-break" style="break-inside: avoid;">
        <div class="rounded p-2 in d-flex justify-content-center align-items-center" style="border-color:#577590 !important">
          <b class="h4 mb-0 text-center">. . .</b>
        </div>
      </div>
    
    </div>
  </div>
  
</div>

<hr>
<p class="h3 text-center my-4">La NSI est une spécialité où la <b style="color:#cd5c5c">pratique</b> est importante</p>

+++pagebreak

<style>
  .donut {
  --donut-size: 260px;
  --donut-border-width: 45px;
  --donut-spacing: 0;
  --donut-spacing-color: 255, 255, 255;
  --donut-spacing-deg: calc(1deg * var(--donut-spacing));
  border-radius: 50%;
  height: var(--donut-size);
  margin: 40px;
  position: relative;
  width: var(--donut-size);
}

.donut__label {
  left: 50%;
  line-height: 1.5;
  position: absolute;
  text-align: center;
  top: 50%;
  transform: translate(-50%, -50%);
  width: 80%;
}

.donut__label__heading {
  font-size: 24px;
  font-weight: 600;
}

.donut__label__sub {
  color: #666666;
  font-size: 14px;
  letter-spacing: 0.05em;
}

.donut__slice {
  height: 100%;
  position: absolute;
  width: 100%;
}

.donut__slice::before,
.donut__slice::after {
  border: var(--donut-border-width) solid rgba(0, 0, 0, 0);
  border-radius: 50%;
  content: "";
  height: 100%;
  left: 0;
  position: absolute;
  top: 0;
  transform: rotate(45deg);
  width: 100%;
}

.donut__slice::before {
  border-width: calc(var(--donut-border-width) + 1px);
  box-shadow: 0 0 1px 0 rgba(var(--donut-spacing-color), calc(100 * var(--donut-spacing)));
}

.donut__slice__first {
  --first-start: 0;
}

.donut__slice__first::before {
  border-top-color: rgba(var(--donut-spacing-color), calc(100 * var(--donut-spacing)));
  transform: rotate(calc(360deg * var(--first-start) + 45deg));
}

.donut__slice__first::after {
  border-top-color:     rgba(50, 32, 200);
  border-right-color:   rgba(50, 32, 200, calc(100 * (var(--first) - .25)));
  border-bottom-color:  rgba(50, 32, 200, calc(100 * (var(--first) - .5)));
  border-left-color:    rgba(50, 32, 200, calc(100 * (var(--first) - .75)));
  transform: rotate(calc(360deg * var(--first-start) + 45deg + var(--donut-spacing-deg)));
}

.donut__slice__second {
  --second-start: calc(var(--first));
  --second-check: max(calc(var(--second-start) - .5), 0);
  clip-path: inset(0 calc(50% * (var(--second-check) / var(--second-check))) 0 0);
}

.donut__slice__second::before {
  border-top-color: rgba(var(--donut-spacing-color), calc(100 * var(--donut-spacing)));
  transform: rotate(calc(360deg * var(--second-start) + 45deg));
}

.donut__slice__second::after {
  border-top-color:   rgba(110, 180, 250);
  border-right-color: rgba(110, 180, 250, calc(100 * (var(--second) - .25)));
  border-bottom-color:rgba(110, 180, 250, calc(100 * (var(--second) - .5)));
  border-left-color:  rgba(110, 180, 250, calc(100 * (var(--second) - .75)));
  transform: rotate(calc(360deg * var(--second-start) + 45deg + var(--donut-spacing-deg)));
}


.donut__slice__first2 {
  --first-start: 0;
}

.donut__slice__first2::before {
  border-top-color: rgba(var(--donut-spacing-color), calc(100 * var(--donut-spacing)));
  transform: rotate(calc(360deg * var(--first-start) + 45deg));
}

.donut__slice__first2::after {
  border-top-color:     rgba(50, 200, 32);
  border-right-color:   rgba(50, 200, 32, calc(100 * (var(--first) - .25)));
  border-bottom-color:  rgba(50, 200, 32, calc(100 * (var(--first) - .5)));
  border-left-color:    rgba(50, 200, 32, calc(100 * (var(--first) - .75)));
  transform: rotate(calc(360deg * var(--first-start) + 45deg + var(--donut-spacing-deg)));
}

.donut__slice__second2 {
  --second-start: calc(var(--first));
  --second-check: max(calc(var(--second-start) - .5), 0);
  clip-path: inset(0 calc(50% * (var(--second-check) / var(--second-check))) 0 0);
}

.donut__slice__second2::before {
  border-top-color: rgba(var(--donut-spacing-color), calc(100 * var(--donut-spacing)));
  transform: rotate(calc(360deg * var(--second-start) + 45deg));
}

.donut__slice__second2::after {
  border-top-color:     rgba(180, 250, 110);
  border-right-color:   rgba(180, 250, 110, calc(100 * (var(--second) - .25)));
  border-bottom-color:  rgba(180, 250, 110, calc(100 * (var(--second) - .5)));
  border-left-color:    rgba(180, 250, 110, calc(100 * (var(--second) - .75)));
  transform: rotate(calc(360deg * var(--second-start) + 45deg + var(--donut-spacing-deg)));
}

</style>

  <div class="d-flex justify-content-around align-items-center">
    <div class="col-3 text-center" style="font-size:1.8rem"><b class="py-1 px-2 rounded-pill border-3" style="color:rgba(180, 250, 110); border:3px solid rgba(180, 250, 110);border-left-width: 15px !important;border-right-width: 15px !important;">1 / 4</b><br><br><b style="color:#cd5c5c">Projets</b><br><br></div>
    <div class="donut col-6 zoomOnHoverNoShadow" style="--first: .75; --second: .25">
      <div class="donut__slice donut__slice__first2">
      </div>
      <div class="donut__slice donut__slice__second2">
      </div>
      <div class="donut__label">
        <div class="donut__label__heading">1<sup>ère</sup></div>
        <div class="donut__label__sub"><b>4 heures par semaine</b></div>
      </div>
    </div>
    <div class="col-3 text-center" style="font-size:1.8rem"><b class="py-1 px-2 rounded-pill" style="color:rgba(50, 200, 32); border:3px solid rgba(50, 200, 32);border-left-width: 15px !important;border-right-width: 15px !important;">3 / 4</b><br><br>cours et <b style="color:#cd5c5c">activités pratiques</b></div>
  </div>
  
  <div class="d-flex justify-content-around align-items-center">
    <div class="col-3 text-center" style="font-size:1.8rem"><b class="py-1 px-2 rounded-pill" style="color:rgba(110, 180, 250); border:3px solid rgba(110, 180, 250);border-left-width: 15px !important;border-right-width: 15px !important;">1 / 3</b><br><br><b style="color:#cd5c5c">Projets</b><br><br></div>
    <div class="donut zoomOnHoverNoShadow" style="--first: .67; --second: .33">
      <div class="donut__slice donut__slice__first">
      </div>
      <div class="donut__slice donut__slice__second">
      </div>
      <div class="donut__label">
        <div class="donut__label__heading">Terminale</div>
        <div class="donut__label__sub"><b>6 heures par semaine</b></div>
      </div>
    </div>
    <div class="col-3 text-center" style="font-size:1.8rem"><b class="py-1 px-2 rounded-pill" style="color:rgba(50, 32, 200); border:3px solid rgba(50, 32, 200);border-left-width: 15px !important;border-right-width: 15px !important;">2 / 3</b><br><br>cours et <b style="color:#cd5c5c">activités pratiques</b></div>
  </div>


<div></div>

<hr>
<p class="h3 text-center my-4">Quelques exemples de parcours supérieurs en fonction des spécialités choisies avec la NSI</p>

+++image "/images/md/73429FCC6D8F78CCBC729FFG7F748H"


<div class="mt-4 boddrder mx-auto col-10">
  <div>
    <div class="d-flex bd-highlight col-10 mx-auto">
      <div class="h4 p-2 w-100 text-center align-self-center">Le choix de la NSI ouvre une voie royale vers les hautes études et écoles d'ingénieurs</div>
      <div class="p-2 flex-shrink-1"><i class="fas fa-search fa-6x "></i></div>
    </div>
    <hr>
    <div class="d-flex bd-highlight col-10 mx-auto">
      <div class="p-2 flex-shrink-1"><i class="fas fa-search fa-6x fa-flip-horizontal"></i></div>
      <div class="h4 p-2 w-100 text-center align-self-center">La combinaison des sciences informatiques avec les domaines des sciences humaines et/ou littéraires ne doit pas être sous-estimée</div>
    </div>
  </div>
</div>

<hr>
<p class="h3 text-center my-4">Quelques exemples de métiers dans le secteur informatique</p>

<div class="d-flex justify-content-around flex-wrap">

  
  <a href="/enseignant/didactique/modeles/projet/roles/developpeur-_frontend.md" class="text-decoration-none card col-3 border border-2 border-dark p-1 pt-5 mx-2 mb-3" style="border-top-right-radius: 50rem !important;border-top-left-radius: 50rem !important;">
    <i class="text-center fa fa-object-group fa-10x" style="color:#5f5fff"></i>
    <hr>
    <div class="card-body pt-0 text-center text-black d-flex flex-wrap">
      <h5 class="card-title col-12"><b>Développeur frontend</b></h5>
      <hr class="col-12">
      <p class="card-text text-black col-12 mb-0">Spécialiste des interfaces graphiques<br><br></p>
    </div>
    <div class="card-footer text-center">
      bac+2 ~ bac+5
    </div>
  </a>
  
  <a href="/enseignant/didactique/modeles/projet/roles/developpeur-_backend.md" class="text-decoration-none card col-3 border border-2 border-dark p-1 pt-5 mx-2 mb-3" style="border-top-right-radius: 50rem !important;border-top-left-radius: 50rem !important;">
    <i class="text-center fas fa-cogs fa-10x" style="color:#5f5fff"></i>
    <hr>
    <div class="card-body pt-0 text-center text-black d-flex flex-wrap">
      <h5 class="card-title col-12"><b>Développeur backend</b></h5>
      <hr class="col-12">
      <p class="card-text text-black col-12 mb-0">Spécialiste du traitement et du transfert des données</p>
    </div>
    <div class="card-footer text-center">
      bac+3 ~ bac+5
    </div>
  </a>
  
  <a href="/enseignant/didactique/modeles/projet/roles/developpeur-_securite.md" class="text-decoration-none card col-3 border border-2 border-dark p-1 pt-5 mx-2 mb-3" style="border-top-right-radius: 50rem !important;border-top-left-radius: 50rem !important;">
    <i class="text-center fas fa-shield-alt fa-10x" style="color:#5f5fff"></i>
    <hr>
    <div class="card-body pt-0 text-center text-black d-flex flex-wrap">
      <h5 class="card-title col-12"><b>Expert en sécurité</b></h5>
      <hr class="col-12">
      <p class="card-text text-black col-12 mb-0">Spécialiste de la sécurité Informatique<br><br></p>
    </div>
    <div class="card-footer text-center">
      bac+3 ~ bac+5
    </div>
  </a>
  
  <a href="" class="text-decoration-none card col-3 border border-2 border-dark p-1 pt-5 mx-2 mb-3" style="border-top-right-radius: 50rem !important;border-top-left-radius: 50rem !important;">
    <i class="text-center fa fa-tachometer-alt fa-10x" style="color:#5f5fff"></i>
    <hr>
    <div class="card-body pt-0 text-center text-black d-flex flex-wrap">
      <h5 class="card-title col-12"><b>Développeur Devops</b><br><br></h5>
      <hr class="col-12">
      <p class="card-text text-black col-12 mb-0">Conçoit et déploie des outils pour faciliter le travail de son équipe<br><br></p>
    </div>
    <div class="card-footer text-center">
      ~ bac+5
    </div>
  </a>
  
  <a href="" class="text-decoration-none card col-3 border border-2 border-dark p-1 pt-5 mx-2 mb-3" style="border-top-right-radius: 50rem !important;border-top-left-radius: 50rem !important;">
    <i class="text-center fa fa-search-dollar fa-10x" style="color:#5f5fff"></i>
    <hr>
    <div class="card-body pt-0 text-center text-black d-flex flex-wrap">
      <h5 class="card-title col-12"><b>Data analyst<br><br></b></h5>
      <hr class="col-12">
      <p class="card-text text-black col-12 mb-0">Analyse, exploite et interprète les données de l'entreprise<br><br></p>
    </div>
    <div class="card-footer text-center">
      bac+3 ~ bac+5
    </div>
  </a>
  
  <a href="" class="text-decoration-none card col-3 border border-2 border-dark p-1 pt-5 mx-2 mb-3" style="border-top-right-radius: 50rem !important;border-top-left-radius: 50rem !important;">
    <i class="text-center fas fa-database fa-10x" style="color:#5f5fff"></i>
    <hr>
    <div class="card-body pt-0 text-center text-black d-flex flex-wrap">
      <h5 class="card-title col-12"><b>Administrateur de base de données</b></h5>
      <hr class="col-12">
      <p class="card-text text-black col-12 mb-0">Garant du stockage et de la protection des données<br><br></p>
    </div>
    <div class="card-footer text-center">
      bac+3 ~ bac+5
    </div>
  </a>
  
  <a href="" class="text-decoration-none card col-3 border border-2 border-dark p-1 pt-5 mx-2 mb-3" style="border-top-right-radius: 50rem !important;border-top-left-radius: 50rem !important;">
    <i class="text-center fa fa-network-wired fa-10x" style="color:#5f5fff"></i>
    <hr>
    <div class="card-body pt-0 text-center text-black d-flex flex-wrap">
      <h5 class="card-title col-12"><b>Architecte réseau</b></h5>
      <hr class="col-12">
      <p class="card-text text-black col-12 mb-0">Expert en réseaux et télécommunications<br><br></p>
    </div>
    <div class="card-footer text-center">
      bac+5
    </div>
  </a>
  
  <a href="" class="text-decoration-none card col-3 border border-2 border-dark p-1 pt-5 mx-2 mb-3" style="border-top-right-radius: 50rem !important;border-top-left-radius: 50rem !important;">
    <i class="text-center fas fa-bug fa-10x" style="color:#5f5fff"></i>
    <hr>
    <div class="card-body pt-0 text-center text-black d-flex flex-wrap">
      <h5 class="card-title col-12"><b>Expert assurance qualité</b></h5>
      <hr class="col-12">
      <p class="card-text text-black col-12 mb-0">Recherche les bugs et failles des applications<br><br></p>
    </div>
    <div class="card-footer text-center">
      bac+3 ~ bac+5
    </div>
  </a>

</div>

<hr class="my-4">

<p class="h3 text-center my-4">Quelques conseils pour préparer son arrivée en NSI</p>

<div class="mt-4 boddrder mx-auto col-10">
  <div>
    <div class="d-flex bd-highlight col-10 mx-auto">
      <div class="p-2 flex-shrink-1"><i class="tr fab fa-python fa-6x"></i></div>
      <div class="h4 p-2 w-100 text-center align-self-center">S'entraîner à programmer en python en s'aidant des nombreux guides sur Internet</div>
    </div>
    <hr>
    <div class="d-flex bd-highlight col-10 mx-auto">
      <div class="h4 p-2 w-100 text-center align-self-center">Créer sa première page Web avec les langages HTML et CSS</div>
      <div class="p-2 flex-shrink-1"><i class="tg fas fa-globe fa-6x "></i></div>
    </div>
    <hr>
    <div class="d-flex bd-highlight col-10 mx-auto">
      <div class="p-2 flex-shrink-1"><i class="tb fas fa-box-open fa-6x"></i></div>
      <div class="h4 p-2 w-100 text-center align-self-center">Développer des mini-projets informatiques personnels</div>
    </div>
  </div>
</div>

<hr class="my-4 mb-5">

+++bulle matthieu
  plaquette faîte par mes soins ! vous avez une question ? voici mon adresse email : <a href="mailto:spacecodeur@gmail.com">spacecodeur@gmail.com</a>
+++

