# Algèbre booléenne et tables de vérité

George Boole, dans ses travaux, formalisa un ensemble d'opérations logiques binaires. Cet ensemble d'opérations (avec leur résultat) est ce qu'on appelle l'algèbre de Boole.

Le but de cette activité est de découvrir cette algèbre qui fait partie des fondations de tout les appareils électriques dans le monde. 

Les puce électronique ont différents types, nous nous concentrons au lycée sur les types suivants :
- les puce électronique de type 'not'
- les puce électronique de type 'and'
- les puce électronique de type 'or'
- les puce électronique de type 'xor'

Il existe de nombreux logiciels et sites Internet proposant de simuler le comportement des puce électronique. Nous utiliserons le site 'logic.ly' qui propose d'essayer gratuitement l'outil qu'il propose depuis un navigateur Internet. Ouvrez la page [https://logic.ly/demo/](https://logic.ly/demo).

Dans cette page, vous pouvez glisser des éléments mis à disposition à gauche pour les glisser à droite. Voici les éléments que vous utiliserez dans cette activité : 

![](https://i.ibb.co/gJ1NN7F/Screenshot-from-2021-09-25-11-17-17.png)

Étudions ensemble le comportement d'une puce électronique de type "not". Ce type de puce électronique n'a qu'une seule entrée : 
- Lorsqu'aucun courant électrique n'est envoyé sur l'entrée, la sortie propage un courant électrique
- Lorsqu'un courant électrique est envoyé sur l'entrée, la sortie ne propage aucun courant

Nous pouvons simuler ce comportement à l'aide de l'outil fournis par le site 'logic.ly' en utilisant les éléments mis à disposition comme ceci : 

![](https://i.ibb.co/p4KPymg/Screenshot-from-2021-09-25-11-31-27.png)

Lorsque la porte 'not' (qui symbolise une puce électronique de type "not") ne reçoit pas de courant, la sortie propage bien un courant électrique et donc l'ampoule s'allume. 
Et lorsque vous activez l'interrupteur, comme prévu la lampe s'éteint : 

![](https://i.ibb.co/p0hpZ1h/Screenshot-from-2021-09-25-11-34-47.png)

À partir de là, nous pouvons dresser la première table de vérité de l'algèbre de Boole, la table de vérité 'not' : 

<table class="table table-fit">
  <thead>
    <tr class="text-center">
      <th colspan="2">Table de vérité 'not'</th>
    </tr>
    <tr>
      <th>Entrée A</th>
      <th>Sortie</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <td>1</td>
      <td>0</td>
    </tr>
  </tbody>
</table>

+++bulle matthieu
  0 signifie qu'il n'y a pas de courant. Par convention en programmation, 0 signifie <code>false</code> (attention, en python c'est <code><b>F</b>alse</code>)
+++

+++bulle matthieu droite
  Et inversement, 1 signifie qu'il y a du courant. Et par convention en programmation, 1 signifie <code>true</code> (attention toujours, en python c'est <code><b>T</b>rue</code>)
+++
<br>
Pour terminer, comme cette algèbre fait partie des fondements même de l'ordinateur que vous avez sous les mains. Cette logique est aussi utilisable dans les langages de programmation utilisés par l'ordinateur. Ainsi, vous pouvez tester à travers un script python le comportement de la porte logique 'not' : 

widget python
entreeA = False # modifier la valeur avec False ou True

sortie = not entreeA

print(sortie)
widget

## À vous de jouer ! 

Dans la suite de l'activité, vous allez établir vous même les tables de vérité en vous aidant du simulateur soumis par le site 'logic.ly'. Puis, vous créerez le code python correspondant (ce qui vous entraînera un peu sur python ;) )

+++question write "Dans l'outil 'logic.ly', utilisez la porte logique 'or' avec en entrée deux interrupteurs et une ampoule en sortie"
- Dessinez grossièrement le schéma réalisé 
- Après avoir testé le comportement de la porte logique 'or', recopiez et complétez la table de vérité 'or'

<table class="table table-fit">
  <thead>
    <tr class="text-center">
      <th colspan="3">Table de vérité 'or'</th>
    </tr>
    <tr>
      <th>Entrée A</th>
      <th>Entrée B</th>
      <th>Sortie</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>0</td>
      <td></td>
    </tr>
    <tr>
      <td>0</td>
      <td>1</td>
      <td></td>
    </tr>
    <tr>
      <td>1</td>
      <td>0</td>
      <td></td>
    </tr>
    <tr>
      <td>1</td>
      <td>1</td>
      <td></td>
    </tr>
  </tbody>
</table>

- Programmez en python le comportement d'une puce électronique 'or' en complétant le code ci-dessous

widget python
# déclarer la variable entreeA et lui affecter la valeur True ou False
# déclarer la variable entreeB et lui affecter la valeur True ou False

# déclarer la variable sortie et lui affecter le résultat de l'opération `entreeA ou entreeB`

# afficher la valeur de la variable sortie
widget

+++question write "Dans l'outil 'logic.ly', utilisez la porte logique 'and' avec en entrée deux interrupteurs et une ampoule en sortie"
- Dessinez grossièrement le schéma réalisé 
- Après avoir testé le comportement de la porte logique 'and', recopiez et complétez la table de vérité 'and'

<table class="table table-fit">
  <thead>
    <tr class="text-center">
      <th colspan="3">Table de vérité 'and'</th>
    </tr>
    <tr>
      <th>Entrée A</th>
      <th>Entrée B</th>
      <th>Sortie</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>0</td>
      <td></td>
    </tr>
    <tr>
      <td>0</td>
      <td>1</td>
      <td></td>
    </tr>
    <tr>
      <td>1</td>
      <td>0</td>
      <td></td>
    </tr>
    <tr>
      <td>1</td>
      <td>1</td>
      <td></td>
    </tr>
  </tbody>
</table>

- Programmez en python le comportement d'une puce électronique 'and'

widget python

widget

+++question write "Dans l'outil 'logic.ly', utilisez la porte logique 'xor' avec en entrée deux interrupteurs et une ampoule en sortie"
- Dessinez grossièrement le schéma réalisé 
- Après avoir testé le comportement de la porte logique "xor", recopiez et complétez la table de vérité 'xor'

<table class="table table-fit">
  <thead>
    <tr class="text-center">
      <th colspan="3">Table de vérité 'xor'</th>
    </tr>
    <tr>
      <th>Entrée A</th>
      <th>Entrée B</th>
      <th>Sortie</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>0</td>
      <td></td>
    </tr>
    <tr>
      <td>0</td>
      <td>1</td>
      <td></td>
    </tr>
    <tr>
      <td>1</td>
      <td>0</td>
      <td></td>
    </tr>
    <tr>
      <td>1</td>
      <td>1</td>
      <td></td>
    </tr>
  </tbody>
</table>

- Programmez en python le comportement d'une puce électronique 'xor'

widget python

widget

+++spoil "aide"
  L'instruction 'xor' telle quelle n'existe pas en python. Une recherche rapide sur Internet vous permettra de trouver la bonne instruction à utiliser pour effectuer une opération type 'xor' entre deux variables :)
spoil+++

## Réponses

<ol start="0">
    <li>
        +++spoil
<div class="text-center"><img src="https://i.ibb.co/fn55r5f/Screenshot-from-2021-09-25-14-07-53.png" alt=""></div>
<br>
<table class="table table-fit">
  <thead>
    <tr class="text-center">
      <th colspan="3">Table de vérité 'or'</th>
    </tr>
    <tr>
      <th>Entrée A</th>
      <th>Entrée B</th>
      <th>Sortie</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>0</td>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr>
      <td>1</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <td>1</td>
      <td>1</td>
      <td>1</td>
    </tr>
  </tbody>
</table>
<br>
widget python
# déclarer la variable entreeA et lui affecter la valeur True ou False
entreeA = True
# déclarer la variable entreeB et lui affecter la valeur True ou False
entreeB = False

# déclarer la variable sortie et lui affecter le résultat de l'opération `entreeA ou entreeB`
sortie = entreeA or entreeB

# afficher la valeur de la variable sortie
print(str(entreeA) + " or " + str(entreeB) + " donne " + str(sortie))
widget

        spoil+++
    </li>
    <li>
        +++spoil
<div class="text-center"><img src="https://i.ibb.co/3vqfyGp/Screenshot-from-2021-09-25-14-11-44.png" alt=""></div>
<br>
<table class="table table-fit">
  <thead>
    <tr class="text-center">
      <th colspan="3">Table de vérité 'and'</th>
    </tr>
    <tr>
      <th>Entrée A</th>
      <th>Entrée B</th>
      <th>Sortie</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>0</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>1</td>
      <td>1</td>
      <td>1</td>
    </tr>
  </tbody>
</table>
<br>
widget python
# déclarer la variable entreeA et lui affecter la valeur True ou False
entreeA = True
# déclarer la variable entreeB et lui affecter la valeur True ou False
entreeB = False

# déclarer la variable sortie et lui affecter le résultat de l'opération `entreeA ou entreeB`
sortie = entreeA and entreeB

# afficher la valeur de la variable sortie
print(str(entreeA) + " and " + str(entreeB) + " donne " + str(sortie))
widget

        spoil+++
    </li>
    <li>
        +++spoil
<div class="text-center"><img src="https://i.ibb.co/PF6G4zy/Screenshot-from-2021-09-25-14-13-12.png" alt=""></div>
<br>
<table class="table table-fit">
  <thead>
    <tr class="text-center">
      <th colspan="3">Table de vérité 'xor'</th>
    </tr>
    <tr>
      <th>Entrée A</th>
      <th>Entrée B</th>
      <th>Sortie</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>0</td>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr>
      <td>1</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <td>1</td>
      <td>1</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
<br>
widget python
# déclarer la variable entreeA et lui affecter la valeur True ou False
entreeA = True
# déclarer la variable entreeB et lui affecter la valeur True ou False
entreeB = False

# déclarer la variable sortie et lui affecter le résultat de l'opération `entreeA ou entreeB`
sortie = entreeA ^ entreeB

# afficher la valeur de la variable sortie
print(str(entreeA) + " xor " + str(entreeB) + " donne " + str(sortie))
widget

        spoil+++
    </li>
</ol>
