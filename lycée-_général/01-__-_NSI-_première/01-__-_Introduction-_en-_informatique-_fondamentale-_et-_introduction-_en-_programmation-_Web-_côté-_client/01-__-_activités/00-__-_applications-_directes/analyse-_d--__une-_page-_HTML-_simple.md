# Analyse d'une page HTML simple

[Dans ce lien](/pageSimple/simple.html) se trouve une page Web simple qui ne contient que du code **HTML**.

Lorsque vous cliquez sur ce lien, votre navigateur **crée une requête** qui demande l'accès à la ressource **'/pageSimple/simple.html'** au serveur qui héberge le site **edusophie**. 

La requête est envoyée à travers Internet jusqu'à l'ordinateur serveur qui héberge le site **edusophie**.

Le serveur génère une réponse qui contient le code de la page demandée, voici ci-dessous le contenu de la ressource **'/pageSimple/simple.html'** du site **edusophie**: 

![](https://i.ibb.co/G9LQ9j4/Screenshot-from-2021-09-26-12-22-35.png)

+++bulle matthieu
contrairement au langage python, l'indentation en langage <b>HTML</b> est libre. Cependant, garder une bonne indentation permet de rendre le code plus facilement lisible !
+++

Le serveur envoie la réponse à votre ordinateur client. Votre navigateur lit, interprète le code **HTML** du contenu de la ressource **'/pageSimple/simple.html'**et affiche sur votre écran la page "Page web (très) simple".

+++question write "Les blocs ci-dessous représentent dans le désordre les étapes effectuées, entre le client et le serveur, lors du clic sur le lien <a href="/pageSimple/simple.html" target="_blank">https://www.edusophie.com/pageSimple/simple.html</a>. Dans quel ordre doivent se dérouler ces étapes ?"

<table class="table">
  <tbody>
    <tr>
      <td>L'ordinateur serveur vérifie que la ressource '/pageSimple/simple.html' existe bien</td>
      <td>a</td>
    </tr>
    <tr>
      <td>Le navigateur génère une requête pour obtenir la ressource '/pageSimple/simple.html' du site edusophie</td>
      <td>b</td>
    </tr>
    <tr>
      <td>L'ordinateur client reçoit la réponse du serveur qui est transmise au navigateur Internet</td>
      <td>c</td>
    </tr>
    <tr>
      <td>L'ordinateur serveur génère une réponse qui contient le contenu de la ressource '/pageSimple/simple.html'</td>
      <td>d</td>
    </tr>
    <tr>
      <td>Depuis mon navigateur, je clique sur le lien <a href="/pageSimple/simple.html" target="_blank">https://www.edusophie.com/pageSimple/simple.html</a></td>
      <td>e</td>
    </tr>
    <tr>
      <td>L'ordinateur serveur reçoit la requête émise par l'ordinateur client</td>
      <td>f</td>
    </tr>
    <tr>
      <td>Le navigateur envoie la requête qu'il a généré au serveur qui héberge le site edusophie</td>
      <td>g</td>
    </tr>
    <tr>
      <td>Le navigateur Internet lit et interprète la réponse du serveur. Puis il affiche sur mon écran le résultat</td>
      <td>h</td>
    </tr>
    <tr>
      <td>L'ordinateur serveur envoie la réponse de la requête à l'ordinateur client</td>
      <td>i</td>
    </tr>
  </tbody>
</table>

- étape 0 : e
- étape 1 : ?
- ...

## À la découverte de l'inspecteur 

La plupart des navigateurs proposent un ensemble d'outil pour analyser et aider au développement de page Web :

- Ouvrez la page <a href="/pageSimple/simple.html" target="_blank">https://www.edusophie.com/pageSimple/simple.html</a> 
- Une fois la page chargée, faîtes un clic droit sur l'image animée puis sélectionnez "Examiner l'élément" (ou "Inspecter" selon le navigateur)

![](https://i.ibb.co/4sXzyKR/Capture-d-ecran-de-2021-09-26-10-48-01.png)

Le panneau d'inspection va s'ouvrir et surligner directement l'élément **HTML** (la balise img) qui génère l'image animée que vous voyez à l'écran.

Tout le code qui figure dans le panneau d'inspection est le code de la ressource 'pageSimple/simple.html' qui, pour rappel, est celui-ci :

![](https://i.ibb.co/G9LQ9j4/Screenshot-from-2021-09-26-12-22-35.png)

+++question write "Dans la balise <code>img</code> quel est le nom de l'attribut qui sert à donner le lien vers l'image que l'on souhaite afficher ?"

+++bulle matthieu
  l'outil inspecteur reste très similaire d'un navigateur à l'autre. Si vous savez l'utiliser sur un navigateur, vous saurez l'utiliser sur tous les navigateurs
+++
<br>

Vous remarquerez qu'en survolant avec la souris le code de la ressource 'pageSimple/simple.html' dans le panneau d'inspection, l'élément **HTML** survolé est mis en surbrillance sur la page Web. Par exemple, si dans l'inspecteur je survole avec la souris l'élément **HTML** `h1` , alors le titre de la page dans la page Web est mis en surbrillance : 

![](https://i.ibb.co/NtBBbLZ/C11-04-43.png)

+++bulle matthieu
  il s'agit d'une fonctionnalité  très pratique ! surtout quand on débute en programmation Web
+++

+++question write "En vous aidant de l'inspecteur, listez les balises utilisées dans la balise <code>body</code> et précisez lorsqu'il s'agit d'une balise orpheline. Pour chaque balise, expliquez en quelques mots à quoi elle sert (vous pouvez rechercher sur Internet le nom de la balise suivi de 'HTML' pour vous aider)"

<table class="table">
  <thead>
    <tr>
      <th class="text-center" colspan="2">Balises contenues dans la balise <code>body</code></th>
    </tr>
    <tr>
      <th>Nom de la balise</th>
      <th>À quoi sert cette balise ?</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>h1</td>
      <td>permet d'afficher du texte en grand dans la page</td>
    </tr>
    <tr>
      <td>img</td>
      <td>...</td>
    </tr>
    <tr>
      <td>...</td>
      <td>...</td>
    </tr>
  </tbody>
</table>

+++spoil "Aide"
  Il y a 7 balises différentes dans la balise <code>body</code> !
spoil+++

<div class="d-none">
## Modifier le contenu de la page

### À travers le panneau d'inspection du code



### En créant votre propre fichier HTML
</div>

## Résultats

<ol start="0">
  <li>

  +++spoil
  
  <ul>
    <li>étape 0 : e</li>
    <li>étape 1 : b</li>
    <li>étape 2 : g</li>
    <li>étape 3 : f</li>
    <li>étape 4 : a</li>
    <li>étape 5 : d</li>
    <li>étape 6 : i</li>
    <li>étape 7 : c</li>
    <li>étape 8 : h</li>
  </ul><br>
  
  <table class="table">
  <tbody>
    <tr>
      <td>Depuis mon navigateur, je clique sur le lien <a href="https://www.edusophie.com/pageSimple/simple.html" target="_blank">https://www.edusophie.com/pageSimple/simple.html</a></td>
      <td>e</td>
      <td>0</td>
    </tr>
    <tr>
      <td>Le navigateur génère une requête pour obtenir la ressource '/pageSimple/simple.html' du site edusophie</td>
      <td>b</td>
      <td>1</td>
    </tr>
    <tr>
      <td>Le navigateur envoie la requête qu'il a généré au serveur qui héberge le site edusophie</td>
      <td>g</td>
      <td>2</td>
    </tr>
    <tr>
      <td>L'ordinateur serveur reçoit la requête émise par l'ordinateur client</td>
      <td>f</td>
      <td>3</td>
    </tr>
    <tr>
      <td>L'ordinateur serveur vérifie que la ressource '/pageSimple/simple.html' existe bien</td>
      <td>a</td>
      <td>4</td>
    </tr>
    <tr>
      <td>L'ordinateur serveur génère une réponse qui contient le contenu de la ressource '/pageSimple/simple.html'</td>
      <td>d</td>
      <td>5</td>
    </tr>
    <tr>
      <td>L'ordinateur serveur envoie la réponse de la requête à l'ordinateur client</td>
      <td>i</td>
      <td>6</td>
    </tr>
    <tr>
      <td>L'ordinateur client reçoit la réponse du serveur qui est transmise au navigateur Internet</td>
      <td>c</td>
      <td>7</td>
    </tr>
    <tr>
      <td>Le navigateur Internet lit et interprète la réponse du serveur. Puis il affiche sur mon écran le résultat</td>
      <td>h</td>
      <td>8</td>
    </tr>
  </tbody>
</table>
  
  spoil+++
  </li>
  <li>
  +++spoil
    Voici le contenu de la balise image : 
    <div class="text-center"><img src="https://i.ibb.co/Httvcxd/Screenshot-from-2021-09-26-11-57-12.png"></div><br>
    Le nom de l'attribut qui permet de renseigner le lien dans lequel se trouve l'image est <b style="color:#4e9a12">src</b> et a comme valeur "<span style="color:#ad7fa8">https://i.ibb.co/zX3D569/tenor.gif</span>"
  spoil+++
  </li>
  <li>
  +++spoil
  
<table class="table">
  <thead>
    <tr>
      <th class="text-center" colspan="2">Balises contenues dans la balise <code>body</code></th>
    </tr>
    <tr>
      <th>Nom de la balise</th>
      <th>À quoi sert cette balise ?</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>h1</td>
      <td>permet d'afficher du texte en grand dans la page</td>
    </tr>
    <tr>
      <td>img</td>
      <td>balise orpheline qui permet d'afficher une image animée (ou non) dans la page</td>
    </tr>
    <tr>
      <td>p</td>
      <td>permet d'afficher un texte simple dans la page</td>
    </tr>
    <tr>
      <td>b</td>
      <td>permet d'afficher un texte en gras</td>
    </tr>
    <tr>
      <td>ul</td>
      <td>balise principale qui permet d'afficher une liste à puces</td>
    </tr>
    <tr>
      <td>li</td>
      <td>permet d'afficher un élément dans une liste à puces</td>
    </tr>
    <tr>
      <td>del</td>
      <td>permet de barrer du texte</td>
    </tr>
  </tbody>
</table>

  spoil+++
  </li>
</ol>
