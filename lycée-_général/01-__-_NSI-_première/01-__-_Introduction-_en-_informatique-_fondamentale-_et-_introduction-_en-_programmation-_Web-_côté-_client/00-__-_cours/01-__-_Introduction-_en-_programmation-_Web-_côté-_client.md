# Introduction en programmation Web côté client

+++programme
Identifier les différents composants graphiques permettant d’interagir avec une application Web
Identifier les événements que les fonctions associées aux différents composants graphiques sont capables de traiter

Définir ce qu'est et comment fonctionne l'architecture client-serveur 
Définir ce qu'est un navigateur Internet
Identifier les éléments HTML couramment utilisés dans une page Web
+++

+++sommaire

## L'architecture client-serveur 

+++activité individuel 15 ../01-__-_activites/00-__-_applications-_directes/le-_Web---_-_rappels-_de-_seconde.md "Le Web, rappels de seconde"
    <ul>
        <li>Faire l'exercice mis en lien</li>
        <li>Si l'exercice est terminé en avance, dissolvez votre groupe. Chaque membre du groupe vient en aide auprès d'un autre binôme</li>
        <ul>
            <li><b class="text-red">Attention :</b> avant dissolution, chaque membre du groupe doit être capable de faire l'exercice seul</li>
            <li><b class="text-red">Attention :</b> respectez bien <b>la charte d'entraide</b></li>
        </ul>
    </ul>
+++

Dans ce cours, nous ne nous intéressons qu'au client, c'est à dire au navigateur Internet. 

Un navigateur Internet est, de base, ni plus ni moins qu'un lecteur de fichier contenant du texte.

+++diagramme col-6 mx-auto
  flowchart TB
    F[<img class="rounded" src="https://i.ibb.co/hcCTwzb/Screenshot-from-2021-09-26-08-08-49.png" width="50"><br>fichier contenant du texte]
    N[<img class="rounded" src="https://i.ibb.co/1RYy30C/browsers-1265309-640.png" width="50"><br>navigateur]
    E[<img class="rounded" src="https://i.ibb.co/tLHTVvr/Screenshot-from-2021-09-26-08-14-59.png" width="150">]
    
    F-->|est chargé par|N
    N-->|génère l'affichage|E
    
    style F fill:#9370DB;
    style N fill:#9370DB;
+++

<br>

+++bulle matthieu
  sur Internet, ce fichier textuel est généré par un ordinateur distant spécialisé : le serveur
+++

+++bulle matthieu droite
  votre ordinateur (qui est le client) reçoit le fichier textuel, votre navigateur lit le fichier et interprète son contenu sur votre écran
+++
<br>
Un Internet ne véhiculant que des fichiers textes brut serait assez triste ! Les navigateurs savent aussi, **et surtout**, interpréter dans un fichier textuel le code **HTML**, **CSS** et **Javascript**.

+++bulle matthieu
  nous nous concentrons dans un premier temps au langage <b>HTML</b>
+++

### Résumons +++emoji-writing_hand 

- Un client est un ordinateur qui émet une requête (exemple de requête : "je souhaite accéder au contenu de la page 'https://fr.wikipedia.org'"). En règle générale, la requête est émise depuis un navigateur Internet
- Un serveur est un ordinateur configuré pour attendre et traiter des requêtes émises par un ordinateur client
- En réponse à la requête du client, un serveur envoie un fichier textuel au client
- Un navigateur Internet est un logiciel qui, de base, sert à lire un fichier contenant du texte. Mais il sait également (et surtout) interpréter du code **HTML**, du code **CSS** et du code **Javascript**

## À la (re?)découverte du langage HTML

### Les balises basiques

En programmation Web, le langage **HTML** est le premier langage auquel on se confronte. Par abus de langage, le langage **HTML** est parfois qualifié de **langage de programmation**, or **HTML** ne permet pas de créer des variables, des boucles, des conditions, etc... (ça sera plutôt le rôle du Javascript !)

Le langage **HTML** permet seulement d'ajouter du contenu brut et de le structurer dans une page Web. Le langage **HTML** est un **langage de balisage**.

Le langage **HTML** mets à disposition un ensemble de balises qui sont de deux formes possibles : 

- les **balises paires** :
    - qui sont de la forme : `<nomdelabalise>contenu de la balide</nomdelabalise>`
    - un exemple : pour afficher le titre principale dans une page Web, nous utilisons la balise h1 comme ceci `<h1>Titre de ma page</h1>`

+++bulle matthieu
  <b> attention !</b> une balise paire peut contenir d'autres balises ! Par exemple : 
+++

<code class="text-center d-block"><h1>Titre de ma <b>page</b></h1></code>

- les **balises orphelines** : 
    - qui sont de la forme : `<nomdelabalise>`
    - un exemple : pour sauter une ligne dans une page Web, nous utilisons la balise br comme ceci `<br>`

+++bulle matthieu
  on dit qu'une balise orpheline n'utilise pas de balise fermante
+++
<br>

Pour terminer, les balises peuvent être personnalisées en y ajoutant des **attributs**. Par exemple, pour créer un lien, il faut utiliser la **balise paire** `a` :
- `<a>Lien vers un site d'échange de blocs de code HTML, CSS et Javascript tout fait !</a>`

Or, nous devons bien sur préciser l'url sur laquelle se trouve le site Internet, il faut pour cela ajouter l'attribut `href` à notre balise :

- `<a href="https://codepen.io/search/pens?q=html+css">Lien vers un site d'échange de blocs de code HTML, CSS et Javascript tout fait !</a>`
- ce qui donne : <a href="https://codepen.io/search/pens?q=html+css">Lien vers mon site préféré</a>

De plus, je souhaite que lorsque je clique sur le lien, le site s'ouvre sur un nouvel onglet. Dans ce cas, il suffit d'utiliser l'attribut `target` avec la bonne valeur : 

- `<a href="https://codepen.io/search/pens?q=html+css" target="_blank">Lien vers un site d'échange de blocs de code HTML, CSS et Javascript tout fait !</a>`
- ce qui donne : <a href="https://codepen.io/search/pens?q=html+css" target="_blank">Lien vers mon site préféré</a>

Dans l'activité qui suit, vous allez analyser le code **HTML** d'une page Web. La page Web présentée dans l'activité est volontairement épurées pour que vous puissiez vous concentrer sur l'essentiel.

+++bulle matthieu
  les sites Internet que nous consultons dans notre quotidien ont un contenu en grande partie généré par des programmes
+++

+++bulle matthieu droite
  Les développeurs de site Internet n'écrivent pas tout le code du site. Certaines parties sont redondantes d'un site à l'autre, et donc, ces parties sont générées par des programmes pour créer plus rapidement le site
+++
<br>

+++activité binome 35 ../01-__-_activites/00-__-_applications-_directes/analyse-_d--__une-_page-_HTML-_simple.md "Analyse d'une page HTML simple"
    <ul>
        <li>Faire l'exercice mis en lien</li>
        <li>Si l'exercice est terminé en avance, dissolvez votre groupe. Chaque membre du groupe vient en aide auprès d'un autre binôme</li>
        <ul>
            <li><b class="text-red">Attention :</b> avant dissolution, chaque membre du groupe doit être capable de faire l'exercice seul</li>
            <li><b class="text-red">Attention :</b> respectez bien <b>la charte d'entraide</b></li>
        </ul>
    </ul>
+++

#### Résumons +++emoji-writing_hand 

- Dans une page web, le code HTML permet d'ajouter du contenu brute et de le structurer
- Le langage HTML met à disposition deux types de balises :
    - les balises paires : qui sont composées d'une balise ouvrante et d'une balise fermante 
    - les balises orphelines : qui n'ont pas besoin de balise fermante
- Les principales balises à connaître sont : 
    - h1 : qui permet d'afficher le titre principal de la page Web
    - p : qui permet d'écrire un **p**aragraphe de texte dans la page Web
    - img : (balise orpheline) qui permet d'insérer une image (animée ou non) dans la page Web
    - a : qui permet de créer un lien vers une autre page

### La balise interactive input

Le langage HTML met à disposition bon nombre de balises permettant d'ajouter du contenu multimédia (texte, image, vidéo, ...) dans une page Web. Il existe une balise qui permet à l'utilisateur d'interagir avec la page Web, il s'agit de la balise orpheline `<input>`. 

Cette balise peut avoir des comportements très variés selon comment est renseigné dedans l'attribut `type`. Par défaut, je nous ne renseignons pas l'attribut 'type' dans la balise `<input>`, un champ de saisie textuel est crée : 

Dans une page Web donc, si j'ajoute dans le code la balise `<input>` sans renseigner l'attribut 'type', j'obtiens ceci : <input>
<br><br>

+++activité binome 35 ../01-__-_activites/00-__-_applications-_directes/analyse-_de-_la-_balise-_interactive-_input.md "Analyse de la balise interactive input"
    <ul>
        <li>Faire l'exercice mis en lien</li>
        <li>Si l'exercice est terminé en avance, dissolvez votre groupe. Chaque membre du groupe vient en aide auprès d'un autre binôme</li>
        <ul>
            <li><b class="text-red">Attention :</b> avant dissolution, chaque membre du groupe doit être capable de faire l'exercice seul</li>
            <li><b class="text-red">Attention :</b> respectez bien <b>la charte d'entraide</b></li>
        </ul>
    </ul>
+++

#### Résumons +++emoji-writing_hand 

- La balise `<input>` permet d'ajouter un élément interactif dans une page Web. Le comportement de cette balise dépend de la valeur utilisée dans l'attribut 'type' : 
    - `<input type="password">` : permet à l'utilisateur de saisir un mot de passe 'cachée' sur la page Web
    - `<input type="checkbox">` : permet à l'utilisateur de checker des options parmi une liste de checkbox
    - `<input type="radio">` : permet à l'utilisateur de sélectionner une option parmi un ensemble d'options
    - `<input type="submit">` : permet à l'utilisateur de valider un formulaire en cliquant sur un bouton

+++bulle matthieu
plus tard dans l'année, nous apprendrons à envoyer les données du client (le navigateur Web) vers le serveur (l'ordinateur qui héberge le site Web) en utilisant des balises <code>input<code>
+++

## Aller plus loin ! 

Nous sortons ici du programme prévu en première, donc assurez vous d'être suffisamment à l'aise avec le contenu du cours avant de vous aventurer par ici ;) 

Dans le programme du lycée, nous devons aborder les langages HTML et Javascript, mais pas le CSS ! 

À quoi sert le langage CSS ? à décorer une page Web. Imaginez un site Web comme si c'était une maison : 
- Le code HTML permet d'agencer les pièces et de placer les meubles 
- Le code CSS, lui, permet de peindre les meubles et les murs d'une certaine couleur

Pour vous donner un exemple, voici à quoi ressemble la page d'accueil du site de la mairie :

<img class="col-8 offset-2" src="https://i.ibb.co/MGyrn13/Screenshot-from-2021-09-30-16-38-29.png">

Et voici à quoi ressemble le site sans CSS : 

<img class="col-8 offset-2" src="https://i.ibb.co/F3pZVK6/Screenshot-from-2021-09-30-16-40-29.png">

Si vous souhaitez apprendre à décorer vos page Web (qui sont assez mornes avec seulement de l'HTML..), les vidéos explicatives sur youtube ne manquent pas. Je peux vous conseiller celle-ci pour "démarrer" par exemple : [https://www.youtube.com/watch?v=qTN9bNaBCK8](https://www.youtube.com/watch?v=qTN9bNaBCK8)


</code></code></nomdelabalise>