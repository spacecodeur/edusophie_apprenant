# Conversion de bases

+++consigne
  <p class="text-center my-0">Sortez votre crayon et une feuille et laissez vous guider par l'activité !</p>
+++

+++sommaire

## Conversion base 2 <=> base 10 

Pour compter, les ordinateur ne disposent que de deux mots : 0 et 1. Un ordinateur peut aligner autant de 0 et de 1 que sa mémoire lui permet. Ainsi par exemple, le nombre 17 dans le système décimal (base 10) vaut 10001 dans le système binaire (base 2), ce qui revient à écrire : 

17<sub>10</sub> = 10001<sub>2</sub> 

### base 2 => base 10

Voici quelques autres exemples de nombres en binaire avec leur correspondance en décimal 

<table class="">
  <thead>
    <tr>
      <th class="px-2">Binaire</th>
      <th class="px-2">Décimal</th>
    </tr>
  </thead>
  <tbody class="text-center">
    <tr><td>0<sub>2</sub></td>
        <td>0<sub>10</sub></td></tr>
    <tr><td>1<sub>2</sub></td>
        <td>1<sub>10</sub></td></tr>
    <tr><td>10<sub>2</sub></td>
        <td>2<sub>10</sub></td></tr>
    <tr><td>11<sub>2</sub></td>
        <td>3<sub>10</sub></td></tr>
    <tr><td>100<sub>2</sub></td>
        <td>4<sub>10</sub></td></tr>
    <tr><td>101<sub>2</sub></td>
        <td>5<sub>10</sub></td></tr>
    <tr><td>110<sub>2</sub></td>
        <td>6<sub>10</sub></td></tr>
    <tr><td>111<sub>2</sub></td>
        <td>7<sub>10</sub></td></tr>
  </tbody>
</table>
  
+++question write "Complétez les égalités suivantes"

- 10<sub>10</sub> = ?<sub>2</sub>
- 9<sub>10</sub> = ?<sub>2</sub>
- 1011<sub>2</sub> = ?<sub>10</sub>
- 1110<sub>2</sub> = ?<sub>10</sub>
- 1000<sub>2</sub> = ?<sub>10</sub>

---

Pour qu'un ordinateur puisse trouver le nombre décimal correspondant à un n'importe quel nombre binaire donné, nous pourrions stocker dans sa mémoire toutes les correspondances entre les nombres binaires et les nombres décimaux. Mais cette approche n'est pas très optimale : 

- déjà parce que la table de correspondance en tant que telle occuperai en permanence une partie de la mémoire de l'ordinateur
- d'autre part car rechercher la correspondance dans la table coûte également des ressources à la machine

Pour passer d'un nombre en base 2 à un nombre en base 10 de manière optimale, nous appliquons une formule sur le nombre binaire qui produit en résultat le nombre décimal correspondant. Cette formule peut être traduite par l'algorithme suivant : 

```markdown
nombreBinaire ← 1011 # si nous voulons par exemple convertir 1011 en base 10
exposant ← 0
resultat ← 0

Pour chaque chiffre contenu dans nombreBinaire (en allant du chiffre le plus à droite au chiffre le plus à gauche)
  resultat = resultat + chiffre x ( 2 ^ exposant )
  exposant = exposant + 1

retourner resultat
```

+++bulle matthieu
  pour rappel en pseudo-code, '←' représente une affectation. 'exposant ← 0' peut se traduire en python 'exposant = 0'
+++

+++question write "Suivez cet algorithme en complétant le tableau suivant avec nombreBinaire = 100101. Seules les cases blanches du tableau sont à remplir"

+++bulle matthieu
  attention ! pour rappel, 2<sup>0</sup> = 1
+++

+++prof <a href="/images/md/4B826B45EA2A856BEG38B33CFDDHG3" target="_blank">tableau à imprimer</a> puis distribuer

<table class="table table-sm border-0">
  <thead>
    <tr class="border-0">
      <th class="border-0"></th>
      <th colspan="3" class="text-center">valeur des variables</th>
      <th class="border-0"></th>
    </tr>
    <tr>
      <th>tour n°x de la boucle 'Pour'</th>
      <th class="text-center">chiffre</th>
      <th class="text-center">resultat</th>
      <th class="text-center">exposant</th>
      <th class="text-center">calcul mathématique total effectué par l'algorithme</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Début du tour n°0</td>
      <td class="text-center">1</td>
      <td class="bg-lightGray"></td>
      <td class="bg-lightGray"></td>
      <td class="bg-lightGray"></td>
    </tr>
    <tr>
      <td>Fin du tour n°0</td>
      <td class="bg-lightGray"></td>
      <td class="text-center">1</td>
      <td class="text-center">1</td>
      <td>1 x 2<sup>0</sup></td>
    </tr>
    <tr>
      <td>Début du tour n°1</td>
      <td class="text-center">0</td>
      <td class="bg-lightGray"></td>
      <td class="bg-lightGray"></td>
      <td class="bg-lightGray"></td>
    </tr>
    <tr>
      <td>Fin du tour n°1</td>
      <td class="bg-lightGray"></td>
      <td></td>
      <td></td>
      <td>1 x 2<sup>0</sup> + 0 x 2<sup>1</sup></td>
    </tr>
    <tr>
      <td>Début du tour n°2</td>
      <td class="text-center">1</td>
      <td class="bg-lightGray"></td>
      <td class="bg-lightGray"></td>
      <td class="bg-lightGray"></td>
    </tr>
    <tr>
      <td>Fin du tour n°2</td>
      <td class="bg-lightGray"></td>
      <td></td>
      <td></td>
      <td>1 x 2<sup>0</sup> + 0 x 2<sup>1</sup> + 1 x 2<sup>2</sup></td>
    </tr>
    <tr>
      <td>Début du tour n°3</td>
      <td></td>
      <td class="bg-lightGray"></td>
      <td class="bg-lightGray"></td>
      <td class="bg-lightGray"></td>
    </tr>
    <tr>
      <td>Fin du tour n°3</td>
      <td class="bg-lightGray"></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>Début du tour n°4</td>
      <td class="text-center">0</td>
      <td class="bg-lightGray"></td>
      <td class="bg-lightGray"></td>
      <td class="bg-lightGray"></td>
    </tr>
    <tr>
      <td>Fin du tour n°4</td>
      <td class="bg-lightGray"></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>Début du tour n°5</td>
      <td></td>
      <td class="bg-lightGray"></td>
      <td class="bg-lightGray"></td>
      <td class="bg-lightGray"></td>
    </tr>
    <tr>
      <td>Fin du tour n°5</td>
      <td class="bg-lightGray"></td>
      <td></td>
      <td></td>
      <td>1 x 2<sup>0</sup> + 0 x 2<sup>1</sup> + 1 x 2<sup>2</sup> + 0 x 2<sup>3</sup> + 0 x 2<sup>4</sup> + 1 x 2<sup>5</sup></td>
    </tr>
  </tbody>
</table>
  
---

Convertir 100101<sub>2</sub> en nombre décimal revient à calculer 1 x 2<sup>0</sup> + 0 x 2<sup>1</sup> + 1 x 2<sup>2</sup> + 0 x 2<sup>3</sup> + 0 x 2<sup>4</sup> + 1 x 2<sup>5</sup>

Donc 100101<sub>2</sub> = (1 x 2<sup>0</sup> + 1 x 2<sup>2</sup> + 1 x 2<sup>5</sup>)<sub>10</sub>

+++question write "En déduire en 2/3 phrases une règle permettant de convertir n'importe quel nombre binaire en nombre décimal"

+++question write "Le programme python ci-dessous génère des nombres binaires aléatoires. Utilisez le pour générez 5 nombres binaires aléatoires et convertissez les ensuite à la main en nombre décimal"

widget python
from random import randint

def genereBinaire():
    # le nombre binaire que je souhaite générer doit contenir entre 8 et 12 chiffres
    nombreChiffres = randint(8,12)
    
    nombreBinaire = "" # pour simplifier le code, j'utilise une chaine de caractère pour stocker le nombre binaire
    
    for i in range(nombreChiffres):
      nombreBinaire += str(randint(0,1))
    
    return nombreBinaire

print(genereBinaire())
widget

+++bulle matthieu
  pour vérifier vos réponses, vous pouvez utiliser des convertisseurs binaire/décimal disponibles en ligne
+++

--- 

### base 10 => base 2

L'ordinateur est amené à convertir des nombres binaires en nombres décimaux, et l'opération inverse est également utilisée. 

Pour convertir un nombre décimal en nombre binaire, nous utilisons une méthode très efficace qui est la méthode par **divisions successives par 2**.

Il s'agit de diviser le nombre décimal par <b>2</b> plusieurs fois tant que <b class="text-red">le quotient n'est pas égal à 0</b>. Il suffit ensuite de lire le nombre binaire obtenu avec <b class="text-green">les restes obtenus successivement <span class="text-blue">de bas en haut</span></b>. 

Les exemples ci-dessous seront plus parlant ! 

#### 27<sub>10</sub> = ?<sub>2</sub> :

<div class="d-flex justify-content-evenly align-items-center mt-3">
  <table class="table-borderless border-0 mx-0">
    <tbody>
      <tr>
        <td class="px-2">27</td>
        <td class="px-2">=</td>
        <td class="px-2">13</td>
        <td class="px-2">x</td>
        <td class="px-2"><b>2</b></td>
        <td class="px-2">+</td>
        <td class="px-2"><b class="text-green">1</b></td>
      </tr>
      <tr>
        <td class="px-2">13</td>
        <td class="px-2">=</td>
        <td class="px-2">6</td>
        <td class="px-2">x</td>
        <td class="px-2"><b>2</b></td>
        <td class="px-2">+</td>
        <td class="px-2"><b class="text-green">1</b></td>
      </tr>
      <tr>
        <td class="px-2">6</td>
        <td class="px-2">=</td>
        <td class="px-2">3</td>
        <td class="px-2">x</td>
        <td class="px-2"><b>2</b></td>
        <td class="px-2">+</td>
        <td class="px-2"><b class="text-green">0</b></td>
      </tr>
      <tr>
        <td class="px-2">3</td>
        <td class="px-2">=</td>
        <td class="px-2">1</td>
        <td class="px-2">x</td>
        <td class="px-2"><b>2</b></td>
        <td class="px-2">+</td>
        <td class="px-2"><b class="text-green">1</b></td>
      </tr>
      <tr>
        <td class="px-2">1</td>
        <td class="px-2">=</td>
        <td class="px-2"><b class="text-red">0</b></td>
        <td class="px-2">x</td>
        <td class="px-2"><b>2</b></td>
        <td class="px-2">+</td>
        <td class="px-2"><b class="text-green">1</b></td>
      </tr>
    </tbody>
  </table>
  <div class="text-center">
    <p>on stoppe les divisions lorsque <b class="text-red">le quotient</b> est égal à <b class="text-red">0</b></p>
    <p><b class="text-blue">en lisant de bas en haut</b> <b class="text-green">les restes</b></p>
    <p>nous obtenons le nombre binaire <b class="text-green">11011</b><sub>2</sub> </p>
    <p>et donc <b>27<sub>10</sub> = 11011<sub>2</sub></b></p>
  </div>
</div>

---

#### 324<sub>10</sub> = ?<sub>2</sub> :

<div class="d-flex justify-content-evenly align-items-center mt-3">
  <table class="table-borderless border-0 mx-0">
    <tbody>
      <tr>
        <td class="px-2">324</td>
        <td class="px-2">=</td>
        <td class="px-2">162</td>
        <td class="px-2">x</td>
        <td class="px-2"><b>2</b></td>
        <td class="px-2">+</td>
        <td class="px-2"><b class="text-green">0</b></td>
      </tr>
      <tr>
        <td class="px-2">162</td>
        <td class="px-2">=</td>
        <td class="px-2">81</td>
        <td class="px-2">x</td>
        <td class="px-2"><b>2</b></td>
        <td class="px-2">+</td>
        <td class="px-2"><b class="text-green">0</b></td>
      </tr>
      <tr>
        <td class="px-2">81</td>
        <td class="px-2">=</td>
        <td class="px-2">40</td>
        <td class="px-2">x</td>
        <td class="px-2"><b>2</b></td>
        <td class="px-2">+</td>
        <td class="px-2"><b class="text-green">1</b></td>
      </tr>
      <tr>
        <td class="px-2">40</td>
        <td class="px-2">=</td>
        <td class="px-2">20</td>
        <td class="px-2">x</td>
        <td class="px-2"><b>2</b></td>
        <td class="px-2">+</td>
        <td class="px-2"><b class="text-green">0</b></td>
      </tr>
      <tr>
        <td class="px-2">20</td>
        <td class="px-2">=</td>
        <td class="px-2">10</td>
        <td class="px-2">x</td>
        <td class="px-2"><b>2</b></td>
        <td class="px-2">+</td>
        <td class="px-2"><b class="text-green">0</b></td>
      </tr>
      <tr>
        <td class="px-2">10</td>
        <td class="px-2">=</td>
        <td class="px-2">5</td>
        <td class="px-2">x</td>
        <td class="px-2"><b>2</b></td>
        <td class="px-2">+</td>
        <td class="px-2"><b class="text-green">0</b></td>
      </tr>
      <tr>
        <td class="px-2">5</td>
        <td class="px-2">=</td>
        <td class="px-2">2</td>
        <td class="px-2">x</td>
        <td class="px-2"><b>2</b></td>
        <td class="px-2">+</td>
        <td class="px-2"><b class="text-green">1</b></td>
      </tr>
      <tr>
        <td class="px-2">2</td>
        <td class="px-2">=</td>
        <td class="px-2">1</td>
        <td class="px-2">x</td>
        <td class="px-2"><b>2</b></td>
        <td class="px-2">+</td>
        <td class="px-2"><b class="text-green">0</b></td>
      </tr>
      <tr>
        <td class="px-2">1</td>
        <td class="px-2">=</td>
        <td class="px-2"><b class="text-red">0</b></td>
        <td class="px-2">x</td>
        <td class="px-2"><b>2</b></td>
        <td class="px-2">+</td>
        <td class="px-2"><b class="text-green">1</b></td>
      </tr>
    </tbody>
  </table>
  <div class="text-center">
    <p>on stoppe les divisions lorsque <b class="text-red">le quotient</b> est égal à <b class="text-red">0</b></p>
    <p><b class="text-blue">en lisant de bas en haut</b> <b class="text-green">les restes</b></p>
    <p>nous obtenons le nombre binaire <b class="text-green">101000100</b><sub>2</sub> </p>
    <p>et donc <b>324<sub>10</sub> = 101000100<sub>2</sub></b></p>
  </div>
</div>

---

+++question "Modifiez le code ci-dessous afin de générer un nombre décimal aléatoire contenant un nombre de chiffres compris entre 3 et 4 (inclus)"

widget python
from random import randint

def genereBinaire():
    # le nombre binaire que je souhaite générer doit contenir entre 8 et 12 chiffres
    nombreChiffres = randint(8,12)
    
    nombreBinaire = "" # pour simplifier le code, j'utilise une chaine de caractère pour stocker le nombre binaire
    
    for i in range(nombreChiffres):
      nombreBinaire += str(randint(0,1))
    
    return nombreBinaire

print(genereBinaire())
widget

+++question write "Générez 3 nombres décimaux aléatoires et convertissez les en nombre décimal en suivant la méthode par divisions successives par 2"

+++bulle matthieu
  (rappel) pour vérifier vos réponses, vous pouvez utiliser des convertisseurs binaire/décimal disponibles en ligne
+++


## Conversion base 2 <=> base 16 

Le système hexadécimal (base 16) permet d'écrire du code binaire en utilisant beaucoup moins de caractères.

Par exemple, la plupart des colorpickers permettent de choisir une couleur en fonction de son code hexadécimal : 

+++image "/images/md/3G8EHE2G99DD9H4C785CEB63EG75DD" col-5 mx-auto


De plus, les conversions base 2 <=> base 16 sont beaucoup **plus efficaces** que les conversions base 2 <=> base 10.

+++bulle matthieu
  '<b>plus efficace</b>' dans un contexte algorithmique, la conversion coûte beaucoup moins de ressources à la machine (calculs, mémoire, ...)
+++

+++question write "Rappelez quels sont les composants qui gèrent la mémoire vive et le calcul dans un ordinateur"

+++spoil "Aide"
  Si vous ne vous souvenez plus du tout, un petit tour dans +++lien "/apprenant/01-__-_NSI-_premiere/01-__-_Introduction-_en-_informatique-_fondamentale-_et-_introduction-_en-_programmation-_Web-_cote-_client/00-__-_cours/00-__-_Introduction-_en-_informatique-_fondamentale.md" "ce précédent cours" ne vous fera pas de mal ;) 
spoil+++

### base 16 => base 2

Pour pouvoir convertir un nombre hexadécimal en nombre binaire, il suffit de connaître la table de correspondance de tous les chiffres hexadécimaux avec les nombres binaires 

<table class="mx-auto">
  <thead>
    <tr class="text-center">
      <th class="px-2">nombre en base 10<br>(mis ici à titre informatif)</th>
      <th class="px-2">nombre en base 2</th>
      <th class="px-2">chiffre en base 16</th>
    </tr>
  </thead>
  <tbody>
    <tr class="text-center">
      <td>0<sub>10</sub></td>
      <td><b>0<sub>2</sub></b></td>
      <td><b>0<sub>16</sub></b></td>
    </tr>
    <tr class="text-center">
      <td>1<sub>10</sub></td>
      <td><b>1<sub>2</sub></b></td>
      <td><b>1<sub>16</sub></b></td>
    </tr>
    <tr class="text-center">
      <td>2<sub>10</sub></td>
      <td><b>10<sub>2</sub></b></td>
      <td><b>2<sub>16</sub></b></td>
    </tr>
    <tr class="text-center">
      <td>3<sub>10</sub></td>
      <td><b>11<sub>2</sub></b></td>
      <td><b>3<sub>16</sub></b></td>
    </tr>
    <tr class="text-center">
      <td>4<sub>10</sub></td>
      <td><b>100<sub>2</sub></b></td>
      <td><b>4<sub>16</sub></b></td>
    </tr>
    <tr class="text-center">
      <td>5<sub>10</sub></td>
      <td><b>101<sub>2</sub></b></td>
      <td><b>5<sub>16</sub></b></td>
    </tr>
    <tr class="text-center">
      <td>6<sub>10</sub></td>
      <td><b>110<sub>2</sub></b></td>
      <td><b>6<sub>16</sub></b></td>
    </tr>
    <tr class="text-center">
      <td>7<sub>10</sub></td>
      <td><b>111<sub>2</sub></b></td>
      <td><b>7<sub>16</sub></b></td>
    </tr>
    <tr class="text-center">
      <td>8<sub>10</sub></td>
      <td><b>1000<sub>2</sub></b></td>
      <td><b>8<sub>16</sub></b></td>
    </tr>
    <tr class="text-center">
      <td>9<sub>10</sub></td>
      <td><b>1001<sub>2</sub></b></td>
      <td><b>9<sub>16</sub></b></td>
    </tr>
    <tr class="text-center">
      <td>10<sub>10</sub></td>
      <td><b>1010<sub>2</sub></b></td>
      <td><b>A<sub>16</sub></b></td>
    </tr>
    <tr class="text-center">
      <td>11<sub>10</sub></td>
      <td><b>1011<sub>2</sub></b></td>
      <td><b>B<sub>16</sub></b></td>
    </tr>
    <tr class="text-center">
      <td>12<sub>10</sub></td>
      <td><b>1100<sub>2</sub></b></td>
      <td><b>C<sub>16</sub></b></td>
    </tr>
    <tr class="text-center">
      <td>13<sub>10</sub></td>
      <td><b>1101<sub>2</sub></b></td>
      <td><b>D<sub>16</sub></b></td>
    </tr>
    <tr class="text-center">
      <td>14<sub>10</sub></td>
      <td><b>1110<sub>2</sub></b></td>
      <td><b>E<sub>16</sub></b></td>
    </tr>
    <tr class="text-center">
      <td>15<sub>10</sub></td>
      <td><b>1111<sub>2</sub></b></td>
      <td><b>F<sub>16</sub></b></td>
    </tr>
  </tbody>
</table>



La conversion depuis le système hexadecimal au système binaire est très simple. Il suffit de convertir chaque chiffre hexadecimal par son équivalant en binaire <u>**sur 4 digits**</u>. Voici deux exemples de conversion  

<div class="d-flex justify-content-evenly my-3">

<div>

  <span class="h4">FF7<sub>16</sub> = ?<sub>2</sub></span>
  
  <div class="ps-5">
    <ul>
      <li>F<sub>16</sub> = <b class="text-red">1111</b><sub>2</sub></li>
      <li>F<sub>16</sub> = <b class="text-green">1111</b><sub>2</sub></li>
      <li>7<sub>16</sub> = <b class="text-blue">0111</b><sub>2</sub></li>
    </ul>
  </div>
  
  Donc : FF7<sub>16</sub> = <b class="text-red">1111</b><b class="text-green">1111</b><b class="text-blue">0111</b><sub>2</sub>

</div>

<div style="width:2px" class="border border-dark"></div>

<div>

  <span class="h4">C3D5<sub>16</sub> = ?<sub>2</sub></span>
  
  <div class="ps-5">
    <ul>
      <li>C<sub>16</sub> = <b class="text-red">1100</b><sub>2</sub></li>
      <li>3<sub>16</sub> = <b class="text-green">0011</b><sub>2</sub></li>
      <li>D<sub>16</sub> = <b class="text-blue">1101</b><sub>2</sub></li>
      <li>5<sub>16</sub> = <b class="text-yellow">0101</b><sub>2</sub></li>
    </ul>
  </div>
  
  Donc : C3D5<sub>16</sub> = <b class="text-red">1100</b><b class="text-green">0011</b><b class="text-blue">1101</b><b class="text-yellow">0101</b><sub>2</sub>

</div>

</div>

+++question "Modifiez le code ci-dessous afin de générer un nombre hexadécimal aléatoire contenant un nombre de chiffres compris entre 4 et 6 (inclus)"

widget python
from random import randint

def genereBinaire():
    # le nombre binaire que je souhaite générer doit contenir entre 8 et 12 chiffres
    nombreChiffres = randint(8,12)
    
    nombreBinaire = "" # pour simplifier le code, j'utilise une chaine de caractère pour stocker le nombre binaire
    
    for i in range(nombreChiffres):
      nombreBinaire += str(randint(0,1))
    
    return nombreBinaire

print(genereBinaire())
widget

+++spoil "Aide"
  Pour générer un caractère aléatoire allant de 0 (zéro) à F, je vous conseil d'utiliser en python <code>random.choice([...])</code>. Vous trouverez des exemples d'utilisation sur Internet !
spoil+++

+++question write "Générez 3 nombres hexadécimaux aléatoires et convertissez les en nombre binaire en suivant la méthode expliquée avant la question 7"

+++bulle matthieu
  (rappel bis) pour vérifier vos réponses, vous pouvez utiliser des convertisseurs binaire/hexadécimal disponibles en ligne
+++

### base 2 => base 16

Pour convertir un nombre binaire en nombre hexadécimal, rien de plus simple ! il suffit de découper, en partant de la droite, le nombre binaire par paquets de 4 chiffres. Il faut ensuite convertir chaque paquet par son équivalent en hexadecimal. Voici deux exemples : 

<div class="d-flex justify-content-evenly my-3">

<div>

  <p class="h4 text-center">10011011<sub>2</sub> = ?<sub>16</sub></p>
  
  <p class="mt-2">Je décompose 10011011<sub>2</sub> en paquets de 4 chiffres en partant de la droite : </p>
  
  <p class="ps-5">1001 1011</p>
  <p class="ps-5">Je traduis chaque paquet en nombre hexadécimal</p>
  
  <div class="ps-5">
    <ul>
      <li>1001<sub>2</sub> = <b class="text-red">9</b><sub>16</sub></li>
      <li>1011<sub>2</sub> = <b class="text-green">B</b><sub>16</sub></li>
    </ul>
  </div>
  
  Et donc : 10011011<sub>2</sub> = <b class="text-red">9</b><b class="text-green">B</b><sub>16</sub>

</div>

</div>

---

<div class="d-flex justify-content-evenly my-3">

<div>

  <p class="h4 text-center">1011100001<sub>2</sub> = ?<sub>16</sub></p>
  
  <p class="mt-2">Je décompose 10011011<sub>2</sub> en paquets de 4 chiffres en partant de la droite : </p>
  
  <p class="ps-5"><u><b>00</b></u>10 1110 0001</p>
  <p class="ps-5">Je traduis chaque paquet en nombre hexadécimal</p>
  
  <div class="ps-5">
    <ul>
      <li>0010<sub>2</sub> = <b class="text-red">2</b><sub>16</sub></li>
      <li>1110<sub>2</sub> = <b class="text-green">E</b><sub>16</sub></li>
      <li>0001<sub>2</sub> = <b class="text-blue">1</b><sub>16</sub></li>
    </ul>
  </div>
  
  Et donc : 1011100001<sub>2</sub> = <b class="text-red">2</b><b class="text-green">E</b><b class="text-blue">1</b><sub>16</sub>

</div>

</div>

+++question write "Le programme python ci-dessous génère des nombres binaires aléatoires. Utilisez le pour générez 5 nombres binaires aléatoires et convertissez les ensuite à la main en nombre hexadécimal"

widget python
from random import randint

def genereBinaire():
    # le nombre binaire que je souhaite générer doit contenir entre 8 et 12 chiffres
    nombreChiffres = randint(8,12)
    
    nombreBinaire = "" # pour simplifier le code, j'utilise une chaine de caractère pour stocker le nombre binaire
    
    for i in range(nombreChiffres):
      nombreBinaire += str(randint(0,1))
    
    return nombreBinaire

print(genereBinaire())
widget

+++bulle matthieu
  (rappel an again) pour vérifier vos réponses, vous pouvez utiliser des convertisseurs binaire/hexadécimal disponibles en ligne
+++

## Réponses

<ol start="0">
  <li>
  +++spoil
<ul>
  <li>10<sub>10</sub> = 1010<sub>2</sub></li>
  <li>9<sub>10</sub> = 1001<sub>2</sub></li>
  <li>1011<sub>2</sub> = 11<sub>10</sub></li>
  <li>1110<sub>2</sub> = 14<sub>10</sub></li>
  <li>1000<sub>2</sub> = 8<sub>10</sub></li>
</ul>
  spoil+++
  </li>
  <li>
  +++spoil
<table class="table table-sm border-0">
  <thead>
    <tr class="border-0">
      <th class="border-0"></th>
      <th colspan="3" class="text-center">valeur des variables</th>
      <th class="border-0"></th>
    </tr>
    <tr>
      <th>tour n°x de la boucle 'Pour'</th>
      <th class="text-center">chiffre</th>
      <th class="text-center">resultat</th>
      <th class="text-center">exposant</th>
      <th class="text-center">calcul mathématique total</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Début du tour n°0</td>
      <td class="text-center">1</td>
      <td class="bg-lightGray"></td>
      <td class="bg-lightGray"></td>
      <td class="bg-lightGray"></td>
    </tr>
    <tr>
      <td>Fin du tour n°0</td>
      <td class="bg-lightGray"></td>
      <td class="text-center">1</td>
      <td class="text-center">1</td>
      <td>1 x 2<sup>0</sup></td>
    </tr>
    <tr>
      <td>Début du tour n°1</td>
      <td class="text-center">0</td>
      <td class="bg-lightGray"></td>
      <td class="bg-lightGray"></td>
      <td class="bg-lightGray"></td>
    </tr>
    <tr>
      <td>Fin du tour n°1</td>
      <td class="bg-lightGray"></td>
      <td class="text-center"><b class="text-green">1</b></td>
      <td class="text-center"><b class="text-green">1</b></td>
      <td>1 x 2<sup>0</sup> + 0 x 2<sup>1</sup></td>
    </tr>
    <tr>
      <td>Début du tour n°2</td>
      <td class="text-center">1</td>
      <td class="bg-lightGray"></td>
      <td class="bg-lightGray"></td>
      <td class="bg-lightGray"></td>
    </tr>
    <tr>
      <td>Fin du tour n°2</td>
      <td class="bg-lightGray"></td>
      <td class="text-center"><b class="text-green">5</b></td>
      <td class="text-center"><b class="text-green">2</b></td>
      <td>1 x 2<sup>0</sup> + 0 x 2<sup>1</sup> + 1 x 2<sup>2</sup></td>
    </tr>
    <tr>
      <td>Début du tour n°3</td>
      <td class="text-center"><b class="text-green">0</b></td>
      <td class="bg-lightGray"></td>
      <td class="bg-lightGray"></td>
      <td class="bg-lightGray"></td>
    </tr>
    <tr>
      <td>Fin du tour n°3</td>
      <td class="bg-lightGray"></td>
      <td class="text-center"><b class="text-green">5</b></td>
      <td class="text-center"><b class="text-green">3</b></td>
      <td><b class="text-green">1 x 2<sup>0</sup> + 0 x 2<sup>1</sup> + 1 x 2<sup>2</sup> + 0 x 2<sup>3</sup></b></td>
    </tr>
    <tr>
      <td>Début du tour n°4</td>
      <td class="text-center">0</td>
      <td class="bg-lightGray"></td>
      <td class="bg-lightGray"></td>
      <td class="bg-lightGray"></td>
    </tr>
    <tr>
      <td>Fin du tour n°4</td>
      <td class="bg-lightGray"></td>
      <td class="text-center"><b class="text-green">5</b></td>
      <td class="text-center"><b class="text-green">4</b></td>
      <td><b class="text-green">1 x 2<sup>0</sup> + 0 x 2<sup>1</sup> + 1 x 2<sup>2</sup> + 0 x 2<sup>3</sup> + 0 x 2<sup>4</sup></b></td>
    </tr>
    <tr>
      <td>Début du tour n°5</td>
      <td class="text-center"><b class="text-green">1</b></td>
      <td class="bg-lightGray"></td>
      <td class="bg-lightGray"></td>
      <td class="bg-lightGray"></td>
    </tr>
    <tr>
      <td>Fin du tour n°5</td>
      <td class="bg-lightGray"></td>
      <td class="text-center"><b class="text-green">37</b></td>
      <td class="text-center"><b class="text-green">5</b></td>
      <td>1 x 2<sup>0</sup> + 0 x 2<sup>1</sup> + 1 x 2<sup>2</sup> + 0 x 2<sup>3</sup> + 0 x 2<sup>4</sup> + 1 x 2<sup>5</sup></td>
    </tr>
  </tbody>
</table>
  spoil+++
  </li>
  <li>
  +++spoil
    <p>Étant donné un nombre binaire, pour le convertir en nombre décimal, il suffit de calculer 2 puissance le numéro de chaque digit à 1 (en partant de zéro et par la droite) et d'additionner le tout : </p>
    <table class="table-borderless border-0">
      <tbody>
        <tr>
          <td><b class="text-red">1</b></td>
          <td><b>0</b></td>
          <td><b>0</b></td>
          <td><b class="text-red">1</b></td>
          <td><b class="text-red">1</b></td>
          <td>(nombre à convertir)</td>
        </tr>
        <tr style="font-size:0.8rem">
          <td class="text-blue">4</td>
          <td>3</td>
          <td>2</td>
          <td class="text-blue">1</td>
          <td class="text-blue">0</td>
          <td>(numéro du digit)</td>
        </tr>
      </tbody>
    </table>
    <p>10011<sub>2</sub> = (<b>2<sup><span class="text-blue">0</span></sup> + 2<sup><span class="text-blue">1</span></sup> + 2<sup><span class="text-blue">4</span></sup></b>)<sub>10</sub> = 19<sub>10</sub></p>
  spoil+++
  </li>
  <li value="4">
  +++spoil
widget python
from random import randint

def genereDecimal():
    # le nombre binaire que je souhaite générer doit contenir entre 8 et 12 chiffres
    nombreChiffres = randint(3,4)

    nombreDecimal = "" # pour simplifier le code, j'utilise une chaine de caractère pour stocker le nombre binaire

    for i in range(nombreChiffres):
      nombreDecimal += str(randint(0,9))

    return nombreDecimal

print(genereDecimal())
widget
  spoil+++
  </li>
  <li value="6">
  +++spoil
    Dans un ordinateur, la mémoire vive est gérée par les barrettes de RAM
    
    +++bulle matthieu
      c'est la mémoire utilisée lorsque vous créez par exemple des variables avec python
    +++
    
    Le calcul dans un ordinateur est effectué par le CPU, appelé autrement, le processeur
    
    +++bulle matthieu
      dans votre programme python, lorsque vous effectuez par exemple une addition, c'est le processeur de votre ordinateur qui se charge de faire le calcul
    +++
  spoil+++
  </li>
  <li>
  +++spoil
widget python
from random import randint, choice

def genereHexadecimal():
    # le nombre binaire que je souhaite générer doit contenir entre 8 et 12 chiffres
    nombreChiffres = randint(4,6)

    nombreHexadecimal = "" # pour simplifier le code, j'utilise une chaine de caractère pour stocker le nombre binaire

    for i in range(nombreChiffres):
      nombreHexadecimal += str(choice(['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F']))

    return nombreHexadecimal

print(genereHexadecimal())
widget
  spoil+++
  </li>
</ol>