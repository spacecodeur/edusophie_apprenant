# Quelques piqûres de rappel Python

+++consignes
  <ol start="0">
    <li>Créez vous un dossier du jour en exécutant chacune des lignes ci-dessous sur un terminal. <b>Attention !</b> n'oubliez pas d'adapter les lignes 2 et 3 </li>
  </ol>
  <pre><code class="language-bash"># lignes à réadapter et à exécuter dans un terminal
nom_prenom=dupond_cedric
dateDuJour=18_10_21

mkdir /home/premiere/Eleves/${nom_prenom}/seances/${dateDuJour}
cd /home/premiere/Eleves/${nom_prenom}/seances/${dateDuJour}
touch fichier.py
thonny fichier.py
  </code></pre>
  <ol start="1">
    <li>Parcourez l'activité et répondez aux questions</li>
  </ol>
+++

## Les fonctions

Vous êtes amenés à créer des programmes de plus en plus consistants. Il devient important de savoir structurer proprement son code et les fonctions aident en ce sens.

Si vous avez du mal à avancer dans les questions de cette partie, je vous invite à re-parcourir +++lien "/apprenant/01-__-_NSI-_premiere/00-__-_rappels-_en-_programmation-_et-_systeme-_d--__exploitation-_Linux/00-__-_cours/3-__-_programmation-_--_-_les-_fonctions.md" ""le cours sur les fonctions avec Python

+++question write "Traduisez l'algorithme ci-dessous en langage python"

```markdown
Créer une variable nommée nombre et affectez lui la valeur 17
Multipliez la variable nombre par 2 et stockez le résultat dans la variable nombre
```

---

+++question write "Créez une fonction foisDeux(nombre) qui prends en paramètre un nombre entier et retourne ce nombre multiplié par deux"

+++question write "Donnez en python un exemple d'utilisation de la fonction foisDeux et afficher dans la console le résultat qu'elle retourne"

---

La fonction foisDeux n'est pas très pratique car elle permet seulement de multiplier un nombre par deux. En informatique, on préfère créer des fonctions génériques utilisables dans différents cas de figures.

+++question write "Créez une fonction foisX(nombre, x) qui prends en paramètre un nombre entier et un nombre x puis retourne le nombre multiplié par x"

+++question write "Sans l'exécuter dans un IDE, qu'affiche vous selon vous le code ci-dessous ?"

```python
# ...
# ,,, définition des fonctions fois2 et foisX
# ...

print(fois2(10))
print(foisX(fois2(10),3))
```
---

+++question "Créez une fonction pileOuFace(n) qui prends en paramètre un nombre n puis affiche dans la console de manière aléatoire pile ou face répété n fois"

Voici un exemple d'affichage de cette fonction lorsqu'elle est exécutée avec n = 7

```markdown
pile
face
pile
pile
face
face
face
```

+++spoil "Aide"
  Il faudra importer la librairie 'random' au tout début du programme. Dans un premier temps ne générer qu'une seule fois pile ou face (Internet regorge d'exemple d'utilisation de la librairie random). Ensuite, ajouter dans votre fonction une boucle pour répéter n fois votre tirage aléatoire
spoil+++

---

## Réponses

<ol start="0">
  <li>
  +++spoil
widget python
nombre = 17
nombre = nombre * 2
widget
  spoil+++
  </li>
  <li>
  +++spoil
widget python
def foisDeux(nombre):
  resultat = nombre * 2
  return resultat
widget

Ci-dessous une version plus compacte :

widget python
def foisDeux(nombre):
  return nombre * 2
widget
  spoil+++
  </li>
  <li>
  +++spoil
widget python (Définition de la fonction foisDeux:1)
resultat = foisDeux(12)
print(resultat)

# ou de manière plus compacte "print(foisDeux(12))"
widget
  spoil+++
  </li>
  <li>
  +++spoil
widget python
def foisX(nombre, x):
  return nombre * x
widget  
  spoil+++
  </li>
  <li>
  +++spoil
widget python (Définition de la fonction foisDeux:4) (Définition de la fonction foisDeux:1)
print(foisDeux(10))
print(foisX(foisDeux(10),3))
widget  
  spoil+++
  </li>
  <li>
  +++spoil
widget python
import random

def pileOuFace(n):
  for i in range(n):
    print(random.choice(["pile", "face"]))
    
pileOuFace(7)
widget
  spoil+++
  </li>
</ol>

## Les tableaux

On l'a vu, un fichier csv contient un ensemble de ligne et chaque ligne contient un ensemble de données. Le type de variable 'tableau' est alors tout indiqué pour gérer une ligne. 

Si vous avez du mal à avancer dans les questions de cette partie, je vous invite à re-parcourir +++lien "/apprenant/01-__-_NSI-_premiere/02-__-_introduction-_aux-_tableaux-_et-_programmation-_Web-_cote-_client/00-__-_cours/00-__-_introduction-_aux-_tableaux.md" "le cours introduisant les tableaux avec Python"

+++question write "Créez un tableau nommé tab et contenant dans l'ordre ces quatre chaînes de caractères : 'foo', 'faa', 'fuu' et 'foo'"

+++question write "Affichez dans la console le premier élément de la variable tab"

+++question write "Affichez dans la console le deuxième et le quatrième élément de la variable tab"

--- 

La variable suivante contient toutes les températures moyennes relevées chaque mois pendant une année : 

```python
temp_moy_par_mois = [4, 8, 9, 10, 20, 27, 33, 34, 29, 19, 12, 6]
```

En janvier par exemple, la température moyenne était de 4 degrés tandis qu'en avril la température moyenne relevée et de 10 degrés.

Pour afficher toutes les températures, nous utilisons l'instruction itérative `for` et la fonction `print` comme suit : 

```python
temp_moy_par_mois = [4, 8, 9, 10, 20, 27, 33, 34, 29, 19, 12, 6]
for temperature in temp_moy_par_mois:
  print(temperature)
```

+++question "Nous souhaitons afficher seulement les températures supérieures ou égales à 10. Complétez le code ci-dessus en vous aidant de l'instruction conditionnelle <code>if</code>"

+++question "Nous souhaitons afficher le nombre total de mois où la température a été inférieure à 10. Implémentez cette fonctionnalité dans votre code"

+++spoil "Aide"
  Vous devrez ajouter un compteur dans votre programme qui va s'incrémenter dans la boucle for SI température est inférieure à 10
spoil+++

+++question "Améliorez le programme afin qu'à la fin de son exécution soit affichée la moyenne totale de toutes les températures de l'année"


## Réponses

<ol start="6">
  <li>
  +++spoil
widget python
tab = ['foo', 'faa', 'fuu', 'foo']
widget
  spoil+++
  </li>
  <li>
  +++spoil
widget python
tab = ['foo', 'faa', 'fuu', 'foo']
print(tab[0])
widget
  spoil+++
  </li>
  <li>
  +++spoil
widget python
tab = ['foo', 'faa', 'fuu', 'foo']
print(tab[1])
print(tab[3])

# autre possibilité
print(tab[1] + " " + tab[3])
widget
  spoil+++
  </li>
  <li>
  +++spoil
widget python
temp_moy_par_mois = [4, 8, 9, 10, 20, 27, 33, 34, 29, 19, 12, 6]

for temperature in temp_moy_par_mois:
  if(temperature >= 10):
    print(temperature)
widget
  spoil+++
  </li>
  <li>
  +++spoil
widget python
temp_moy_par_mois = [4, 8, 9, 10, 20, 27, 33, 34, 29, 19, 12, 6]
compteur = 0

for temperature in temp_moy_par_mois:
  if(temperature >= 10):
    print(temperature)
  else:
    compteur += 1

print('Le nombre de mois où la température est inférieure à 10 est : ' + str(compteur))
widget  
  spoil+++
  </li>
  <li>
  +++spoil
  
  Voici la version du code qui ne contient que la réponse à la question 11 : 

widget python
temp_moy_par_mois = [4, 8, 9, 10, 20, 27, 33, 34, 29, 19, 12, 6]
cumul = 0

for temperature in temp_moy_par_mois:
  cumul += temperature
  
moyenneTotale = cumul / 12
print("la moyenne totale de toutes les températures de l'année est :" + str(moyenneTotale) + " degrés")
widget

  et voici le code complet : 
  
widget python
temp_moy_par_mois = [4, 8, 9, 10, 20, 27, 33, 34, 29, 19, 12, 6]
compteur = 0
cumul = 0

for temperature in temp_moy_par_mois:
  cumul += temperature
  if(temperature >= 10):
    print(temperature)
  else:
    compteur += 1

print('Le nombre de mois où la température est inférieure à 10 est : ' + str(compteur))

moyenneTotale = cumul / 12
print("la moyenne totale de toutes les températures de l'année est :" + str(moyenneTotale) + " degrés")
widget  
  spoil+++
  </li>
</ol>

