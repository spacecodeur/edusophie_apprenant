# Additions de nombres binaires

+++consigne
  <p class="text-center my-0">Sortez votre crayon et une feuille et laissez vous guider par l'activité !</p>
+++

Pour additionner deux nombres binaires entre eux, il suffit de poser l'addition comme si vous additionniez deux nombres décimaux. 

<table class="table-borderless border-0">
  <tbody>
    <tr>
      <td></td>
      <td></td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td><i>(neuf)</i></td>
    </tr>
    <tr>
      <td>+</td>
      <td></td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td><i>(treize)</i></td>
    </tr>
    <tr>
      <td></td>
      <td>1</td>
      <td></td>
      <td></td>
      <td>1</td>
      <td></td>
      <td><i>(retenues)</i></td>
    </tr>
    <tr>
      <td colspan="6"><div class="bg-dark" style="height:2px"></div></td>
    </tr>
    <tr>
      <td></td>
      <td><b class="text-blue">1</b></td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td><i>(vingt-deux)</i></td>
    </tr>
  </tbody>
</table>
  
+++bulle matthieu
  <b>attention !</b> la taille de la mémoire d'un ordinateur est finie
+++

Supposons que nous utilisons une mémoire de 4 bits maximum, alors dans l'addition ci-dessus, <b class="text-blue">le bit à gauche</b> est perdu ! Donc pour la machine, la somme de 1001 et de 1101 vaut 0110. 

Autrement dit, si nous demandons à la machine de calculer 13 + 9, **elle nous répond 6** !

Pour la suite de l'activité, nous supposons que notre mémoire contient au maximum 8 digits (+++flag-us bits), ce qui correspond à 1 octet (+++flag-us byte)

+++question write "Entraînez vous à additionner, sur un octet, les nombres suivants : "

0. 1101<sub>2</sub> + 1000011<sub>2</sub>
1. 101010<sub>2</sub> + 110<sub>2</sub>
2. 10010110<sub>2</sub> + 1101100<sub>2</sub> 
3. 111111<sub>2</sub> + 1010101<sub>2</sub> 

+++question write "Pour chaque addition ci-dessus, convertissez les nombres binaires en nombre décimaux en utilisant la règle vue +++lien "/apprenant/01-__-_NSI-_premiere/06-__-_recherche-_de-_donnees-_dans-_une-_table---_-_systemes-_binaire-_et-_hexadecimal/activites/applications-_directes/conversion-_de-_bases.md" "dans l'activité #0". Vérifiez ainsi si vos additions sont justes"


## Réponses

<ol start="0">
  <li>
  +++spoil
    sur un octet : 
    <ul>
      <li>1101<sub>2</sub> + 1000011<sub>2</sub> = <b>0</b>1010000<sub>2</sub></li>
      <li>101010<sub>2</sub> + 110<sub>2</sub> = <b>00</b>110000<sub>2</sub></li>
      <li>10010110<sub>2</sub> + 1101100<sub>2</sub> = 00000010<sub>2</sub>, le résultat étant plus grand que 8 bits ce dernier devient alors faux étant donné que nous avons supposé que notre mémoire est de taille 1 octet</li>
      <li>111111<sub>2</sub> + 1010101<sub>2</sub> = 10010100<sub>2</sub></li>
    </ul>
  spoil+++
  </li>
  <li>
  +++spoil
    <ul>
      <li>1101<sub>2</sub> + 1000011<sub>2</sub> = <b>0</b>1010000<sub>2</sub> <=> 13<sub>10</sub> + 67<sub>10</sub> = 80<sub>10</sub></li>
      <li>101010<sub>2</sub> + 110<sub>2</sub> = <b>0</b>110000<sub>2</sub> <=> 42<sub>10</sub> + 6<sub>10</sub> = 48<sub>10</sub></li>
      <li>10010110<sub>2</sub> + 1101100<sub>2</sub> = <b>0</b>00000010<sub>2</sub> <=> 150<sub>10</sub> + 108<sub>10</sub> = 2<sub>10</sub>, un résultat absurde provoqué par la limite de la taille de la mémoire</li>
      <li>111111<sub>2</sub> + 1010101<sub>2</sub> = <b>0</b>10010100<sub>2</sub> <=> 63<sub>10</sub> + 85<sub>10</sub> = 148<sub>10</sub></li>
    </ul> 
  spoil+++
  </li>
</ol>


<div class="d-none">

http://portail.lyc-la-martiniere-diderot.ac-lyon.fr/srv1/co/2_nombres_entiers_relatifs.html

Vous avez appris à convertir des nombres décimaux et nombres binaires dans l'activité précédente. Cependant, les nombres étaient systématiquement des nombres positifs. 

Nous nous intéressons ici à comment représenter des nombres négatifs en binaire. Par exemple, lorsque vous utilisez votre calculatrice, vous devez parfois utiliser des nombres négatifs dans vos calculs.

La question est donc : <b>comment un ordinateur représente-t-il un nombre binaire relatif qui peut donc être négatif ?</b>



## Approche naïve

Une première idée <sub>mais naïve</sub> consiste à consacrer le bit d'extrême gauche comme bit de signe :

- 0 : cela signifie que le nombre binaire est un nombre positif
- 1 : cela signifie que le nombre binaire est un nombre négatif 

Par exemple : 

</div>