# Systèmes binaire et hexadécimal

+++programme
  Passer de la représentation d’une base dans une autre
  Évaluer le nombre de bits nécessaires à l’écriture en base 2 d’un entier, de la somme ou du produit de deux nombres entier

  Convertir un nombre binaire en un nombre décimal, et inversement
  Convertir un nombre binaire en un nombre hexadécimal, et inversement
  Additionner des nombres binaires
+++

+++sommaire

## (rappel) D'où proviennent les nombres binaires ? 

+++image "/images/md/A95AC4G642FD34EEG62F55H79GDB34" col-2 mx-auto

+++bulle matthieu
  nous avions parlé +++lien "/apprenant/01-__-_NSI-_premiere/01-__-_Introduction-_en-_informatique-_fondamentale-_et-_introduction-_en-_programmation-_Web-_cote-_client/00-__-_cours/00-__-_Introduction-_en-_informatique-_fondamentale.md" "il y a quelques séances" du mathématicien Georges Boole
+++

+++bulle matthieu droite
  quels en sont vos souvenirs ? :)
+++

<div class="text-center">

+++spoil "La naissance de l'algèbre de Boole"
  <p>En 1847, George Boole (32 ans) publie son premier livre qui introduit un nouvel algèbre Mathématique : l'algèbre de Boole.</p>
  <p>Cet algèbre permet de traduire des énoncés logiques en formules mathématiques avec les valeurs <b>1</b> (<code>True</code>) ou <b>0</b> (<code>False</code>) et des expressions logiques comme <b>ET</b> (+++flag-us <code>and</code>), <b>OU</b> (+++flag-us <code>or</code>) ou encore <b>NON</b> (+++flag-us <code>not</code>)</p>
spoil+++

+++spoil "Application physique de l'algèbre de Boole"
  <p>L'invention des premiers transistors en 1947 a permis de mettre en application l'algèbre de Boole. Ainsi, il devient possible de simuler des énoncés logiques avec des machines électroniques</p>
spoil+++

+++spoil "Des nombres binaires dans notre quotidien"
  <p>Tous les circuits électroniques dans le monde se basent aujourd'hui sur l'algèbre conçu par Georges Boole. Et des circuits électroniques...il y en a partout !</p>
  
  +++image "/images/md/C32GC6EB556DD8482EH825HC68GC87" col-6 mx-auto
spoil+++
  
</div>

## Passer d'une base à l'autre

Dans notre quotidien, nous utilisons le **système décimal** qui se repose sur la base 10 comme par exemple pour compter le nombre de personnes qui se trouvent dans une pièce.

Nous utilisons également le **système sexagésimal** reposant sur la base 60.

+++bulle matthieu
  quels exemples pouvez vous donner sur des utilisations quotidiennes su système sexagésimal ?
+++

<div class="text-center">
+++spoil "Quelques exemples d'applications de la base 60" nolabel
  <ul class="text-start">
    <li>Lorsque nous lisons l'heure, les minutes et secondes sont en base 60</li>
    <li>Ou encore pour compter le nombre de tour fait dans une figure de snowboard : 180°, 360°, ...</li>
  </ul>
spoil+++
</div>


La base la plus utilisée dans le monde est la base 10, et elle est aussi très largement utilisée dans les logiciels que nous utilisons sur un ordinateur. 

Mais comment fait un ordinateur pour utiliser la base 10 sachant que, fondamentalement, il se repose sur un système binaire ?

+++activité binome 40 ../activites/applications-_directes/conversion-_de-_bases.md "Conversion de bases"

### +++emoji-write Résumons

- Un ordinateur ne comprends fondamentalement que le système binaire (base 2)
- Pour s'adapter à nos usages, les ordinateurs possèdent des fonctions conversions pour passer de la base 2 à la base 10 (système décimal classique):
- Les nombres binaires prennent rapidement beaucoup de place. Le système hexadécimal (base 16) permet de raccourcir l'écriture d'un nombre binaire et donc de limiter notamment les erreurs de saisies
- Les méthodes de conversion base 2 <=> base 16 sont également beaucoup moins coûteuses en ressource machine que les conversions base 2 <=> base 10

## Additionner des nombres binaires

Nous avons vu en début d'année qu'un ordinateur ne connaît de base que des opérations assez simplistes : (appelées autrement 'portes logiques')

- l'opération and (et)
- l'opération or (or)
- l'opération not (non)

+++bulle matthieu
  il existe d'autres opérations comme le xor (ou exclusif), le nand (non 'et') et le nor (non 'ou')
+++

En combinant ces opérations primitives, il est possible d'apprendre à un ordinateur à faire des additions, des additions de nombres binaires ! 

Nous ne l'aborderons pas ici mais nous pouvons également apprendre à l'ordinateur à faire des soustractions en binaire, nous avons alors les outils nécessaires pour faire faire des multiplications et des divisions (en binaire toujours) à notre ordinateur.

+++bulle matthieu
  nous obtenons alors une calculatrice ! un 'mini ordinateur' conçu seulement pour faire des calculs
+++

+++activité individuel 15 ../activites/applications-_directes/additions-_de-_nombres-_binaires.md "Additions de nombres binaires"

---

+++bulle matthieu
  pour les personnes arrivées ici avant la fin de la séance, créez les fonctions suivantes (après avoir créé au préalable un dossier du jour)
+++

- **base2ToBase10(nombreBinaire)** : une fonction qui prends un nombre binaire en paramètre et qui retourne un nombre décimal (sous forme de chaîne de caractère / string). Aidez vous de l'algorithme donné dans l'activité #0. Voici un exemple de début de cette fonction : 

```python
def base2ToBase10(nombreBinaire):
  assert isinstance(nombreBinaire, str) # pour des questions pratiques, nombreBinaire doit être une chaine de caractères
  listNombreBinaire = list(listNombreBinaire)
  
  # suite à compléter en s'aidant de l'algorithme donnée dans l'activité #0
  
print(base2ToBase10('10010110'))
```

- **base10ToBase2(nombreDecimal)** : une fonction qui prends un nombre décimal en paramètre et qui retourne un nombre binaire (sous forme de chaîne de caractère / string)
- **base2ToBase16(nombreBinaire)** : une fonction qui prends un nombre binaire en paramètre et qui retourne un nombre hexadécimal (sous forme de chaîne de caractère / string)
- **base16ToBase2(nombreHexadecimal)** : une fonction qui prends un nombre hexadecimal en paramètre et qui retourne un nombre binaire (sous forme de chaîne de caractère / string)

---

### +++emoji-write Résumons

- Les portes logiques, générées via des transistors, permettent de simuler des additions et des soustractions de nombres binaires
- En utilisant simplement des additions et des soustractions, un ordinateur est alors capable de faire des multiplications et des divisions binaires
- Il est possible de programmer des fonctions de conversion de base, il devient alors possible avec un ordinateur de faire des calculs avec des nombres décimaux