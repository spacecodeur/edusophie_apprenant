# Recherche de données dans une table

+++programme
  Importer une table depuis un fichier texte tabulé ou un fichier CSV
  Rechercher les lignes d’une table vérifiant des critères exprimés en logique propositionnelle
  Lire les éléments d’un tableau grâce à leurs index
  Itérer sur les éléments d’un tableau

  Ouvrir un fichier csv avec Python
  Lire les données d'un fichier csv avec Python
  Effectuer des recherches sur un fichier csv avec Python
+++

+++prof ce cours devrais durer entre 2 et 3 heures. Soit le dernier exo est à terminer à la maison, soit ce cours s'étale sur deux séances de deux heures. Ceci en fonction de l'avancée dans la progression annuelle

+++sommaire

## Les fichiers de données structurés 

### Introduction

Lors de communications entre ordinateurs, des données peuvent être échangées : 

- les scores des dernières rencontres sportives
- les articles achetés sur un site e-commerce
- les résultats de capteurs d'une mini-station météo personnelle
- ...

+++bulle matthieu
  à quoi pourraient ressembler les architectures <b class="text-red">client</b>-<b class="text-blue">serveur</b> et <b class="text-green">réseaux</b> des exemples ci-dessus ? 
+++

+++prof partie a améliorer ? élèves peu réceptifs

<div class="text-center">
+++spoil "rencontres sportives depuis une application mobile"
  +++image "/images/md/96GBDF347FCCF324A8D955A8284AFF"
spoil+++

+++spoil "achat d'article depuis son ordinateur personnel"
  +++image "/images/md/567FGEBFA85AB3C2H7DAB2F68C6BE3"
spoil+++

+++spoil "mini station météo installée dans son domicile"
  +++image "/images/md/GE2A3FE42588HB9C79ADEH436GAB3E"
spoil+++
</div>

---

Pour qu'un programme puisse traiter efficacement des données, il faut les structurer.

Imaginez vous en train d'appeler le pizzaiolo de votre quartier un samedi soir: 

+++bulle camille
  Bienvenue à la pizzeria "Yes, we Pizz'", que puis je faire pour vous ?
+++

+++bulle enzo droite
  Bonsoir ! Je vous appelle pour une commande pour une livraison 
+++

+++bulle camille droite
  Bien sur, que voulez vous dans votre commande ?
+++

+++bulle enzo droite
  Une pizza 4 fromages, et hmmmm... une bouteille d'eau. Puis une portion de frites s'il vous plaît aussi
+++

+++bulle camille
  C'est noté et ça fera 19 euros ! ça sera tout ? voulez vous de la sauce piquante avec votre pizza ?
+++

+++bulle enzo droite
  Ça sera tout oui ! et non merci pour la sauce piquante 
+++

Cette conversation contient un certain nombre de données, mais des données **non structurées**. Si à la place, vous aviez commandé depuis le site Web de la pizzeria, les données échangées entre le client (votre navigateur Web, votre smartphone, ...) et le serveur (le serveur qui héberge le site du pizzaiolo) seraient sous une forme structurées.

Les données peuvent être structurées dans des fichiers textes respectant différents formats, deux des formats les plus utilisés aujourd'hui sont le format **csv** et le format **json**

<div class="d-flex justify-content-evenly">
  <div>
    <div class="border border-dark p-2"><pre class="mb-0">id,produit,prix
1,pizza 4fro,13
2,eau,2
3,frites,4</pre></div>
  <p class="text-center">format <b>csv</b></p></div>
  <div>
    <div class="border border-dark p-2"><pre class="mb-0">[
  {
    "id": 1,
    "produit": "pizza 4fro",
    "prix": 13
  },
  {
    "id": 2,
    "produit": "eau",
    "prix": 2
  },
  {
    "id": 3,
    "produit": "frites",
    "prix": 4
  }
]</pre></div>
  <p class="text-center">format <b>json</b></p></div>
</div>

+++bulle matthieu
  chacun des formats présente des avantages et des inconvénients : taille du fichier, temps d'extraction d'une donnée, temps pour modifier une donnée, etc...
+++

+++bulle matthieu droite
  l'objet de cette séance est de se focaliser sur le format <b>csv</b>
+++

### Le format CSV

L'organisation de données sous forme d'une table est très répandue et est très ancienne. Cette organisation "en table" se retrouve dans les comptes d'Égypte ancienne et aujourd'hui dans nos logiciel tableurs (Excel, libreOffice, ...)

<div class="d-flex align-items-center justify-content-evenly">
+++image "/images/md/944HA2E784D3DE9C53FA25EF84E9E9" col-5
+++image "/images/md/DGA2H9C4AGCHAF86C5F6B7B45DDDAH" col-5
</div>

La structure du format csv (**C**omma-**S**eparated **V**alues) se calque complètement sur une organisation de données "en table". 

Un fichier .csv décrit une table contenant un ensemble de colonnes et de lignes. Les **colonnes** correspondent à des **catégories d'information** (une colonne peut stocker des numéros de téléphone, une autre des noms...) et les **lignes** à des **enregistrements**.

Remarque : les données dans un fichier .csv sont séparées par une virgule mais il est possible de choisir un autre caractère de séparation (tabulation, point virgule, espace, ...)


### +++emoji-write Résumons

- Lorsque les données sont structurées, leur traitement par des ordinateurs (au sens large) devient très efficace
- Différents formats de fichiers de données structurées existent, deux des plus connus sont le format csv et le format json
- Un fichier csv est structuré comme suit :

<div class="d-flex justify-content-center">
  <pre class="p-2 border border-dark">num;nom;type
1;bulbizarre;plante
4;salamèche;feu
7;carapuce;eau</pre>
</div>

<div class="d-flex justify-content-between col-10 mx-auto my-3">
  <div class="border border-dark">
    <table class="table table-sm table-borderless border-0">
      <tbody>
        <tr>
          <td class="text-red">num</td>
          <td>;</td>
          <td class="text-red">nom</td>
          <td>;</td>
          <td class="text-red">type</td>
        </tr>
        <tr>
          <td>1</td>
          <td>;</td>
          <td>bulbizarre</td>
          <td>;</td>
          <td>plante</td>
        </tr>
        <tr>
          <td>4</td>
          <td>;</td>
          <td>salamèche</td>
          <td>;</td>
          <td>feu</td>
        </tr>
        <tr>
          <td>7</td>
          <td>;</td>
          <td>carapuce</td>
          <td>;</td>
          <td>eau</td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="w-100 py-1 px-4 align-self-center">
    La première ligne du fichier contient les <b class="text-red">descripteurs des données</b>. Dans cet exemple, un pokémon a un <span class="text-red">num</span> (numéro), un <span class="text-red">nom</span> et un <span class="text-red">type</span>
  </div>
</div>

<div class="d-flex justify-content-between col-10 mx-auto mb-3">
  <div class="w-100 py-1 px-4 align-self-center">
    Une <b class="text-green">colonne</b> contient les différentes valeurs que peut prendre un <b class="text-red">descripteur</b>. Dans cet exemple, seul les types <span class="text-green">plante feu</span> et <span class="text-green">eau</span> sont utilisés dans notre table. 
  </div>
  <div class="border border-dark">
    <table class="table table-sm table-borderless border-0">
      <tbody>
        <tr>
          <td>num</td>
          <td>;</td>
          <td>nom</td>
          <td>;</td>
          <td class="text-red">type</td>
        </tr>
        <tr>
          <td>1</td>
          <td>;</td>
          <td>bulbizarre</td>
          <td>;</td>
          <td class="text-green">plante</td>
        </tr>
        <tr>
          <td>4</td>
          <td>;</td>
          <td>salamèche</td>
          <td>;</td>
          <td class="text-green">feu</td>
        </tr>
        <tr>
          <td>7</td>
          <td>;</td>
          <td>carapuce</td>
          <td>;</td>
          <td class="text-green">eau</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>

<div class="d-flex justify-content-between col-10 mx-auto mb-3">
  <div class="border border-dark">
    <table class="table table-sm table-borderless border-0">
      <tbody>
        <tr>
          <td>num</td>
          <td>;</td>
          <td>nom</td>
          <td>;</td>
          <td>type</td>
        </tr>
        <tr>
          <td>1</td>
          <td>;</td>
          <td>bulbizarre</td>
          <td>;</td>
          <td>plante</td>
        </tr>
        <tr>
          <td class="text-blue">4</td>
          <td>;</td>
          <td class="text-blue">salamèche</td>
          <td>;</td>
          <td class="text-blue">feu</td>
        </tr>
        <tr>
          <td>7</td>
          <td>;</td>
          <td>carapuce</td>
          <td>;</td>
          <td>eau</td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="w-100 py-1 px-4 align-self-center">
    Une <b class="text-blue">ligne</b> contient les données d'un enregistrement (ou entrée) dans la table. Dans cet exemple, la deuxième entrée dans le fichier contient les données liées à <span class="text-blue">salamèche</span> qui est le pokémon numéro <span class="text-blue">4</span> et qui est du type <span class="text-blue">feu</span>
  </div>
</div>

## Les limites des logiciels tableurs

Un fichier csv pour être ouvert et manipulé sans difficulté avec un logiciel type 'tableur'. Cependant, cette approche peut présenter deux défauts majeurs :

- nous sommes liés à la logique du logiciel (possibilités de recherche de données, d'insertion, ...)
- ces mêmes logiciels, aux interfaces graphiques riches, ne supportent pas les fichiers de données volumineux

Lorsque le fichier de données devient volumineux et/ou lorsque nous souhaitons effectuer des manipulations avancées sur les données, le logiciel ne suffit plus et nous avons alors recours à un langage de programmation.

+++bulle matthieu
  si je devais stocker une ligne dans une variable, quel type de variable serait le plus adapté ?
+++

+++activité binome 50 ../activites/applications-_directes/quelques-_piqures-_de-_rappel-_Python.md "Quelques piqûres de rappel Python"

### Lire et afficher des données d'un fichier csv avec Python

Il est possible de parcourir un fichier csv de différentes manières en python. Dans ce cours, je vous montre la manière la plus générique possible de le faire. Générique dans le sens : facilement réutilisable pour un autre langage de programmation.

Imaginons un fichier nommé **achats.csv** dans lequel se trouvent toutes les informations liées à une série d'achat sur un site e-commerce : 

```markdown
id,produit,prix,quantité
0,fauteuil,499,1
1,souris,60,1
2,écran,70,3
3,clavier,99,1
4,rubik cube,20,1
```

Pour parcourir le contenu de ce fichier en python, nous pouvons procéder comme suit : 

```python
achats = open('achats.csv','r',encoding='utf8')

for achat in achats:
  # ...
```

La variable 'achat' est une variable automatiquement créée par python. Et cette variable contient à chaque tour de la boucle `for` la ligne suivante du fichier.

+++bulle matthieu
  que va afficher le code suivant dans la console ?
+++

+++todo : a améliorer, les élèves ne trouvent pas la réponse ici lorsque la question est posée à l'oral

```python
achats = open('achats.csv','r',encoding='utf8')

for achat in achats:
  print('debut boucle')
  print(achat)
```

+++spoil

```markdown
debut boucle
id,produit,prix,quantité
debut boucle
0,fauteuil,499,1
debut boucle
1,souris,60,1
debut boucle
2,écran,70,3
debut boucle
3,clavier,99,1
debut boucle
4,rubik cube,20,1
```

spoil+++

+++activité binome 30 ../activites/applications-_directes/lecture-_d--__un-_fichier-_csv.md "titre de l'activité"

+++prof pour les élèves en avance : donner des liens vers des fichiers CSV accessibles sur Internet et leur demander de créer leurs propres stats 

### +++emoji-write Résumons

- Lorsque les données deviennent conséquentes, les logiciels type 'tableur' deviennent inopérants
- En python, la fonction `open` permet d'ouvrir et de stocker un fichier dans une variable
- En python, nous pouvons facilement en peu de lignes de code parcourir un fichier csv : 

```python
fichier = open('chemin/vers/fichier.csv','r',encoding='utf8')
next(fichier)

for ligne in fichier:
  ligne_en_liste = ligne.split('#')
  print(ligne_en_liste[1]) # affiche la deuxième donnée de chaque lignes du fichier csv
```

<ul>
  <ul>
    <li>la fonction <code>next</code> permet de sauter la ligne courante (par défaut. la première ligne du fichier est la ligne courante)</li>
    <li>la fonction <code>split</code> permet de transformer une chaîne de caractère en un tableau selon un séparateur ('#' dans cet exemple)</li>
  </ul>
</ul>