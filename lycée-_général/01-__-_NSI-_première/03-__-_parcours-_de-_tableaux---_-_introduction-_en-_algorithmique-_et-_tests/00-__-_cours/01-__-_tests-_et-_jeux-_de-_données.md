# 01. Tests et jeux de données

+++programme
Décrire des postconditions sur les résultats
Utiliser des jeux de tests

Sécurisez vos fonctions en entrée et en sortie
Créer et utiliser un jeu de test pour tester une fonction
+++

+++sommaire 1

## L'informatique et le test

Un programme fonctionne rarement du premier coup. Le pire des cas est lorsque l'erreur ne se produit qu'occasionnellement ou que le programme va bien jusqu'au bout mais en faisant n'importe quoi. 

De tels cas ont pu entraîner dans l'histoire de l'informatique de graves accidents impliquant vies humaines et matériel.

La part dédiée aux tests est donc aujourd'hui très importante dans l'industrie informatique. Les tests peuvent être effectués **manuellement**, mais aussi être fait **automatiquement** avec des programmes d'exécution de tests automatiques. 

Il existe un grand nombre de types de tests : 

- Les **tests fonctionnels** qui permettent de vérifier si une fonctionnalité d'une application fonctionne (exemple : l'authentification d'un utilisateur sur un site)
- Les **tests de charge** ou de performance qui permettent de vérifier si l'application fonctionnera si beaucoup d'utilisateurs la sollicite
- Les **tests de sécurité** qui permettent de vérifier la sécurité d'une application
- ...

## Les tests unitaires

En NSI au lycée, nous nous focalisons sur les **tests unitaires**. Un test unitaire est un type de test qui **vérifie si une partie précise** d'un programme fonctionne comme on s'y attend.

Un programme peut être découpé **en fonctions**, et ce sont elles qui sont la cible des tests unitaires.

### Vérifier les paramètres d'entrée d'une fonction

Voici un exemple de fonction python

widget python
def attaque(perso1, perso2, degat):
  print(perso1 + ' attaque ' + perso2 + ' et inflige ' + str(degat) + ' dégat(s) !')

personnageJoueur = 'genos'
attaque(personnageJoueur, 'saitama', 1)
widget

+++bulle matthieu
imaginons que cette fonction fait partie d'un très grand programme et sur lequel plusieurs développeurs travaillent
+++

+++bulle matthieu droite
comment s'assurer que les paramètres donnés en entrée sont bien du type attendu ?
+++

<div class="text-center">

+++spoil
widget python
def attaque(perso1, perso2, degat):
  assert isinstance(perso1, str) 
  assert isinstance(perso2, str) 
  assert isinstance(degat, int)
  
  assert degat >= 0
  
  print(perso1 + ' attaque ' + perso2 + ' et inflige ' + str(degat) + ' dégat(s) !')

personnageJoueur = 'genos'
attaque(personnageJoueur, 'saitama', 0)
# ...
attaque(personnageJoueur, 'saitama', 'poing')
widget
spoil+++

</div>

### Vérifier le résultat en sortie d'une fonction

Il est possible de tester les paramètres d'entrée que reçoit une fonction, mais il est également possible de tester le resultat que retourne une fonction.

widget python
from random import choice

def pileOuFace():
  return choice(["pile", "face"])

print(pileOuFace())
widget

+++bulle matthieu
quelles vérifications peut on faire sur le résultat que retourne la fonction pileOuFace() ?
+++

+++spoil

<ul>
  <li>nous pouvons vérifier que la fonction retourne bien une donnée de type 'chaîne de caractères' (+++flag-us <code>str</code> ou <code>string</code>)</li>
  <li>nous pouvons vérifier que la fonction retourne soit 'pile' soit 'face'</li>
</ul>

widget python
from random import choice

def pileOuFace():
  return choice(["pile", "face"])

resultat = pileOuFace()

assert isinstance(resultat, str)
assert resultat == 'pile' or resultat == 'face'

print(resultat)
widget

spoil+++

+++activité binome 15 ../01-__-_activites/00-__-_applications-_directes/securiser-_une-_fonction.md "Sécuriser une fonction"

## Améliorer la fiabilité d'une application avec un jeu de tests

Nous avons vu comment tester le resultat d'une fonction, voici comme nouvel exemple une fonction qui permet d'obtenir la division entière entre deux nombres :  

```python
def divisionEntiere(dividende, diviseur):
  return int(dividende / diviseur)

resultat = divisionEntiere(20, 6)
print(resultat)
```

Nous sécurisons ensuite les paramètres d'entrée de la fonction

```python
def divisionEntiere(dividende, diviseur):
  assert isinstance(dividende, int) or isinstance(dividende, float) 
  assert isinstance(diviseur, int) or isinstance(diviseur, float) 
  return int(dividende / diviseur)

resultat = divisionEntiere(20, 6)
print(resultat)
```

Nous sécurisons ensuite le résultat de sortie de la fonction

```
def divisionEntiere(dividende, diviseur):
  assert isinstance(dividende, int) or isinstance(dividende, float) 
  assert isinstance(diviseur, int) or isinstance(diviseur, float) 
  return int(dividende / diviseur)

resultat = divisionEntiere(20, 6)
assert isinstance(resultat, int)

print(resultat)
```

Le soucis par contre, est qu'on ne test la fonction que dans un cas particulier où le dividende est égal à 20 et le diviseur à 6.

Nous mettons alors en place un **jeu de test**. Dans le domaine du test, un **jeu de test** est un ensemble de tests vérifiant une partie spécifique d'un programme.

Imaginons le **jeu de test** suivant : 
- dans mon programme, je veux tester si la fonction `divisionEntiere` fonctionne bien avec le dividende **0** et le diviseur 7
- dans mon programme, je veux tester si la fonction `divisionEntiere` fonctionne bien avec le dividende **1** et le diviseur 7
- dans mon programme, je veux tester si la fonction `divisionEntiere` fonctionne bien avec le dividende **2** et le diviseur 7
- ...
- dans mon programme, je veux tester si la fonction `divisionEntiere` fonctionne bien avec le dividende **10** et le diviseur 7

Nous pourrions modifier à la main plusieurs appels de la fonctions `divisionEntiere` mais cela s'avère très peu pratique. Heureusement, la programmation nous permet d'automatiser facilement des tests : 

widget python
def divisionEntiere(dividende, diviseur):
  assert isinstance(dividende, int) or isinstance(dividende, float) 
  assert isinstance(diviseur, int) or isinstance(diviseur, float) 
  return int(dividende / diviseur)

dividendes = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
for dividende in dividendes:
  resultat = divisionEntiere(dividende, 7)
  assert isinstance(resultat, int)
  print(resultat)
widget

+++activité binome 30 ../01-__-_activites/00-__-_applications-_directes/tests-_et-_jeu-_de-_test.md "Tests et jeu de test"

## Résumons +++emoji-write 

- Un test unitaire est un test qui vérifie si une partie précise d'un programme produit bien le résultat attendu
- Nous pouvons sécuriser une fonction en vérifiant ses paramètres d'entrée (préconditions) et également en vérifiant le résultat qu'elle renvoie (postconditions)
- Un jeu de test est un ensemble de tests permettant de vérifier la qualité d'un programme