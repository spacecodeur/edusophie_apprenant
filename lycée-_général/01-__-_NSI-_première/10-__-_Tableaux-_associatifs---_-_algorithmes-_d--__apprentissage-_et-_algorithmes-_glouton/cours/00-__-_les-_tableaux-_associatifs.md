# Les tableaux associatifs

+++programme
  Construire une entrée de dictionnaire
  Itérer sur les éléments d’un dictionnaire

  Différencier tableau séquentiel et dictionnaire
  Construire une entrée de dictionnaire
  Parcourir les éléments d’un dictionnaire
+++

+++sommaire 3

<style>
  h4{font-weight:1000;}
</style>

## Introduction

+++bulle matthieu
  en programmation, nous sommes souvent amenés à créer, lire, modifier et supprimer des données
+++

+++bulle matthieu droite
  ces opérations récurentes sont décrites à travers l'acronyme <b class="text-blue">CRUD</b> (<b class="text-blue">C</b>reate, <b class="text-blue">R</b>ead, <b class="text-blue">U</b>pdate, <b class="text-blue">D</b>elete)
+++

+++activité classe 40 ../activites/applications-_directes/rappels-_sur-_les-_tableaux-_sequentiels.md "Rappels sur les tableaux séquentiels"

## Les tableaux associatifs

+++bulle matthieu
  petit point <b>vocabulaire</b>, un tableau associatif (+++flag-us associative array), un tableau clé-valeur (+++flag-us key-value array) ou encore un dictionnaire représentent la même chose
+++

Jusqu'à maintenant, les tableaux que nous avons manipulés sont des tableaux **séquentiels** : 

- Toutes les données d'un tableau sont rangées les unes après les autres
- Nous accédons à une données spécifique à travers son **index** dans le tableau, dit autrement, à travers sa **position** dans le tableau

Voici un exemple pour rappel : 

widget python
nombres_premiers = [2, 3, 5, 7, 11]

print('le premier nombre premier est : ' + str(nombres_premiers[0]))
# le premier nombre premier est stocké à l'index 0 du tableau

print('le troisième nombre premier est : ' + str(nombres_premiers[2]))
# le troisième nombre premier est stocké à l'index 2 du tableau
widget

Il existe un autre type de tableau, dit tableau **associatif** (ou tableau clé-valeur ou dictionnaire), qui présente 2 principaux avantages par rapport à un tableau séquentiel : 

- sa structure et son utilisation rend le code plus lisible
- les manipulations d'un tableau associatif (opérations <b class="text-blue">CRUD</b>) sont de complexités en général meilleures que les tableaux séquentiels (nous ne rentrerons pas plus en détail sur ce point)

### Création (<b>C</b>RUD)

Voici un exemple de création d'un tableau associatif : 

widget python

# exemple de création de dictionnaire vide
dictionnaire_vide = {}

# exemple de création de dictionnaire avec des données
monstres_points_de_vie = {
  'poring' : 17,
  'squelette pirate' : 41,
  'fantôme' : 38
}

print(monstres_points_de_vie)

# l'ordre des éléments dans un tableau associatifs n'a pas d'importance
# ainsi, la variable ci-dessous est strictement égale à la variable 'monstres_points_de_vie'
copie_monstres_points_de_vie = {
  'squelette pirate' : 41,
  'poring' : 17,
  'fantôme' : 38
}

assert copie_monstres_points_de_vie == monstres_points_de_vie

widget

+++bulle matthieu
  modifiez les points de vies d'un fantôme dans la variable <code>monstres_points_de_vie</code> et exécutez le code. Que se passe-t'il ?
+++

+++bulle matthieu droite
  ajoutez un nouveau monstre dans la variable <code>monstres_points_de_vie</code>
+++

### Lecture (C<b>R</b>UD) et parcours de dictionnaire

Un tableau associatif contient un ensemble d'éléments décrits sous la forme de clé->valeur : 

- dans un tableau séquentiel, nous accédons à un élément via son index
- dans un tableau associatif, nous accédons à un élément via sa clé 

<table class="text-center">
  <thead>
    <tr>
      <th colspan="2" class="px-2">monstres_points_de_vie</th>
    </tr>
    <tr>
      <th class="text-blue px-2">clé</th>
      <th class="text-green px-2">valeur</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="text-blue">poring</td>
      <td class="text-green">34</td>
    </tr>
    <tr>
      <td class="text-blue">squelette pirate</td>
      <td class="text-green">41</td>
    </tr>
    <tr>
      <td class="text-blue">fantôme</td>
      <td class="text-green">38</td>
    </tr>
  </tbody>
</table>
  
+++bulle matthieu
  il est possible de créer un dictionnaire de dictionnaires, un dictionnaire de tableaux séquentiels, un tableau séquentiel de dictionnaires, des dictionnaires de dictionnaires de dictionnaires, etc...
+++

Revenons à notre tableau de monstres, voici comment accéder aux données contenues dans notre tableau associatif : 

widget python

monstres_points_de_vie = {
  'poring' : 17,
  'squelette pirate' : 41,
  'fantôme' : 38
}

print(monstres_points_de_vie['poring'])

widget

+++bulle matthieu
  que se passe-t'il si on essaie d'afficher les points de vie d'un poring en utilisant <code>print(monstres_points_de_vie[0])</code> ?
+++

+++bulle matthieu droite
  comment afficher les points de vie d'un fantôme ?
+++

Et voici comment parcourir chaque éléments contenus dans un tableau associatif : 

widget python

monstres_points_de_vie = {
  'poring' : 17,
  'squelette pirate' : 41,
  'fantôme' : 38
}

for cle in monstres_points_de_vie:
  print(monstres_points_de_vie[cle])

widget

+++bulle matthieu
  la console n'affiche actuellement que les points de vie. Améliorez le contenu de la boucle <code>for</code> afin d'afficher à la fois le monstre et ses points de vie
+++

+++bulle matthieu droite
  modifiez le programme afin d'afficher seulement les monstres dont les points de vie sont supérieurs à 25
+++

+++spoil "Aide"
  vous aurez besoin d'utiliser l'instruction <code>if</code> dans la boucle <code>for</code>
spoil+++

### Modification (CR<b>U</b>D)

Dans un tableau associatif, nous pouvons modifier les clés et les valeurs. 

Voici un exemple de dictionnaire : 

widget python
annuaire = {
  '0715427819' : 'jean',
  '0677997722' : 'michelle',
  '0711221144' : 'emma'
}

# Code à ajouter en dessous 

widget

+++bulle matthieu
  en vous aidant de recherches sur Internet, quel code faut-il ajouter à partir de la ligne 8 pour modifier le prénom de 'jean' en 'jeanne' ?
+++

+++bulle matthieu
  toujours en vous aidant de recherches sur Internet, quel code faut-il ajouter à partir de la ligne 8 pour modifier le numéro de michelle ? (<b>note</b> : le changement de clé d'un dictionnaire sera très peu utilisé au lycée)
+++

+++spoil "Aide"
  Pour vos recherches : nous sommes actuellement en train de faire du <b>python</b> et nous cherchons à <b>changer</b> une <b>valeur</b> dans un <b>dictionnaire</b> ou à <b>renommer</b> une <b>clé</b> dans un <b>dictionnaire</b>
spoil+++

+++spoil

widget python
annuaire = {
  '0715427819' : 'jean',
  '0677997722' : 'michelle',
  '0711221144' : 'emma'
}

# Code à ajouter en dessous 

## changer une valeur : changer le prénom (jean => jeanne)
annuaire['0715427819'] = 'jeanne'

## renommer une clé : changer le numéro de téléphone de michelle ('0677997722' => '0700000001')
annuaire['0700000001'] = 'michelle' # on ajoute une nouvelle entrée dans notre annuaire
annuaire.pop('0677997722') # on supprime l'entrée dont la clé est '0677997722', l'ancien numéro de michelle

print(annuaire)

widget

spoil+++

### Quelques fonctions python usuelles

- <code>keys()</code> : permet d'extraire les clés d'un dictionnaire
- <code>values()</code> : permet d'extraire les valeurs d'un dictionnaire

Exemples : 

widget python

annuaire = {
  '0715427819' : 'jean',
  '0677997722' : 'michelle',
  '0711221144' : 'emma'
}

print(annuaire.keys())
print(annuaire.values())
widget

+++activité binome 45 ../activites/applications-_directes/tableau-_associatif-_et-_loterie.md "Tableau associatif et loterie"

## +++emoji-write Résumé 

- tableau associatif = tableau clé-valeur = dictionnaire
- un tableau associatif contient un ensemble de données décrites sous la forme de <b class="text-blue">clé</b>-<b class="text-green">valeur</b>
    - toutes les <b class="text-blue">clés</b> dans un tableau associatif <b class="text-red">doivent être uniques</b>. Une <b class="text-blue">clé</b> <b class="text-red">doit être</b> un nombre ou une chaîne de caractères
    - nous accédons aux informations d'un élément d'un tableau associatif grace à sa <b class="text-blue">clé</b>, un élément représente une <b class="text-blue">clé</b> et sa <b class="text-green">valeur</b>
    - une <b class="text-green">valeur</b> peut une donnée simple (un nombre, une chaine de caractère, ...) mais aussi une donnée plus complexe (un sous-tableau séquentiel, un sous-tableau associatif, ...)
    - nous utilisons les **accolades {}** avec un tableau associatif (et non **les crochets []** qui eux sont utilisés par un tableau séquentiel)
- en règle générale : 
    - par rapport à l'utilisateur de tableau séquentiel, utiliser un tableau associatif facilite davantage la lisibilité du code d'un programme
    - manipuler un tableau associatif est algorithmiquement moins complexe qu'un tableau séquentiel