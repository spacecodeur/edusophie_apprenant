# Rappels sur les tableaux séquentiels

+++consigne
  Cette activité évolue étape par étape en classe entière
+++

+++sommaire

<hr>

Cette piqure de rappel reprend différents points abordés pendant l'année illustrés à travers un cas pratique.

+++bulle matthieu
  dîtes bonjour à Alexandre !
+++

+++bulle alexandre droite
  c'est moi !
+++

Alexandre partage ces recettes de cuisines japonaises sur sa chaîne hébergée depuis une plateforme de streaming. Pour animer ses lives, Àlexandre aimerait organiser une loterie parmis ses viewers, le gagnant recevrait par exemple le livre de cuisine d'Alexandre dédicacé. 

Alexandre souhaite créer un système de loterie élaboré et qui répond précisemment à ce dont il a besoin. Il décide alors de se créer un programme sur mesure. Alexandre a quelques notions en **python** et en **javascript** mais aura besoin d'un coup de main !

+++bulle alexandre
  j'ai créé ce début de programme qui permet de choisir l'action à faire lors de l'exécution du programme
+++

widget python
#V-- imports --V#

import random # bibliothèque qui sera nécessaire pour la loterie

#V-- variables générales --V#

emails = []

#V-- définition des fonctions nécessaires --V#

def afficher(emails): # affiche tous les emails
  pass
  
def supprimer(emails, email_a_supprimer): # supprime un email
  emails_a_jour = []
  for email in emails:
    if(email != email_a_supprimer):
      emails_a_jour.append(email)
  return emails_a_jour

def loterie(emails): # déclenche une loterie
  pass

#V-- coeur du programme --V#

while(True): # boucle infinie
  choix = input('Choisir une action : afficher | supprimer | loterie')
  if(choix == 'afficher'):
    afficher(emails)
  elif(choix == 'supprimer'):
    email_a_supprimer = input('quel email supprimer ?')
    emails = supprimer(emails, email_a_supprimer)
  elif(choix == 'loterie'):
    loterie(emails)
  else:
    print("ce choix n'existe pas ! fin du programme")
    break # on sort de la boucle infinie
widget

+++bulle matthieu
analysez/testez cette première version du programme
+++

## Les tableaux à une dimension

### Création (<b>C</b>RUD)

+++bulle alexandre
  voici la liste des mails de mes viewers habituels
+++

```text
hellfire@optonline.net
jeffcovey@outlook.com
smeier@icloud.com
eurohack@sbcglobal.net
duncand@yahoo.ca
drjlaw@gmail.com
paina@live.com
```

+++bulle matthieu
  complétez ci-dessous le contenu de la variable <code>emails</code> à la ligne 5 en y ajoutant les emails donnés par Alexandre
+++

widget python
#V-- imports --V#

import random # bibliothèque qui sera nécessaire pour la loterie

#V-- variables générales --V#

emails = []

#V-- définition des fonctions nécessaires --V#

def afficher(emails): # affiche tous les emails
  pass
  
def supprimer(emails, email_a_supprimer): # supprime un email
  emails_a_jour = []
  for email in emails:
    if(email != email_a_supprimer):
      emails_a_jour.append(email)
  return emails_a_jour

def loterie(emails): # déclenche une loterie
  pass

#V-- coeur du programme --V#

while(True): # boucle infinie
  choix = input('Choisir une action : afficher | supprimer | loterie')
  if(choix == 'afficher'):
    afficher(emails)
  elif(choix == 'supprimer'):
    email_a_supprimer = input('quel email supprimer ?')
    emails = supprimer(emails, email_a_supprimer)
  elif(choix == 'loterie'):
    loterie(emails)
  else:
    print("ce choix n'existe pas ! fin du programme")
    break # on sort de la boucle infinie
widget

+++spoil "Aide"
  +++lien "/apprenant/01-__-_NSI-_premiere/02-__-_introduction-_aux-_tableaux-_et-_programmation-_Web-_cote-_client/00-__-_cours/00-__-_introduction-_aux-_tableaux.md" "lien vers le cours d'introduction sur les tableaux"
spoil+++

### Lecture (C<b>R</b>UD)

+++bulle alexandre
  lors de mes lives, je souhaite pouvoir retirer de la liste des viewers un viewer si je le souhaite
+++

+++bulle alexandre droite
  si par exemple un de mes viewers dit 'chocolatine' à la place de 'pain au chocolat', il est hors de question de le faire participer à la loterie !
+++

+++bulle alexandre droite
  j'ai déjà codé la fonction <code>supprimer</code> qui permet de retirer une adresse email mais j'aimerai vérifier si elle fonctionne...
+++

+++bulle matthieu
  après avoir supprimé un email, nous allons donc afficher tous les emails restants
+++

+++bulle matthieu droite
  complétez ci-dessous le contenu de la fonction <code>afficher</code> afin d'afficher tous les mails contenus dans la variable <code>emails</code> (pensez à retirer l'instruction <code>pass</code> du contenu de la fonction <code>afficher</code>)
+++

widget python
#V-- imports --V#

import random # bibliothèque qui sera nécessaire pour la loterie

#V-- variables générales --V#

emails = ['hellfire@optonline.net','jeffcovey@outlook.com','smeier@icloud.com','eurohack@sbcglobal.net','duncand@yahoo.ca','drjlaw@gmail.com','paina@live.com']

#V-- définition des fonctions nécessaires --V#

def afficher(emails): # affiche tous les emails
  pass
  
def supprimer(emails, email_a_supprimer): # supprime un email
  emails_a_jour = []
  for email in emails:
    if(email != email_a_supprimer):
      emails_a_jour.append(email)
  return emails_a_jour

def loterie(emails): # déclenche une loterie
  pass

#V-- coeur du programme --V#

while(True): # boucle infinie
  choix = input('Choisir une action : afficher | supprimer | loterie')
  if(choix == 'afficher'):
    afficher(emails)
  elif(choix == 'supprimer'):
    email_a_supprimer = input('quel email supprimer ?')
    emails = supprimer(emails, email_a_supprimer)
  elif(choix == 'loterie'):
    loterie(emails)
  else:
    print("ce choix n'existe pas ! fin du programme")
    break # on sort de la boucle infinie
widget

+++spoil "Aide"
  +++lien "/apprenant/01-__-_NSI-_premiere/03-__-_parcours-_de-_tableaux---_-_introduction-_en-_algorithmique-_et-_tests/00-__-_cours/00-__-_introduction-_en-_algorithmique---_-_parcours-_de-_tableaux.md" "lien vers le cours sur le parcours de tableau"
spoil+++

### Modification (CR<b>U</b>D)

+++bulle alexandre
  il est possible qu'en cours de live, un utilisateur souhaite changer d'email pour le mettre à jour avant que je déclenche la loterie
+++

+++bulle matthieu
  définissez ci-dessous une nouvelle fonction nommée <code>modifier</code> (ajoutez aussi le choix 'modifier' dans le coeur du programme)
+++

+++bulle matthieu droite
  cette fonction prends trois paramètres en entrée : tous les emails (stockées dans <code>emails</code>), l'email à modifier et la nouvelle valeur qui doit remplacer l'email à modifier
+++

widget python
#V-- imports --V#

import random # bibliothèque qui sera nécessaire pour la loterie

#V-- variables générales --V#

emails = ['hellfire@optonline.net','jeffcovey@outlook.com','smeier@icloud.com','eurohack@sbcglobal.net','duncand@yahoo.ca','drjlaw@gmail.com','paina@live.com']

#V-- définition des fonctions nécessaires --V#

def afficher(emails): # affiche tous les emails
  for email in emails:
    print(email)
  
def supprimer(emails, email_a_supprimer): # supprime un email
  emails_a_jour = []
  for email in emails:
    if(email != email_a_supprimer):
      emails_a_jour.append(email)
  return emails_a_jour

def loterie(emails): # déclenche une loterie
  pass

#V-- coeur du programme --V#

while(True): # boucle infinie
  choix = input('Choisir une action : afficher | supprimer | loterie')
  if(choix == 'afficher'):
    afficher(emails)
  elif(choix == 'supprimer'):
    email_a_supprimer = input('quel email supprimer ?')
    emails = supprimer(emails, email_a_supprimer)
  elif(choix == 'loterie'):
    loterie(emails)
  else:
    print("ce choix n'existe pas ! fin du programme")
    break # on sort de la boucle infinie
widget

## Les tableaux à deux dimensions

### Création (<b>C</b>RUD)

+++bulle alexandre
  en début de live, j'invite mes viewers à choisir un nombre entre 0 et 10
+++

+++bulle alexandre droite
  la loterie fera gagner des lots aux viewers qui ont choisi le bon nombre
+++

+++bulle matthieu
  commencez par modifier ci-dessous la structure de la variable <code>emails</code>
+++

+++bulle matthieu droite
  la variable <code>emails</code> doit être renommée par <code>viewers</code> et doit maintenant contenir un ensemble de sous-tableaux : chaque sous-tableau doit contenir les informations d'un viewer (son email, puis, le chiffre qu'il a choisi)
+++

widget python
#V-- imports --V#

import random # bibliothèque qui sera nécessaire pour la loterie

#V-- variables générales --V#

emails = ['hellfire@optonline.net','jeffcovey@outlook.com','smeier@icloud.com','eurohack@sbcglobal.net','duncand@yahoo.ca','drjlaw@gmail.com','paina@live.com']

#V-- définition des fonctions nécessaires --V#

def modifier(emails, email_ancien, email_nouveau):
  index = 0
  for email in emails:
    if(email == email_ancien):
      emails[index] = email_nouveau
    index = index + 1
  return emails

def afficher(emails): # affiche tous les emails
  for email in emails:
    print(email)
  
def supprimer(emails, email_a_supprimer): # supprime un email
  emails_a_jour = []
  for email in emails:
    if(email != email_a_supprimer):
      emails_a_jour.append(email)
  return emails_a_jour

def loterie(emails): # déclenche une loterie
  pass

#V-- coeur du programme --V#

while(True): # boucle infinie
  choix = input('Choisir une action : afficher | supprimer | modifier | loterie ')
  if(choix == 'afficher'):
    afficher(emails)
  elif(choix == 'supprimer'):
    email_a_supprimer = input('quel email supprimer ?')
    emails = supprimer(emails, email_a_supprimer)
  elif(choix == 'loterie'):
    loterie(emails)
  elif(choix == 'modifier'):
    emails = modifier(emails, input('quel email modifier ?'), input('quelle est la nouvelle valeur ?'))
  else:
    print("ce choix n'existe pas ! fin du programme")
    break # on sort de la boucle infinie
widget

+++spoil "Aide"
  +++lien "/apprenant/01-__-_NSI-_premiere/09-__-_tableaux-_a-_2-_dimensions-_et-_systemes-_d--__encodage/cours/00-__-_les-_tableaux-_a-_2-_dimensions.md" "lien vers le cours sur les tableaux à 2 dimensions"
spoil+++

### Lecture (C<b>R</b>UD)

+++bulle matthieu
  actuellement, la fonction <code>afficher</code> affiche le contenu du tableau <code>viewers</code> comme ceci : 
+++

```text
['hellfire@optonline.net', '7']
['jeffcovey@outlook.com', '1']
['smeier@icloud.com', '1']
['eurohack@sbcglobal.net', '4']
['duncand@yahoo.ca', '9']
['drjlaw@gmail.com', '8']
['paina@live.com', '1']
```

+++bulle matthieu
  améliorez ci-dessous la fonction <code>afficher</code> afin d'obtenir un affichage plus explicite
+++

+++bulle matthieu droite
  voici l'affichage attendu après amélioration de la fonction <code>afficher</code>
+++

```text
'hellfire@optonline.net' a choisi le numéro 7 !
'jeffcovey@outlook.com' a choisi le numéro 1 !
'smeier@icloud.com' a choisi le numéro 1 !
'eurohack@sbcglobal.net' a choisi le numéro 4 !
'duncand@yahoo.ca' a choisi le numéro 9 !
'drjlaw@gmail.com' a choisi le numéro 8 !
'paina@live.com' a choisi le numéro 1 !
```

widget python
#V-- imports --V#

import random # bibliothèque qui sera nécessaire pour la loterie

#V-- variables générales --V#

viewers = [
  ['hellfire@optonline.net', '7'],
  ['jeffcovey@outlook.com','1'],
  ['smeier@icloud.com','1'],
  ['eurohack@sbcglobal.net','4'],
  ['duncand@yahoo.ca','9'],
  ['drjlaw@gmail.com','8'],
  ['paina@live.com','1']
]

#V-- définition des fonctions nécessaires --V#

def modifier(emails, email_ancien, email_nouveau):
  index = 0
  for email in emails:
    if(email == email_ancien):
      emails[index] = email_nouveau
    index = index + 1
  return emails

def afficher(viewers): # affiche les informations de tous les viewers
  for viewer in viewers:
    print(viewer)
  
def supprimer(emails, email_a_supprimer): # supprime un email
  emails_a_jour = []
  for email in emails:
    if(email != email_a_supprimer):
      emails_a_jour.append(email)
  return emails_a_jour

def loterie(emails): # déclenche une loterie
  pass

#V-- coeur du programme --V#

while(True): # boucle infinie
  choix = input('Choisir une action : afficher | supprimer | modifier | loterie ')
  if(choix == 'afficher'):
    afficher(viewers)
  elif(choix == 'supprimer'):
    email_a_supprimer = input('quel email supprimer ?')
    emails = supprimer(emails, email_a_supprimer)
  elif(choix == 'loterie'):
    loterie(emails)
  elif(choix == 'modifier'):
    emails = modifier(emails, input('quel email modifier ?'), input('quelle est la nouvelle valeur ?'))
  else:
    print("ce choix n'existe pas ! fin du programme")
    break # on sort de la boucle infinie
widget

+++spoil "Aide"
  +++lien "/apprenant/01-__-_NSI-_premiere/09-__-_tableaux-_a-_2-_dimensions-_et-_systemes-_d--__encodage/cours/00-__-_les-_tableaux-_a-_2-_dimensions.md" "lien vers le cours sur les tableaux à 2 dimensions"
spoil+++

### Modification (CR<b>U</b>D)

+++bulle alexandre
  lors d'un live, je permet à mes viewers de modifier une fois le chiffre de la loterie qu'ils ont choisi
+++

+++bulle alexandre droite
  j'ai déjà adapté l'appel de la fonction <code>modifier</code> mais il reste le contenu de la fonction <code>modifier</code> à adapter !
+++

+++bulle matthieu
  améliorez ci-dessous le contenu de la fonction <code>modifier</code> afin de permettre à Alexandre de modifier le numéro choisi par un des viewers
+++

widget python
#V-- imports --V#

import random # bibliothèque qui sera nécessaire pour la loterie

#V-- variables générales --V#

viewers = [
  ['hellfire@optonline.net', '7'],
  ['jeffcovey@outlook.com','1'],
  ['smeier@icloud.com','1'],
  ['eurohack@sbcglobal.net','4'],
  ['duncand@yahoo.ca','9'],
  ['drjlaw@gmail.com','8'],
  ['paina@live.com','1']
]

#V-- définition des fonctions nécessaires --V#

def modifier(viewers, email, choix, nouvelle_valeur):
## ancienne version de la fonction modifier
# index = 0
# for email in emails:
#   if(email == email_ancien):
#     emails[index] = email_nouveau
#   index = index + 1
  return viewers

def afficher(viewers): # affiche les informations de tous les viewers
  for viewer in viewers:
    print("'" + viewer[0] + "' a choisi le numéro " + viewer[1] + " !")
  
def supprimer(emails, email_a_supprimer): # supprime un email
  emails_a_jour = []
  for email in emails:
    if(email != email_a_supprimer):
      emails_a_jour.append(email)
  return emails_a_jour

def loterie(emails): # déclenche une loterie
  pass

#V-- coeur du programme --V#

while(True): # boucle infinie
  choix = input('Choisir une action : afficher | supprimer | modifier | loterie ')
  if(choix == 'afficher'):
    afficher(viewers)
  elif(choix == 'supprimer'):
    email_a_supprimer = input('quel email supprimer ?')
    emails = supprimer(emails, email_a_supprimer)
  elif(choix == 'loterie'):
    loterie(emails)
  elif(choix == 'modifier'):
    viewers = modifier(viewers, input("quel est l'email de l'utilisateur concerné ?"), input('Quelle information modifier ? ( Choisir une action : email | numero )'), input('Quelle est la nouvelle valeur à enregistrer ?'))
  else:
    print("ce choix n'existe pas ! fin du programme")
    break # on sort de la boucle infinie
widget

+++spoil "Aide : <b>un exemple</b> d'algorithme que doit suivre le contenu de la fonction <code style="background-color:white;padding:1px 2px;border-radius:2px">modifier</code>"

```text
Fonction modifier: viewers, email, choix, nouvelle_valeur
  index ← 0
  Pour chaque viewer dans le tableau viewers
    Si viewer[0] == email
      Si choix == 'email'
        viewers[index][0] ← nouvelle_valeur
      Sinon 
        viewers[index][1] ← nouvelle_valeur
      Fin Si
    Fin Si
    index ← index + 1
  Fin Pour
Fin Fonction
```

spoil+++

+++spoil

widget python
#V-- imports --V#

import random # bibliothèque qui sera nécessaire pour la loterie

#V-- variables générales --V#

viewers = [
  ['hellfire@optonline.net', '7'],
  ['jeffcovey@outlook.com','1'],
  ['smeier@icloud.com','1'],
  ['eurohack@sbcglobal.net','4'],
  ['duncand@yahoo.ca','9'],
  ['drjlaw@gmail.com','8'],
  ['paina@live.com','1']
]

#V-- définition des fonctions nécessaires --V#

def modifier(viewers, email, choix, nouvelle_valeur):
  index = 0
  for viewer in viewers:
    if(viewer[0] == email):
      if(choix == 'email'):
        viewers[index][0] = nouvelle_valeur
      else:
        viewers[index][1] = nouvelle_valeur
    index += 1
  return viewers

def afficher(viewers): # affiche les informations de tous les viewers
  for viewer in viewers:
    print("'" + viewer[0] + "' a choisi le numéro " + viewer[1] + " !")
  
def supprimer(emails, email_a_supprimer): # supprime un email
  emails_a_jour = []
  for email in emails:
    if(email != email_a_supprimer):
      emails_a_jour.append(email)
  return emails_a_jour

def loterie(emails): # déclenche une loterie
  pass

#V-- coeur du programme --V#

while(True): # boucle infinie
  choix = input('Choisir une action : afficher | supprimer | modifier | loterie ')
  if(choix == 'afficher'):
    afficher(viewers)
  elif(choix == 'supprimer'):
    email_a_supprimer = input('quel email supprimer ?')
    emails = supprimer(emails, email_a_supprimer)
  elif(choix == 'loterie'):
    loterie(emails)
  elif(choix == 'modifier'):
    viewers = modifier(viewers, input("quel est l'email de l'utilisateur concerné ?"), input('Quelle information modifier ? ( Choisir une action : email | numero )'), input('Quelle est la nouvelle valeur à enregistrer ?'))
  else:
    print("ce choix n'existe pas ! fin du programme")
    break # on sort de la boucle infinie
widget

spoil+++

<hr>

## Vous êtes en avance ? 

- Modifier le reste du programme afin de remplacer les variables <code>emails</code> par <code>viewers</code>

puis : 

- Programmez le contenu de la fonction <code>loterie</code>

+++spoil "Aide : <b>un exemple</b> d'algorithme que doit suivre le contenu de la fonction <code style="background-color:white;padding:1px 2px;border-radius:2px">loterie</code>"

```text
Fonction loterie: viewers
  nombre ← Générer un nombre aléatoire entre 0 et 10
  Afficher : 'le bon numéro est ' + nombre
  Pour chaque viewer dans le tableau viewers
    Si viewer[1] == nombre
      Afficher : viewer[0] + 'à gagné un lot !'
    Fin Si
  Fin Pour
Fin Fonction
```

spoil+++

puis, au choix : 

0. Le nombre demandé est compris entre 0 et 10. Mais si le live est vu par des milliers de viewers, cela signifie que le nombre de lots à distribuer sera très conséquent ! Améliorez le programme afin de permettre d'effectuer une loterie entre le nombre 0 et le nombre X, où X sera une valeur à saisir au tout début du programme.
1. Un utilisateur peut modifier son numéro en cours de live. Si un live est visionné par des milliers de viewers, il sera difficile tout au long du live de se souvenir des viewers qui ont déjà modifié leur numéro. Améliorez le contenu de la variable <code>viewers</code> afin d'y stocker l'information qui dit si un viewer a déjà, ou non, modifié son numéro en cours de live. Adaptez ensuite la fonction <code>modifier</code> afin d'interdire la modification d'un numéro qui a déjà été modifié