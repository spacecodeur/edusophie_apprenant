# Les tableaux à deux dimensions

+++programme
  Utiliser des tableaux de tableaux pour représenter des matrices : notation a [i] [j]

  Créer des tableaux à deux dimensions
  Lire/Modifier éléments contenus dans un tableau à deux dimensions
+++

+++prof ce cours est prévu pour deux séances de deux heures

+++sommaire

## Rappels

+++bulle matthieu
  comment définiriez vous ce qu'est un tableau en programmation ?
+++

+++spoil
  Un tableau est un type de variable structuré qui contient un ensemble d'éléments
spoil+++

---

+++bulle matthieu
  améliorez le code ci-dessous
+++

widget python
sac = ['potion de vie', 'parchemin', 'lance']

print(sac)
widget

0. ajoutez un nouvel item dans le tableau sac
1. affichez le troisième item contenu dans le sac
2. à l'aide d'une boucle, affichez tous les items contenus dans le sac
3. (optionnel) créer un tableau nommé 'coffre' contenant 2 items. À l'aide d'une boucle, ajoutez chaque item contenu dans le coffre dans le sac

+++spoil "Aide"
  Les liens vers les cours précédents : +++lien "/apprenant/01-__-_NSI-_premiere/02-__-_introduction-_aux-_tableaux-_et-_programmation-_Web-_cote-_client/00-__-_cours/00-__-_introduction-_aux-_tableaux.md" "introduction aux tableaux" et +++lien "/apprenant/01-__-_NSI-_premiere/03-__-_parcours-_de-_tableaux---_-_introduction-_en-_algorithmique-_et-_tests/00-__-_cours/00-__-_introduction-_en-_algorithmique---_-_parcours-_de-_tableaux.md" "parcours de tableau". Vous pouvez bien sur également vous aider de recherches sur Internet
spoil+++

+++spoil
  widget python
# 0
sac = ['potion de vie', 'parchemin', 'lance', 'bouclier']

# ou alors : sac.append('bouclier')

# 1
print(sac[2])
print()

# 2
for item in sac:
  print(item)
print()

# 3
coffre = ['baguette', 'lance']

for item in coffre:
  sac.append(item)

for item in sac:
  print(item)
  
  widget
spoil+++

## Les tableaux à deux dimensions : introduction

### Construire un tableau à deux dimensions

+++bulle matthieu
  petit <b>point vocabulaire</b>, en programmation un tableau de tableaux, un tableau à deux dimensions ou encore une matrice représentent la même chose
+++

Un tableau à une dimension correspond au type de tableau que vous avez appris à utiliser jusqu'à maintenant. Ainsi le contenu de la variable <code>sac</code> ci-dessous : 

widget python
sac = ['potion de vie', 'parchemin', 'lance', 'baguette', 'lance']
widget

Peut être représenté graphiquement/mentalement comme suit : 

<style>
  .tableau > tbody > tr > td {
    border:2px solid black !important;
    font-size: 0.9rem;
  }
  .arrow > b {
    font-size: 1.5rem;
  }
  .tableau > tbody > tr > td.arrow{
    border:0 !important;
  }
  .tableau > tbody > tr > td.arrow > b > p{
    margin:0 !important;
  }
  .tableau > tbody > tr > td.indexi{
    border-color:blue !important;
    border-width:1px !important;
    color:blue !important;
  }
  .tableau > tbody > tr > td.indexj{
    border-color:green !important;
    border-width:1px !important;
    color:green !important;
  }
</style>

<table class="text-center tableau ace_editor mx-auto border-0 col-3">
  <tbody>
    <tr>
      <td class="px-2 ace_string">'potion de vie'</td>
    </tr><tr>
    </tr>
      <tr><td class="px-2 ace_string">'parchemin'</td>
    </tr><tr>
    </tr>
      <tr><td class="px-2 ace_string">'lance'</td>
    </tr><tr>
    </tr>
      <tr><td class="px-2 ace_string">'baguette'</td>
    </tr><tr>
    </tr>
      <tr><td class="px-2 ace_string">'lance'</td>
    </tr>
  </tbody>
</table>

+++bulle matthieu
  note au passage, chaque élément contenu dans le tableau occupe un espace mémoire dans la <b>RAM</b> de la machine
+++

<hr>

Un tableau peut contenir des entiers (<code>int</code>), des chaînes de caractères (<code>string</code>), ...

Mais également des tableaux ! 


widget python
# nom de l'iem | effet | prix de revente
sac = [
  ['potion de vie', 'rend 50 pv', 5],
  ['parchemin', 'invoque une boule de feu', 12],
  ['lance', 'inflige 12 de dégâts', 10],
  ['baguette', 'inflige 7 de dégât', 8],
  ['lance', 'inflige 12 de dégâts', 10]
]

print(sac)
widget

Et ce tableau peut être représenté graphiquement/mentalement comme ceci : 

<table class="text-center tableau ace_editor mx-auto border-0">
  <tbody>
    <tr>
      <td class="px-2 ace_string col-4">'potion de vie'</td>
      <td class="px-2 ace_string col-4">'rend 50 pv'</td>
      <td class="px-2 ace_constant ace_numeric col-4">5</td>
    </tr>
    <tr>
      <td class="px-2 ace_string">'parchemin'</td>
      <td class="px-2 ace_string">'invoque une boule de feu'</td>
      <td class="px-2 ace_constant ace_numeric">12</td>
    </tr>
    <tr>
      <td class="px-2 ace_string">'lance'</td>
      <td class="px-2 ace_string">'inflige 12 de dégâts'</td>
      <td class="px-2 ace_constant ace_numeric">10</td>
    </tr>
    <tr>
      <td class="px-2 ace_string">'baguette'</td>
      <td class="px-2 ace_string">'inflige 7 de dégât'</td>
      <td class="px-2 ace_constant ace_numeric">8</td>
    </tr>
    <tr>
      <td class="px-2 ace_string">'lance'</td>
      <td class="px-2 ace_string">'inflige 12 de dégâts'</td>
      <td class="px-2 ace_constant ace_numeric">10</td>
    </tr>
  </tbody>
</table>

+++activité binome 20 ../activites/applications-_directes/construction-_de-_matrices.md "Construction de matrices"

### Lire les données d'un tableau à deux dimensions

Pour rappel dans le cas d'un tableau à une dimension, nous pouvons accéder aux éléments de ce dernier via l'<b class="text-blue">index</b>. Par exemple : 

widget python
produits = ['bottes roses', 'escarpins', 'rangers', 'bottes bleues']

print(produits[0]) # affiche 'bottes roses'
print(produits[3]) # affiche 'tongs'
widget

<table class="text-center tableau ace_editor mx-auto border-0 col-3">
  <tbody>
    <tr>
      <td class="arrow col-1" rowspan="4"><b class="text-blue text-center"><p>↓</p><p>i</p><p>↓</p></b></td>
      <td class="px-2 ace_string col-1 indexi">0</td>
      <td class="px-2 ace_string col-3">'bottes roses'</td>
    </tr>
    <tr>
      <td class="px-2 ace_string indexi">1</td>
      <td class="px-2 ace_string">'escarpins'</td>
    </tr>
    <tr>
      <td class="px-2 ace_string indexi">2</td>
      <td class="px-2 ace_string">'rangers'</td>
    </tr>
    <tr>
      <td class="px-2 ace_string indexi">3</td>
      <td class="px-2 ace_string">'bottes bleues'</td>
    </tr>
  </tbody>
</table>

<hr>

Dans le cas d'un tableau à deux dimensions, nous utilisons deux index : 
<table class="text-center tableau ace_editor mx-auto border-0">
  <tbody>
    <tr>
      <td colspan="2" class="arrow"></td>
      <td colspan="3" class="pb-2 arrow"><b class="text-green">→ j →</b></td>
    </tr>
    <tr>
      <td class="arrow"></td>
      <td class="arrow"></td>
      <td class="indexj"><b class="text-green">0</b></td>
      <td class="indexj"><b class="text-green">1</b></td>
      <td class="indexj"><b class="text-green">2</b></td>
    </tr>
    <tr>
      <td class="arrow col-1" rowspan="4"><b class="text-blue text-center"><p>↓</p><p>i</p><p>↓</p></b></td>
      <td class="px-2 ace_string col-1 indexi">0</td>
      <td class="px-2 ace_string col-3">'bottes roses'</td>
      <td class="px-2 ace_string col-3">39</td>
      <td class="px-2 ace_constant ace_numeric col-3">10</td>
    </tr>
    <tr>
      <td class="px-2 ace_string col-1 indexi">1</td>
      <td class="px-2 ace_string">'escarpins'</td>
      <td class="px-2 ace_string">38</td>
      <td class="px-2 ace_constant ace_numeric">1</td>
    </tr>
    <tr>
      <td class="px-2 ace_string col-1 indexi">2</td>
      <td class="px-2 ace_string">'rangers'</td>
      <td class="px-2 ace_string">44</td>
      <td class="px-2 ace_constant ace_numeric">10</td>
    </tr>
    <tr>
      <td class="px-2 ace_string col-1 indexi">3</td>
      <td class="px-2 ace_string">'bottes bleues'</td>
      <td class="px-2 ace_string">38</td>
      <td class="px-2 ace_constant ace_numeric">2</td>
    </tr>
  </tbody>
</table>

widget python
# nom du produit | pointure | quantité en stock
produits = [
  ['bottes roses', 39, 10],
  ['escarpins', 38, 1],
  ['rangers', 44, 10],
  ['bottes bleues', 38, 2]
]

print(produits[0]) # affiche le contenu du tableau d'index i = 0
print(produits[0][2]) # affiche 10
widget

+++bulle matthieu
  quel code doit-on ajouter pour afficher toutes les données liées aux bottes bleues ? 
+++

+++bulle matthieu droite
  quel code doit-on ajouter pour afficher la pointure des rangers ? 
+++

+++spoil
widget python
# nom du produit | pointure | quantité en stock
produits = [
  ['bottes roses', 39, 10],
  ['escarpins', 38, 1],
  ['rangers', 44, 10],
  ['bottes bleues', 38, 2]
]

print(produits[0]) # affiche le contenu du tableau d'index i = 0
print(produits[0][2]) # affiche 10

print(produits[3]) # affiche toutes les données liées aux bottes bleues
print(produits[2][1]) # affiche la pointure des rangers
widget
spoil+++

### Modifier les données d'un tableau à deux dimensions

+++activité binome 25 ../activites/applications-_directes/modifier-_les-_valeurs-_dans-_un-_tableau-_a-_deux-_dimensions.md "Modifier les valeurs dans un tableau à deux dimensions"

### +++emoji-write Résumons

- Un tableau à une dimension est un tableau contenant un ensemble de données de type simple (<code>int</code>, <code>str</code>, <code>bool</code>, ...)
    - pour accéder à un élément d'un tableau à une dimension d'indice <b class="text-blue">i</b>, il faut utiliser la syntaxe <code>tableauQuelconque</code><b>[<span class="text-blue px-1">i</span>]</b> (où <b class="text-blue">i</b> est un nombre)
- Un tableau à deux dimensions est un tableau qui contient un ensemble de tableaux
    - tableau à deux dimensions <=> tableau de tableaux <=> matrice
    - pour accéder à 'une des cases' d'un tableau à deux dimensions d'indice <b class="text-blue">i</b> et <b class="text-green">j</b>, il faut utiliser la syntaxe <code>tableauQuelconque</code><b>[<span class="text-blue px-1">i</span>][<span class="text-green px-1">j</span>]</b> (où <b class="text-blue">i</b> et <b class="text-green">j</b> sont des nombres)


## Parcours d'un tableau à deux dimensions

Pour parcourir et afficher tous les éléments d'un tableau à une dimension, une simple boucle <code>for</code> suffit : 

widget python
sac = ['potion de vie', 'parchemin', 'lance', 'baguette', 'lance']

for item in sac:
  print(item)
widget

+++bulle matthieu
  en partant du principe qu'un sac peut contenir n items, quelle est la complexité du programme ci-dessus ?
+++

+++spoil
  La boucle for va itérer autant de fois qu'il y a d'élèment dans le tableau <code>sac</code>. Dans la boucle <code>for</code> nous avons simplement une afectation, donc la complexité du programme est donc O(n) 
spoil+++

Que se passe-t-il si nous utilisons le même code mais pour un tableau à deux dimensions ? 

widget python
# nom de l'iem | effet | prix de revente
sac = [
  ['potion de vie', 'rend 50 pv', 5],
  ['parchemin', 'invoque une boule de feu', 12],
  ['lance', 'inflige 12 de dégâts', 10],
  ['baguette', 'inflige 7 de dégât', 8],
  ['lance', 'inflige 12 de dégâts', 10]
]

for element in sac:
  print(element)
widget

+++bulle matthieu
  quel est le type de la variable <code>element</code> ? 
+++

+++bulle matthieu droite
  comment afficher tous les éléments, un à un, contenus dans la variable sac ?
+++

+++spoil
La variable <code>element</code> à chaque tous de boucle est de type 'tableau' (ou liste)
  
widget python
# nom de l'iem | effet | prix de revente
sac = [
  ['potion de vie', 'rend 50 pv', 5],
  ['parchemin', 'invoque une boule de feu', 12],
  ['lance', 'inflige 12 de dégâts', 10],
  ['baguette', 'inflige 7 de dégât', 8],
  ['lance', 'inflige 12 de dégâts', 10]
]

for item in sac:
  for caracteristique in item: 
    print(caracteristique)
widget
spoil+++

+++bulle matthieu
  quelle est la complexité du programme ?
+++


### +++emoji-write Résumons

- D'une manière générale, il faut une boucle imbriquée par dimension pour parcourir un tableau
    - Pour un tableau à une dimension, une seule boucle suffit. (en général O(n))
    - Pour un tableau à deux dimensions, il faut deux boucles imbriquées pour pouvoir parcourir tous ses éléments (en général O(n²))
    - (hors programme) Pour un tableau à trois dimensions, il faut trois boucles imbriquées (en général O(n³)), et ainsi de suite
- En programmation, nous utilisons la plupart du temps des tableaux à deux dimensions (exemple : contenu d'un fichier CSV) et plus rarement à trois dimensions (exemple : un ensemble d'établissement qui contiennent un ensemble de salles qui contiennent un ensemble d'objet). Plus la dimension d'un tableau est grande, plus la complexité de l'algorithme qui manipulera le tableau est importante.

<hr>

+++activité classe 60 ../activites/perfectionnement/creation-_d--__un-_echiquier.md "Création d'un échiquier avec la bibliothèque python turtle"