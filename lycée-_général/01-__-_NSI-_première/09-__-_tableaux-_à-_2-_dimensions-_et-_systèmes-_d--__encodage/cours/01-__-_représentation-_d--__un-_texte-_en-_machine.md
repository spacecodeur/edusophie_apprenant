# Représentation d'un texte en machine

+++programme
  Identifier l’intérêt des différents systèmes d’encodage
  Convertir un fichier texte dans différents formats d’encodage
  
  Utiliser une table d'encodage
  Manipuler différents types d'encodage
+++

+++sommaire

## Introduction

Assez tôt dans l'histoire de l'informatique, nous avons eu besoin que les machines puissent afficher du texte. Or, nous avons vu que les ordinateurs ne savent fondamentalement manipuler que des 0 et des 1. 

+++bulle matthieu
  petit rappel, pour quelle raison un ordinateur ne sait fondamentalement manipuler que des 0 et des 1 ?
+++

Dans nos quotidiens, les nombres que nous manipulons sont en base 10 (système décimal). Nous avons vu plus tôt dans l'année comment un ordinateur peut stocker et manipuler des nombres décimaux. 

+++bulle matthieu
  à votre avis, comment un ordinateur peut à partir de 0 et de 1, stocker et manipuler du texte ?
+++

+++spoil "Suite du cours" nolabel

## La table d'encodage ASCII

Une machine utilise des **tables d'encodage** pour pouvoir traduire une lettre en un nombre binaire, et inversement. Quelque part dans la mémoire de la machine sont stockées les correspondances entre les caractères et leur équivalent en binaire. 

+++citation /images/md/5E4525H25H28BBFC6CACH24884G2CH gauche
Avant la standardisation, de nombreux codages de caractères incompatibles entre eux existaient. Chaque matériel avait son propre codage, lié aux techniques qu'il utilisait. 
+++wikipedia https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange#Histoire

En 1963, une première version de la table d'encodage ASCII est publiée et devient rapidement un standard. Regardons à quoi ressemble une partie du contenu de cette table de correspondance : 

<style>
table.ascii th, table.ascii td{
  padding:0.5rem;
  text-align: center;
}
</style>

<table class="text-center ascii my-4">
  <thead>
    <tr>
      <th>n<sup>o</sup></th>
      <th>binaire</th>
      <th>caractère</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td colspan="3">...</td>
    </tr>
    <tr>
      <td>32</td>
      <td>0100000</td>
      <td>'espace'</td>
    </tr>
    <tr>
      <td>33</td>
      <td>0100001</td>
      <td>!</td>
    </tr>
    <tr>
      <td>34</td>
      <td>0100010</td>
      <td>"</td>
    </tr>
    <tr>
      <td>35</td>
      <td>0100011</td>
      <td>#</td>
    </tr>
    <tr>
      <td>36</td>
      <td>0100100</td>
      <td>$</td>
    </tr>
    <tr>
      <td>37</td>
      <td>0100101</td>
      <td>%</td>
    </tr>
    <tr>
      <td colspan="3">...</td>
    </tr>
    <tr>
      <td>48</td>
      <td>0110000</td>
      <td>0</td>
    </tr>
    <tr>
      <td>49</td>
      <td>0110001</td>
      <td>1</td>
    </tr>
    <tr>
      <td>50</td>
      <td>0110010</td>
      <td>2</td>
    </tr>
    <tr>
      <td>51</td>
      <td>0110011</td>
      <td>3</td>
    </tr>
    <tr>
      <td colspan="3">...</td>
    </tr>
    <tr>
      <td>65</td>
      <td>1000001</td>
      <td>A</td>
    </tr>
    <tr>
      <td>66</td>
      <td>1000010</td>
      <td>B</td>
    </tr>
    <tr>
      <td colspan="2">...</td>
    </tr>
    <tr>
      <td>97</td>
      <td>1100001</td>
      <td>a</td>
    </tr>
    <tr>
      <td>98</td>
      <td>1100010</td>
      <td>b</td>
    </tr>
    <tr>
      <td colspan="3">...</td>
    </tr>
  </tbody>
</table>

+++bulle matthieu
  cette structure en table ressemble furieusement à un type de structure de données vu en programmation...
+++

+++activité binome 20 ../activites/applications-_directes/utilisation-_de-_la-_table-_d--__encodage-_ASCII.md "Utilisation de la table d'encodage ASCII"

<hr>

L'encodage de caractère accentués peut parfois poser problème lorsque nous éditons un fichier texte ou encore lors de l'affichage d'une page Web.

### +++emoji-write Résumons

- Une machine utilise des tables d'encodages pour pouvoir traduire un texte en suite de chiffres binaires, et inversement
- Les tables d'encodage sont stockées en mémoire dans la machine. Sur ordinateur (au sens large), ce stockage est en règle général géré par le système d'exploitation
- L'ASCII fut une des premières tables d'encodage standardisée, l'inconvénient de cette table est qu'elle gère assez peu de caractères (elle ne gère pas les caractères accentués par exemple)

## Les tables d'encodage UNICODE

L'évolution des technologies, du matériel informatique (grandement boosté par les nanosciences) et la popularisation de l'informatique ont poussé à la création du standard UNICODE qui a pour ambition de stocker tous les caractères de tous les pays du monde. (et même plus : emojis, pièces d'échec, ...)

De ce standard a émergé 3 tables d'encadages à partir de 1992 :
- UTF-8
- UTF-16 
- UTF-32

+++bulle matthieu
  à quoi peut bien correspondre le chiffre à votre avis ?
+++

+++spoil
  Le chiffre correspond au nombre de bits qui encondent un caractère. Ainsi, UTF-16 encode par exemple les caractères sur 16 bits, soit 2 octets.
spoil+++

+++bulle matthieu droite
  combien de bits peut on encoder en UTF-8, UTF-16 et UTF-32 ?
+++

+++spoil
<ul>
  <li>En UTF-8, nous pouvons encoder 2<sup>8</sup> caractères, soit 256 caractères</li>
  <li>En UTF-16, nous pouvons encoder 2<sup>16</sup> caractères, soit 65536 caractères</li>
  <li>En UTF-32, nous pouvons encoder 2<sup>32</sup> caractères, soit 4 294 967 296 caractères</li>
</ul>
spoil+++

+++activité binome 20 ../activites/applications-_directes/manipulations-_de-_tables-_d--__encodages.md "Manipulations de tables d'encodages"

## Vous êtes en avance ? 

Développez un programme python qui permet de convertir un texte en son équivalant binaire et inversement. (en utilisant les mêmes correspondances qui se trouvent dans la table d'encodage UTF-8)

Par exemple si dans le programme je soumet : "Ou as tu péché ce poisson ?"

Le programme doit afficher le résultat : <br>
<code>01001111 01110101 00100000 01100001 01110011 00100000 01110100 01110101 00100000 01110000 11000011 10101001 01100011 01101000 11000011 10101001 00100000 01100011 01100101 00100000 01110000 01101111 01101001 01110011 01110011 01101111 01101110 00100000 00111111</code>

Le programme doit proposer au début de son exécution à l'utilisateur vers quel sens il souhaite faire la conversion. (utilisez la fonction python <code>input</code> :) )

### +++emoji-write Résumons

- Les tables d'encodages utf-8, utf-16 et utf-32 permettent de prendre en charge beaucoup plus de caractères que la table ASCII
- En terme de quantité de caractères gérés : utf-8 < utf-16 < utf-32
