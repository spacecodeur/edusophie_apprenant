# Modifier les valeurs dans un tableau à deux dimensions

+++consigne
  Parcourez l'activité et répondez aux questions
+++

Revenons sur les tableaux à une dimension. Ci-dessous nous stockons dans une variable les emails des utilisateurs inscrits sur un site : 

```python
membres = ['toto@jemail.com', 'tata@jemail.com', 'titi@jemail.com', 'foo@jemail.com', 'faa@jemail.com']
print(membres)
```

L'utilisateur titi souhaite changer d'adresse email, sa nouvelle adresse est 'titi@ailcloude.com'.

+++question write "Sans modifier la ligne 1, quel code faut il ajouter après la ligne 2 pour pouvoir modifier l'adresse mail de titi ? Autrement dit, comment modifier le contenu d'un tableau après sa création ?"

+++spoil "Aide"
  Le premier cours sur les tableaux est disponible +++lien "/apprenant/01-__-_NSI-_premiere/02-__-_introduction-_aux-_tableaux-_et-_programmation-_Web-_cote-_client/00-__-_cours/00-__-_introduction-_aux-_tableaux.md" "ici". Vous pouvez bien sur également vous aider de recherches sur Internet
spoil+++

<hr>

Dans le site, nous stockons aussi le prénom et l'age de chaque membre.

+++question "Modifiez le contenu de la variable <code>membres</code> afin qu'elle puisse stocker les informations décrites juste au dessus"

+++question write "En utilisant une approche similaire à la question 0, comment peut on modifier l'age du deuxième membre inscrit sur le site ?"

+++question "Complétez le code ci-dessous afin d'afficher le nombre de membres inscrits sur le site"

```python
# à compléter
for membre in membres:
  # à compléter

# à compléter
```

<hr>

+++question "Complétez le code de la question précédente afin de compter seulement le nombre de personnes dont l'age est supérieur à 25"

+++spoil "Aide"
  Un test doit être effectué, quelle instruction en programmation python permet de tester une condition ?
spoil+++

## Réponses

<ol start="0">
  <li>
  +++spoil
widget python
membres = ['toto@jemail.com', 'tata@jemail.com', 'titi@jemail.com', 'foo@jemail.com', 'faa@jemail.com']
print(membres)

membres[2] = 'titi@ailcloude.com'
print(membres)
widget  
  spoil+++
  </li>
  <li>
  +++spoil
widget python
membres = [
  ['toto@jemail.com', 'toto', 17],
  ['tata@jemail.com', 'tata', 27],
  ['titi@jemail.com', 'titi', 16],
  ['foo@jemail.com', 'foo', 30],
  ['faa@jemail.com', 'faa', 25]
]
print(membres)

widget  
  spoil+++
  </li>
  <li>
  +++spoil
widget python
membres = [
  ['toto@jemail.com', 'toto', 17],
  ['tata@jemail.com', 'tata', 27],
  ['titi@jemail.com', 'titi', 16],
  ['foo@jemail.com', 'foo', 30],
  ['faa@jemail.com', 'faa', 25]
]

membres[1][2] = 28

print(membres)

widget  
  spoil+++
  </li>
  <li>
  +++spoil
widget python
membres = [
  ['toto@jemail.com', 'toto', 17],
  ['tata@jemail.com', 'tata', 27],
  ['titi@jemail.com', 'titi', 16],
  ['foo@jemail.com', 'foo', 30],
  ['faa@jemail.com', 'faa', 25]
]

compteur = 0
for membre in membres:
  compteur += 1
  
print(compteur)
widget
  spoil+++
  </li>
  <li>
  +++spoil
widget python
membres = [
  ['toto@jemail.com', 'toto', 17],
  ['tata@jemail.com', 'tata', 27],
  ['titi@jemail.com', 'titi', 16],
  ['foo@jemail.com', 'foo', 30],
  ['faa@jemail.com', 'faa', 25]
]

compteur = 0
for membre in membres:
  if(membre[2] > 25):
    compteur += 1
  
print(compteur)
widget
  spoil+++
  </li>
</ol>