# Création d'un échiquier

<style>
  pre.speudoc{
    line-height: 30px;
    border:2px grey solid;
    padding: 2px;
  }
  pre.speudoc span.c{
    color:#803378;
    background-color: lightgrey;
    padding:0.3rem;
  }
</style>

Le but de cette activité est de vous faire découvrir une bibliothèque python graphique (turtle) et de mettre en application ce qui a été vu dans le cours dédié aux tableau à deux dimensions

+++consigne
Se laisser guider par l'activité. Vous êtes invités à rechercher / à vous documenter sur le Web.
+++

## Présentaiton de la bibliothèque turtle

La bibliothèque python turtle permet de dessiner des éléments graphiques avec des lignes de code python. Vous devez télécharger et installer cette bibliothèque pour pouvoir l'utiliser. 

Sur thonny, l'installation de turtle se fait très simplement en quelques étapes :
  - depuis thonny, cliquer sur 'Outils' puis sur 'Gérer les paquets'
  - rechercher le paquet 'turtle' et l'installer
  - et c'est tout !
  
Avant de vous pencher sur la création d'une échiquier, je vous invite à rechercehr des exemples de code python utilisant thonny sur le Web et de coper le code sur thonny pour l'analyser / l'exécuter / le modifier. Cela vous permettra de comprendre comment fonction la bibliothèque. 

+++question "Trouver un programme python sur le Web utilisant la bibliothèque thonny. Recopier le code dans thonny, analyser-le, exécutez-le, modifiez-le !"

+++bulle matthieu
  évidemment, plus le programme est simple, plus il sera facile à analyser / modifier
+++

+++bulle matthieu droite
  prenez le temps que vous souhaitez dans cette partie, n'hésitez pas à montrer / partager vos trouvailles auprès de vos autres collègues
+++

## Création d'un échiquier

La création d'un échiquier se fait en trois principales étapes :

0. La première étape consiste à dessiner un plateau vide à l'aide de la bibliothèque turtle 
1. la deuxième étape consiste à créer un tableau python à deux dimensions contenant les emplacements des pièces d'échec
2. pour terminer, il faut adapter/améliorer les deux premières parties afin que l'échiquier affiche les pièces aux bon endroit en fonction du contenu du tableau python de la deuxième partie

Dans la suite de l'activité, vous devrez vous aider de recherches sur Internet pour mener à bien les différentes étapes. Plusieurs approches sont possibles pour dessiner un échiquier, les étapes qui suivent ne présente qu'une approche parmis d'autres.

### Étape 0 : dessiner un plateau vide avec turtle

Dans cette étape 0, vous travaillez essentiellement avec la bibliothèque turtle.

+++question write "Comment peut on dessiner un carré simple et vide ?"

+++question write "Comment peut on colorer le contenu d'un carré ?"

<hr>

Vous avez dessiné un carré et vous savez comment le colorer. Il faut maintenant créer 64 carrés au total (8 lignes * 8 colones). Il est évidemment hors de question de dupliquer 64 fois le code d'un carré !

+++bulle matthieu
  pourquoi ? imaginez que vous voulez modifier par exemple la couleur de fond de vos carrés, il va donc falloir apporter beaucoup (trop) de modifications dans votre code 
+++

+++bulle matthieu droite
  l'erreur étant humaine, plus vous modifiez un programme, plus il y a de chance d'y insérer sans le vouloir un bug 
+++

Afin d'éviter d'écrire 64 fois la même chose dans votre code, vous allez bien sur vous aider des boucles et des fonctions en programation ! 

+++question "Créez une fonction nommée 'ligne' qui permet de dessiner 8 carrés côte à cote"

+++spoil "Aide"
  la fonction doit utiliser une boucle qui bouclera 8 fois (car il y a 8 cases dans une ligne). À chaque tour de boucle, un carré est dessiné
spoil+++

+++spoil "Aide 2 : pseudo code d'une première version de la fonction 'ligne'"

voici le pseudo code qui décrit le contenu de la fonction 'ligne' : (qu'il faut bien sur adapter/compléter pour pouvoir l'utiliser avec turtle)

<pre class="speudoc">début fonction ligne(<span class="c">coordonnee_x</span>, <span class="c">coordonnee_y</span>)
  <span class="c">taille_carre</span> = 10
  Pour <span class="c">i</span> allant de 1 à 8 compris
    dessiner un carré de côté <span class="c">taille_carre</span> en partant des coordonnées (<span class="c">coordonnee_x</span> + <span class="c">i</span> * <span class="c">taille_carre</span>, <span class="c">coordonnee_y</span>) 
  fin Pour
fin fonction
</pre>

/!\ cette première version de la fonction 'ligne' ne prend pas en compte la coloration des cases /!\

spoil+++

+++spoil "Aide 3 : prise en compte des cases noires et blanches"
  voici une amélioration de la fonction 'ligne' (en pseudo-code toujours) qui permet de prendre en compte les cases noires et les cases blanches. Pour implémenter ce pseudo-code en python, vous devrez chercher sur le Web comment on peut déterminer si un nombre est pair ou non en python.

<pre class="speudoc">début fonction ligne(<span class="c">coordonnee_x</span>, <span class="c">coordonnee_y</span>)
  <span class="c">taille_carre</span> = 10
  Pour <span class="c">i</span> allant de 1 à 8 compris
    Si i est pair
      dessiner un carré blanc de côté <span class="c">taille_carre</span> en partant des coordonnées (<span class="c">coordonnee_x</span> + <span class="c">i</span> * <span class="c">taille_carre</span>, <span class="c">coordonnee_y</span>)
    Sinon
      dessiner un carré noir de côté <span class="c">taille_carre</span> en partant des coordonnées (<span class="c">coordonnee_x</span> + <span class="c">i</span> * <span class="c">taille_carre</span>, <span class="c">coordonnee_y</span>)
    Fin Si
  fin Pour
fin fonction
</pre>
  
spoil+++

+++question "Créez une fonction nommée 'plateau' qui permet de dessiner 8 lignes de carrés les unes en dessous des autres. La fonction 'plateau' doit bien entendu utiliser la fonction 'ligne'"

+++spoil "Aide : pseudo code d'une première version de la fonction 'plateau'"
<pre class="speudoc">début fonction plateau(<span class="c">coordonnee_x</span>, <span class="c">coordonnee_y</span>)
  <span class="c">taille_carre</span> = 10
  Pour <span class="c">i</span> allant de 1 à 8 compris
    ligne(<span class="c">coordonnee_x</span>, <span class="c">coordonnee_y</span> + i * <span class="c">taille_carre</span>)
  fin Pour
fin fonction
</pre>

/!\ cette première version de la fonction 'plateau' devra être améliorée pour prendre en compte correctement les cases blanches et les cases noires /!\

spoil+++

### Étape 1

+++question "Stockez dans une variable nommée 'pieces' un tableau à deux dimensions contenant les informations des pièces de l'échiquier"

+++bulle matthieu
  pour chaque pièce, il est donc important d'avoir la nature de la pièce (cheval, fou, ...). sa couleur (noir ou blanc) et sa position sur l'échiquier
+++

+++bulle matthieu
  deux approches sont possible pour la sctructuration de la variable ''pieces 
+++

#### Approche 1 

Chaque sous tableaux de la variable 'pieces' contient les informations de chaque pièces. Le premier sous-tableau peut par exemple contenir les informations 'cavalier', 'noir', '20' (coordonnée x) et '-30' (coordonnée y)

#### Approche 2 

Chaque sous tableaux de la variable 'pieces' contient 8 éléments. Chaque élément représente une case de l'échiquier. Par exemple, dans le premier sous-tableau nous pouvons avoir les éléments : 

'cheval blanc', '', '', 'pion blanc', 'pion noir', 'roi blanc', 'fou blanc' et ''

Ce qui signifie que tout en haut à gauche de l'échiquier se trouve un cavalier blanc

### Étape 2

Dans cette étape, vous devez adapter et améliorer le code produit dans les deux première étapes afin d'afficher sur l'échiquier toutes les pièces représentées dans le tableau 'pieces'

En première étape, je vous conseil de représenter les pièces par leur première lettre (il est possible d'afficher des lettres avec turtle). Ainsi, un cavalier sera représenté par la lettre c, un fou par la lettre f, et ains de suite...

Il n'y a pas d'aides supplémentaires dans cette étape, entre aidez vous, manipulez, testez, documentez vous sur le Web :)
