# Programmation Web côté client

+++programme
Repérer, dans un nouveau langage de programmation,les traits communs et les traits particuliers à ce langage
Analyser et modifier les méthodes exécutées lors d’un clic sur un bouton d’une page Web

Analyser une page dynamisée par du code javascript
Établir les points communs et les différences entre les langages python et javascript
+++

<br>

+++todo javascript + event un peu trop complexe, surtout pour le groupe de 16 a 18, cette séance pourrait être changée par une séance projet je pense pour donner plus de temps pour le projet qui arrive

+++sommaire 1

## Javascript, nouveau langage de programmation !

+++imageFloat /images/md/45272DAH58GG2292E47G45FDHAH8GD
  Le javascript est un langage de programmation interprété par les navigateurs Web. Un langage de programmation au même titre que Python
+++

Ainsi en Javascript nous pouvons : 
- Créer des variables ou des tableaux, les manipuler, ...
- Créer des instructions conditionnelles et itératives
- Créer des fonctions

+++bulle matthieu
  en fait, la plupart des langages de partagent ces caractéristiques
+++

+++bulle matthieu droite
  la bonne nouvelle donc est que la maîtrise d'un langage de programmation vous ouvre à tout un panel de langages de programmation
+++

<br>

## Python VS Javascript

Les principales différences entre Python et Javascript sont : 
- Javascript est exécuté par un navigateur Internet, et donc profite nativement de tout l'écosystème technique qu'offre un navigateur Internet (les langages HTML et CSS)
- Python, plus proche du coeur de l'ordinateur, est compilé en code binaire avant d'être traité par une application qui se charge d'adapter le code binaire en fonction du système d'exploitation hôte 
- des points mineurs dans la syntaxe

Une difficulté qu'on rencontre lorsqu'on débute en programmation, et qui plus est, lorsqu'on découvre plusieurs langages de programmation en même temps est de confondre les syntaxes utilisées par les différents langage. 

Il existe bon nombre de documents sur Internet qui résument les principales commandes dans un langage de programmation. Vous pouvez trouver de tels documents sur votre moteur de recherche préféré avec les mots-clés : "<a href="https://duckduckgo.com/?q=cheatsheet+python+beginner&t=hx&va=g&iax=images&ia=images&iaf=size%3ALarge" target="_blank">cheatsheet python beginner</a>" ou encore "<a href="https://duckduckgo.com/?q=cheatsheet+javascript+beginner&t=hx&va=g&iar=images&iaf=size%3ALarge&iax=images&ia=images" target="_blank">cheatsheet javascript beginner</a><a>"

+++bulle matthieu
  dans ce cours est fournis un cheatsheet comparant la syntaxe de Python et de Javascript
+++

+++bulle matthieu droite
  voici la </a><a href="/fichiers/72GAC6A52CE86AACEHH725EBAC2339" target="_blank">version .pdf</a> et voilà la <a href="/fichiers/6CD93A37B7CEGABAB932996GHD6F5F" target="_blank">version éditable .odt</a>
+++

## S'approprier Javascript

Pour découvrir un nouveau langage, rien de mieux que la pratique !

+++activité binome 60 ../01-__-_activites/00-__-_applications-_directes/HTML-_et-_javascript.md "HTML et javascript"
    <ul>
        <li>Faire l'exercice mis en lien</li>
        <li>L'entraide entre les groupes est encouragée</li>
        <ul>
            <li><b class="text-red">Attention :</b> respectez bien <b>la charte d'entraide</b></li>
        </ul>
    </ul>
+++

### Un nouveau paradigme de programmation : la programmation événementielle

En python, vous avez appris à créer des programmes en suivant le paradigme classique qu'est la programmation procédurale : 

- une suite d'instruction est exécutée 
- des appels à des fonctions ont lieu

En javascript, nous utilisons le paradigme de programmation événementiel : 

- ce paradigme fonctionne comme le paradigme de programmation procédurale
- mais en plus, une partie du code est déclenché suite des actions effectuées par l'utilisateur (le visiteur du site ici)

Javascript permet donc de capturer des événements et de déclencher certaines parties du code en conséquence :

- l'utilisateur clique sur le bouton like de facebook : javascript capte le clic et modifie le contenu de la page html en conséquence


## Résumons +++emoji-writing_hand 

- Javascript est un langage de programmation au même titre que Python
- la plupart des langages de programmation partagent les mêmes concepts fondamentaux : 
    - créer et manipuler des variables
    - créer des instructions conditionnelles et itératives
    - créer des fonctions
- Javascript permet de faire de la programmation événementielle
- La programmation événementielle consiste à orienter le comportement d'un programme en fonction des actions effectuées par un utilisateur
- En javascript, il existe un multitude d'événements utilisables, les principaux sont : onload, onkeydown, onclick et onmouseover

