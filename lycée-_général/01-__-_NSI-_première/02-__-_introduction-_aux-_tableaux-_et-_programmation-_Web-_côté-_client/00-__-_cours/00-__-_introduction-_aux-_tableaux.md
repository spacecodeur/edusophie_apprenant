# Introduction aux tableaux

+++programme
Lire et modifier les éléments d’un tableau grâce à leurs index

Créer un nouveau type de variable : les tableaux
Accéder aux données d'un tableau
Modifier un tableau
+++

+++sommaire

## Introduction

### Précédemment...

<br>
+++bulle matthieu
  Quels données peut on retrouver dans ces différents types de variables ?
+++
+++diagramme
    flowchart TD
    A[\variable de type <code>integer</code>/]
    B[\variable de type <code>float</code>/]
    C[\variable de type <code>char</code>/]
    E[\variable de type <code>boolean</code>/]
+++
<br>

+++bulle matthieu
  on dit que ces types de variable sont <b>des types de bases</b>
+++

+++bulle matthieu droite
  Toutes les variables de ces types ont un point commun, le voyez vous ?
+++

+++spoil text-center
  Ces variables ne peuvent contenir qu'une seule donnée (du type concerné)
spoil+++

<br>
Nous allons découvrir un nouveau type de variables, **les tableaux** ! En programmation, un tableau est une variable de type **construit**. Ce cours repose sur un fil rouge où vous allez porter la casquette d'un développeur de jeux vidéo.

### Préparer son environnement

Avant de continuer, créez vous un "dossier du jour" sur votre ordinateur : 

+++bulle matthieu
  <b>attention toujours !</b> vous devez modifier la commande ci-dessous pour l'adapter par rapport à vous (votre nom, votre prénom, la date du jour)
+++

```bash
# bash
mkdir /home/premiere/Eleves/monNom_monPrenom/seance/16_09_21
```

Puis à l'aide de la commande bash `cd`, dirigez votre terminal dans le dossier du jours. 

+++bulle matthieu
  souvenez vous que la touche tab vous permet de taper les commandes plus rapidement grâce à l'auto-complétion
+++

+++bulle matthieu droite
  n'hésitez pas à tapoter cette touche pendant que vous écrivez des commandes dans votre terminal
+++

Laissez le terminal de côté.

### Fil rouge de ce cours

Dans ce cours, nous nous intéressons à la gestion de l'expérience et du niveau d'un personnage dans un jeux vidéo. Avant de rentrer dans les détails de ce qu'est un tableau, démarrez cette activité introductive dans laquelle vous allez constater les limites des variables de type de base.

+++activité binome 40 ../01-__-_activites/00-__-_applications-_directes/les-_limites-_des-_variables-_de-_type-_de-_base.md "Limites des variables de type de base"
<ul>
  <li>Faire l'exercice mis en lien</li>
  <li><b class="text-red"></b><b class="text-red">Attention :</b> respectez bien <b>la charte d'entraide</b></li>
</ul>
+++

## Les tableaux

En informatique, nous sommes amenés à manipuler beaucoup de données. Enfermer chacune des données dans une variable devient rapidement problématique et c'est ici qu'interviennent les variables de type tableau ! 

+++diagramme
    flowchart TB
    subgraph G2[ ]
      direction TB
      B2[une donnée A, une donnée B, une donnée C, ...]
      A2[\variable de type <b>tableau</b>/]
      B2-->A2
    end
    subgraph G1[ ]
      direction TB
      B[une donnée]
      A[\variable de type <b>de base</b>/]
      B-->A
    end
+++

+++bulle matthieu
  <b>attention !</b> il ne s'agit pas de tout mettre dans un seul et unique tableau !
+++

+++bulle matthieu droite
  on aura par exemple un tableau pour stocker des coordonnées, un tableau pour stocker des noms de métiers, un tableau pour stocker des couleurs en hexadécimal, etc ...
+++
<br>

### Vocabulaire Python

Dans le langage python, nous utilisons plutôt le terme 'liste'  (+++emoji-us list) à la place de 'tableau' (+++emoji-us array)

+++bulle matthieu
  il n'y a aucune différences entre un 'tableau' (vocabulaire lié à de la programmation en général) et une 'liste' ! (vocabulaire lié à python)
+++

+++bulle matthieu droite
  je continuerai à parler dans le cours de tableau, mais n'oublier pas qu'en python on utilise plutôt le terme 'liste'
+++
<br>

### Comment déclarer un tableau en Python

widget python
# diverses déclaration de variables de types de base
a = 17 # type integer
b = 'a' # type char
c = True  # type boolean

# déclaration d'un tableau d'entier
d = [0, 12, -1, 28] 
print(d)

# déclaration d'un tableau de char
e = ['a', 'b', 'c'] 
print(e)

# déclaration un tableau contenant différents types de données
# => au lycée nous nous focalisons sur les tableaux ne contenant que des variables du même type
f = [True, 'f', 40] 
print(f)

# nous pouvons aussi créer des tableaux de tableaux, nous aborderons ce point plus tard dans l'année
g = [ [1,2,3], [4,5,6], d ]
print(g)
  
widget

Un tableau est un type de variable, donc si vous créez un tableau, vous le stockez dans une variable : 

```python
monTableau = 
```

Ensuite, un tableau commence par le caractère `[` et fini par le caractère `]`. Par exemple pour déclarer un tableau 'vide' (c'est à dire, sans élément à l'intérieur) : 

```python
monTableau = [] 
```

Et si nous souhaitons déclarer un nouveau tableau, mais avec par exemple 3 données dedans, voici comment faire : 

```python
monTableau = ['le chat', 'la souris', 'le chat, la souris'] 
# les différentes données dans un tableau sont séparées par une virgule
```

+++bulle matthieu
  nous verrons plus tard comment modifier des éléments dans un tableau !
+++

<br>
### Comment accéder à une donnée d'un tableau en Python

widget python
monTableau = ['le chat', 'la souris', 'le chat, la souris'] 

print(monTableau[0])
widget

+++bulle matthieu
  à votre avis, comment modifier le code pour accéder au deuxième élément du tableau ou au troisième élément ?
+++

<br>

+++activité binome 20 ../01-__-_activites/00-__-_applications-_directes/premiers-_pas-_avec-_les-_tableaux.md "Premiers pas avec les tableaux"
<ul>
  <li>Faire l'exercice mis en lien</li>
  <li><b class="text-red"></b><b class="text-red">Attention :</b> respectez bien <b>la charte d'entraide</b></li>
</ul>
+++

### Comment modifier une valeur dans tableau en Python

widget python
t1 = [15, 22, 0]
t2 = ['aaa', 'bbb', 'ccc']
t3 = [True, False, False]

print('tableaux non modifiés: \n')

print(t1)
print(t2)
print(t3)

t1[1] = 33
t2[2] = 'fff'
t3[0] = False

print('\ntableaux modifiés: \n')

print(t1)
print(t2)
print(t3)

widget

### Résumons  +++emoji-writing_hand 

- Les variables de type `int`, `char`, `float`, `boolean` sont dîtes de type de base
- Un tableau est une variable de **type construit**, ce type de variable peut contenir un ensemble de données
    - voici un exemple de déclaration d'un tableau vide : `monTableau = []`
    - voici un exemple de déclaration d'un tableau avec des données dedans : `monTableau2 = [10, 17, 22]`
- Il est possible d'**accéder** à chacune des données d'un tableau, par exemple : `print(monTableau2[0]) # est affiché le premier élément du tableau`
- Il est possible de **modifier** les données d'un tableau, par exemple : `monTableau2[2] = 42 # le troisième élément du tableau, qui était 22, est modifié par 42`