# Projet "Clicker game"

+++sommaire 1

## Sujet du projet

Dans ce projet vous développez un jeu de type 'clicker game'. Un des jeux les plus connu de ce genre est +++lien "https://orteil.dashnet.org/cookieclicker" "Cookie Clicker" où le but est d'amasser le plus de cookies possibles à l'aide de simples clics et/ou en laissant le jeu s'exécuter.

Une première version assez légère du jeu est attendue, il devra être jouable depuis un navigateur Web.

## Spécifications fonctionnelles

+++bulle matthieu
  les spécification fonctionnelles décrivent les fonctionnalités attendues du projet
+++
+++bulle matthieu droite
  un utilisateur non technique ( = non informaticien) doit pouvoir comprendre les spécification fonctionnelles
+++

Voici ci-dessous les fonctionnalités principales attendues à l'issue de ce projet. Une fois l'implémentation de ces fonctionnalités terminée, libre à vous d'améliorer comme vous le souhaitez votre jeu.

### Actions du joueur

Depuis son navigateur Web préféré, le joueur doit pouvoir : 
- cliquer sur un élément graphique
    - chaque clic doit augmenter en temps réel un score affiché dans la page
- exporter sa partie en cliquant sur un bouton 'exporter sa partie'
    - lorsque le joueur clic sur ce bouton une nouvelle page se charge avec affiché dedans un <b>code</b>
- charger une partie exportée depuis un autre ordinateur
    - pour cela, le joueur doit pouvoir renseigner dans un champs texte un <b>code</b>. Et une fois le code validé, la page du jeu se charge avec le score de la partie exportée
- **[déjà développé]** accéder à une page d'administration en saisissant le mot de passe de l'administrateur
    - dans cette page d'administration, l'administrateur peut voir les informations de la dernière sauvegarde enregistrée côté serveur

### Sécurité

- Le jeu doit être un minimum sécurisé, pour cela vous devez simuler une attaque puis mettre en place une solution pour s'en défendre. Dans ce projet, vous aurez le choix entre deux types d'attaques : 
    - l'attaque par force brute ( +++flag-us brute force) : une attaque qui cible directement le formulaire du site permettant d'accéder à la page d'administration 
    - l'attaque de l'homme du milieu ( +++flag-us **M**an **I**n **T**he **M**iddle) : un attaque qui opère sur le réseau entre le client (le joueur) et le serveur

Plus de détails sont donnés dans les spécifications techniques

## Spécifications techniques

+++bulle matthieu
  les spécification techniques décrivent les moyens techniques choisis par les informaticiens pour mettre en œuvre le projet
+++

+++bulle matthieu droite
  cette partie d'un cahier des charges est destinée à l'équipe qui développe le projet
+++

### Rôles 

Dans ce projet, vous aurez besoin :
- d'un +++lien "/enseignant/didactique/modeles/projet/roles/developpeur-_frontend.md" "**développeur frontend**" : cette personne se chargera de développer l'interface graphique du jeu
- d'un +++lien "/enseignant/didactique/modeles/projet/roles/developpeur-_backend.md" "**développeur backend**" : cette personne se chargera de développer le serveur du jeu
- d'un +++lien "/enseignant/didactique/modeles/projet/roles/developpeur-_securite-_SI.md" "**développeur sécurité SI**" : cette personne se chargera d'assurer la sécurité de la page d'administration

#### Besoin d'aide pour démarrer ? 

<span class="d-block mt-3"><b>Équipe entière</b> : télécharger la première version du projet disponible +++fichier "/fichiers/CFB3F5H6H6GACC9AB6C9829CBH47AE" "dans ce lien" et analyser le code contenu dedans (il s'agit d'une archive, <b>pensez à la décompresser</b> après l'avoir téléchargée). Le code est proprement découpé dans des fichiers et il sera facile de répartir les tâches entre développeur frontend et développeur backend. La page principale du jeu affiche actuellement : </span>

+++image "/images/md/85AEA33D7723GH25AFB9B9HFED67A9" col-6 mx-auto

**Développeur frontend** : se trouve facilement sur Internet des exemples de code pour développer un jeu type 'Cookie Clicker'. Pour rappel, le jeu doit être jouable depuis un navigateur Web, donc le(s) développeur(se)s frontend devra(ont) utiliser les langages tels que l'**html**, le **css** ou encore le **javascript**. Attention à ne pas trop s'éparpiller et à ne pas oublier de développer en priorité les fonctionnalités principales du jeu

**Développeur backend** : une partie du backend est déjà développée, il faut analyser le code contenu dans le fichier server.py et les commentaires qui se trouvent dedans. Dans un premier temps, changez le mot de passe d'accès à la page d'administration par un autre code à **4 chiffres** et ne le communiquez pas au développeur sécurité SI. Dans un second temps, complétez le code du serveur pour terminer la fonctionnalité de sauvegarde.

**Développeur sécurité SI** : votre première priorité est de démontrer à vos collègues que la page d'administration n'est pas sécurisée. Pour cela vous allez leur faire une démonstration d'un test d'intrusion, deux types d'attaques sont possibles, choisissez une des deux approches : 

<table class="table table-sm my-4">
  <thead class="text-center">
    <tr>
      <th></th>
      <th colspan="2">Difficulté selon l'approche choisie pour l'attaque</th>
    </tr>
    <tr>
      <th></th>
      <th>approche brute force via un bot Web</th>
      <th>approche man in the middle côté réseau</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>démonstration de l'attaque</th>
      <td class="text-center">moyen</td>
      <td class="text-center">moyen</td>
    </tr>
    <tr>
      <th>mise en place d'une solution pour s'en protéger</th>
      <td class="text-center">difficile</td>
      <td class="text-center">facile</td>
    </tr>
  </tbody>
</table>

#### <u>**Approche brute force**</u>

Vous créez un robot virtuel qui va automatiquement remplir le formulaire pour accéder à la page d'administration jusqu'à trouver le bon mot de passe. Vous devez : 

0. créer un dossier puis, dedans, un fichier python dans lequel vous allez créer votre robot ( +++flag-us bot )
1. installer sur thonny le module 'selenium'
2. télécharger un moteur de robot de navigateur Web, ci-dessous les manipulations à effectuer pour utiliser un moteur de robot spécialisé pour le navigateur firefox :

<ol start="3"><li><b>si vous êtes sur un OS type Linux :</b><ul>
  <li>Exécutez les commandes <b>bash</b> ci-dessous : </li>
</ul></li></ol>
```bash
cd /home/premiere/lechemin/vers/votredossier
wget https://github.com/mozilla/geckodriver/releases/download/v0.30.0/geckodriver-v0.30.0-linux64.tar.gz
tar zxvf geckodriver-v0.30.0-linux64.tar.gz
```

<ul><ul>
      <li>Dans votre fichier python (depuis thonny), ajouter les lignes suivantes </li>
</ul></ul>

```python
from selenium import webdriver
driver = webdriver.Firefox(executable_path=r'./geckodriver') # geckodriver, qui a été extrait de l'archive , doit se trouver dans le même dossier que le fichier python que vous êtes en train de modifier
```

<ol start="3"><li><b>sinon, si vous êtes sur un OS type Windows :</b><ul>
      <li>Télécharger l'archive contenant le moteur de robot pour windows en cliquant +++lien "https://github.com/mozilla/geckodriver/releases/download/v0.30.0/geckodriver-v0.30.0-win64.zip" "sur ce lien"</li>
      <li>Dézippez le contenu de l'archive dans votre dossier</li>
      <li>Dans votre fichier python, ajouter les lignes : </li>
</ul></li></ol>

```python
from selenium import webdriver
driver = webdriver.Firefox(executable_path=r'./geckodriver.exe') # geckodriver.exe, qui a été extrait de l'archive , doit se trouver dans le même dossier que le fichier python que vous êtes en train de modifier
```

<hr>

<div class="border border-danger p-2 mb-4 rounded">

<div class="d-flex justify-content-around align-items-center mb-3">
  
  <i class="fas fa-exclamation-triangle fa-2x mx-3 text-red"></i> 

  <p class="my-0 py-0">Si vous ouvrez dans thonny à la fois votre serveur de jeu (server.py) et votre script qui exécute le robot pour l'attaque en brute force, et que vous exécutez le script de votre robot, thonny va automatiquement stopper les autres fichiers python en cours d'exécution (et donc le serveur de jeu). Donc votre bot ne pourra pas se balader sur la page Web du jeu. Deux solutions s'offrent à vous : </p>

</div>

<ul class="mb-0">
<li> utiliser deux ordinateurs différents connectés dans un même réseau local : un qui démarre le serveur du jeu avec thonny et l'autre qui exécute le robot avec thonny</li>
<li> ou alors, exécutez le fichier server.py (ou le script de votre robot) en passant par une ligne de commande (et non depuis thonny)</li>
</ul>

</div>

<ol start="4">
  <li>Apprenez à vous servir du robot en glanant sur Internet des guides, comme +++lien "https://fr.acervolima.com/principes-de-base-de-selenium-python/" "celui ci" ou encore +++lien "https://www.scrapingbee.com/blog/selenium-python/" "celui-là"  par exemple </li>
  <li>Trouvez ensuite un moyen pour obtenir le mot de passe de la page d'administration du jeu que vous développez en utilisant votre robot sur le formulaire permettant d'accéder à l'espace d'administration</li>
</ol>


#### <u>**Approche man in the middle**</u>
Vous espionnez sur le réseau les informations passées entre le serveur et le client. Voici les étapes à suivre pour démarrer : 

0. Recherchez sur Internet comment créer un hotspot wifi avec votre ordinateur (qui sera considéré comme étant l'ordinateur du hacker)
1. Connectez votre téléphone portable en wifi sur votre ordinateur, ce téléphone portable est utilisé par un administrateur du jeu qui sera donc la victime de votre attaque, vous pouvez demandez au développeur backend de jouer la victime (car il connaît le mot de passe pour se connecter sur l'espace d'administration du jeu)
2. Sur votre ordinateur, ouvrez un terminal et utilisez la commande `tshark` (qui est déjà installé), renseignez vous sur Internet (+++lien "https://www.kali-linux.fr/configuration/tshark-analyse-paquet-sniffing" "comme ce premier site" par exemple) pour savoir comment l'utiliser. `tshark` est une commande qui affiche beaucoup (trop) d'information, avec quelques recherches. trouver le moyen d'afficher seulement les requêtes de type http




  
