# À la découverte des formulaires HTML

+++programme
Analyser le fonctionnement d’un formulaire simple
Distinguer les transmissions de paramètres par les requêtes POST ou GET
Distinguer ce qui est exécuté sur le client ou sur le serveur et dans quel ordre

Créer des formulaires WEB
Choisir selon la situation si il faut créer un formulaire type GET ou type POST
+++

+++sommaire 2

+++activité binome 30 ../01-__-_activites/applications-_directes/rappels-_HTML.md "Rappels HTML"

## Les formulaires HTML

Sur le Web, un formulaire est un ensemble d'éléments dans lesquels l'utilisateur saisit des informations avant de les valider. Ils sont utilisés partout, ils peuvent permettre :
- de faire une simple recherche
- d'ajouter du contenu sur un site : un nouveau post sur un réseau social, un nouveau fichier dans un espace de stockage en ligne, ...
- de créer un compte sur un site
- de modifier les informations d'un contenu

### Fonctionnement d'un formulaire

<div class="d-flex justify-content-evenly mb-4 border border-dark border-2 p-2">
  +++image "/images/md/HFC3DC4CFBH43G86BB48A4FB96DGDH" col-7 align-self-center
  <div class="offset-1 col-4 align-self-center">
    +++spoil "Étape 0" small
      Je saisis une url dans mon navigateur et j'appuie sur la touche "Entrée" du clavier
    spoil+++
    +++spoil "Étape 1" small
      Le navigateur envoie une requête au serveur hébergeant le site
    spoil+++
    +++spoil "Étape 2" small
      Le serveur analyse la requête puis génère, en réponse, une page Web
    spoil+++
    +++spoil "Étape 3" small
      La page Web est envoyée au client. Le navigateur affiche le contenu de la réponse du serveur
    spoil+++
    +++spoil "Étape 4" small
      Je remplis le formulaire contenu dans la page générée par le serveur
    spoil+++
    +++spoil "Étape 5" small
      Je valide le formulaire en cliquant sur le bouton Valider
    spoil+++
    +++spoil "Étape 6" small
      Le navigateur envoie une requête au serveur hébergeant le site. Dans cette requête se trouve les informations du formulaire (ici, ce que je veux chercher)
    spoil+++
    +++spoil "Étape 7" small
      Le serveur analyse la requête (qui contient les informations du formulaire) puis génère, en réponse, une page Web
    spoil+++
    +++spoil "Étape 8" small
      La page Web est envoyée au client. Le navigateur affiche le contenu de la réponse du serveur
    spoil+++
  </div>
</div>

+++bulle matthieu
quels formulaires utilisez vous sur Internet ? analysons les ! 
+++

## Formulaire de type GET 

### Code HTML

Un formulaire se compose d'une balise <span class="text-red">form</span> (paire) qui contient <span class="text-green">l'ensemble des champs</span> que l'utilisateur peut renseigner puis <span class="text-blue">un bouton</span> pour valider le formulaire. 

<pre class="p-2"><<span class="text-red">form</span> <span class="text-violet">action</span>="https://edusophie.com/mini-server" <span class="text-yellow">method</span>="GET">
  <span class="text-green">\<input type="text" name="recherche_de_lutilisateur"></span>
  <span class="text-blue">\<input type="submit" value="Rechercher"></span>
<​/<span class="text-red">form</span>>
</pre>

+++bulle matthieu
ce code produit le résultat suivant 
+++

<form action="https://edusophie.com/mini-server" method="GET" class="mb-3 text-center">
  <input type="text" name="recherche_de_lutilisateur">
  <input type="submit" value="Rechercher">
</form>

<hr>

Dans la balise form sont utilisés deux attributs : 
- <span class="text-violet">action</span> : le contenu de cet attribut est l'url vers laquelle est envoyé le formulaire après validation
- <span class="text-yellow">method</span> : le contenu de cet attribut définit la méthode utilisée lors de l'envoie du formulaire

+++activité binome 40 ../01-__-_activites/applications-_directes/formulaire-_de-_type-_GET.md "Formulaire de type GET"

### +++emoji-write Résumons

- Pour créer un formulaire HTML, il faut utiliser la balise <​form>
- Une balise form contient un ensemble d'élément HTML permettant à l'utilisateur de saisir des informations
- L'attribut action de la balise <​form> contient l'url vers laquelle sont envoyées les informations du formulaire après l'avoir validé
- L'attribut method de la balise <​form> définit le type de méthode d'envoi utilisé par le formulaire. 
- <span class="text-green">La méthode GET permet d'afficher les informations du formulaire (après validation) dans l'url du navigateur. Ce qui peut être pratique pour partager une page avec une autre personne</span>
- <span class="text-red">La méthode GET ne doit pas être utilisée lorsque le formulaire contient des données sensibles car ces données sont ensuite visibles de tous sur l'url de notre navigateur</span>
- <span class="text-red">La méthode GET limite la taille des informations qu'un formulaire peut envoyer</span>
  
## Formulaire de type POST 

Pour pallier aux <span class="text-red">principaux inconvénients</span> des formulaires de type GET, il est possible d'utiliser une autre méthode, la méthode POST. Il suffit pour cela de modifier la valeur de l'attribut 'method' par 'POST' (à la place de 'GET').

+++activité binome 15 ../01-__-_activites/applications-_directes/formulaire-_de-_type-_POST.md "Formulaire de type POST"

### +++emoji-write Résumons

- Pour créer un formulaire HTML de type POST, il suffit d'ajouter l'attribut 'method' dans la balise \<form> avec la valeur 'POST'
- La méthode POST sécurise davantage les données transmises au serveur à travers un formulaire. Cela en n'affichant pas les données dans l'url du navigateur.
- La méthode POST permet d'envoyer de grandes quantités de données à travers un formulaire, ce que la méthode GET ne permet pas

</form>