# rappels HTML

+++consignes
  <ol start="0">
    <li>Créez vous un dossier du jour en exécutant chacune des lignes ci-dessous sur un terminal. <b>Attention !</b> n'oubliez pas d'adapter les lignes 2 et 3 </li>
  </ol>
  <pre><code class="language-bash"># lignes à réadapter et à exécuter dans un terminal
nom_prenom=dupond_cedric
dateDuJour=18_10_21

mkdir /home/premiere/Eleves/${nom_prenom}/seances/${dateDuJour}
cd /home/premiere/Eleves/${nom_prenom}/seances/${dateDuJour}
touch rappels_HTML.html
gedit rappels_HTML.html
  </code></pre>
  <ol start="1">
    <li>Parcourez l'activité et répondez aux questions</li>
  </ol>
+++

Dans le +++lien "/fichiers/EG91FH3389E3AAHE3G9E39BDCD3AG8" "lien suivant" se trouve une page HTML simple. 

+++question "Copiez/Collez le code source de la page html dans votre fichier rappels_HTML.html"

+++spoil "Aide"
  Pour afficher le code source d'une page Web, il faut faire un clic droit dans la page puis cliquer sur l'option qui permet d'afficher le code source de la page
spoil+++

+++question "Ouvrez le fichier 'rappels_HTML.html' sur un navigateur"

<hr>

Dans un premier temps, vous allez analyser le code de la page. Voici quelques rappels généraux : 

- une balise est un élément HTML qui permet de structurer le contenu et/ou d'ajouter du contenu dans une page Web 
- la première ligne DOCTYPE permet d'indiquer au navigateur qu'il est en train de lire une page HTML
- la balise <​html> est la balise principale d'une page Web, elle contient toutes les autres balises de la page. Il ne peut y avoir qu'une seule balise html
- la balise <​body> contient les balises qui permettent d'afficher des éléments (texte, bouton, image, ...) sur notre navigateur. Il ne peut y avoir qu'une seule balise body, et dans la suite de l'activité nous nous focalisons principalement sur le contenu de cette balise

+++question write "En analysant le contenu de la balise <​body>, déterminez pour chaque balise ci-dessous si il s'agit d'une balise paire ou orpheline"

+++spoil "Aide"
  Si vous ne vous souvenez plus de ce qu'est une balise paire ou orpheline, +++lien "/apprenant/01-__-_NSI-_premiere/01-__-_Introduction-_en-_informatique-_fondamentale-_et-_introduction-_en-_programmation-_Web-_cote-_client/00-__-_cours/01-__-_Introduction-_en-_programmation-_Web-_cote-_client.md" "revoyez le cours"
spoil+++

- html : 
- body : 
- h1 : 
- h2 : 
- h3 : 
- img : 
- p : 

<hr>

Actuellement, le code de la page contient des balises \<img> mais aucune image ne s'affiche pourtant sur le navigateur. Pour y remédier, il faut modifier la balise \<img> pour indiquer un lien vers l'image que nous souhaitons afficher.

+++question write "Quel est le nom de l'attribut qu'il faut utiliser dans une balise \<img> pour préciser l'url de l'image que l'on souhaite utiliser ?"

+++spoil "Aide"
  Aidez vous des cours précédents ou de recherches sur Internet
spoil+++

+++question "Recherchez sur Internet 3 images libres de droits (une image de cookies, une image de tiramisu et une image de sushis). Copiez/collez dans un coin les urls des images"

+++bulle matthieu
  <b>Attention !</b> assurez vous bien que la fin des urls que vous relevez soit bien le lien directe vers l'image
+++

+++bulle matthieu droite
  à la fin de l'url vous devez voir l'extension de l'image par exemple : .jpeg)
+++

<hr>

Nous souhaitons maintenant ajouter un lien vers une recette (trouvée sur Internet) pour chaque recette contenues dans notre page Web. 

+++question write "Rappelez quelle est la balise HTML qu'il faut utiliser pour créer un lien"

+++question "Ajouter dans votre page Web un lien vers une page Web détaillant la recette à suivre pour faire des cookies"

+++question "Répétez la même opération pour le tiramisu et les sushis"

<hr>

Votre page Web expose actuellement 3 recettes : les cookies, le tiramisu et les sushis.

+++question "Ajoutez dans votre page Web une nouvelle recette en suivant la même structure que les autres recettes présentes déjà dans votre page"

<hr>

## Allez plus loin

Vous allez décorer votre page Web ( qui est bien morne pour l'instant ). Pour cela, il faut utiliser le langage CSS.

+++question "Juste après le début de la balise <​html> (donc entre l'ouverture de la balise <​html> et <​body>), ajoutez les trois lignes ci dessous : "

<pre><​head>  
  \<link href="style.css" rel="stylesheet" type="text/css">
<​/head>
</pre>

+++question "Dans le même dossier que le fichier rappels_HTML.html, créez le fichier style.css"

+++question "À l'aide de recherches sur Internet, modifiez le fichier style.css pour décorer votre page comme suit"

- colorez tous les titres h1 en vert 

+++spoil "Aide"
  Recherchez sur un moteur de recherche : css changer couleur titre
spoil+++

- colorez tous les titres h2 en bleu
- ajouter une couleur de fond à votre page

+++spoil "Aide"
  Recherchez sur un moteur de recherche : css changer background couleur body
spoil+++

- lorsque vous survolez un lien avec votre souris, affichez le lien en orange

+++spoil "Aide"
  Recherchez sur un moteur de recherche : css changer couleur survol a
spoil+++

- arrondissez les bordures de vos images 

+++spoil "Aide"
  Recherchez sur un moteur de recherche : css image bord arrondi
spoil+++

## Réponses

<ol start="2">
  <li>
  +++spoil
<ul>
  <li>html : paire</li>
  <li>body : paire</li>
  <li>h1 : paire</li>
  <li>h2 : paire</li>
  <li>h3 : paire</li>
  <li>img : orpheline</li>
  <li>p : paire</li>
</ul>
  spoil+++
  </li>
  <li>
  +++spoil
    Il faut utiliser l'attribut <code>src</code>. Voici un exemple d'utilisation de cet attribut dans la balise \<img> : 
    <pre><​img src='https://lien/vers/limage.png'></pre>
  spoil+++
  </li>
  <li value="5">
  +++spoil
    Il faut utiliser la balise \<a> en n'oubliant pas de renseigner dedans l'attribut <code>href</code>
  spoil+++
  </a></li><a>
</a></ol>