# Création et suppression de cookie

+++consignes
  <ol>
    <li>Dans cette activité certaines tâches seront réparties entre deux ordinateurs</li>
    <li>Parcourez l'activité et répondez aux questions</li>
  </ol>
+++

Vous allez utiliser un programme python permettant de générer un serveur. Une fois ceci fait, vous utiliserez un navigateur Web pour naviguer sur le site Web mis à disposition par le serveur et vous analyserez le cookie créé par le serveur.

+++question "Choisissez dans votre groupe l'ordinateur qui aura pour tâche de démarrer le serveur"

+++question "Sur l'ordinateur choisi, ouvrez un terminal est exécutez la commande <code>hostname -I</code>"

+++question write "Quel résultat retourne la commande <code>hostname -I</code> et à quoi correspond ce résultat ?"

<hr>

Dans le même terminal, saisissez les commandes pour vous déplacer dans le dossier <code>tmp</code> qui est à la racine de l'ordinateur (/tmp)

+++question write "Quelle(s) commande(s) bash devez vous exécuter dans le terminal pour vous déplacer dans le dossier <code>tmp</code> ?"

+++question write "Rappelez quelle est l'utilité du dossier <code>/tmp</code> dans un système d'exploitation type Linux"

<hr>

Une fois que votre terminal se trouve dans le dossier <code>/tmp</code>, vous allez télécharger une archive contenant le code d'un serveur python. Pour se faire, vous allez utiliser la commande bash <code>wget</code>

+++question "Depuis le terminal, exécutez la commande <code>wget https://www.edusophie.com/fichiers/27733DFE57FD686C5FF6BE483C3EDF</code>"

<hr>

Après avoir télécharger l'archive, vous allez la décompresser avec la commande bash <code>unzip</code>

+++question "Depuis le terminal, exécutez la commande <code>unzip 27733DFE57FD686C5FF6BE483C3EDF</code>"

<hr>

L'archive a été décompressée et un dossier <code>serveur</code> est maintenant apparu dans le dossier <code>tmp</code>

+++question "Depuis le terminal, exécutez la commande <code>tree serveur</code>"

+++question write "Expliquez en quelques mots à quoi sert la (très pratique) commande bash <code>tree</code>"

<hr>

À l'aide de la commande bash <code>cd</code>, déplacez vous dans le dossier serveur. Ouvrez ensuite le fichier <code>server.py</code> à l'aide de la commande bash <code>thonny</code>

Depuis l'IDE thonny, exécutez le code du fichier <code>server.py</code>

+++question "Dans les messages affichés par le serveur dans la console de thonny, notez dans un coin l'adresse url utilisée par le serveur"

<hr>

Basculez maintenant sur l'autre ordinateur et ouvrez dedans un navigateur Web. 

+++question "Sur le navigateur Web, ouvrez l'url que vous avez notée dans la question 9"

+++question "Cliquez sur le lien 'Accéder à la page de création de cookie'"

+++question "Ouvrez ensuite le débogueur de votre navigateur (raccourcis F12 ou fn+F12)"

Depuis le débogueur, cliquez sur l'onglet qui contient les informations enregistrées sur le navigateur : 

- sur Firefox, il s'agit de l'onglet "stockage"
- sur Chrome ou Chromium, il s'agit de l'onglet "application"

+++question write "D'après le débogueur, quel est le nom et la valeur du cookie que le serveur a fait enregistrer dans votre navigateur ?"

+++question "Sur le navigateur, revenez sur la page principale du site"

+++question write "D'après la console du serveur (donc sur thonny), quelles données sont automatiquement collectées sur le serveur ?"

<hr>

Vous avez observé la création d'un cookie sur le navigateur, vous allez maintenant faire la même chose pour le localstorage. Le localstorage fonctionne de la même manière qu'un cookie, excepté du fait que le contenu du localstorage n'est jamais transmis au serveur.

+++question "depuis le navigateur, cliquez sur le lien 'Accéder à la page de création de localstorage'"

+++question "Répétez les étapes 12 à 15"

<hr>

+++bulle matthieu
 pour les personnes en avance, analysez le code du serveur
+++

Le serveur génère un site Web à l'aide du framework flask. 

+++bulle matthieu droite
  Améliorez le serveur en ajoutant de nouvelles pages au site. Le contenu des pages est libre, le but est de vous familiariser avec flask
+++

Vous pouvez vous fouiller +++lien "https://perso.liris.cnrs.fr/pierre-antoine.champin/2017/progweb-python/cours/cm2.html" "dans cette documentation" française dédiée au framework flask

## Réponses

<ol start="2">
  <li>
  +++spoil
    La commande affiche dans le terminal l'IP locale (= IP privée) de l'ordinateur
  spoil+++
  </li>
  <li>
  +++spoil
    Pour se déplacer de dossier en dossier en bash, il faut utiliser la commande <code>cd</code>. Pour accéder au dossier <code>/tmp</code>, on peut par exemple exécuter la commande <code>cd /tmp</code>
  spoil+++
  </li>
  <li>
  +++spoil
    Dans un système d'exploitation type Linux, le dossier <code>tmp</code> situé à la racine du disque dur sert à stocker des données temporaires. Tous les fichiers et dossiers crées dans le dossier <code>tmp</code> sont automatiquement supprimé lorsqu'on éteint l'ordinateur.
  spoil+++
  </li>
  <li value="8">
  +++spoil
    La commande <code>tree</code> permet d'afficher la structure complète du contenu d'un dossier
  spoil+++
  </li>
  
</ol>