# Rappels de seconde

+++consignes
  <ul>
    <li>Lire les questions et y répondre en visualisant une vidéo avec toute la classe</li>
  </ul>
+++

+++video "https://www.youtube.com/watch?v=aX3z3JoVEdE" "MOOC SNT / Internet, IP un protocole universel ?"

## +++emoji-movie_camera 00:00 -> 02:32 

+++question write "Rappelez ce qu'est un protocole en informatique"

+++question write "Que gère le protocole IP ?"

+++question write "Quelles sont les valeurs minimales et maximales de chaque partie d'une adresse IP ?"

+++question write "Pourquoi la vidéo utilise le terme octet quand elle parle des parties d'une adresse IP ?"

+++bulle matthieu
  sur Internet nous utilisons des urls pour naviguer d'un site à l'autre.
+++

+++bulle matthieu droite
  quel est le nom du service qui fait la correspondance entre une url et une adresse IP ?
+++

## +++emoji-movie_camera 02:32 -> 04:34

+++question write "Qu'est-ce qu'un routeur ?"

+++bulle matthieu
  quel est l'intérêt selon vous d'envoyer une donnée en petit paquet
+++

+++bulle matthieu droite
  quel est le nom du service qui fait la correspondance entre une url et une adresse IP ?
+++

## 04:34 -> 06:33

+++question write "Que gère le protocole TCP ?"


## Réponses

<ol start="0">
  <li>
  +++spoil
    Un protocole en informatique est un ensemble de règles qui permettent à un ensemble d'ordinateurs de communiquer. 
  spoil+++
  </li>
  <li>
  +++spoil
    Le protocole IP (Internet Protocol) sert acheminer un paquet d'un nœud du réseau à l'autre en s'assurant que le chemin emprunté dans le réseau soit le plus rapide possible
  spoil+++
  </li>
  <li>
  +++spoil
    Une adresse IP (version 4) se présente sous la forme xxx.xxx.xxx.xxx où xxx est un nombre pouvant aller de 0 à 255 (donc 256 valeurs possibles)
  spoil+++
  </li>
  <li>
  +++spoil
    <p>Un bit correspond à 0 ou 1. Un octet correspond à 8 bits, donc un octet peut être : 10001010, 00011100, 10101111, 00000001, 00000000, ... </p>
    <p>Le nombre de combinaison possible dans 8 bits est de 2^8, soit 256. Donc un octet contient 256 valeurs possibles et c'est la raison pour laquelle chaque partie d'une IP est nommée "octet" dans la vidéo</p>
  spoil+++
  </li>
  <li>
  +++spoil
    Un routeur est un équipement informatique dont la fonction principale consiste à orienter les données à travers un réseau
  spoil+++
  </li>
  <li>
  +++spoil
    Le protocole TCP gère le transport de données entre un émetteur et un récepteur. Ce protocole s'assure que les données envoyées ont bien été toutes envoyées et dans l'ordre au récepteur.
  spoil+++
  </li>
  
</ol>