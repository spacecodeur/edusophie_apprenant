# Introduction au réseau

+++programme
Mettre en évidence l’intérêt du découpage des données en paquets et de leur encapsulation
Dérouler le fonctionnement d’un protocole simple de récupération de perte de paquets (bit alterné)

Définir ce qu'est le modèle TCP/IP et ses principaux mécanismes
Suivre un protocole de transmission de paquet
+++

+++sommaire 1

+++activité classe 20 ../01-__-_activites/applications-_directes/debranche-_--_-_rappels-_de-_seconde.md "Rappels de seconde"

## Le modèle TCP/IP

Avant les années 1970, les réseaux informatiques étaient soit gérés par les gouvernement soit par un petit nombre d'entreprises sous **licence propriétaire**. Depuis, les premiers réseaux publiques ont émergés et en 1983, le projet +++lien "https://fr.wikipedia.org/wiki/ARPANET" "Arpanet" utilise une architecture réseau implémentant le tout nouveau modèle **TCP/IP** conçu par l'américain +++lien "https://fr.wikipedia.org/wiki/Robert_Elliot_Kahn" "Bob Kahn". Aujourd'hui ce modèle s'est démocratisé partout dans le monde. Internet se base sur le modèle **TCP/IP** et permet, entre autres, l’élaboration d’applications et de services variés tels que :

- Courrier électronique
- World Wide Web (aussi abrégé en www)
- Transfert de fichiers
- Partage de fichiers
- Jeux en réseau
- Site Web
- ...

+++bulle matthieu
  quelle est la différence entre Internet et le Web ?
+++

Le TCP/IP est un modèle conceptuel de réseau. Ce modèle décompose un réseau en 5 couches. Chaque couche résout un certain nombre de problèmes relatifs à la transmission de données, et fournit des services bien définis aux couches supérieures. Les couches hautes sont plus proches de l'utilisateur et gèrent des données plus abstraites, en utilisant les services des couches basses qui mettent en forme ces données afin qu'elles puissent être émises sur un médium physique. 

+++imageFloat /images/md/A27E5E9HE8DG424C32GB4ABG8A4FG4
  la <b>couche applicative</b> qui représente les applications qu'utilise un utilisateur (applications mobiles, navigateur internet, ...). Voici des exemples de protocoles utilisés à ce niveau : HTTP, HTTPS, SMTP, FTP, SFTP, SSH, ...
+++

+++imageFloat /images/md/9EC2A9BA25G2HECF7A4HC5F42569F2 droite
  la <b>couche transport</b> qui assure que les données envoyées par un expéditeur dans le réseau arrivent bien à leur destinataire. Dans le modèle TCP/IP, le protocole TCP est utilisé dans la <b>couche transport</b>. Pour s'assurer que toutes les données sont arrivées à destination, le protocole TCP utilise un système d'accusé de réception (comme pour l'envoi d'une lettre en recommandé)
+++

+++imageFloat /images/md/396DF75GGHC25H37773A84E8FHE4EB
  la <b>couche réseau</b> qui représente l'architecture du réseaux utilisé. Lorsqu'une donnée circule dans le réseau, cette données circule de routeur à routeur jusqu'à arriver au destinataire. Pour connaître le bon chemin de routeur qu'il faut traverser, chacun des routeurs sont identifiés grâce à leur adresse IP. Dans le modèle TCP/IP, le protocole IP est utilisé dans la <b>couche réseau</b>.
+++

+++imageFloat /images/md/DG4H6F9C6CHH59F2E785HF69C2DAH3 droite
  la <b>couche liaison</b> qui représente de quelle manière sont connectés les nœuds du réseau. Une partie du réseau peut par exemple être reliée par des câbles éthernets, du wifi, de la fibre, ... et la <b>couche physique</b> qui détaille physiquement les liaisons établies au sein d'un réseau. Par exemple, concernant une liaison éthernet entre deux nœuds, nous pouvons utiliser soit un cable RJ45 ou alors un cable RJ11 etc... Pour une liaison Wifi entre deux nœuds du réseau, nous pouvons utiliser la norme 802.11 ou alors la norme nv2 etc...
+++

### Résumons +++emoji-write 

- Le TCP/IP est un modèle conceptuel de réseau
- Le modèle décompose un réseau en 5 couches. Les couches ont un rôle bien défini et ce découpage permet facilite l'implémentation de ce modèle pour un réseau de taille quelconque (un réseau allant de la taille d'une salle de cours à un réseau de la taille d'une planète)
- La couche applicative est la couche la plus haut niveau du modèle et est donc située au niveau de l'utilisateur et de l'application qu'il utilise (navigateur Web, jeu vidéo en ligne, application smartphone d'un réseau social, ...)
- Le protocole TCP, qui est utilisé dans la couche réseau, assure qu'une donnée émise par un émetteur soit bien remise au destinataire prévu
- Le protocole IP, qui est utilisé dans la couche liaison, permet de cartographier un ensemble de nœuds dans un réseau et donc, de déterminer quel est le chemin de nœud le plus optimal pour le trajet d'une donnée

### Encapsulation et décapsulation des données

Le modèle TCP/IP utilise un mécanisme d'encapsulation et de décapsulation de données.
+++video "https://www.youtube.com/watch?v=FJIFfkpUO7o" "Encapsulation et décapsulation de données à travers une réseau TCP/IP"

### Résumons +++emoji-write 

- N'importe quelle donnée peut transiter sur un réseau TCP/IP, ces données peuvent représenter : une image, du texte, une musique, une film, un flux streaming, ...
- Les données sont découpées en paquets de petites tailles. Cela permet entre autres de répartir plus facilement la charge dans un réseau
- Chaque paquet est encapsulé dans une série de conteneurs avant d'être expédié
- Un des principaux avantages de ce mécanisme d'encapsulation et que les informations d'un paquet insérées par une couche A ne peuvent être modifiées par une couche B

+++todo premiere heure de cours terminée ici, heure très lourde pour les élèves qui interagissent peu. Il faut trouver une manière de faire cette partie là (classe inversée par exemple ?)

## Étude d'un protocole réseau

De nombreux protocoles sont utilisés à travers toutes les couches du modèle TCP/IP. En première NSI, nous étudions le protocole du bit alterné qui est un protocole jadis utilisé dans le projet +++lien "https://fr.wikipedia.org/wiki/ARPANET" "Arpanet", l'ancêtre d'Internet.

+++activité binome 40 ../01-__-_activites/applications-_directes/debranche-_--_-_etude-_du-_protocole-_du-_--__bit-_alterne--__.md "Étude du protocole du 'bit alterné'"