# TP réseaux

+++programme
Simuler ou mettre en œuvre un réseau

Créer un réseau virtuel reposant sur le modèle TCP/IP
Déterminer l'adresse IP d'un équipement sur un réseau
Analyser un réseau pour observer ce qui y est connecté
+++

+++sommaire 1

## Installation du logiciel filius

Dans ce TP, nous allons construire et simuler des réseaux à l'aide d'un logiciel de simulation allemand nommé +++lien "https://www.lernsoftware-filius.de/Herunterladen" "Filius".

+++consignes
  <ol start="0">
    <li>Ouvrir un terminal et créer un dossier nommé "Applications" dans le 'home'</li>
    <li>Créer ensuite un dossier nommé "filius" dans le dossier "Applications"</li>
    <li>Se rendre sur le +++lien "https://www.lernsoftware-filius.de/Herunterladen" "site du logiciel"</li>
    <li>Depuis le terminal, se rendre dans le dossier "filius" puis télécharger dans le dossier "filius" l'archive zip du logiciel à l'aide de la commande bash <code>wget</code></li>
    <li>Décompressez l'archive à l'aide de la commande bash <code>unzip</code></li>
    <li>Démarrez l'application avec la commande bash <code>bash filius.sh</code></li>
  </ol>
+++

## Simuler un réseau simple entre deux ordinateurs

Échauffons nous en créant un réseau très simple : deux ordinateurs et un cable les reliant

+++question "À l'aide de l'interface graphique, créez deux ordinateurs et reliez les avec un cable"

+++question "Donner un nom à chaque ordinateur en double cliquant dessus"

+++image "/images/md/6DH3845437HG7GE6936G6ABFH4587E" col-6 mx-auto

Les deux ordinateurs possèdent la même adresse IP (192.168.0.10), ce qui va poser problème pour la suite.

+++question "Pour y remédier, double cliquez sur le portable B et assignez lui l'ip '192.168.0.11'"

+++image "/images/md/HDH67373H6EHE2B6484HDD43EE34EC" col-6 mx-auto

<hr>

<p>Une adresse IP, dans sa 4ème version (IPv4), est composée de quatre nombres séparés par des points. Chaque nombre peut prendre une valeur comprise entre 0 à 255, donc 256 valeurs sont possibles pour un nombre.</p>

<p> Nous pouvons dire qu'une adresse IP est composée de 4 octets (+++flag-us bytes) car, par définition, un octet corresponds à <b class="text-green">8 bits</b> et <b class="text-green">8 bits</b> peut contenir 256 valeurs possibles.</p>

<table class="table table-sm">
  <tbody>
    <tr class="text-center align-middle">
      <!-- <th rowspan=2 class='text-vertical px-2 text-center'>Adresse IP</th> -->
      <th rowspan="3">Adresse IP</th>
      <th>en décimal (base 10)</th>
      <td class="border-end-0">192</td>
      <td class="border-start-0 border-end-0">.</td>
      <td class="border-start-0 border-end-0">168</td>
      <td class="border-start-0 border-end-0">.</td>
      <td class="border-start-0 border-end-0">0</td>
      <td class="border-start-0 border-end-0">.</td>
      <td class="border-start-0">11</td>
    </tr>
    <tr class="text-center align-middle">
      <th>en binaire (base 2)</th>
      <td class="border-end-0"><b class="text-green">11000000</b></td>
      <td class="border-start-0 border-end-0">.</td>
      <td class="border-start-0 border-end-0"><b class="text-green">10101000</b></td>
      <td class="border-start-0 border-end-0">.</td>
      <td class="border-start-0 border-end-0"><b class="text-green">00000000</b></td>
      <td class="border-start-0 border-end-0">.</td>
      <td class="border-start-0"><b class="text-green">00001011<b></b></b></td>
    </tr>
    <tr class="text-center align-middle">
      <th>nombre de bits</th>
      <td class="border-end-0"><b class="text-green">8</b> (= 1 octet)</td>
      <td class="border-start-0 border-end-0"></td>
      <td class="border-start-0 border-end-0"><b class="text-green">8</b> (= 1 octet)</td>
      <td class="border-start-0 border-end-0"></td>
      <td class="border-start-0 border-end-0"><b class="text-green">8</b> (= 1 octet)</td>
      <td class="border-start-0 border-end-0"></td>
      <td class="border-start-0"><b class="text-green">8</b> (= 1 octet)</td>
    </tr>
  </tbody>
</table>

+++question write "Sachant qu'un octet contient <b class="text-green">8 bits</b>, combien de bits sont contenus dans une adresse IP ?"

+++question write "Sachant qu'un octet contient 2<sup><b class="text-green">8</b></sup> valeurs possibles, combien de valeurs sont possibles avec 4 octets ?"

<hr>

Revenons sur Filius

+++question "Cliquez sur le bouton play situé en haut de l'interface graphique afin de passer en mode simulation"

+++bulle matthieu
  le mode simulation permet d'interagir avec les différents composant du réseau comme si ils étaient réels
+++

+++bulle matthieu droite
  vous allez donc depuis un ordinateur virtuel exécuter des commandes pour analyser le réseau virtuel que vous avez créé
+++

+++question "Double cliquez sur les deux ordinateurs portables, deux popup doivent s'ouvrir"

+++image "/images/md/7BGD2DFFA2E58BGB2284EH333C843G" col-6 mx-auto

+++question "Dans chacun des ordinateurs, installez le 'logiciel' 'Ligne de commande' (n'oubliez pas de cliquer sur le bouton 'Appliquer les modifications')"

+++image "/images/md/EC5526DC7D24G7244EGBEC725B884F" col-6 mx-auto

La ligne de commande est une version allégée du terminal que vous avez l'habitude d'utiliser sur vos ordinateur de lycée sous Linux. Voici les commandes que vous allez utiliser : 

<table class="table table-sm">
  <thead>
    <tr>
      <th>commande</th>
      <th>description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code class="text-nowrap">ping [Adresse IP]</code></td>
      <td>envoie 4 paquets de données vers l'adresse IP saisie pour tester la connexion</td>
    </tr>
    <tr>
      <td><code class="text-nowrap">ipconfig</code></td>
      <td>affiche les caractéristiques réseaux de la machine</td>
    </tr>
    <tr>
      <td><code class="text-nowrap">traceroute [Adresse IP]</code></td>
      <td>permet de suivre le chemin qu'un paquet de données va mettre d'une machine à une autre</td>
    </tr>
  </tbody>
</table>

+++question "Ouvrez le logiciel 'Ligne de commande' depuis un des deux ordinateurs portable virtuel"

+++question "Dans la 'ligne de commande' utilisez la commande bash <code>ipconfig</code> et vérifiez que l'IP affichée par cette commande correspond bien à l'IP de l'ordinateur virtuel que vous utilisez"

+++question write "Effectuez un <code>ping</code> vers l'autre ordinateur. Quelle est la durée totale (en ms) de l'envoi du ping ?"

+++question "Faites un clic droit sur un des deux ordinateurs portable et cliquez sur 'Affichez les échanges de données (192.168...'"

Une nouvelle popup s'ouvre nommée "Échange de données". Dans cette fenêtre vous pouvez observer les échanges de données effectués avec l'ordinateur sur lequel vous avez cliquez. 

Si vous laissez cette popup ouverte et que vous effectuez de nouveau un ping depuis la 'ligne de commande', vous verrez que 4 nouvelles lignes vont s'ajouter dans la popup "Échange de données". Chaque ligne correspond à un paquet envoyé ou reçu par l'ordinateur. 

+++bulle matthieu
  Vous pouvez remarquer aussi que le protocole utilisé est le protocole ICMP (<b>I</b>nternet <b>C</b>ontrol <b>M</b>essage <b>P</b>rotocol)
+++

+++bulle matthieu droite
  Il s'agit d'un protocole qui sert à véhiculer des messages de contrôle et d’erreur dans la couche réseau du modèle TCP/IP
+++

## Simuler un réseau avec un serveur et un switch

Un switch est un équipement informatique permettant de créer un réseau entre différents ordinateurs (ordinateur au sens large : tablette, smartphone, frigo connecté, etc...)

+++question "En utilisant Filius, créez le réseau suivant"

+++image "/images/md/5543F8EG9G4667DH4D4364A7AFHDED" col-6 mx-auto

+++question "Configurez ce réseau en suivant les étapes ci-dessous"

<ul>
  <li>Configurez l'adresse IP du serveur avec 192.168.0.30</li>
  <li>Configurez l'adresse IP du portable A avec 192.168.0.10</li>
  <li>Configurez l'adresse IP du portable B avec 192.168.0.11</li>
  <li>Lancer la simulation</li>
  <li>Sur le serveur, installer un 'Serveur générique'</li>
  <li>Sur le portable A, installer un 'Client générique'</li>
</ul>

+++question "Et maintenant, simulez un échange de données client-serveur en suivant les étapes ci-dessous"

<ul>
  <li>Toujours en restant en mode simulation, cliquez sur le serveur puis dans la popup cliquez sur l'icône 'Serveur générique'. Démarrez le serveur sur le port 55555</li>
  <li>cliquez ensuite sur le portable A, puis dans la popup cliquez sur l'icône 'Client générique'. Connectez vous sur le serveur et envoyez un message au serveur</li>
</ul>

Vous devriez avoir un écran ressemblant à ceci : (en ouvrant aussi la fenêtre d'échange de données)

+++image "/images/md/3C8H2EB3G8GA4AADEFEHFDEGHD97EB" col-10 mx-auto

+++question write "Quel est le protocole utilisé pour le 'transport' des données entre le client et le serveur ?"

+++question write "Lors de la création du serveur générique, il est possible de préciser quel port utiliser. Qu'est-ce qu'un port et à quoi ça sert ? (s'aider de recherches sur Internet si besoin)"

## Simuler un super réseau de réseaux avec un routeur

Dans cette partie vous allez créer, configurer et manipuler le réseaux suivant :

+++image "/images/md/HCEA4F222HDCG5922FF84734B2596E" col-8 mx-auto

+++bulle matthieu
  lorsque vous ajoutez un routeur avec le logiciel Filius, ce dernier vous demande le nombre d'interface que dois gérer le routeur
+++

+++bulle matthieu droite
  un sous réseau occupe une interface. Comme il y a deux sous réseaux (gérés par deux switch) alors le routeur doit gérer 2 interfaces
+++

+++question "Après avoir créé les différents nœuds du super réseau (en respectant bien les noms), configurez chacun des nœuds comme suit :"

<ul>
  <li>Chaque ordinateur portable doit posséder l'adresse IP qui correspond à son nom. Cela permet en un coup d'œil  de visualiser toutes les IP utilisées dans le super réseau</li>
  <li>Dans un des ordinateurs portables, installer le logiciel 'Ligne de commande'</li>
</ul>

+++question "Depuis l'ordinateur où est installé la 'ligne de commande', effectuez un ping vers un autre ordinateur du même sous réseau"

+++question write "Depuis le même ordinateur, effectuez un ping vers un autre ordinateur situé dans l'autre sous-réseau. Quel message d'erreur s'affiche dans ce cas ?"

Ce message d'erreur est assez explicite, un ordinateur d'un sous-réseau n'a pas accès, par défaut, à un autre ordinateur d'un autre sous-réseau. Pour établir une connexion entre les deux sous-réseau, il faut alors configurer correctement le routeur qui lie les deux sous-réseaux. 

+++question "Configurez le routeur comme suit : "

<ul>
  <li>La connexion au switch 0 doit utiliser l'adresse IP 192.168.0.1<ul>
    +++image "/images/md/DA9GF6B83B7A67A95H4952C8DE94GC" col-10 mx-auto
  </ul></li>
  <li>La connexion au switch 1 doit utiliser l'adresse IP 192.168.1.1<ul>
    +++image "/images/md/3323D7AB9GGD34CH2H893227E6HFA7" col-10 mx-auto
  </ul></li>
</ul>

+++question "Depuis un ordinateur, effectuez un ping vers un autre ordinateur situé dans l'autre sous-réseau. Le message d'erreur apparaît toujours"

En effet, il faut aussi indiquer l'adresse passerelle à utiliser dans chaque ordinateur portable de chaque sous réseau. 

+++question "Donc configurez les ordinateurs utilisant l'IP 192.168.0.x pour que chacun d'entre eux utilise l'adresse passerelle 192.168.0.1"

+++question "Effectuer les mêmes opérations pour les ordinateurs utilisant l'IP 192.168.1.x, qui doivent eux, utiliser l'adresse passerelle 192.168.1.1"

+++question "Depuis un ordinateur, effectuez de nouveau un ping vers un autre ordinateur situé dans l'autre sous-réseau. Cela doit fonctionner maintenant ! "

+++question "Depuis un ordinateur, effectuez un traceroute vers un autre ordinateur situé dans l'autre sous-réseau et observer le cheminement "

## Vous êtes arrivés ici ? 

+++imageFloat /images/md/68B2AF9E7G94F6B27A7G8G5HHC67D3 droite
Et bien bravo déjà ! :D 
+++

Je vous invite à télécharger (puis charger) +++file "/fichiers/5453GF8EAHDAG6D3BCHHFH7DFCF864" "ce fichier filius" qui contient un réseau complexe simulant un "mini-Internet" : 
- amusez vous avec ! 
- exécutez des ping et des traceroute ! 
- essayez de comprendre comment est configuré le serveur DSN et comment il est utilisé
- essayez les autres commandes disponibles depuis la 'ligne de commande' ! 
- testez les autres logiciels installables via filius ! 


## Réponses

<ol start="3">
  <li>
    +++spoil
      Étant donné qu'une adresse IP contient l'équivalent de 4 octets, alors une adresse IP comptabilise au total 8 * 4 bits, soit 32 bits
    spoil+++
  </li>
  <li>
    +++spoil
      2<sup>8 * 4 octets</sup> = 2<sup>32</sup> = 4 294 967 296
    spoil+++
  </li>
</ol>

<ol start="10">
  <li>
    +++spoil
      Le temps peut varier d'un ping à l'autre. En moyenne, le temps dure environ 400ms (406ms au total avec le screenshot ci-dessous)
    
      +++image "/images/md/CD8BEB25DD32865B62G93GH573499A" col-8 mx-auto
    spoil+++
  </li>
</ol>

<ol start="15">
  <li>
    +++spoil
      D'après la fenêtre 'Échange de donnees', le protocole utilisé est le TCP.
    spoil+++
  </li>
  <li>
    +++spoil
      Un port est une porte virtuelle ouverte au niveau d'un serveur. Un port est code sur 16 bits, donc un port peut être entre la valeur 0 et 65536. Voici deux exemples de ports très connus, le 80 (ouvert pour une connexion sur un serveur web en utilisant le protocole HTTP) et le port 443 (ouvert pour une connexion sur un serveur web utilisant le protocole HTTPS)
    spoil+++
  </li>
</ol>

<ol start="19">
  <li>
    +++spoil
      Le message d'erreur est : 'Destination inaccessible'
      
      +++image "/images/md/AB9H23BDGG5A79337EADHAH59F9D6B" col-8 mx-auto
    spoil+++
  </li>
</ol>