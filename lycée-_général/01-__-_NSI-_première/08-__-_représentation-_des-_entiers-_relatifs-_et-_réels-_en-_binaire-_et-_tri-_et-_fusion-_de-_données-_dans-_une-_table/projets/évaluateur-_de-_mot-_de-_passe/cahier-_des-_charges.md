# Projet "Évaluateur de mot de passe"

+++sommaire

## Sujet du projet

La plupart des utilisateurs ont recours à des mots de passe simples à retenir et rapide à écrire. **Pire !** un même mot de passe est souvent utilisé à différents endroits à la fois. (ce dernier point n'est pas le sujet de ce projet)

Pour votre culture générale, je vous invite à parcourir +++lien "https://securite.developpez.com/actu/316250/Cybersecurite-66-statistiques-sur-les-mots-de-passe-qui-vont-changer-vos-habitudes-en-ligne-par-Panda-Security/" "cet article" qui regroupe quelques statistiques intéressantes sur nos habitudes avec les mots de passe dans le monde.

Le but de ce projet est de créer un site Web qui permet de noter un mot de passe saisi par un visiteur. Il y a deux principaux critères qui déterminent la robustesse d'un mot de passe : 

0. sa longueur et le type de caractères utilisés
1. la fréquence à laquelle le mot de passe est utilisé (si un mot de passe, aussi robuste soit il, est utilisé par tout le monde, alors il perd tout son intérêt)

Pour le premier critère, vous aurez la liberté de déterminer vos propres règles, vous pouvez bien sur vous aider de recherches sur le Web pour nourrir vos réflexions.

Pour le second critère, vous allez utiliser un fichier CSV contenant quelques mots de passe récurrents (vous pouvez obtenir une telle liste +++lien "https://fr.wikipedia.org/wiki/Liste_des_mots_de_passe_les_plus_courants#Projet_Richelieu" "sur wikipédia" par exemple). Une des options d'amélioration du site, décrite plus bas, consiste à consolider le fichier CSV au fur et à mesure que des utilisateurs saisissent des mots de passe à évaluer.

La note du mot de passe obtenue peut être une simple note (sur 20 par exemple). Une des options d'amélioration du site, décrite plus bas, propose d'afficher le score sous la forme d'un nutriscore. 

## Spécifications fonctionnelles

+++bulle matthieu
  les spécification fonctionnelles décrivent les fonctionnalités attendues du projet
+++
+++bulle matthieu droite
  un utilisateur non technique ( = non informaticien) doit pouvoir comprendre les spécification fonctionnelles
+++

Voici ci-dessous les fonctionnalités principales et obligatoires attendues à l'issue de ce projet. Une fois l'implémentation de ces fonctionnalités terminées, libre à vous d'améliorer votre site en vous intéressant entre autre à la génération de mot de passe évoqué en introduction.

### Fonctionnalités obligatoires du site 

Depuis son navigateur Web préféré, l'internaute doit pouvoir : 
0. Saisir dans un formulaire un mot de passe et valider sa saisie. 
1. La page chargée après la saisie du mot de passe doit afficher la note du mot de passe. Le calcul de cette note doit en partie se baser sur le contenu du fichier CSV. 
2. La page principale doit également afficher les 5 mots de passes les plus récurrents (d'après le contenu du fichier CSV)

Un travail supplémentaire devra être apporté. **Au moins** une des quatre options ci-dessous devra être choisie :

<table class="border-0">
  <tbody>
    <tr>
      <th class="px-2 text-center" colspan="2">Option #0 : générer et afficher un mot de passe aléatoire</th>
    </tr>
    <tr>
      <td class="px-2" colspan="2"><ul class="my-1">
        <li>Dans la page d'accueil, affichez un exemple de mot de passe (<b>généré aléatoirement</b>) avec une robustesse maximale d'après le système de notation que vous avez établi</li>
      </ul></td>
    </tr>
    <tr>
      <th class="px-2">Difficulté</th>
      <td class="px-2 text-center">+</td>
    </tr>
    <tr>
      <th class="px-2">Langage(s) utilisé(s)</th>
      <td class="px-2 text-center">HTML, python</td>
    </tr>
    <tr>
      <th class="px-2">rôle(s) principalement sollicité(s)</th>
      <td class="px-2 text-center">développeur front end, développeur front back</td>
    </tr>
    <tr>
      <th class="py-4 border-start-0 border-end-0" colspan="2"></th>
    </tr>
    <tr>
      <th class="px-2 text-center" colspan="2">Option #1 : afficher la notation sous la forme d'un nutriscore</th>
    </tr>
    <tr>
      <td class="px-2" colspan="2"><ul class="my-1">
        <li>Le nutriscore est une indication graphique, simple à lire et simple à comprendre, qui permet d'évaluer la qualité d'un produit consommable</li>
        +++image "/images/md/D87DC9G8G79ECHB5D5C39348E5G483" w-fit mx-auto
        <li>Il s'agit donc ici de modifier le score affiché pour le mot de passe en adoptant une notation similaire au nutriscore (nous pouvons parler de passscore). Ainsi, l'utilisateur obtiendra sur sa page un <b class="text-green">A</b> si le mot de passe est considéré comme très robuste selon vos critères, et un <b class="text-red">E</b> si le mot de passe est très mauvais.
        </li>
        <li>Un travail soigné sur l'affichage graphique du passscore est attendu</li>
        <li>En première étape peut consister à afficher seulement la bonne lettre (et non les 5 lettres côte à côte avec une des lettres mise en avant)</li>
      </ul></td>
    </tr>
    <tr>
      <th class="px-2">Difficulté</th>
      <td class="px-2 text-center">++</td>
    </tr>
    <tr>
      <th class="px-2">Langage(s) utilisé(s)</th>
      <td class="px-2 text-center">HTML, CSS</td>
    </tr>
    <tr>
      <th class="px-2">rôle(s) principalement sollicité(s)</th>
      <td class="px-2 text-center">développeur front end</td>
    </tr>
    <tr>
      <th class="py-4 border-start-0 border-end-0" colspan="2"></th>
    </tr>
    <tr>
      <th class="px-2 text-center" colspan="2">Option #2 : tester plusieurs mots de passe à la fois</th>
    </tr>
    <tr>
      <td class="px-2" colspan="2"><ul class="my-1">
        <li>Le site doit être amélioré, côté front comme côté back, afin de permettre à un utilisateur de tester plusieurs mots de passes en même temps</li>
        </ul></td>
    </tr>
    <tr>
      <th class="px-2">Difficulté</th>
      <td class="px-2 text-center">+++</td>
    </tr>
    <tr>
      <th class="px-2">Langage(s) utilisé(s)</th>
      <td class="px-2 text-center">HTML, CSS, python</td>
    </tr>
    <tr>
      <th class="px-2">rôle(s) principalement sollicité(s)</th>
      <td class="px-2 text-center">développeur frontend, développeur backend</td>
    </tr>
    <tr>
      <th class="py-4 border-start-0 border-end-0" colspan="2"></th>
    </tr>
    <tr>
      <th class="px-2 text-center" colspan="2">Option #3 : enregistrer les mots de passe saisis par les utilisateurs pour améliorer le système de notation</th>
    </tr>
    <tr>
      <td class="px-2" colspan="2"><ul class="my-1">
        <li>Lors de la saisie d'un mot de passe, ce dernier doit être ajouté dans le fichier CSV si il n'existe pas déjà</li>
        <li>Le fichier CSV doit également enregistrer le nombre de fois qu'un mot de passe a été saisi</li>
        <li>Le système de notation doit être amélioré pour prendre en compte la fréquence de saisie d'un mot de passe. Autrement dit, plus un mot de passe a été saisi, moins sa note doit être bonne</li>
        </ul></td>
    </tr>
    <tr>
      <th class="px-2">Difficulté</th>
      <td class="px-2 text-center">++++</td>
    </tr>
    <tr>
      <th class="px-2">Langage(s) utilisé(s)</th>
      <td class="px-2 text-center">HTML, python</td>
    </tr>
    <tr>
      <th class="px-2">rôle(s) principalement sollicité(s)</th>
      <td class="px-2 text-center">développeur frontend, développeur backend</td>
    </tr>
  </tbody>
</table>

## Spécifications techniques

+++bulle matthieu
  les spécification techniques décrivent les moyens techniques choisis par les informaticiens pour mettre en œuvre le projet
+++

+++bulle matthieu droite
  cette partie d'un cahier des charges est destinée à l'équipe qui développe le projet
+++

### Rôles 

Dans ce projet, vous aurez besoin :
- d'un +++lien "/enseignant/didactique/modeles/projet/roles/developpeur-_frontend.md" "**développeur frontend**" : cette personne se chargera de développer l'interface graphique du site
- d'un +++lien "/enseignant/didactique/modeles/projet/roles/developpeur-_backend.md" "**développeur backend**" : cette personne se chargera de développer le serveur du site

### Environnement technique

- Le site est hébergé dans un serveur développé en python (utilisant le framework flask)
- Il est tout à fait possible d'utiliser des bibliothèques python facilitant la manipulation de fichier csv (comme la bibliothèque pandas ou encore la bibliothèque csv)
- L'utilisation de bibliothèques ou frameworks supplémentaires est libre

## Comment démarrer ? 

### Réunion d'équipe !

0. Tous ensemble : +++fichier "/fichiers/8CCHEHD6H77BCDH755GC76DC7BH76E" "téléchargez" l'archive zip et extrayez-la afin d'obtenir la base de code du projet/ Analyser ensuite rapidement la structure des dossiers et fichiers, le contenu des fichiers <code>server.py</code>, <code>motsdepasse.csv</code> et des fichiers contenus dans le dossier <code>templates</code>
1. Répartissez vous les rôles, un rôle peut être pris par plusieurs membres du groupe et un membre de l'équipe peut changer de rôle en cours de projet. En fin de projet, tous les rôles doivent être pourvus

+++bulle matthieu
  n'oubliez pas que lors de la présentation du projet, je vous questionnerai sur des parties sur lesquelles vous n'avez pas forcément travaillé
+++

+++bulle matthieu droite
  donc partagez les informations au sein de l'équipe !
+++

2. Réfléchissez ensemble quelques minutes au système de notation du mot de passe ("à partir de combien de caractère le mot de passe a la note maximale ?", "quels caractères doivent être obligatoires dans le mot de passe pour qu'il soit considéré comme robuste ?", etc...)
3. Définissez vous des objectifs informels, faîtes des points rapides au sein de l'équipe toutes les 30/45/60 minutes
4. Assurez vous que chaque ordinateur utilisé soit bien configuré pour pouvoir travailler dessus (projet extrait sur l'ordinateur et serveur local déployé)

### Pour démarrer, je choisis le rôle de développeur backend !

- Le fichier CSV fourni est très léger, complétez le en ajoutant dedans des mots de passe couramment utilisés. Vous pouvez vous inspirer de +++lien "https://fr.wikipedia.org/wiki/Liste_des_mots_de_passe_les_plus_courants" "cette page" pour ajouter des mots de passe très utilisés.
- Vous devez dans un premier temps évaluer un mot de passe écrit **en dur** (car la partie frontend n'est pas encore développée) dans le code et afficher la note obtenue dans une page Web. Ceci peut être fait en plusieurs étapes :
    - en python : compléter la fonction 'evaluation' dont le but consiste à évaluer un mot de passe. Dans cette fonction, il faut vérifier entre autres si le mot de passe ne fait pas partie des mots de passes contenus dans le fichier CSV (si c'est le cas, la note sera négativement impactée). Restez simple dans cette première étape dans le contenu de cette fonction (n'implémentez pas toutes les règles que vous avez établi avec votre équipe !)
    - en python : se servir de la fonction 'evaluation' pour générer une note à partir d'un mot de passe écrit en dur puis transmettre cette note dans un fichier html grâce à flask
    - en html : récupérer la note et l'afficher dans une balise p (par exemple)

+++bulle matthieu
  bravo ! vous avez généré une première note et l'avez affichée sur une page web générée par votre serveur
+++

- À cette étape, travaillez avec un développeur front afin de récupérer le mot de passe depuis un formulaire Web. Ainsi, le mot de passe évalué par votre fonction 'evaluation' sera le mot de passe saisi par un visiteur sur votre site
- Améliorez le contenu de votre fonction 'evaluation' afin de respecter toutes les règles de notation que vous souhaitez et/ou répondre aux demandes du développeur frontend selon le travail qu'il a choisi d'effectuer
- Pour la suite, choisissez une options d'amélioration décrites dans les spécifications fonctionnelles

### Pour démarrer, je choisis le rôle de développeur frontend !

- La note est en partie évaluée grâce au contenu d'un fichier CSV. Ce fichier au début va être travaillé par un développeur backend, vous ne vous en occupez pas
- Dans un premier temps, vous devez modifier la page principale HTML pour y ajouter un formulaire. Ce formulaire servira au visiteur à saisir un mot de passe et à le soumettre à une évaluation
- Une fois ceci fait, aidez si besoin le développeur backend à terminer la première version de la fonction python d'évaluation de note.
- Travaillez avec le développeur backend afin d'afficher la note d'un mot de passe sur une page Web
- Pour la suite, choisissez une options d'amélioration décrites dans les spécifications fonctionnelles

#### **Détaillé de l'option #0**

- côté backend : l'approche la plus propre consiste à créer une fonction python qui retourne un mot de passe généré de robustesse maximale. La bibliothèque 'random' de python sera sans doute utile et il faudra s'assurer que le mot de passe n'est pas présent dans la liste des mots de passe contenue dans le fichier CSV
- côté frontend : le travail est assez simple ici, il faut juste aménager un espace dans la page d'accueil (avec du code HTML) dans lequel le mot de passe généré devra apparaître
- Pour rappel : le mot de passe généré et affiché sur la page doit obtenir la note maximale si on l'évalue à travers votre site

#### **Détaillé de l'option #1**

- côté backend : il faut adapter la fonction python 'evaluation' pour qu'elle retourne non pas un chiffre, mais une lettre (comprise entre A et E)
- côté frontend : il faut afficher visuellement un passscore mettant en avant la lettre obtenue suite à la saisie du mot de passe. Le plus gros du travail s'effectuera avec le langage CSS et un peut dans le fichier template HTML dans lequel vous pouvez utiliser des instructions fournies par flask (vous allez avoir besoin notamment de faire un 'if' dans la page 'resultat.html' grâce à flask, je vous laisse faire des recherches !)