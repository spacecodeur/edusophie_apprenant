# Projet "Site e-commerce"

+++sommaire

## Sujet du projet

Dans ce projet vous développez un mini-site e-commerce. Le thème de ce site est libre, il n'y a pas de contraintes particulières sur les produits qui sont proposés. Un produit peut être par exemple un bien matériel fictif (une licorne) ou non (un poney) ou encore un service (un cours d'équitation sur ??)

Bien entendu, il n'est pas attendu ici le développement d'un site complet mais d'une première partie importante qui est la présentation de la liste de produits. 

Si vous êtes en avance dans le projet, vous pourrez vous **pencher** sur les autres briques importantes qui sont nécessaires pour le développement d'un site e-commerce : gestion d'un panier,  système d'authentification, tunnel de ventre, communication avec une API bancaire

## Spécifications fonctionnelles

+++bulle matthieu
  les spécification fonctionnelles décrivent les fonctionnalités attendues du projet
+++
+++bulle matthieu droite
  un utilisateur non technique ( = non informaticien) doit pouvoir comprendre les spécification fonctionnelles
+++

Voici ci-dessous les fonctionnalités principales et obligatoires attendues à l'issue de ce projet. Une fois l'implémentation de ces fonctionnalités terminées, libre à vous d'améliorer votre site en vous intéressant à une des briques évoquées en introduction.

### Fonctionnalités obligatoires du site 

Depuis son navigateur Web préféré, l'internaute doit pouvoir : 
0. Accéder à la liste des produits sur une page dédiée 
1. Depuis la page affichant tous les produits, l'utilisateur doit pouvoir trier les produits selon au moins deux critères différents : par prix, par nom
2. Accéder au détaillé d'un produit dans une page dédiée

Un travail supplémentaire devra être apporté. **Au moins** une des quatre options ci-dessous devra être choisie :

<table class="border-0">
  <tbody>
    <tr>
      <th class="px-2 text-center" colspan="2">Option #0 : rendre le site accessible aux malvoyants (début de projet)</th>
    </tr>
    <tr>
      <td class="px-2" colspan="2"><ul class="my-1">
        <li>Se renseigner sur comment on développe un site pour qu'il soit accessible à des personnes malvoyantes</li>
        <li>Adapter le site e-commerce afin qu'il soit accessible à des personnes malvoyantes</li>
      </ul></td>
    </tr>
    <tr>
      <th class="px-2">Difficulté</th>
      <td class="px-2 text-center">+</td>
    </tr>
    <tr>
      <th class="px-2">Langage(s) utilisé(s)</th>
      <td class="px-2 text-center">HTML</td>
    </tr>
    <tr>
      <th class="px-2">rôle(s) principalement sollicité(s)</th>
      <td class="px-2 text-center">développeur front end</td>
    </tr>
    <tr>
      <th class="py-4 border-start-0 border-end-0" colspan="2"></th>
    </tr>
    <tr>
      <th class="px-2 text-center" colspan="2">Option #1 : créer le design du site à l'aide d'un outils connu (début de projet)</th>
    </tr>
    <tr>
      <td class="px-2" colspan="2"><ul class="my-1">
        <li>Apprendre à utiliser le framework <b>bootstrap</b> qui facilite le développement d'interfaces graphiques Web complexes</li>
        <li>En utilisant bootstrap, implémenter sur la page présentant tous les produits une grille de cartes ( +++flag-us card as grid ) qui s'adapte en fonction de la taille de l'écran (smartphone, ordinateur, ...)</li>
        <li>En affichage <b>smartphone</b>, les produits sont affichés les uns en dessous des autres. En affichage <b>desktop</b> (écran d'ordinateur), plusieurs produits peuvent être affichés côte à côte
        
        <ul><li>affichage <b>desktop</b> : </li></ul>
        +++image "/images/md/E74HF9FB79G5AHEH88GC4E7FFD7FC2" col-6 mx-auto
        
        <ul><li>affichage <b>smartphone</b> : </li></ul>
        +++image "/images/md/67F6B6H4F4C3552B3HB58EGA6F95H2" col-2 mx-auto
        
        </li>
      </ul>
      +++bulle matthieu
        l'outil de développement permet de simuler un affichage comme si nous visualisions le site depuis un smartphone
      +++
      </td>
    </tr>
    <tr>
      <th class="px-2">Difficulté</th>
      <td class="px-2 text-center">++</td>
    </tr>
    <tr>
      <th class="px-2">Langage(s) utilisé(s)</th>
      <td class="px-2 text-center">HTML, CSS</td>
    </tr>
    <tr>
      <th class="px-2">rôle(s) principalement sollicité(s)</th>
      <td class="px-2 text-center">développeur front end</td>
    </tr>
    <tr>
      <th class="py-4 border-start-0 border-end-0" colspan="2"></th>
    </tr>
    <tr>
      <th class="px-2 text-center" colspan="2">Option #2 : développer une page qui permet d'ajouter ses propres produits</th>
    </tr>
    <tr>
      <td class="px-2" colspan="2"><ul class="my-1">
        <li>Créer une nouvelle page contenant un formulaire permettant d'ajouter dans le fichier CSV une nouvelle entrée (une entrée = un produit pour rappel)</li>
        <li>Dans le cadre de ce projet, la mise en place d'une protection de cette page (mot de passe..) n'est pas requise</li>
        </ul></td>
    </tr>
    <tr>
      <th class="px-2">Difficulté</th>
      <td class="px-2 text-center">+++</td>
    </tr>
    <tr>
      <th class="px-2">Langage(s) utilisé(s)</th>
      <td class="px-2 text-center">HTML, CSS, python</td>
    </tr>
    <tr>
      <th class="px-2">rôle(s) principalement sollicité(s)</th>
      <td class="px-2 text-center">développeur frontend, développeur backend</td>
    </tr>
    <tr>
      <th class="py-4 border-start-0 border-end-0" colspan="2"></th>
    </tr>
    <tr>
      <th class="px-2 text-center" colspan="2">Option #3 : créer un "infinite scroll"</th>
    </tr>
    <tr>
      <td class="px-2" colspan="2"><ul class="my-1">
        <li>Un "infinite scroll" représente l'apparition continuelle d'éléments dans une page au fur et à mesure que l'utilisateur scroll vers le bas. Ce comportement est très utilisé notament dans les réseaux sociaux.</li>
        <li>Pour développer cette fonctionnalité, une première étape peut consister à afficher les dix premiers produits au chargement de la page de votre site. Puis lorsque l'utilisateur scroll tout en bas de la page, la page charge automatiquement le reste des articles</li>
        </ul></td>
    </tr>
    <tr>
      <th class="px-2">Difficulté</th>
      <td class="px-2 text-center">++++</td>
    </tr>
    <tr>
      <th class="px-2">Langage(s) utilisé(s)</th>
      <td class="px-2 text-center">HTML, CSS, javascript, python</td>
    </tr>
    <tr>
      <th class="px-2">rôle(s) principalement sollicité(s)</th>
      <td class="px-2 text-center">développeur frontend, développeur backend</td>
    </tr>
  </tbody>
</table>

## Spécifications techniques

+++bulle matthieu
  les spécification techniques décrivent les moyens techniques choisis par les informaticiens pour mettre en œuvre le projet
+++

+++bulle matthieu droite
  cette partie d'un cahier des charges est destinée à l'équipe qui développe le projet
+++

### Rôles 

Dans ce projet, vous aurez besoin :
- d'un +++lien "/enseignant/didactique/modeles/projet/roles/developpeur-_frontend.md" "**développeur frontend**" : cette personne se chargera de développer l'interface graphique du site
- d'un +++lien "/enseignant/didactique/modeles/projet/roles/developpeur-_backend.md" "**développeur backend**" : cette personne se chargera de développer le serveur du site

### Environnement technique

- Le site est hébergé dans un serveur développé en python (utilisant le framework flask)
- Il est tout à fait possible d'utiliser des bibliothèques python facilitant la manipulation de fichier csv (comme la bibliothèque pandas ou encore la bibliothèque csv)
- option #1 : le framework CSS utilisé est bootstrap (version 5)
- option #2 : le chargement dynamique de contenu dans une page utilise une technologie javascript très connue, la technologie AJAX (Asynchronous JavaScript and XML)
- L'utilisation de bibliothèques ou frameworks supplémentaires est libre

## Comment démarrer ? 

### Réunion d'équipe !

0. Tous ensemble : +++fichier "/fichiers/F5ABCG2H2DF358A689F6A94889CH7F" "téléchargez" l'archive zip et extrayez-la afin d'obtenir la base de code du projet/ Analyser ensuite rapidement la structure des dossiers et fichiers, le contenu des fichiers <code>server.py</code>, <code>produits.csv</code> et des fichiers contenus dans le dossier <code>templates</code>
1. Répartissez vous les rôles, un rôle peut être pris par plusieurs membres du groupe et un membre de l'équipe peut changer de rôle en cours de projet. En fin de projet, tous les rôles doivent être pourvus

+++bulle matthieu
  n'oubliez pas que lors de la présentation du projet, je vous questionnerai sur des parties sur lesquelles vous n'avez pas forcément travaillé
+++

+++bulle matthieu droite
  donc partagez les informations au sein de l'équipe !
+++

2. Définissez vous des objectifs informels, faîtes des points rapides au sein de l'équipe toutes les 30/45/60 minutes
3. Assurez vous que chaque ordinateur utilisé soit bien configuré pour pouvoir travailler dessus (projet extrait sur l'ordinateur et serveur local déployé)

### Pour démarrer, je choisis le rôle de développeur backend !

- Le fichier CSV fourni est très léger, complétez le en ajoutant de nouveaux produits et éventuellement de nouvelles colonnes
- Vous devez trouver un moyen d'afficher le contenu du fichier CSV dans une page Web en utilisant flask, une première étape consisterai à : 
    - en python : extraire dans une chaîne de caractère les informations du premier produit depuis le fichier CSV
    - en python : extraire le nom du produit (ou du service)
    - en python : trouver le moyen, avec flask, d'envoyer le nom du produit dans une page HTML
    - en html : récupérer le nom du produit et l'afficher dans une balise p (par exemple)

+++bulle matthieu
  bravo ! vous avez extrait une donnée du fichier CSV et l'avez affichée sur une page web générée par votre serveur
+++

+++bulle matthieu droite
  améliorez maintenant votre code afin que <b>tous les produits soient envoyés</b> au template HTML, et que toutes les informations de tous les produits soient affichées sur la page Web
+++

- Une fois ceci fait, partagez votre code (et fichier CSV) avec les développeurs front afin qu'ils puissent travailler avec les données du fichier CSV
- Il faut ensuite réfléchir / rechercher et implémenter un système qui permet d'accéder à la page détaillée d'un produit. Ce travail nécessitera aussi l'intervention d'un développeur frontend
- Pour la suite, choisissez une options d'amélioration décrites dans les spécifications fonctionnelles

### Pour démarrer, je choisis le rôle de développeur frontend !

- Les données du site sont puisées depuis un fichier CSV. Ce fichier au début va être travaillé par un développeur backend, vous ne vous en occupez pas
- Dans un premier temps, vous devez choisir entre une des deux premières options décrites dans les spécifications fonctionnelles : 
    - l'option #0 (accessibilité du site) **ou** l'option #1 (travail sur le design du site)

+++bulle matthieu
  je vous conseille de laisser de côté au début les options #2 et #3, ces dernières qui sont plus avancées nécessitent d'avoir avancé à la fois côté front et côté back
+++

- Des données statiques (!= dynamique) sont déjà disponibles dans le code HTML fourni en début de projet. Vous pouvez donc travailler dessus par rapport à l'une des deux options que vous choisissez. À partir d'un moment, le backend aura codé la partie permettant d'extraire les données depuis un fichier CSV, vous devrez alors fusionner vos codes afin que côté frontend vous puissiez travailler sur des données (dynamiques) qui proviennent du fichier CSV
- Pour la suite, choisissez une options d'amélioration décrites dans les spécifications fonctionnelles

#### **front** : Je choisis l'option #0

- Renseignez vous sur le Web sur ce qu'est l'accessibilité dans le développement Web
- Recherchez et installez un plugin de navigateur qui permet de lire une page Web, testez le plugin avec des pages Web quelconques 
- Recherchez des exemples de bonne pratique de programmation HTML qui facilitent l'accessibilité du site e-commerce aux personnes malvoyantes. Implémentez ces bonnes pratiques dans le code HTML nécessaire pour développer les fonctionnalités obligatoires du site

+++bulle matthieu
  l'accessibilité Web est un domaine assez vaste, ainsi vous ne pourrez bien sur pas couvrir tous les cas de figures sur le site
+++

+++bulle matthieu droite
  ainsi, concentrez vous sur quelques points tournant autour de bonnes pratiques de programmation HTML
+++

#### **front** : Je choisis l'option #1

- Une réunion avec l'enseignant aura lieu en début de projet avec toutes les personnes qui choisissent de travailler sur cette option
- Le code fourni en début de projet utilise la version 5 de bootstrap (vous n'avez donc pas à installer le framework bootstrap), soyez vigilant dans vos recherches par rapport à cette version
- Choisissez ensuite entre : 
    - comment mettre en place une jolie grille de produit sur votre site **ou** intéressez vous à comment rendre l'interface graphique du site adaptable en fonction de la taille de l'écran (on parle de site responsive)