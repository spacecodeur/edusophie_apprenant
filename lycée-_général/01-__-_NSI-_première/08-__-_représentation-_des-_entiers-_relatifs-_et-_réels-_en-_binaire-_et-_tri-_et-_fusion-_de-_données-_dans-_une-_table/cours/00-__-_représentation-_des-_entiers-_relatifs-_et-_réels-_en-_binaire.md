# Représentation des entiers relatifs et réels

+++programme
Utiliser le complément à 2
Calculer sur quelques exemples la représentation de nombres réels : 0.1, 0.25 ou 1/3  
  
Représenter un nombre binaire négatif
Calculer des approximations de nombre binaires réels
+++

+++sommaire

## Représentation de nombre binaire négatif 

Nous avons vu dans une séance précédente comment représenter un nombre en base 10 (que nous utilisons quotidiennement) en base 2 (la base que comprennent les composants des machines). Prenons l'exemple du nombre 17<sub>10</sub> : 

+++bulle matthieu
  quel est l'équivalent binaire de 17<sub>10</sub> sur un octet ?
+++

+++spoil
  En utilisant le méthode de divisions successives, 17<sub>10</sub> = 00010001<sub>2</sub>
spoil+++

+++bulle matthieu
  sachant que nous ne pouvons utiliser que des 0 et des 1, comment représenter en binaire le nombre -17<sub>10</sub> ?
+++

+++spoil "Réponse naïve"
  une méthode naïve, jamais utilisée en pratique, consiste à utiliser le bit de droite pour représenter le signe, 0 correspond à un nombre positif et 1 à un nombre négatif. Donc : 
<ul>
  <li>-17<sub>10</sub> donne en binaire <b>0</b>0010001<sub>2</sub></li>
  <li>17<sub>10</sub> donne en binaire <b>1</b>0010001<sub>2</sub></li>
</ul>
 
spoil+++

Ainsi, sur un octet, nous pourrions représenter les nombres -127<sub>10</sub> à 127<sub>10</sub>. 

+++bulle matthieu
  en suivant cette approche naïve, quels seraient les équivalents en binaire des nombres 127<sub>10</sub> et -127<sub>10</sub> ?
+++

+++spoil
<ul>
  <li>127<sub>10</sub> = <b>0</b>111111<sub>2</sub></li>
  <li>-127<sub>10</sub> = <b>1</b>111111<sub>2</sub></li>
</ul>
spoil+++

<hr>

Cette approche présente <b class="text-red">deux défauts</b> :
- le premier est que le nombre 0<sub>10</sub> à deux représentations : <b>1</b>000000<sub>2</sub> et <b>0</b>000000<sub>2</sub>
- <u>Plus grave encore</u>, l'addition entre nombres binaires ne fonctionne tout simplement plus ! vérifions cela
    - 54<sub>10</sub> en binaire, sur un octet, a comme valeur <b>0</b>0110110<sub>2</sub>
    - et donc, d'après l'approche naïve, -54<sub>10</sub> en binaire, sur un octet, a comme valeur <b>1</b>0110110<sub>2</sub>

+++bulle matthieu
  quel est le résultat de l'addition 01001100 + 11001100 ? est-ce que le résultat correspond bien à l'addition 54<sub>10</sub> + -54<sub>10</sub> ?
+++

Dans l'activité suivante, vous allez apprendre à représenter un nombre binaire négatif en utilisant le complétment à 2, Il s'agit d'une méthode plus robuste que l'approche naïve d'au dessus et qui permet d'effectuer des opérations mathématiques avec des nombres binaires relatifs.

+++activité binome 35 ../activites/applications-_directes/representation-_d--__un-_binaire-_relatif-_--_-_methode-_du-_complement-_a-_2.md "Représentation d'un binaire relatif : méthode du complément à 2"


### +++emoji-write Résumons

- plusieurs stratégies sont possibles pour représenter un nombre binaire négatif
- une stratégie connue est la méthode du complément à 2
    - à partir d'un nombre binaire, il faut inverser tous ses bits
    - ensuite, il faut ajouter 1 au nombre binaire
- la méthode du complément à 2 a l'avantage de convertir des nombres avec lesquels nous pouvons ensuite effectuer des opérations mathématiques courantes (addition, soustraction, ...). Cette méthode n'est pas parfaite pour autant (voir question 1 de l'activité #0)
- la taille de la mémoire est importante lorsqu'on effectue des opérations avec des nombres binaires. Si lors d'une addition, le nombre obtenu dépasse la taille de la mémoire utilisée (par exemple 1 octet, qui est égal à 8 bits), alors les bits qui dépassent la taille maximale de la mémoire sont tout simplement perdus

## Représentation de nombre binaire réel

En programmation, un nombre réel est appelé nombre **flottant**. D'où le type de variable <code>float</code> que nous avons vu en début d'année mais très peu utilisé depuis. Comment est représenté un nombre réel en binaire ?

Un ordinateur, aussi puissant soit il, n'est pas capable **fondamentalement** de calculer avec précision des opérations qui utilisent des nombres réels

widget python
a = 0.1 # a est une variable de type float
b = 0.2 # b est une variable de type float
print(0.1+0.2) # même résultat sur votre IDE
widget 

**Fondamentalement** car, entre deux entiers naturels, il existe une infinité de nombre réels. Un ordinateur ne pouvant pas contenir l'ensemble des nombres réels existants, il va alors utiliser des représentations approximatives des nombres réels.

+++bulle matthieu
  il existe (fort heureusement) bon nombre de domaines dans lesquels nous restons meilleurs que les ordinateurs ;)
+++

+++activité binome 35 ../activites/applications-_directes/representation-_de-_nombres-_reels-_en-_binaire.md "Représentation de nombres réels en binaire"

### +++emoji-write Résumons

- Les ordinateurs, tels qu'ils sont conçus **fondamentalement** à ce jour, ne peuvent faire que des opérations approximatives avec les nombres à virgule
- Dans un programme (python, javascript, ...), il faut dans la mesure du possible éviter de manipuler des nombres flottants 
- Pour représenter un nombre flottant, il faut entre autres utiliser la méthode des multiplications successives par 2