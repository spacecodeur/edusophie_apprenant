# Tri et fusion de données en table avec pandas

+++consignes
  <ul>
    <li>Créez dans votre dossier du jour le fichier tri_csv_avec_pandas.py. Vous travaillerez sur ce fichier dans cette activité</li>
    <li>Suivre l'activité et répondre aux questions</li>
    <li>Si vous avez terminé l'activité avant votre binôme, aidez le à terminer de son côté</li>
  </ul>
+++

Dans +++lien "./tri-_de-_donnees-_en-_table.md" "la précédente activité" nous avons analysé un code permettant de charger le contenu d'un fichier CSV et de trier les données qui se trouvent dedans. 

À travers plusieurs étapes, nous allons apprivoiser la bibliothèque **python pandas** qui va nous permettre de réduire radicalement le code nécessaire pour pouvoir trier des données structurées en table (fichier au format **CSV**).

Nous travaillons toujours sur le même fichier CSV dont le contenu doit ressembler à :

```csv
email,telephone,annee_ajout,genre
junie@hotmail.fr,0682145672,2017,2
john@gmail.com,0745632109,2017,1
marc@icloud.fr,0675489125,2018,1
nicolas@wanadoo.fr,0601084421,2019,1
mathilde@yopmail.com,0675489112,2021,2
```

## Tri de données

Voici un aperçu général des principales étapes que doit suivre le programme python : 

0. je charge le contenu du fichier CSV
1. j'affiche le contenu du fichier CSV
2. je tri les données en fonction d'un critère
3. j'affiche le contenu du fichier CSV avec les données triées

+++bulle matthieu
  pour répondre aux questions de cette activité, des recherches sur Internet sont nécessaires
+++

+++bulle matthieu droite
  le but secondaire de cette activité est de vous entraîner à rechercher par vous même des informations techniques sur le Web :) 
+++

+++question write "En utilisant les fonctions fournies par la bibliothèque pandas, écrire le programme python qui charge le contenu d'un fichier CSV puis l'affiche"

+++question write "En utilisant toujours les fonctions fournies par la bibliothèque pandas, améliorer votre programme afin qu'il trie (+++flag-us sort) les données en fonction de l'année d'ajout. Pour terminer, le programme affiche les données ainsi triées"

## Fusion de données

Fusionner des données est une opération courante dans le traitement de données : 
0. la fusion permet de centraliser des informations dans un même endroit (un même fichier)
1. voir mieux ! fusionner différentes données entres elles permet de créer alors de nouvelles données plus intéressantes

+++bulle matthieu
  dans le second cas, on parle d'enrichissement de données
+++

+++bulle matthieu droite
  bon nombre d'entreprise génèrent du profil grâce à de l'enrichissement de données
+++

### Accumuler des mêmes données dans un seul fichier

Imaginons que notre annuaire soit un annuaire d'un club sportif. Chaque club en France remplit de son côté son annuaire. Tous les annuaires respectent la même structure.

La fédération de ce club de sport veut générer un annuaire national. Cet annuaire va donc contenir toutes les données de tous les annuaires des clubs. Vous allez développer le programme python qui permet de fusionner deux annuaires entre eux.

+++bulle matthieu
  il faut toujours bien sur utiliser les outils fournis par la bibliothèque pandas
+++

+++question "Créez un autre fichier annuaire (nommé annuaire2.csv) et saisissez manuellement dedans les informations de 3 personnes"

+++question write "Écrire un programme python qui charge le contenu de deux fichiers CSV, les concatène entre eux (+++flag-us concat) et enregistre un nouveau fichier csv sur votre ordinateur (qui est la fusion des deux fichiers CSV initiaux)"

+++bulle matthieu
  le premier fichier CSV contient 5 personnes, le deuxième contient 3 personnes 
+++

+++bulle matthieu droite
  Donc le programme python doit générer un nouveau fichier CSV contenant les informations de 8 personnes
+++

### Fusionner des données différentes dans un même fichier

Imaginez vous en 2004 à la place de +++lien "https://fr.wikipedia.org/wiki/Mark_Zuckerberg" "Mark Zuckerberg", le fondateur et développeur initial du réseau social +++lien "https://fr.wikipedia.org/wiki/Facebook#D%C3%A9veloppement_(2004%E2%80%932005)" "Facebook".

En plus du temps de développement, le coût de maintenance d'un site Web de cette ampleur devient rapidement important : 
- la location et la maintenance des serveurs : pour gérer le site Web, stocker les vidéos/musiques/etc ... 
- le recrutement de nouveaux développeurs, de modérateurs, etc ...

Où trouver le financement pour maintenir le site et son évolution ? la publicité bien sur ! et plus précisément la publicité ciblée ! et c'est là que l'**enrichissement de données** intervient.

Dans un serveur Facebook sont stockées nos données : 
- notre adresse email
- notre age
- notre genre
- etc...

Ces données sont stockées dans des bases de données (concept qui sera étudié en Terminale NSI). Pour simplifier, un fichier CSV est une sorte de base de données minimaliste.

Supposons donc que les données des utilisateurs soient stockées dans un serveur Facebook dans un fichier CSV : 

```csv
email;age;genre
paul@hotmail.fr;24;1
francine@gmail.com;32;2
camille@icloud.fr;13;2
nicolas@wanadoo.fr;17;1
aurelia@yopmail.com;48;2
```

Lorsqu'un internaute se connecte sur Facebook, un cookie de session est créé dans son navigateur Web. La valeur de ce cookie est unique et permet à un visiteur de rester connecté avec son compte personnel sur Facebook.

Quelques jours plus tard, cet internaute visite divers sites Web comme par exemple un site de presse. Dans ce site de presse, l'internaute, appréciant un des articles, clique sur le bouton "like" et partage l'article sur Facebook. 

Lors du clic sur un bouton "Like" ou lors d'un partage "vers Facebook", un programme côté Facebook se charge d'enregistrer ces actions dans des fichiers CSV. Ceci est possible grâce au cookie de session qui, même si vous ne naviguez pas sur facebook, est toujours enregistré dans votre navigateur Web.

Ainsi, il existe un fichier CSV qui enregistre les clics sur les boutons like : 

```csv
email;like
paul@hotmail.fr;article dans un blog sur les chats, article dans un site de presse
francine@gmail.com;article dans un site de club de foot
camille@icloud.fr;photo sur instagram d'un paysage d'un autre pays, podcast audio sur les bébés
```

Et, un autre fichier CSV qui enregistre les partages de contenus vers Facebook : 

```csv
email;partage vers facebook
paul@hotmail.fr;article dans un blog sur les chats
nicolas@wanadoo.fr;article dans un site de presse
aurelia@yopmail.com;photo sur instagram d'un arbre à chat
```

+++question "Enregistrez chacun des CSV dans des fichiers CSV (vous aurez donc 3 fichiers CSV)"

+++question "Créez un programme Python, s'appuyant sur la bibliothèque pandas, qui permet de fusionner (+++flag-us merge) les 3 fichiers CSV et de créer un quatrième fichier CSV qui est le résultat de la fusion"

Le résultat attendu est le suivant : 

```csv
email;age;genre;like;partage vers facebook
paul@hotmail.fr;24;1;article dans un blog sur les chats, article dans un site de presse;article dans un blog sur les chats
francine@gmail.com;32;2;article dans un site de club de foot;Nan
camille@icloud.fr;13;2;photo sur instagram d'un paysage d'un autre pays, podcast audio sur les bébés;Nan
nicolas@wanadoo.fr;17;1;Nan;article dans un site de presse
aurelia@yopmail.com;48;2;Nan;photo sur instagram d'un arbre à chat
```

<hr>

+++bulle matthieu
  avec ces nouvelles données, il devient possible de générer des publicités ciblées pour chaque internaute sur son fil d'actualité facebook
+++

## Réponses

<ol start="0">
  <li>
  +++spoil
<pre><code class="python language-python hljs">import pandas

annuaire = pandas.read_csv("annuaire.csv")
print(annuaire)</code></pre>
  spoil+++
  </li>
  <li>
  +++spoil
<pre><code class="python language-python hljs">import pandas

annuaire = pandas.read_csv("annuaire.csv")
print(annuaire)

annuaire = fichier_csv.sort_values(by=["email"])
print(annuaire)</code></pre>
  spoil+++
  </li>
  <li value="3">
  +++spoil
<pre><code class="python language-python hljs">import pandas

annuaire = pandas.read_csv("annuaire.csv")
annuaire2 = pandas.read_csv("annuaire2.csv")

# on concatène les contenus des deux fichiers csv avec la fonction 'concat'
resultat = pandas.concat([annuaire, annuaire2])
print(resultat)

# on enregistre le contenu de la variable 'resultat' dans un nouveau fichier csv à l'aide de la fonction 'to_csv'
resultat.to_csv('resultat.csv')
</code></pre>
  spoil+++
  </li>
  <li value="5">
  +++spoil
<pre><code class="python language-python hljs">import pandas

donnees = pandas.read_csv("donnees.csv", sep=";")
likes = pandas.read_csv("likes.csv", sep=";")
partages = pandas.read_csv("partages.csv", sep=";")

# on fusionne les contenus des deux fichiers csv avec la fonction 'merge'
donnees_enrichies = pandas.merge(donnees, likes, on="email", how="left")
donnees_enrichies = pandas.merge(donnees_enrichies, partages, on="email", how="left")
print(donnees_enrichies)

# on enregistre le contenu de la variable 'donnees_enrichies' dans un nouveau fichier csv à l'aide de la fonction 'to_csv'
donnees_enrichies.to_csv('donnees_enrichies.csv',index=False)
</code></pre>
  spoil+++
  </li>
</ol>