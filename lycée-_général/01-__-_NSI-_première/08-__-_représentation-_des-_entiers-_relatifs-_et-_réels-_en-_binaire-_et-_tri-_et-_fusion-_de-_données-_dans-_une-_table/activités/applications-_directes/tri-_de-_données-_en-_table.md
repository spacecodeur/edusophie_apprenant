# Tri de données en table

+++consignes
  <ol start="0">
    <li>Créez vous un dossier du jour en exécutant chacune des lignes ci-dessous sur un terminal. <b>Attention !</b> n'oubliez pas d'adapter les lignes 2 et 3 </li>
  </ol>
  <pre><code class="language-bash"># lignes à réadapter et à exécuter dans un terminal
  nom_prenom=dupond_cedric
  dateDuJour=18_10_21

  mkdir /home/premiere/Eleves/${nom_prenom}/seances/${dateDuJour}
  cd /home/premiere/Eleves/${nom_prenom}/seances/${dateDuJour}
  touch tri_csv.py
  thonny tri_csv.py
  </code></pre>
  <ol start="1">
    <li>Parcourez l'activité et répondez aux questions</li>
  </ol>
+++

Un fichier CSV peut regrouper un nombre varié de données. Selon l'utilisation qu'on veut faire de ces données, nous avons parfois besoin de les trier (via différents critères).

Le but de cette activité est de voir comment trier (réarranger) des données contenues dans un fichier CSV en python.

+++question "Téléchargez le fichier CSV et placez le dans le même dossier que le fichier tri_csv.py"

- vous pouvez le télécharger +++lien "https://www.edusophie.com/d/nsi_1ere/annuaire.csv" "ici" et le déplacer manuellement dans votre dossier du jour
- ou alors, vous pouvez exécuter la commande bash suivante : 
    - **/!\\** assurez vous que votre terminal est placé dans votre dossier du jour
    - voici la commande bash : <code>wget https://www.edusophie.com/d/nsi_1ere/annuaire.csv</code>

<hr>

+++bulle matthieu
  le fichier CSV représente un annuaire fictif de personnes
+++

+++bulle matthieu droite
  les données sont triées par année d'ajout
+++

+++question "Modifiez le fichier CSV avec un éditeur de texte simple et ajoutez dedans deux nouvelles personnes (avec les données de votre choix). Attention, les lignes doivent rester triées par année d'ajout"

<hr>

Nous supposons que le fichier contient plus d'un millions de ligne et donc que son volume ne permet son exploitation avec un logiciel tableur traditionnel. Nous cherchons à trier les données de l'annuaire en fonction des emails des personnes.

Voici le code permettant de trier les données par email : 

```python
def convertirFichierCSV_en_listePython(fichierCSV):
    contenuDuFichierCSV = open(fichierCSV,'r',encoding='utf8')
    
    liste = []
    for ligne in contenuDuFichierCSV:
      liste.append(ligne.split(','))
    return liste

def triParSelection(lignes):
  n = len(lignes)
  for i in range(n-1):
    minimum = i
    for j in range(i+1, n):
      # l'email est le premier élément de chaque ligne
      if(lignes[j][0] < lignes[minimum][0]):
        minimum = j
    if(minimum != i):
      tmp = lignes[i]
      lignes[i] = lignes[minimum]
      lignes[minimum] = tmp
  return lignes

# on stocke le contenu du fichier CSV dans une liste python
annuaire = convertirFichierCSV_en_listePython('annuaire.csv')

# on met de côté la première ligne qui contient les descripteurs des données
descripteurs = annuaire.pop(0)

# on affiche les lignes dans leur ordre actuel
print('contenu par défaut du fichier CSV : \n')
print(descripteurs)
for ligne in annuaire:
    print(ligne)

# on tri les lignes du fichier CSV en fonction du mail (classement par ordre alphabétique)
annuaire = triParSelection(annuaire);

# on affiche les lignes triées par l'email
print('\ncontenu trié du fichier CSV : \n')
print(descripteurs)
for ligne in annuaire:
    print(ligne)
```

+++question "Recopiez le code ci-dessus dans votre fichier tri_csv.py et exécutez le pour vérifier si il produit bien le résultat attendu"

+++question "Modifiez le code pour que le programme affiche les données du CSV triées, non pas par email, mais par année d'ajout"

+++spoil "Aide"
  Il n'y a qu'une seule ligne a modifier :)
spoil+++


## Vous êtes en avance ? 

Le réarrangement des données se fait actuellement à travers un tri par sélection.

+++question "Modifiez le code afin de faire, à la place d'un tri par sélection, +++lien "https://fr.wikipedia.org/wiki/Tri_%C3%A0_bulles" "un tri à bulles" (de légères modifications devront être faites)"

Voici l'algorithme du tri à bulle : 

<pre class="p-2" style="background-color:#eeeeee">0.  fonction tri_à_bulles(tableau T)
1.    POUR i allant de taille(T) - 1 à 1 (compris)
2.        POUR j allant de 0 à i - 1 (compris)
3.            SI T[j+1] < T[j]
4.                echanger T[j+1] et T[j]
5.            fin SI
6.        fin POUR
7.    fin POUR
8.  retourner T
9.  fin fonction
</pre>

<hr>

+++question "Modifiez le code afin de faire, à la place d'un à bulle, un tri par insertion (de légères modifications devront être faites)"

Pour rappel, voici l'algorithme du tri par insertion : 

<pre class="p-2" style="background-color:#eeeeee">0.  procedure tri_insertion(tableau T)
1.    POUR i allant de 1 à taille(T) - 1 (compris)
2.        # mémoriser T[i] dans n
3.        n ← T[i]                            
4.        # décaler les éléments T[0]..T[i-1] qui sont plus grands que n, en partant de T[i-1]
5.        j ← i                               
6.        TANT QUE j > 0 et T[j - 1] > n
7.            T[j] ← T[j - 1]
8.            j ← j - 1
9.        fin TANT QUE
10.       # placer n dans le "trou" laissé par le décalage
11.       T[j] ← n
12.   fin POUR
13.   retourner T
14. fin procedure
</pre>



## Réponses

<ol start="1">
  <li>
  +++spoil
Voici le contenu du fichier CSV avec deux entrées supplémentaires. 
  
<pre><code class="csv language-csv hljs">email,telephone,annee_ajout,genre
junie@hotmail.fr,0682145672,2017,2
john@gmail.com,0745632109,2017,1
marc@icloud.fr,0675489125,2018,1
nicolas@wanadoo.fr,0601084421,2019,1
mathilde@yopmail.com,0675489112,2021,2</code></pre>
  spoil+++
  </li>
  <li value="3">
  +++spoil
<p>Le tri est effectué grâce à la fonction <code>triParSelection</code>, la modification doit sans doute être faite dans la fonction.</p>
<p>Le commentaire ligne 14 semble indiquer que le code en dessous est adapté en fonction du premier élément de chaque ligne, ce premier élément dans le fichier CSV correspond à l'email.</p>
<p>ligne 15, le <code>[0]</code> doit vous mettre la puce à l'oreille, cette syntaxe est utilisée lorsqu'on souhaite accéder au premier élément d'un tableau.</p>
<p>Il suffit alors de modifier la ligne 15 : </p>
<code>if(lignes[j][0] < lignes[minimum][0]):</code>
<p>par : </p>
<code>if(lignes[j][2] < lignes[minimum][2]):</code>
<p>car le troisième élément qui se trouve dans chaque ligne du fichier CSV correspond à l'année d'ajout</p>
  spoil+++
  </li>
</ol>