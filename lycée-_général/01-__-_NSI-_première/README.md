# README

+++sommaire

## Matériel demandé en classe

- Un classeur format A4
    - avec des grands anneaux (2 ou 4)
    - contenant des pochettes plastifiées
    - contenant des feuilles A4 perforées (grand carreau ou petit carreau)
    - contenant 12 intercalaires avec une étiquette sur chacun, le nom de chaque étiquette doit être : 

```python
intercalaires = {
   "module 0"  : "Rappels en programmation et système d'exploitation Linux",
   "module 1"  : "Introductions en informatique fondamentale et introduction en programmation Web côté client",
   "module 2"  : "Introduction aux tableaux et programmation Web côté client",
   "module 3"  : "Parcours de tableaux, introduction en algorithmique et tests",
   "module 4"  : "Intoduction au réseau",
   "module 5"  : "Programmation Web et architecture client-serveur",
   "module 6"  : "Recherche de données dans une table, systèmes binaire et hexadécimal",
   "module 7"  : "Algorithmes de recherche, de tri et invariant de boucle",
   "module 8"  : "Représentation des entiers relatifs et réels en binaire et tri et fusion de données dans une table",
   "module 9"  : "Tableaux à 2 dimensions et systèmes d'encodage",
   "module 10" : "Tableaux associatifs, algorithmes d'apprentissage et algorithmes gloutons",
   "module 11" : "Module de fin d'année",
}
```

- Une clé usb (>= 3 Go), attention à ne pas la perdre !

**Pendant l'année je ramasserai les classeurs.**

## Planning

### Première NSI G1

- Séance de 2 heures le lundi de 10h à 12h en salle PRO2
- Séance de 2 heures le Mardi de 16h à 18h en salle PRO2

### Première NSI G2

- Séance de 2 heures le Mardi de 8h à 10h en salle PRO2
- Séance de 2 heures le Mercredi de 9h à 11h en salle PRO2

## Ressources du cours

Les ressources du cours sont disponibles en ligne à cette adresse : 
[https://www.edusophie.com/apprenant/1-__-_NSI-_premiere](https://www.edusophie.com/apprenant/1-__-_NSI-_premiere)

## Évaluations notées 

Contrôle notés : 

0. **Contrôle de fin de module** (coef 1)
1. **Évaluation de projet** (coef 1.5)
2. **Contrôle trimestriel** (coef 2)

## Progression de l'année scolaire

- La plupart des modules s'étalent sur 2 à 4 semaines 
- Sous condition de votre travail pendant l'année : à partir de fin Avril, nous basculons en mode full-projet : sujets proposés ou créés à partir de vos idées en s'appuyant bien sur sur ce qui a été vu pendant l'année
- Entre aide encouragée (voir **charte d'entraide** ci-dessous)
- Formation de groupes aléatoires, ou non, dans les projets

## Charte d'entraide +++emoji-warning 
- Ne **jamais** donner la réponse directement à ses camarades
- Ne **jamais** écrire du code à la place de ses camarades
- Poser des questions
- Orienter vers les parties du cours concernées
- Orienter vers les exercices en lien avec la problématique rencontrée
