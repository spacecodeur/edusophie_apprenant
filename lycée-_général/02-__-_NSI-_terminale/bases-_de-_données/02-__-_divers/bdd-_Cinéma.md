# exemples de requêtes SQL sur une base de données 'Cinéma'

+++ sommaire

## Objectifs

+++bulle matthieu
Hello ! Ce document présente la conception d'une base de données 'Cinéma' avec le lots de requêtes SQL permettant de la créer, de la remplir et de l'exploiter ! 
+++

La partie de la conception est peu détaillée, nous nous concentrons en priorité sur l'utilisation du langage SQL.

## Structure de la base de données

### Conception

+++bulle matthieu
Nous souhaitons créer une base de données pour stoker des informations sur les acteurs, les réalisateurs et les films de cinéma. La structure de la base de données doit pouvoir satisfaire 5 règles métiers
+++

1. Un **acteur** joue dans 0 à plusieurs **films**. On peut donc enregistrer dans notre base de données des acteurs qui n'ont pas encore joué dans un film
2. Un **film** est joué par 1 à plusieurs **acteurs**
3. Un **réalisateur** réalise 0 à plusieurs **films**. On peut donc enregistrer dans notre base de données des réalisateurs qui n'ont pas encore réalisé de film
4. Un **film** est réalisé par 1 et 1 seul **réalisateur**. Nous n'enregistrerons donc pas les films réalisés par plusieurs réalisateurs
5. Chaque fois qu'un **acteur** joue dans un **film**, nous souhaitons stoker le salaire qu'il a perçu

Nous pouvons déduire de ces règles métiers le diagramme entité-relation :

+++diagramme
erDiagram
    acteur }|--o{ film: "joue dans"
    realisateur ||--o{ film: "réalise"
+++
<p class="small text-center col-8 mx-auto">Ce type de diagramme permet de visualiser l'interaction entre les différentes 'entités' de notre base de données.</p>

Ainsi qu'un schéma relationnel : 

- **acteur**(<u>`idA`</u>, `nom`, `prenom`)
- **film**(<u>`idF`</u>, `titre`, *`idRealisateur`*)
- **realisateur**(<u>`idR`</u>, `nom`, `prenom`)

Ces trois relations vont composer les 3 principales tables de notre base de données.

+++bulle matthieu
Attention ! un <b>acteur</b> peut jouer dans <u>plusieurs</u> <b>films</b> et un <b>film</b> peut être joué par <u>plusieurs</u> <b>acteurs</b>. On parle de relation <u>many to many</u>
+++

+++bulle matthieu droite
Entre deux tables liées par une relation <u>many to many</u>, il est nécessaire d'ajouter une table intermédiaire pour faire le pont entre <b>acteur</b> et <b>film</b>. Nous nommons cette table <b>joueDans</b>.
+++

Cette table nous permet également de satisfaire la règle métier n°5 en incluant dedans l'attribut `salaire`.

Nous obtenons donc : 
- **acteur**(<u>`idA`</u>, `nom`, `prenom`)
- **joueDans**(*<u>`idActeur`</u>*, *<u>`idFilm`</u>*, `salaire`)
- **film**(<u>`idF`</u>, `titre`, *`idRealisateur`*)
- **realisateur**(<u>`idR`</u>, `nom`, `prenom`)

Pour terminer, nous ajoutons quelques attributs supplémentaires dans nos relations pour rendre notre base de données plus intéressante à manipuler. Nous obtenons donc au final : 

- **acteur**(<u>`idA`</u>, `nom`, `prenom`, `nationalite`)
- **joueDans**(*<u>`idActeur`</u>*, *<u>`idFilm`</u>*, `salaire`)
- **film**(<u>`idF`</u>, `titre`, `annee`, `pays`, `nbspectateurs`, *`idRealisateur`*)
- **realisateur**(<u>`idR`</u>, `nom`, `prenom`, `nationalite`)

### Création

Une multitude d'outils permettent de créer et de gérer une base de données. Vous pouvez par exemple utiliser un logiciel installé sur votre ordinateur **depuis lequel il faudra créer la base de données 'Cinéma'** avant de jouer les requêtes ci-dessous.

+++bulle matthieu
Dans les logiciels gratuits et libresource disponibles, je vous conseille le très pratique <a href="https://sqlitebrowser.org/dl" target="_blank">DB Browser</a> qui propose même une version portable ! (= sans installation nécessaire)
+++

<span class="d-print-none">Sinon, pour tester rapidement le code, vous pouvez en live <button class="btn btn-primary p-1">Exécuter le code</button> des requêtes grâce à l'**Éditeur SQL** intégré. (la base de données 'Cinéma' est automatiquement créée)</span>

Voici donc les requêtes de création des tables de la base de données 

widget sql
CREATE TABLE acteur(
    idA INTEGER NOT NULL,
    nom VARCHAR(100),
    prenom VARCHAR(100),
    nationalite VARCHAR(30),
    PRIMARY KEY (idA)
);

CREATE TABLE realisateur(
    idR INTEGER NOT NULL,
    nom VARCHAR(100),
    prenom VARCHAR(100),
    nationalite VARCHAR(30),
    PRIMARY KEY (idR)
);

CREATE TABLE film(
    idF INTEGER NOT NULL,
    titre VARCHAR(100),
    annee INTEGER,
    pays VARCHAR(50),
    nbspectateurs INTEGER,
    idRealisateur INTEGER,
    PRIMARY KEY(idF),
    FOREIGN KEY(idRealisateur) REFERENCES realisateur(idR)
);

CREATE TABLE joueDans(
    idActeur INTEGER NOT NULL,
    idFilm INTEGER NOT NULL,
    salaire INTEGER,
    PRIMARY KEY (idActeur, idFilm),
    FOREIGN KEY (idActeur) REFERENCES acteur(idA),
    FOREIGN KEY (idFilm) REFERENCES film(idF)
);
widget

## Peuplement de la base de données

+++bulle matthieu
Les tables, qui composent la structure de notre base de données, sont créées. Cependant, notre base de données ne contient pour l'instant aucune données ! nous allons donc <b>peupler</b> notre base de données (🇬🇧 <b>populate a database</b>)
+++

Ajoutons : 

- Six **acteurs**
- Quatre **réalisateurs**
- Trois **films**
- Sept relations dans **joueDans**

widget sql (requêtes de création des tables:0)
-- ajout des 6 acteurs
INSERT INTO acteur(idA, nom, prenom, nationalite) VALUES
(1, "Pratt", "Chris", "us"),
(2, "Sy", "Omar", "fr"),
(3, "Chastain", "Jessica", "us"),
(4, "Lupita", "Nyong'o", "ke-mx"),
(5, "Astier", "Alexandre", "fr"),
(6, "Watson", "Emma", "gb");

-- ajout des 4 réalisateurs
INSERT INTO realisateur(idR, nom, prenom, nationalite) VALUES
(1, "Emma", "Thomas", "en"), 
(2, "Lee", "Dong-ha", "kr"),
(3, "Astier", "Alexandre", "fr"),
(4, "Kim", "Bora", "kr");

-- ajout des 3 films
INSERT INTO film(idF, titre, annee, pays, nbspectateurs, idRealisateur) VALUES
(1, "Kaamelott: Deuxième Volet", "2040", "france", 71000000, 3),
(2, "Fast and Furious 23", "2028", "états-unis", 1220000000, 2),
(3, "StarWars 13 : le retour de Palpatine", "2039", "chine", 6385000000, 2);

-- ajout de 7 relations dans 'joueDans'
INSERT INTO joueDans(idFilm, idActeur, salaire) VALUES
(2, 1, 20000000), --  = l'acteur "Pratt Chris" (id 1) participe au film "Fast and Furious 23" (id 2) et touche un cachet de 20 000 000 d'euros
(2, 2, 19000000),
(3, 1, 1500000),
(3, 3, 13000000),
(3, 4, 1000000),
(1, 5, 0),
(1, 2, 2000000);

widget

## Extraction des données de la base de données

Notre base de données étant construite et peuplée, nous allons maintenant pouvoir l'exploiter ! 

Ci-dessous, des exemples de requêtes pour extraire des données.

### Utilisation de SELECT + FROM + WHERE avec/sans fonction(s) d'agrégation

widget sql (peuplement de la BDD:1:39) (création des tables et champs de la BDD:0)
-- REQUÊTE : Sélectionner le titre de tous les films français
[bloc afficher la réponse
SELECT *
FROM film
WHERE pays = 'france';
bloc]
widget

widget sql (peuplement de la BDD:1:39) (création des tables et champs de la BDD:0)
-- REQUÊTE : Compter le nombre total de film
[bloc afficher la réponse
SELECT count(*) as "nombre total de film"
FROM film;
bloc]
widget

widget sql (peuplement de la BDD:1:39) (création des tables et champs de la BDD:0)
-- REQUÊTE : Compter le nombre d'acteur par nationalité
[bloc afficher la réponse
SELECT nationalite, count(*) as nombre
FROM acteur
GROUP BY nationalite;
bloc]
widget

### Utilisation de jointure simple

widget sql (peuplement de la BDD:1:39) (création des tables et champs de la BDD:0)
-- REQUÊTE : Compter le nombre de film réalisés par des réalisateurs Sud-coréen (kr)
[bloc afficher la réponse
SELECT COUNT(*) as 'nombre de films'
FROM film
JOIN realisateur ON film.idRealisateur = realisateur.idR
WHERE realisateur.nationalite == "kr";
bloc]
widget

widget sql (peuplement de la BDD:1:39) (création des tables et champs de la BDD:0)
-- REQUÊTE : Sélectionner le nombre minimum, moyen et maximum d'entrée de ses films pour chaque réalisateur (nom et prénom)
[bloc afficher la réponse
SELECT nom, prenom, MIN(nbspectateurs), AVG(nbspectateurs), MAX(nbspectateurs)
FROM realisateur
JOIN film ON realisateur.idR = film.idRealisateur
GROUP BY nom,prenom;
bloc]
widget

widget sql (peuplement de la BDD:1:39) (création des tables et champs de la BDD:0)
-- REQUÊTE : Sélectionner les réalisateurs (nom et prénom) qui n'ont pas de film inscrits dans la BDD
[bloc afficher la réponse
SELECT nom, prenom
FROM realisateur
LEFT JOIN film ON realisateur.idR = film.idRealisateur
WHERE idF is Null;
bloc]
widget

widget sql (peuplement de la BDD:1:39) (création des tables et champs de la BDD:0)
-- REQUÊTE : Compter le nombre de film joués par chaque acteur (nom, prénom) en ordonnant les noms par ordre alphabétique décroissant
[bloc afficher la réponse
SELECT nom, prenom, COUNT(idFilm)
FROM acteur
LEFT JOIN joueDans ON acteur.idA = joueDans.idActeur
GROUP BY idA
ORDER BY nom DESC;
bloc]
widget

widget sql (peuplement de la BDD:1:39) (création des tables et champs de la BDD:0)
-- REQUÊTE : Compter le nombre de film réalisés par chaque réalisateur Sud-coréen (kr)
[bloc afficher la réponse
SELECT nom, prenom, COUNT(idF)
FROM realisateur
LEFT JOIN film ON idR = idRealisateur /* peut être que certains réalisateurs n'ont pas de film inscrit dans la BDD d'où le 'LEFT'*/
WHERE nationalite = 'kr'
GROUP BY idR;
bloc]
widget

widget sql (peuplement de la BDD:1:39) (création des tables et champs de la BDD:0)
-- REQUÊTE : Sélectionner le film (titre) étant le plus grand succès (en nombre de spectateurs) de chaque réalisateur désigné par son nom et prénom
[bloc afficher la réponse
SELECT nom, prenom, titre, MAX(nbspectateurs)
FROM realisateur
JOIN film ON realisateur.idR = film.idRealisateur
GROUP BY idR;
bloc]
widget

### Utilisation de jointures multiple

widget sql (peuplement de la BDD:1:39) (création des tables et champs de la BDD:0)
-- REQUÊTE : Sélectionner le nom et le prénom des réalisateurs qui ont dirigé l'acteur Omar Sy
-- Note : nous savons que l'identifiant de l'acteur "Omar Sy" est 2 (acteur.idA)
[bloc afficher la réponse
SELECT nom, prenom
FROM realisateur
JOIN film ON realisateur.idR = film.idRealisateur
JOIN joueDans ON film.idF = joueDans.idFilm
WHERE joueDans.idActeur = 2;
bloc]
widget

widget sql (peuplement de la BDD:1:39) (création des tables et champs de la BDD:0)
-- REQUÊTE : Sélectionner le film (titre) étant le plus grand succès (en nombre de spectateurs) de chaque acteur désigné par son nom et prénom
[bloc afficher la réponse
SELECT nom, prenom, titre, MAX(nbspectateurs)
FROM acteur
JOIN joueDans ON acteur.idA = joueDans.idActeur
JOIN film ON joueDans.idFilm = film.idF
GROUP BY idA;
bloc]
widget

widget sql (peuplement de la BDD:1:39) (création des tables et champs de la BDD:0)
-- REQUÊTE : Sélectionner le film (titre) ayant rapporté le plus d'argent pour chaque acteur désigné par son nom et prénom
[bloc afficher la réponse
SELECT nom, prenom, titre, MAX(salaire)
FROM acteur
JOIN joueDans ON acteur.idA = joueDans.idActeur
JOIN film ON joueDans.idFilm = film.idF
GROUP BY idA;
bloc]
widget

### Utilisation de requête imbriqué

widget sql (peuplement de la BDD:1:39) (création des tables et champs de la BDD:0)
-- REQUÊTE : Sélectionner les acteurs (nom et prénom) qui ont eu au moins un salaire supérieur au salaire moyen
[bloc afficher la réponse
SELECT nom, prenom
FROM acteur
JOIN joueDans ON acteur.idA = joueDans.idActeur
WHERE joueDans.salaire > (
	SELECT AVG(salaire)
	FROM joueDans
)
GROUP BY idA;
bloc]
widget

widget sql (peuplement de la BDD:1:39) (création des tables et champs de la BDD:0)
-- REQUÊTE : Sélectionner les acteurs (nom et prénom) ayant touché le salaire le plus élevé
[bloc afficher la réponse
SELECT nom, prenom
FROM acteur
JOIN joueDans ON acteur.idA = joueDans.idActeur
WHERE joueDans.salaire = (
	SELECT MAX(salaire)
	FROM joueDans
)
GROUP BY idA;
bloc]
widget

widget sql (peuplement de la BDD:1:39) (création des tables et champs de la BDD:0)
-- REQUÊTE : Sélectionner les acteurs (nom et prénom) dont chaque salaire est supérieur au salaire moyen
[bloc afficher la réponse
SELECT nom, prenom
FROM acteur
JOIN joueDans ON acteur.idA = joueDans.idActeur
GROUP BY idA
HAVING MIN(joueDans.salaire) > (
	SELECT AVG(salaire)
	FROM joueDans
);
bloc]
widget

widget sql (peuplement de la BDD:1:39) (création des tables et champs de la BDD:0)
-- REQUÊTE : Sélectionner les acteurs (nom et prénom) ayant joué dans, au moins, les 3/4 des films
[bloc afficher la réponse
SELECT nom, prenom
FROM acteur
JOIN joueDans ON acteur.idA = joueDans.idActeur
GROUP BY idA
HAVING COUNT(idA) >= (
	SELECT COUNT(*) * 3/4
	FROM film
);
bloc]
widget

### Utilisation des opérateurs ensemblistes UNION, INTERSECT et EXCEPT

widget sql (peuplement de la BDD:1:39) (création des tables et champs de la BDD:0)
-- REQUÊTE : Sélectionner les acteurs (nom et prénom) et les réalisateurs (nom et prénom)
[bloc afficher la réponse
SELECT nom, prenom
FROM acteur
UNION
SELECT nom, prenom
FROM realisateur;
bloc]
widget

Nous remarquons que l'acteur ET réalisateur Alexandre Astier n'apparaît qu'une fois. C'est normal, l'opérateur ensembliste UNION retire automatiquement les doublons !

widget sql (peuplement de la BDD:1:39) (création des tables et champs de la BDD:0)
-- REQUÊTE : Sélectionner les personnes qui soient à la fois acteur et réalisateur
[bloc afficher la réponse
SELECT nom, prenom
FROM acteur
INTERSECT
SELECT nom, prenom
FROM realisateur;
bloc]
widget

widget sql (peuplement de la BDD:1:39) (création des tables et champs de la BDD:0)
-- REQUÊTE : Sélectionner les réalisateurs qui ne sont pas acteurs
[bloc afficher la réponse
SELECT nom, prenom
FROM realisateur
EXCEPT
SELECT nom, prenom
FROM acteur;
bloc]
widget