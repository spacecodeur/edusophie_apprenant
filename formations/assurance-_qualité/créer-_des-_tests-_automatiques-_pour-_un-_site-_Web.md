# Créer des tests fonctionnels automatiques pour un site Web

+++sommaire

Lorsque le code d'un site Web conséquent est mis à jour, les développeurs peuvent sans le vouloir et sans s'en apercevoir introduire des bugs. Il devient alors rapidement nécessaire de mettre en place des tests automatiques pour s'assurer que les fonctionnalités vitales du site fonctionnent toujours après une mise à jour de ce dernier.

Il existe plusieurs types de tests ; les tests unitaires, les tests de performance, les tests fonctionnels, etc.. 

Nous allons nous focaliser ici sur les tests fonctionnels.

+++bulle matthieu
    Ajouter un produit au panier, remplir un formulaire d’authentification, cliquer sur un bouton like, etc.. sont des <b>fonctionnalités</b> d'un site Web. Il s'agit d'actions que peuvent effectuer des visiteurs
+++


Il existe une multitude d'outils permettant de créer des tests fonctionnels. Ici, nous nous intéressons à l'outil **cypress** qui est un outil <u>developer-friendly</u> et relativement <u>sexy</u>.

+++bulle matthieu
    <u>developer-friendly</u>, car optimisé en priorité pour faciliter le travail des rédacteurs des tests 
+++

+++bulle matthieu droite
    <u>sexy</u>, car se skiller sur cet outil permet de capitaliser des connaissances sur le très utilisé langage de programmation <b>javascript</b>, sur la très utilisée plateforme logicielle libre <b>Node.js</b> ainsi que sur des pratiques de développement de test automatiques robustes telles que le <b>T</b>est-<b>D</b>riven <b>D</b>evelopment
+++

## Mise en place de son environnement de travail

### Installation de Cypress

Dans le cadre de cette introduction, nous allons procéder à une **installation simple** de Cypress et partir du principe que vous êtes sur un ordinateur qui tourne sous un système d'exploitation Windows. Cette installation permet d'essayer Cypress. Dans ce contexte, les tests automatiques seront à déclencher manuellement pour qu'ils s'exécutent. 

Voici la démarche à suivre : 
1. Rendez vous sur le [site officiel Cypress](https://www.cypress.io/)
2. Téléchargez la dernière version **desktop** de Cypress (ou cliquer sur [ce lien direct](https://download.cypress.io/desktop))
3. Décompressez l'archive téléchargée

Afin de vérifier que Cypress se lance bien, il vous suffit d'exécuter le fichier `Cypress.exe` se trouvant dans le dossier décompressé. Au bout de quelques secondes, un fenêtre s'ouvre, il s'agit d'une interface graphique permettant d'exécuter rapidement des tests automatiques au préalable rédigés.

![](https://i.ibb.co/MN0Fdbx/Capture.png)

### Installation de l'IDE VSCodium

Cypress apporte des outils permettant de créer des tests automatique ( on parle de **framework** de test ). Les tests, sur cypress, s'écrivent avec le langage de programmation **javascript**. 

Pour faciliter le travail de rédaction de code, on utilise un IDE (**I**ntegrated **D**evelopment **E**nvironment). Je vous propose d'utiliser l'IDE **VSCodium** qui est gratuit, compatible Windows/Mac/Linux et sous licence **MIT** (donc open source). Le dernière version est téléchargeable [dans cette page](https://github.com/VSCodium/vscodium/releases). (pour Windows, prenez le .exe dont le nom commence par 'VSCodiumSetup-x64')

VSCodium permet de programmer avec une multitude de langages. Et comme bien d'autre IDE, il propose un market d'extensions permettant de le spécialiser vers les technos que l'on souhaite manipuler. Il se trouve que la communauté propose une extension facilitant le travail avec Cypress :

1. Installez VSCodium
2. Démarrez VSCodium 
3. Cliquez sur le bouton "extensions" situé à gauche dans l'éditeur
4. Recherchez 'Cypress' et installez l'extension 'Cypress Helper'

![](https://i.ibb.co/wgF49Hs/Screenshot-from-2021-05-27-09-00-09.png)

### Création de son tout premier projet

Créer un dossier sur votre ordinateur nommé `mes_premiers_pas_cypress`. Vous allez rédiger vos premiers tests automatiques dans ce dossier. 

Depuis VSCodium, cliquez sur 'File > Add Folder to Workspace...' et sélectionnez votre dossier `mes_premiers_pas_cypress`. Une fois ceci fait, cliquez sur le bouton 'Explorer' situé à gauche de l'IDE et vous verrez votre dossier `mes_premiers_pas_cypress` dans l'IDE

![](https://i.ibb.co/SvbsVgJ/Screenshot-from-2021-05-27-09-34-36.png)

Puis, depuis l'interface graphique Cypress, recherchez et sélectionnez le dossier `mes_premiers_pas_cypress`. Cela aura pour effet d'ajouter le dossier dans l'interface graphique Cypress et de créer automatiquement un ensemble d'exemples de tests automatiques. Nous n'y prêtons pas attention pour l'instant.

![](https://i.ibb.co/1v6J5KF/Capture.png)

## Création et exécutions de tests automatiques

+++bulle matthieu
    Je récapitule ! Vous avez installé une version 'simple' de Cypress qui contient le moteur qui va exécuter les tests automatiques 
+++

+++bulle matthieu droite
    Et un IDE permettant de faciliter la rédaction des tests, écrits en javascript, qui seront exécutés par Cypress 
+++

### Rédaction d'un premier test automatique 

Pour ce tout premier essai, vous allez checker si le moteur de recherche duckduckgo fonctionne bel et bien. Voici un scénario de test assez simple : 

1. On charge la page https://duckduckgo.com
2. On saisie au clavier la recherche 'jouets'
3. On clic sur l'icône en forme de loupe pour valider la recherche
4. On vérifie dans la nouvelle page chargée qu'il y a au moins un résultat

Pour faire simple à l'étape 4, nous vérifions dans la page de résultats de recherche que le message 'Aucun résultats trouvé pour' n'est pas présent.

Depuis l'interface graphique Cypress, créez un nouveau test en cliquant sur '<span style="color:blue">+ New Spec File</span>' situé en haut à droite. Enregistrez le fichier à côté du dossier `examples` et nommez le fichier `duckduckgo.spec.js`.

Depuis votre IDE, ouvrez le fichier `duckduckgo.spec.js` fraîchement crée par Cypress et copiez/collez les étapes du test comme suit (en ajoutant un '//' devant chaque étape)

![](https://i.ibb.co/6XdZfKJ/Capture.png)

Maintenant nous devons traduire en Javascript chacune de ses étapes et en utilisant les instructions fournis par Cypress. Voici les lignes en Javascript qui expliquent à Cypress ce qu'il doit faire pour mener à bien notre test : 

<div style="background: white; border:solid black 1px;padding:.5em .6em;"><pre style="margin: 0; line-height: 125%">describe(<span style="color: #0000FF">"duckduckgo : page d'accueil"</span>, () => {

  specify(<span style="color: #0000FF">'Fonctionnalité de recherche'</span>, () => {
    
   <span style="color: #008800; font-style: italic">// 1. On charge la page https://duckduckgo.com</span>
   cy.visit(<span style="color: #0000FF">'https://duckduckgo.com'</span>)
   <span style="color: #008800; font-style: italic">// 2. On saisie au clavier la recherche 'jouets'</span>
   cy.get(<span style="color: #0000FF">'input#search_form_input_homepage'</span>).type(<span style="color         #0000FF">'jouets'</span>)
   <span style="color: #008800; font-style: italic">// 3. On clic sur l'icône en forme de loupe pour valider la recherche</span>
   cy.get(<span style="color: #0000FF">'input#search_button_homepage'</span>).click()
   <span style="color: #008800; font-style: italic">// 4. On vérifie dans la nouvelle page chargée qu'il y a au moins un résultat</span>
   cy.contains(<span style="color: #0000FF">'Aucun résultats trouvé pour'</span>).should(<span style="color   #0000FF">'not.exist'</span>)
   
  });
});
</pre></div>
<br>
Pour qu'un script Cypress fonctionne, il est nécessaire d'utiliser les instructions **describe** et **specify** d'où leur présence dans le code ci-dessus.

Il ne reste plus qu'à copier/coller ce code dans votre IDE, de sauvegarder et de l'exécuter depuis l'interface graphique Cypress. Et Tadaa ! un navigateur se lance et joue sagement chaque étapes du test et détaille chacune d'entres elles dans le panneau gauche de la fenêtre

![](https://i.ibb.co/0GRCdft/Capture.png)

## Aller plus loin : procéder à une installation complète de Cypress

Vous souhaitez profiter du plein potentiel de Cypress ? comme par exemple, pouvoir exécuter automatiquement vos tests après la mise à jour d'une nouvelle fonctionnalité de votre site Web. 

il faudra alors procéder à une **installation complète** de Cypress en suivant [ce guide officiel](https://docs.cypress.io/guides/getting-started/installing-cypress) puis, en suivant l'exemple ci-dessus, configurer votre forge logicielle (gitlab, github, azur devops, ...) pour déclencher automatiquement vos tests aux moments voulus.

La communauté Linux est la plus active sur cet outil, je vous conseille d'installer Cypress sur une distribution Linux telle qu'**Ubuntu** ou **Debian** par exemple. Vous profiterez de toute la puissance de l'outil et trouver de l'aide sur Internet sera également plus simple.