# Systèmes de fichier et partitionnement

+++programme
  Gestion des fichiers, disques et périphériques 
  
  Différencier les systèmes de fichiers
  Monter une partition
+++

Avant d'aborder le partitionnement, quelques notions portant sur la gestion des systèmes de fichiers sous Unis sont nécessaires

## Le système de fichiers

Les données contenues dans des espaces de stockages physiques (disque dur, clé usb, ...) sont présentées à l'utilisateur et aux programmes selon une organisation structurée et sous la forme de **répertoires** et de **fichiers**. 

Stocker des données structurées, au niveau du **périphérique de stockage**, peut se faire de différentes manières et sous différentes règles. On parle alors de **système de fichier**. Certaines approches privilégieront par exemple : 
- les stratégies de stockage : utilisation de table d'allocation de fichier, système de métadonnées, ...
- la sécurité des données : systèmes de droits, chiffrement, ...
- la portabilité multi-OS
- ...

+++bulle matthieu
  ces différents critères impactent directement la cohérence des données et donc par conséquent, les performances générales de la machine
+++

Un périphérique de stockage peut être découpé en **partitions** et une partition **peut** utiliser un système de fichier.

+++bulle matthieu
  quels systèmes de fichiers connaissez vous ? connaissez vous leurs différences ?
+++

+++bulle matthieu  droite
  dans quel cas une partition peut ne pas avoir besoin d'utiliser un système de fichier ?
+++

+++question write "En vous aidant +++lien "https://en.wikipedia.org/wiki/Comparison_of_file_systems" "de ce comparatif" (merci à la commu' wikipédia !), trouvez un système de fichiers répondant aux critères ci-dessous : "

0. j'ai besoin de stocker des données chiffrées, ces données doivent pouvoir être exploitées par des OS récents de type WIndows ou Linux
1. j'ai besoin de stocker des données volumineuses, certains fichiers peuvent peser jusqu'à 10 TerraBytes. De plus, je souhaite pouvoir à tout moment changer la taille de la partition sur laquelle je stocke mes fichiers
2. j'utilise un OS type Linux expérimental donc non exempt de bugs, je dois par contre m'assurer qu'en cas de plantage de mon OS, je pourrais toujours accèder à la date de création et la dernière date de modification des fichiers stockées dans mon disque dur

## La gestion des périphériques sous système Unix

Une des particularités des OS dérivants d'UNIX est le fait de pouvoir accéder aux périphériques à travers un fichier dédié. Ces fichiers se trouvent dans le dossier `/dev` (= **dev**ice). Voici une liste non exhaustive de ces fichiers : 

<div class="col-10 mx-auto mb-3">
  <table class="table table-sm">
    <thead>
      <tr>
        <th>Emplacement du fichier</th>
        <th>Périphérique concerné</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><code>/dev/input/mouse0</code></td>
        <td>souris</td>
      </tr>
      <tr>
        <td><code>/dev/hda</code></td>
        <td>Premier disque dur primaire connecté via une nappe IDE</td>
      </tr>
      <tr>
        <td><code>/dev/hdb</code></td>
        <td>Disque dur asservi de la première nappe</td>
      </tr>
      <tr>
        <td><code>/dev/hdc</code></td>
        <td>Premier disque dur primaire connecté via une nappe IDE</td>
      </tr>
      <tr>
        <td><code>/dev/hdd</code></td>
        <td>Disque dur asservi de la deuxième nappe</td>
      </tr>
      <tr>
        <td><code>/dev/sda</code></td>
        <td>Premier disque dur SATA, SCSI ou USB</td>
      </tr>
      <tr>
        <td><code>/dev/sdb</code></td>
        <td>Deuxième disque dur SATA, SCSI ou USB</td>
      </tr>
      <tr>
        <td><code>/dev/lp0</code></td>
        <td>Imprimante parallèle</td>
      </tr>
      <tr>
        <td><code>/dev/usb/lp0</code></td>
        <td>Imprimante USB</td>
      </tr>
    </tbody>
  </table>
</div>

+++bulle matthieu
  dans le cas où un disque, par exemple le sda, est composé de plusieurs partitions, il existera alors dans <code>/dev</code> les fichiers <code>/dev/sda1</code>, <code>/dev/sda2</code> ... <code>/dev/sdaX</code> représentant chacune des partitions
+++

+++bulle matthieu droite
  pour lister le détaillé des périphériques connectés sur votre machine, vous pouvez utiliser la commande <code>fdisk -l</code>
+++

## Le partitionnement

+++bulle matthieu
  qu'est-ce que le partitionnement dans un disque dur ?
+++

+++bulle matthieu droite
  pourquoi utiliser le partitionnement ?
+++

Voici un exemple de montage de deux partitions sous une système Linux. Nous avons une première partition qui contient une arborescence racine et une seconde partition qui contient seulement des dossiers personnels


<div class="d-flex justify-content-evenly align-items-center">
  <div>
    <div class="p-3 border border-dark d-inline-block mx-auto rounded text-white bg-dark">
    ...<br>
    /<span class="isDirectory">etc</span>/<br>
    /<span class="isDirectory">home</span>/<br>
     <span style="color:#A46534"><b>matthieu</b></span>/<br>
      <span class="isDirectory">Bureau</span>/<br>
      <span class="isDirectory">Documents</span>/<br>
      <span class="isDirectory">Images</span>/<br>
      <span class="isDirectory">Modèles</span>/<br>
      <span class="isDirectory">Musique</span>/<br>
      <span class="isDirectory">Public</span>/<br>
      <span class="isDirectory">Téléchargement</span>/<br>
      <span class="isDirectory">Vidéos</span>/<br>
    ...<br>
    /<span class="isDirectory">tmp</span>/<br>
    ...<br>
    </div>
    <div class="text-center"><b>/sda1</b></div>
  </div>
  
  <div>
    <div class="p-3 border border-dark d-inline-block mx-auto rounded text-white bg-dark">
    <span style="color:#A46534"><b>nicolas</b></span>/<br>
  
     <span class="isDirectory">...</span>/<br>
    <span style="color:#A46534"><b>mathilde</b></span>/<br>
  
     <span class="isDirectory">...</span>/<br>
    ...<br>
    </div>
    <div class="text-center"><b>/sda6</b></div>
  </div>
</div>

Nous pouvons intégrer le deuxième système de fichiers dans le répertoire <span class="text-white bg-dark p-1 rounded">/<b class="isDirectory">home</b>/</span> du premier à l'aide de la commande <code>mount</code>. On dit qu'on **monte** le second système de fichiers dans **le point de montage** <span class="text-white bg-dark p-1 rounded">/<b class="isDirectory">home</b>/</span>

la commande : `mount /dev/hda1 /home`

Nous obtenons le résultat : 

<div class="d-flex justify-content-evenly align-items-center">
  <div>
    <div class="p-3 border border-dark d-inline-block mx-auto rounded text-white bg-dark">
    ...<br>
    /<span class="isDirectory">etc</span>/<br>
    /<span class="isDirectory">home</span>/<br>
     <span style="color:#A46534"><b>matthieu</b></span>/<br>
      <span class="isDirectory">Bureau</span>/<br>
      <span class="isDirectory">Documents</span>/<br>
      <span class="isDirectory">Images</span>/<br>
      <span class="isDirectory">Modèles</span>/<br>
      <span class="isDirectory">Musique</span>/<br>
      <span class="isDirectory">Public</span>/<br>
      <span class="isDirectory">Téléchargement</span>/<br>
      <span class="isDirectory">Vidéos</span>/<br>
     <span style="color:#A46534"><b>nicolas</b></span>/<br>
  
      <span class="isDirectory">...</span>/<br>
     <span style="color:#A46534"><b>mathilde</b></span>/<br>
  
      <span class="isDirectory">...</span>/<br>
     ...<br>
    ...<br>
    /<span class="isDirectory">tmp</span>/<br>
    ...<br>
    </div>
    <div class="text-center"><b>/sda1</b></div>
  </div>

</div>