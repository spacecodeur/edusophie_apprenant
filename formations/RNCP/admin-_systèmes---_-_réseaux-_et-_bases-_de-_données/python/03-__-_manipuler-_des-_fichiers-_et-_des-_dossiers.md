# Manipuler des fichiers et des dossiers

+++programme
  Manipulation du système de fichiers
  
  Créer/déplacer/modifier/... des fichiers et des dossiers
  Lire et ajouter du contenu dans un fichier texte
+++

+++sommaire

## Introduction

Nous avons vu que le système Linux permet de manipuler les fichiers et dossier de la machine avec le langage Bash. Python, comme une grande partie des langages de programmations existant, propose aussi ses fonctions pour manipuler des fichiers

### Petite digression

+++bulle matthieu
  mais alors, faut il plutôt utiliser le langage de script bash (ou PowerShell sur Windows par exemple) ou un langage plus généraliste tel que Python ?
+++

Il n'y a pas de réponse absolue à la question, le choix de la répartition du travail entre un langage de scripting / proche de l'OS et un langage plus haut niveau / généraliste se fait en fonction d'une multitude de critères. mais voici le principal choix cornélien qu'il faut en général trancher : 


- il y a de fait une perte en performance lorsqu'on utilise un langage de programmation plus généraliste / haut niveau...
- ...mais vous souhaiterez plutôt profiter de la meilleur maintenabilité/portabilité d'un langage plus généraliste ?

D'autres aspects peuvent également faire pencher la balance : 

- avec quel langage vous et/ou votre équipe êtes le plus à l'aise ?
- les deux langages s'inscrivent dans des stacks techniques très utilisées, garder donc un équilibre entre les deux pour monter en compétences sur plusieurs fronts

## Fonctions python de manipulation de fichier/dossiers

### Fonctions générales

<div class="col-10 mx-auto">
<table class="table table-sm">
  <tbody>
    <tr>
      <td colspan="3" class="text-center" style="background-color:lightblue">Création</td>
    </tr>
    <tr>
      <td class="fw-bold align-middle text-center">Fichier</td>
      <td><code>open('nouveauFichier.txt', 'a').close()</code></td>
    </tr>
    <tr>
      <td class="fw-bold align-middle text-center">Dossier</td>
      <td><code>import os
os.mkdir('nouveauDossier')</code></td>
    </tr>
    
    <tr>
      <td colspan="3" class="text-center" style="background-color:lightblue">Suppression</td>
    </tr>
    <tr>
      <td class="fw-bold align-middle text-center">Fichier</td>
      <td><code>import os
os.remove("image.jpg")</code></td>
    </tr>
    <tr>
      <td class="fw-bold align-middle text-center">Dossier</td>
      <td><code>import os
os.rmdir("dossierPhotos")</code></td>
    </tr>
    
    <tr>
      <td colspan="3" class="text-center" style="background-color:lightblue">Déplacement</td>
    </tr>
    <tr>
      <td class="fw-bold align-middle text-center">Fichier</td>
      <td><code>import os
os.rename("paysage.jpg", "mesPhotos/photo.jpg")</code></td>
    </tr>
    <tr>
      <td class="fw-bold align-middle text-center">Dossier</td>
      <td><code>import os
os.rename("mesPhotos", "~/Images")</code></td>
    </tr>
    
    <tr>
      <td colspan="3" class="text-center" style="background-color:lightblue">Renommer</td>
    </tr>
    <tr>
      <td class="fw-bold align-middle text-center">Fichier</td>
      <td><code>import os
os.rename("paysage.jpg", "lesCalanques.jpg")</code></td>
    </tr>
    <tr>
      <td class="fw-bold align-middle text-center">Dossier</td>
      <td><code>import os
os.rename("mesPhotos", "mesPhotosDeVacance")</code></td>
    </tr>
    
    <tr>
      <td colspan="3" class="text-center" style="background-color:lightblue">Changer les droits (Linux)</td>
    </tr>
    <tr>
      <td class="fw-bold align-middle text-center">Fichier</td>
      <td><code>import os
os.chmod('fichierCompromettant.txt', 0o700)</code></td>
    </tr>
    <tr>
      <td class="fw-bold align-middle text-center">Dossier</td>
      <td><code>import os
os.chmod('mesPhotosDeVacance', 0o744)</code></td>
    </tr>
  </tbody>
</table>
</div>

### Lecture d'un fichier texte

Plusieurs approches sont possibles, je vous présente une des approches les moins 'Pythonesque' afin de pouvoir la ré-utliser dans d'autres langages : 

```python
for ligne in open('le/chemin/vers/le/fichierQueJeVeuxLire.txt','r',encoding='utf8'): # 'r' comme read
  print(ligne) # affiche sur le terminal la ligne suivante à chaque tour de boucle
```

### Écriture dans un fichier texte

#### Ajouter une nouvelle ligne à la fin d'un fichier

Il s'agit d'une des opérations les plus courantes, voici un exemple de code : 

```python
file_object = open('le/chemin/vers/le/fichierQueJeVeuxModifier.txt', 'a') # 'a' comme add

file_object.write('hello') # j'ajoute une nouvelle ligne à la fin du fichier et qui contient 'hello'
 
file_object.close()
```

#### Remplacer des données dans un fichier

Plusieurs stratégies sont possibles, une des plus simples (mais pas la plus performante..) à mettre en oeuvre suit l'algorithme suivant : 

- Lire le contenu fichier avec un peu de code python (voir l'exemple dans la partie consacrée à la lecture du fichier texte)
- Modifier les lignes voulues pendant la lecture du contenu du fichier (l'utilisation de REGEX peut être pertinante ici !)
- Enregistrer les nouvelles données dans un nouveau fichier

+++bulle matthieu
  vous pourriez bien sur modifier les données et les enregistrer dans le même fichier texte initial, mais cette approche demandera quelques lignes python supplementaires
+++

Voici ce que donne l'algorithme décrit ci-dessus en python : 

```python
nouveauContenu = ""

for ligne in open('le/chemin/vers/le/fichier/quejeveuxlire.txt','r',encoding='utf8'):
  if('toto' in ligne): # si la ligne contient toto, alors de remplace toute la ligne par tata
    nouveauContenu += 'tata\n'
  else:
    nouveauContenu += ligne + '\n'

file_object = open("le/chemin/vers/le/nouveauFichier.txt", "w") # w comme 'write'
file_object.write(nouveauContenu)
file_object.close()
```



## Amélioration des projets

### Sujet 0 : les calculatrices

- La calculatrice doit afficher les 20 derniers calculs effectués par l'utilisateur
- Les calculs (et leur résultat) doivent être enregistrés dans un fichier log textuel, il y a donc un fichier pour la calculatrice 'primaire' et un fichier pour la calculatrice 'collège'
- Si je redémarre mon ordinateur, je dois pouvoir retrouver affiché mes 20 derniers calculs lorsque je ré-ouvre une des calculatrices
- Aller plus loin : permettre à l'utilisateur de supprimer l'historique des calculs **depuis la calculatrice concernée**

### Sujet 1 : POO-RPG enhanced edition

- Donner la possibilité au joueur de créer plusieurs parties
- Les données de chaque partie doit être enregistrées dans un fichier de sauvegarde distinct (donc il y a autant de fichiers de saugegardes que de parties crées)
- Aller plus loin : permettre au joueur de supprimer une partie **depuis le jeu** 