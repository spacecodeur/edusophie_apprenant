# Extraction de données dans un texte : <br>les REGEX

+++programme
  Utilisation du moteur d’expressions régulières
  
  Utiliser des pattern REGEX pour rechercher une à plusieurs informations dans du texte
  Utiliser des pattern REGEX pour extraire une à plusieurs informations dans du texte
+++

+++sommaire

<style>
  reg{
    white-space: nowrap !important;
    overflow: auto !important;
    font-size: .875em !important;
    color: #0bf !important;
    word-wrap: break-word !important;
    font-family: var(--bs-font-monospace) !important ;
    font-size: 1em !important;
    direction: ltr !important;
    unicode-bidi: bidi-override !important;
    font-weight: bold;
  }
</style>

## Introduction

La recherche d'informations dans du texte est une opération courante en informatique. Dans un contexte d'administration de serveurs, vous êtes par exemple amenés à consulter divers fichiers de log pour suivre en détail l'état des serveurs et des applications hébergées dessus. 

Prenons l'extrait de fichier de log simplifié ci-dessous : 

```
83.149.9.216 - - [17/May/2015:10:05:34 +0000] "GET /presentations/slides.php HTTP/1.1" 200
46.105.14.53 - - [17/May/2015:11:05:33 +0000] "GET /blog/tags/puppet?flav=rss20 HTTP/1.1" 500
79.185.184.23 - - [17/May/2015:12:05:22 +0000] "GET /images/web/2009/banner.png HTTP/1.1" 200
37.115.186.244 - - [17/May/2015:12:33:16 +0000] "POST /blog/geekery/xvfb-firefox HTTP/1.1" 500
50.195.216.83 - - [17/May/2015:14:05:57 +0000] "GET /presentations/ HTTP/1.1" 200
84.85.130.133 - - [17/May/2015:14:05:28 +0000] "GET /projects/xdotool.html HTTP/1.1" 200
180.180.119.57 - - [17/May/2015:16:05:32 +0000] "GET / HTTP/1.1" 200
195.250.34.144 - - [17/May/2015:17:05:50 +0000] "GET /admin.php HTTP/1.1" 404
74.218.234.48 - - [17/May/2015:18:05:03 +0000] "GET /articles/dynamic-dns-with-dhcp/ HTTP/1.1" 200
72.174.22.174 - - [17/May/2015:20:05:38 +0000] "GET /reset.css HTTP/1.1" 200
99.252.100.83 - - [17/May/2015:20:05:27 +0000] "GET /style2.css HTTP/1.1" 200
```

+++bulle matthieu
  en supposant que le fichier de log contient plusieurs dizaines de milliers de lignes, comment je pourrais compter le nombre de requêtes qui ont rencontré l'erreur 500 ?
+++

+++bulle matthieu droite
  comment je pourrais compter toutes les IP uniques qui ont accédé aux ressources '/presentations/*' du serveur ?
+++

---

Le concept des expressions régulières ou REGEX (**REG**ular **EX**pression) est **apparu dans les années 1950** lorsque le mathématicien **Stephen Cole Kleene** ( +++flag-us ) a décrit des **modèles neuronaux** via des **notations algébriques**.

Par la suite **en 1968**, le concept a été informatisé par **Ken Thompson** ( +++flag-us ) qui n'est autre qu'un des **fondateurs du système UNIX**. Cette implémentation permettait aux utilisateurs d'effecuter **des recherches avancées dans un document textuel** et profitait également grandement au **développement de compilateurs**.

C'est à partir de là que les REGEX ont fait leur entrée dans le monde informatique. Depuis, **de nombreux langagues** (Python, Bash, Java, PHP, ...) offrent des implémentations des REGEX et permettent alors à leur tour d'**effectuer des opérations avancées dans des contenus textuels**.

Les REGEX peuvent paraître aux premiers abords intimidantes, voici par exemple un pattern REGEX qui permet de récolter toutes les adresses IPV4 contenu dans un fichier textuel : <reg>((?:\d{1,3}[.]){3}\d{1,3})</reg>. Il est alors important d'une part de pratiquer pour s'entraîner, d'autre part de construire une REGEX en procédant étapes après étapes. 

### Premier exemple d'utilisation

widget python
import re

extraitFichierLog = ("83.149.9.216 - - [17/May/2015:10:05:34 +0000] \"GET /presentations/slides.php HTTP/1.1\" 200\n"
	"46.105.14.53 - - [17/May/2015:11:05:33 +0000] \"GET /blog/tags/puppet?flav=rss20 HTTP/1.1\" 500\n"
	"79.185.184.23 - - [17/May/2015:12:05:22 +0000] \"GET /images/web/2009/banner.png HTTP/1.1\" 200\n"
	"37.200.186.244 - - [17/May/2015:12:33:16 +0000] \"POST /blog/geekery/xvfb-firefox HTTP/1.1\" 500\n"
	"50.195.216.83 - - [17/May/2015:14:05:57 +0000] \"GET /presentations/ HTTP/1.1\" 200\n"
	"84.85.130.133 - - [17/May/2015:14:05:28 +0000] \"GET /projects/xdotool.html HTTP/1.1\" 200\n"
	"180.180.119.57 - - [17/May/2015:16:05:32 +0000] \"GET / HTTP/1.1\" 200\n"
	"195.250.34.144 - - [17/May/2015:17:05:50 +0000] \"GET /admin.php HTTP/1.1\" 404\n"
	"74.218.234.48 - - [17/May/2015:18:05:03 +0000] \"GET /articles/dynamic-dns-with-dhcp/ HTTP/1.1\" 200\n"
	"72.174.22.174 - - [17/May/2015:20:05:38 +0000] \"GET /reset.css HTTP/1.1\" 200\n"
	"99.200.100.83 - - [17/May/2015:20:05:27 +0000] \"GET /style2.css HTTP/1.1\" 200")

regex = r"500"

matches = re.finditer(regex, extraitFichierLog)

if(matches):
  print('erreur(s) 500 détectée(s) !')
else:
  print('aucune erreur 500 !')

widget

+++bulle matthieu
  modifiez le programme afin de détecter si des requêtes retournent des codes 200
+++

+++bulle matthieu droite
  modifiez le programme afin de compter le nombre de requêtes qui retournent des codes 200
+++

+++spoil
widget python
import re

extraitFichierLog = ("83.149.9.216 - - [17/May/2015:10:05:34 +0000] \"GET /presentations/slides.php HTTP/1.1\" 200\n"
	"46.105.14.53 - - [17/May/2015:11:05:33 +0000] \"GET /blog/tags/puppet?flav=rss20 HTTP/1.1\" 500\n"
	"79.185.184.23 - - [17/May/2015:12:05:22 +0000] \"GET /images/web/2009/banner.png HTTP/1.1\" 200\n"
	"37.200.186.244 - - [17/May/2015:12:33:16 +0000] \"POST /blog/geekery/xvfb-firefox HTTP/1.1\" 500\n"
	"50.195.216.83 - - [17/May/2015:14:05:57 +0000] \"GET /presentations/ HTTP/1.1\" 200\n"
	"84.85.130.133 - - [17/May/2015:14:05:28 +0000] \"GET /projects/xdotool.html HTTP/1.1\" 200\n"
	"180.180.119.57 - - [17/May/2015:16:05:32 +0000] \"GET / HTTP/1.1\" 200\n"
	"195.250.34.144 - - [17/May/2015:17:05:50 +0000] \"GET /admin.php HTTP/1.1\" 404\n"
	"74.218.234.48 - - [17/May/2015:18:05:03 +0000] \"GET /articles/dynamic-dns-with-dhcp/ HTTP/1.1\" 200\n"
	"72.174.22.174 - - [17/May/2015:20:05:38 +0000] \"GET /reset.css HTTP/1.1\" 200\n"
	"99.200.100.83 - - [17/May/2015:20:05:27 +0000] \"GET /style2.css HTTP/1.1\" 200")

regex = r" 200" # attention, il faut ajouter un espace avant 200, sinon on détecte les 200 qui se trouvent dans les adresses IPs

matches = re.finditer(regex, extraitFichierLog)

if(matches):
  print(len(matches))
else:
  print('aucune requête avec le code 200')

widget
spoil+++

### Des outils pour aider à la création d'un pattern REGEX

Il existe une multitude d'outils Web pour pouvoir tester des REGEX et même générer automatiquement le code python correspondant à la REGEX que nous souhaitons utiliser. 

+++bulle matthieu
  nous allons par la suite utiliser <a href="https://regex101.com/">https://regex101.com</a> qui est très complet
+++

+++bulle matthieu droite
  prenons quelques instants ensemble pour exploirer l'outils
+++

## Rechercher des informations avec REGEX

### Préciser le début et/ou la fin de ligne du pattern à rechercher

Travaillons sur la chaîne de caractères suivante : 
`poire pomme banane cerise poire banane`

+++bulle matthieu
  quel pattern REGEX dois-je utiliser pour vérifier si la ligne contient le mot pomme ?
+++

+++bulle matthieu droite
  pour vérifier que la ligne <b>commence</b> par le mot pomme, nous devons utiliser le caractère <reg>^</reg>
+++

widget python
import re

regex = r"^pomme"

test_str = "poire pomme banane cerise poire banane"

matches = re.search(regex, test_str)

if(matches):
  print('pomme se trouve bien au début de la chaîne de caractères')
else:
  raise Exception('pomme ne se trouve pas au début de la chaîne de caractères')

widget

---

Pour détecter à l'inverse la fin de la chaîne de caractère, nous utilisons le caractère <reg>$</reg>. 

Nous voulons maintenant tester deux nouvelles chaînes de caractères : 

- `poire pomme banane poire cerise cerise`
- `cerise poire pomme banane cerise poire banane cerise`

+++bulle matthieu
  modifiez le code ci-dessus en créant un tableau, le tableau nommé <code>tab</code> doit contenir les trois chaînes de caractères dont les deux nouvelles chaînes ci-dessus
+++

+++spoil "Aide" nolabel
  vous pouvez retrouver un exemple de création de tableau en python <a href="00-__-_programmation-_objet.md#editeur-8">dans une des réponses du support dédié à la programmation POO</a>
spoil+++

+++bulle matthieu droite
  afin de vérifier que votre tableau fonctionne, ajoutez les lignes ci-dessous à la fin de votre programme puis exécutez le, il doit alors afficher sans erreurs le contenu du tableau <code>tab</code>
+++

```python
print(tab[0]) # doit afficher la première chaîne de caractères
print(tab[1]) # doit afficher la deuxième chaîne de caractères
print(tab[2]) # doit afficher la troisième chaîne de caractères
```

---

+++bulle matthieu
  le programme doit maintenant afficher quelle(s) chaîne(s) de caractères <b>commence</b> par <code>cerise</code> et aussi afficher quelle(s) chaîne(s) de caractères <b>finit</b> par <code>cerise</code>
+++

+++bulle matthieu droite
  utilisez une boucle pour parcourir chaque éléments du tableau <code>tab</code>, et à chaque tour de boucle, testez la REGEX sur la chaîne de caractères courante
+++

+++spoil
widget python
import re

regex1 = r"^cerise"
regex2 = r"cerise$"

tab = [
  "poire pomme banane cerise poire banane",
  "poire pomme banane poire cerise cerise",
  "cerise poire pomme banane cerise poire banane cerise",
]

print(tab[0])
print(tab[1])
print(tab[2])

print('\n')

for fruits in tab:
  matches1 = re.search(regex1, fruits)
  matches2 = re.search(regex2, fruits)
  if(matches1):
    print(f"cerise se trouve au début de la chaîne de caractères '{fruits}'")
  if(matches2):
    print(f"cerise se trouve à la fin de la chaîne de caractères '{fruits}'")

# j'ai utilisé une boucle for qui est la plus simple/pratique lorsqu'on veut parcourir les éléments d'un tableau les uns après les autres
# mais voici aussi le code qu'il aurai fallu écrire avec une boucle while (à la place de la boucle for)

# i = 0
# while i < len(tab):
#   matches1 = re.search(regex1, tab[i])
#   matches2 = re.search(regex2, tab[i])
#   if(matches1):
#     print(f"cerise se trouve au début de la chaîne de caractères '{tab[i]}'")
#   if(matches2):
#     print(f"cerise se trouve à la fin de la chaîne de caractères '{tab[i]}'")
#   i = i + 1 
widget
spoil+++

+++bulle matthieu
  pour les personnes en avance, modifiez le programme afin d'afficher un message si une chaîne de caractères donnée <b>commence <u>et</u> finit</b> par le mot <code>cerise</code>
+++

### Utiliser des ensembles REGEX customisés ou préconçus

+++bulle matthieu
  à partir de maintenant, nous mettons de côté Python pour nous concentrer sur les REGEX
+++

Dans nos chaînes de caractères, nous voulons maintenant vérifier si chacune d'entre elles contient **exactement** 6 fruits, nous pouvons voir que c'est le cas de la première et de la deuxième chaînes de caractères : 

- `poire pomme banane cerise poire banane`
- `poire pomme banane poire cerise cerise`
- `cerise poire pomme banane cerise poire banane cerise`

+++bulle matthieu
  une approche naïve pourrait consister à utiliser l'opérateur '<b>ou</b>' en REGEX de la manière suivante 
+++

<div class="text-center mb-3">"<span class="regex">poire pomme banane cerise poire banane<b>|</b>poire pomme banane poire cerise cerise</span>"</div>

+++bulle matthieu
  mais on peut comprendre que l'approche présente un <b>sévère défaut</b>, en effet, que se passerait-il si le nombre de lignes (et donc de combinaisons possible) devennait important ?
+++

+++bulle matthieu droite
  nous allons alors utiliser un des ensembles préconçu par REGEX, il en existe un nombre important, voici les principaux
+++

<div class="mb-3 col-10 mx-auto">
<table class="table table-sm">
  <thead>
    <tr class="text-center">
      <th></th>
      <th>description</th>
      <th>équivalent à</th>
      <th>exemple de pattern</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td colspan="4" class="text-center" style="background-color:lightblue">Les ensembles préconçus</td>
    </tr>
    <tr>
      <td class="text-center fw-bold align-middle"><reg>.</reg></td>
      <td class="text-center align-middle">représente n'importe quel caractère</td>
      <td class="bg-lightgrey"></td>
      <td><reg><b>L..t</b></reg><br>reconnaît : <b class="text-green">Leet</b>, <b class="text-green">L33t</b> et <b class="text-green">L$$t</b></td>
    </tr>
    <tr>
      <td class="text-center fw-bold align-middle"><reg>\w</reg></td>
      <td class="text-center align-middle">représente un caractère alpha-numérique <b>ou</b> '_'</td>
      <td class="text-center align-middle"><reg>[a-zA-Z0-9_]</reg></td>
      <td><reg>L<b>\w\w</b>t</reg><br>reconnaît : <b class="text-green">Leet</b>, <b class="text-green">L33t</b> et <b class="text-green">L3_t</b><br>ne reconnait pas : <b class="text-red">L$$t</b></td>
    </tr>
    <tr>
      <td class="text-center fw-bold align-middle"><reg>\d</reg></td>
      <td class="text-center align-middle">représente un caractère numérique</td>
      <td class="text-center align-middle"><reg>[0-9]</reg></td>
      <td><reg>L<b>\d\d</b>t</reg><br>reconnaît : <b class="text-green">L33t</b><br>ne reconnait pas : <b class="text-red">Leet</b> et <b class="text-red">L$$t</b></td>
    </tr>
    <tr>
      <td class="text-center fw-bold align-middle"><reg>\n</reg></td>
      <td class="text-center align-middle">représente un saut à la ligne</td>
      <td class="bg-lightgrey"></td>
      <td></td>
    </tr>
    <tr>
      <td class="text-center fw-bold align-middle regex"><reg>\t</reg></td>
      <td class="text-center align-middle">représente une tabulation</td>
      <td class="bg-lightgrey"></td>
      <td></td>
    </tr>
  </tbody>
</table>
</div>

+++bulle matthieu
  vous pouvez donc soit utiliser des ensembles préconçus en utilisant la caractère<reg><b> \ </b></reg>suivi de la lettre appropriée
+++

+++bulle matthieu droite
  soit créer vos propres ensembles avec les caractères <reg><b>[</b></reg> et <reg><b>]</b></reg> en vous aidant éventuellement de certains raccourcis tels que <reg><b>D-J</b></reg> pour gérer automatiquement les lettres allant de D à J
+++

Par exemple, nous souhaitons vérifier que la chaîne de caractères `1561691` ne contient que des chiffres, nous utiliserons par exemple le pattern REGEX suivant : <reg><b>^\d\d\d\d\d\d\d$</b></reg>

+++bulle matthieu
  d'après le tableau plus haut, comment peut on vérifier que la chaîne de caractères "024435" ne contient que des chiffres allant de 0 à 5 ?
+++

+++spoil
  <p>Il suffit d'utiliser le sous-ensemble <b class="regex">[0-5]</b> autant de fois qu'il y a de chiffres. Le pattern REGEX est donc : <reg><b>^[0-5][0-5][0-5][0-5][0-5][0-5]$</b></reg></p>
spoil+++

--- 

#### À vous de jouer !

En vous aidant de ce qui a été vu auparavant et éventuellement de recherches sur Internet, trouvez les patterns REGEX demandés.

+++bulle matthieu
  souvenez qu'il est possible de tester vos patterns REGEX sur des sites Internet tel que +++lien "https://regex101.com" "https://regex101.com"
+++


+++question "Trouvez un pattern REGEX qui détecte si une chaîne de caractères contient exactement 10 chiffres et commence par les chiffres 06"

<table class="table table-sm">
  <thead>
    <tr class="text-center">
      <th colspan="2">Comportement attendu du pattern REGEX</th>
    </tr>
    <tr class="text-center">
      <th class="text-green">Je dois reconnaître</th>
      <th class="text-red">je <u>ne</u> dois <u>pas</u> reconnaître</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><ul>
        <li>'<code>0678555548</code>'</li>
        <li>'<code>0645222201</code>'</li>
        <li>'<code>0699999999</code>'</li>
      </ul></td>
      <td><ul>
        <li>'<code>+33678555548</code>'</li>
        <li>'<code>067855554</code>'</li>
        <li>'<code>67855554888</code>'</li>
      </ul></td>
    </tr>
  </tbody>
</table>

+++spoil
  <p>Plusieurs réponses sont possiblers : </p><ul>
    <li><reg>^06\d\d\d\d\d\d\d\d$</reg></li>
    <li><reg>^06[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$</reg></li>
  </ul><p></p>
spoil+++

+++question "Trouvez un pattern REGEX qui détecte si une chaîne de caractères contient exactement 10 chiffres et commence par les chiffres 06 ou 07"

<table class="table table-sm">
  <thead>
    <tr class="text-center">
      <th colspan="2">Comportement attendu du pattern REGEX</th>
    </tr>
    <tr class="text-center">
      <th class="text-green">Je dois reconnaître</th>
      <th class="text-red">je <u>ne</u> dois <u>pas</u> reconnaître</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><ul>
        <li>'<code>0678555548</code>'</li>
        <li>'<code>0645222201</code>'</li>
        <li>'<code>0699999999</code>'</li>
        <li>'<code>0745222201</code>'</li>
      </ul></td>
      <td><ul>
        <li>'<code>+33678555548</code>'</li>
        <li>'<code>067855554</code>'</li>
        <li>'<code>67855554888</code>'</li>
        <li>'<code>07452222012</code>'</li>
      </ul></td>
    </tr>
  </tbody>
</table>

+++spoil
  <p>Plusieurs réponses sont possiblers : </p><ul>
    <li><reg>^0[6-7]\d\d\d\d\d\d\d\d$</reg></li>
    <li><reg>^0[6|7][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$</reg></li>
  </ul><p></p>
spoil+++

+++question "Trouvez un pattern REGEX qui permet de vérifier qu'une chaîne de caractères représente une année au format 'YYYY' avec le premier Y supérieur à 0"

<table class="table table-sm">
  <thead>
    <tr class="text-center">
      <th colspan="2">Comportement attendu du pattern REGEX</th>
    </tr>
    <tr class="text-center">
      <th class="text-green">Je dois reconnaître</th>
      <th class="text-red">je <u>ne</u> dois <u>pas</u> reconnaître</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><ul>
        <li>'<code>1986</code>'</li>
        <li>'<code>2022</code>'</li>
        <li>'<code>3411</code>'</li>
        <li>'<code>9999</code>'</li>
      </ul></td>
      <td><ul>
        <li>'<code>0124</code>'</li>
        <li>'<code>154</code>'</li>
        <li>'<code>77845</code>'</li>
        <li>'<code>s485</code>'</li>
      </ul></td>
    </tr>
  </tbody>
</table>

+++spoil
  <p>Plusieurs réponses sont possiblers : </p><ul>
    <li><reg>^[1-9][0-9][0-9][0-9]$</reg></li>
    <li><reg>^[1-9]\d\d\d$</reg></li>
  </ul><p></p>
spoil+++

+++question "Trouvez un pattern REGEX qui permet de vérifier si une chaîne de caractères contient exactement <b><u>un</u></b> des mots suivant : <code>mits</code>, <code>mjts</code>, <code>mkts</code>, <code>mlts</code>, <code>mmts</code>, <code>mnts</code>, <code>mots</code>, <code>mpts</code>, <code>mqts</code>, <code>mrts</code>, <code>msts</code>, <code>mtts</code>"

+++spoil
  <reg>^m[i-t]ts$</reg>
spoil+++

+++question "Trouvez un pattern REGEX qui permet de vérifier si une chaîne de caractères contient exactement <b><u>un</u></b> des mots suivant : <code>mits</code>, <code>mjts</code>, <code>mkts</code>, <code>mlts</code>, <code>mmts</code>, <code>mnts</code>, <code>mots</code>, <code>mpts</code>, <code>mqts</code>, <code>mrts</code>, <code>msts</code>, <code>mtts</code>, <code>m5ts</code>, <code>m6ts</code>, <code>m7ts</code>"

+++spoil
  <reg>^m[i-t5-7]ts$</reg>
spoil+++

+++question "Trouvez un pattern REGEX qui détecte si une chaîne de caractères contient <b>exactement</b> 3 mots composés chacun d'exactement 4 caractères, un espace doit se trouver entre chacuns des mots"

<table class="table table-sm">
  <thead>
    <tr class="text-center">
      <th colspan="2">Comportement attendu du pattern REGEX</th>
    </tr>
    <tr class="text-center">
      <th class="text-green">Je dois reconnaître</th>
      <th class="text-red">je <u>ne</u> dois <u>pas</u> reconnaître</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><ul>
        <li>'<code>tata toto titi</code>'</li>
        <li>'<code>a1c9 $ded ddff</code>'</li>
      </ul></td>
      <td><ul>
        <li>'<code>abcd a$$e ddff kkll</code>'</li>
        <li>'<code>abcd aaee</code>'</li>
        <li>'<code>abcd aaee ddff </code>'</li>
      </ul></td>
    </tr>
  </tbody>
</table>

+++spoil
  L'énoncé ne détaillant pas précisemment ce qu'est un caractère, nous pouvons partir du principe qu'un caractère englobe le plus large choix possible, un pattern REGEX pourrait être alors : 
  <reg>^.... .... ....$</reg>
spoil+++

---

Revenons-en à la question initiale, comment vérifier que notre chaîne de caractères contient <b>exactement</b> 6 fruits qui peuvent être des pommes et des poires ?

+++bulle matthieu
  une première approche pourrait ressembler à ceci
+++

<div class="text-center mb-3">"<reg>^(pomme|poire) (pomme|poire) (pomme|poire) (pomme|poire) (pomme|poire) (pomme|poire)$</reg>"</div>

+++bulle matthieu
  pour quelle(s) raison(s) cette approche n'est pas satisfaisante ?
+++

+++bulle matthieu droite
  Les <b>quantificateurs REGEX</b> vont nous permettre de résoudre ce problème !
+++

### Quantifier les sous-patterns recherchés

Ci-dessous se trouvent les quantificateurs utilisables en REGEX

<div class="mb-3 col-10 mx-auto">
<table class="table table-sm">
  <thead>
    <tr class="text-center">
      <th></th>
      <th>description</th>
      <th>équivalent à</th>
      <th>exemple de pattern</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td colspan="4" class="text-center" style="background-color:lightblue">Les quantificateurs</td>
    </tr>
    <tr>
      <td class="text-center fw-bold align-middle"><reg>*</reg></td>
      <td class="text-center align-middle">0 à plusieurs répétitions</td>
      <td class="align-middle regex text-center"><reg>{0,}</reg></td>
      <td><reg>ch<b>[u]*</b>t</reg><br>
      reconnait : <b class="text-green">cht</b>, <b class="text-green">chut</b>,<b class="text-green">chuuuuut</b><br>
      ne reconnait pas : <b class="text-red">chuuuaut</b><br>
      </td>
    </tr>
    <tr>
      <td class="text-center fw-bold align-middle"><reg>+</reg></td>
      <td class="text-center align-middle">1 à plusieurs répétitions</td>
      <td class="align-middle text-center"><reg>{1,}</reg></td>
      <td><reg>ch<b>[u]+</b>t</reg><br>
      reconnait : <b class="text-green">chut</b>,<b class="text-green">chuuuuut</b><br>
      ne reconnait pas : <b class="text-red">cht</b>, <b class="text-red">chuuuaut</b><br>
      </td>
    </tr>
    <tr>
      <td class="text-center fw-bold align-middle"><reg>?</reg></td>
      <td class="text-center align-middle">0 à 1 répétition</td>
      <td class="align-middle text-center"><reg>{,1}</reg></td>
      <td><reg>ch<b>[u]?</b>t</reg><br>
      reconnait : <b class="text-green">cht</b>, <b class="text-green">chut</b><br>
      ne reconnait pas : <b class="text-red">chuut</b><br>
      </td>
    </tr>
  </tbody>
</table>
</div>

Comment peut on raccourcir alors le pattern regex ci-dessous ?

<div class="text-center mb-3">"<reg>^(pomme|poire) (pomme|poire) (pomme|poire) (pomme|poire) (pomme|poire) (pomme|poire)$</reg>"</div>

+++bulle matthieu
  procédons par étapes ! simplifions le problème en ne prenant en compte que les 5 premiers mots pour commencer
+++

+++question "À l'aide des quantifiers REGEX, trouvez une version plus courte du pattern REGEX suivant"

<div class="text-center mb-3">"<reg>^(pomme|poire) (pomme|poire) (pomme|poire) (pomme|poire) (pomme|poire) $</reg>"</div>

<table class="table table-sm">
  <thead>
    <tr class="text-center">
      <th colspan="2">Comportement attendu du pattern REGEX</th>
    </tr>
    <tr class="text-center">
      <th class="text-green">Je dois reconnaître</th>
      <th class="text-red">je <u>ne</u> dois <u>pas</u> reconnaître</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><ul>
        <li>'<code>pomme pomme pomme pomme poire </code>'</li>
        <li>'<code>poire poire poire pomme poire </code>'</li>
      </ul></td>
      <td><ul>
        <li>'<code>poire poire poire pomme poire</code>'</li>
        <li>'<code>pomme pomme fruit poire pomme </code>'</li>
        <li>'<code>poirepoirepommepoirepomme</code>'</li>
        <li>'<code>pomme pomme poire pomme</code>'</li>
      </ul></td>
    </tr>
  </tbody>
</table>

+++spoil
  <p>Plusieurs réponses sont possiblers : </p><ul>
    <li><reg>^(pomme |poire ){5}$</reg></li>
    <li><reg>^((pomme|poire) ){5}$</reg></li>
  </ul><p></p>
  
spoil+++

+++question write "Améliorez le pattern REGEX afin de prendre en compte le dernier mot (qui est soit pomme, soit poire), <b>attention</b> il n'y a pas d'espace après le dernier mot"

+++spoil
  <p>Plusieurs réponses sont possiblers : </p><ul>
    <li><reg>^(pomme |poire ){5}(pomme|poire)$</reg></li>
    <li><reg>^((pomme|poire) ){5}(pomme|poire)$</reg></li>
  </ul><p></p>
spoil+++

+++question "Trouvez un pattern REGEX qui permet de vérifier si une phrase quelconque commence par le caractère '>' et finit par le mot <code>STOP</code>"

<table class="table table-sm">
  <thead>
    <tr class="text-center">
      <th colspan="2">Comportement attendu du pattern REGEX</th>
    </tr>
    <tr class="text-center">
      <th class="text-green">Je dois reconnaître</th>
      <th class="text-red">je <u>ne</u> dois <u>pas</u> reconnaître</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><ul>
        <li>'<code>> CECI EST UN TELEGRAMME STOP</code>'</li>
        <li>'<code>> CECI EST UN AUTRE TELEGRAMME STOP</code>'</li>
        <li>'<code>> dans le code de la route, nous trouvons plusieurs panneaux dont le fameux STOP</code>'</li>
      </ul></td>
      <td><ul>
        <li>'<code>> CECI N'EST PAS UN TELEGRAMME</code>'</li>
        <li>'<code>Chuck Norris had a problem so he decided to use regular expressions. Now, all the World’s problems are solved.</code>'</li>
      </ul></td>
    </tr>
  </tbody>
</table>

+++spoil
  <p>Le pattern REGEX qui est probablement le plus simples est : </p><ul>
    <li><reg>^>.*STOP$</reg></li>
  </ul><p></p>
spoil+++

+++question "Trouvez un pattern REGEX pouvant vérifier si le format d'une adresse email est correct"

+++bulle matthieu
  plusieurs approches sont possibles, commencez par déterminer 2/3 règles simples de bon formatage d'adresse IP, trouve le pattern REGEX pour vérifier
+++

+++bulle matthieu droite
  et ensuite ajoutez de nouveaux critères de validation de formatage d'adresse Email et complexifiez votre pattern REGEX
+++

### Extraire de l'information depuis un contenu textuel

Les REGEX permettent de vérifier le format d'une chaîne de caractères ou d'un texte. L'autre grande puissance des REGEX réside dans leur capacité à extraire avec précision des informations. Vous allez apprendre à utiliser cette mécanique à travers les étapes suivantes (des recherches Internet seront sans doute nécessaires). 

+++question "Téléchargez le fichier de apache 'access.log' qui se trouve +++lien "/d/formations/extrait_access.log" "dans ce lien" "

+++bulle matthieu
  apache est une serveur applicatif HTTP, ce dernier enregistre dans des fichiers access.log les informations relatives aux requêtes reçus par le serveur
+++

+++bulle matthieu droite
  sur Linux, la plupart des fichiers de log sont enregistrés dans le dossier <code>/var/log</code>
+++

+++question "Écrivez un programme python qui affiche le nombre de lignes contenues dans le fichier <code>extrait_access.log</code>"

+++spoil
  Plusieurs approches sont possibles en python pour parcourir le contenu d'un fichier textuel. Certaines approches sont spécifiques à python (utilisation de l'instruction <code>with</code>), d'autres ressemblent d'avantages à ce qu'on peut trouver dans la plupart des langages de programmation. La correction ci-dessous présente une approche généraliste qui n'est pas propre/dépendante à python : 
  
  <pre>    <code class="language-python">fichier = open("extrait_access.log", "r")

nbrLignes = 0 
for ligne in fichier:
    nbrLignes = nbrLignes + 1

print(f"Le fichier contient {nbrLignes} lignes")
fichier.close()
</code></pre>
spoil+++


+++question "Modifiez le programme afin qu'il affiche les 5 premières lignes du fichier <code>extrait_access.log</code>, le programme doit s'arrêter après avoir affiché les 5 premières lignes (il est inutile de parcourir tous le fichier)"

+++spoil
  <pre>    <code class="language-python">fichier = open("extrait_access.log", "r")

numLigne = 0
for ligne in fichier:
    print(ligne)
    numLigne = numLigne + 1
    if(numLigne == 5):
      fichier.close()
      exit()
</code></pre>
spoil+++

+++question "Pour les personnes en avance, améliorez le programme afin qu'il affiche 5 lignes aléatoires. Le programme doit s'arrêter après avoir affiché les 5 premières lignes"

+++spoil "Aide"
  Plusieurs approches sont possibles, une des plus simple pourrait consister à générer un nombre entre 0 et 9994 (le nombre total de lignes - 5 et - 1 en comptant le 0) et de lire les 5 lignes à partir du nombre généré
spoil+++

+++spoil
  <pre>    <code class="language-python">import random

fichier = open("extrait_access.log", "r")
ligneX = random.randint(0, 9994)

print(f"la valeur de 'ligneX' est {ligneX}")

numLigne = 0
for ligne in fichier:
    if(numLigne >= ligneX and numLigne <= ligneX + 5):
        print(f"{numLigne} : {ligne}")
    if(numLigne == ligneX + 5):
      fichier.close()
      exit()
    numLigne = numLigne + 1
</code></pre>
spoil+++

---

+++bulle matthieu
  chaque ligne dans le fichier contient l'ensemble des informations <b>d'une requête</b> reçue par un serveur (IP émettrice de la requête, date de la requête, code de la requête, ressource serveur sollicitée, user agent, ...)
+++

+++question "Qu'est-ce qu'un user agent ?"

---

Comme vous l'avez vu, les REGEX permettent à l'aide d'un pattern de vérifier le format d'une chaîne de caractère. Vous pouvez utiliser dans le pattern ce qu'on appelle en REGEX des **groupes de captures**. Un **groupe de capture** permet d'extraire une donnée précise contenue dans le pattern.

Voici un exemple de ligne qui peut se trouver dans le fichier 'extrait_access.log' : 


```text
45.132.207.221 - - [20/Dec/2020:05:12:58 +0100] "GET /index.php?option=com_contact&view=contact&id=1 HTTP/1.1" 200 9873 "-" "Mozilla/5.0(Linux;Android10;SM-J600FN)AppleWebKit/537.36(KHTML,likeGecko)Chrome/84.0.4147.125MobileSafari/537.36" "-"
```

+++question "Trouvez un pattern REGEX permettant de vérifier si cette ligne contient bien une adresse IP. Pour nous simplifier la tâche, nous partons du principe que chaque digit dans l'adresse IP peut avoir une valeur comprise entre 0 et 9"

+++spoil
  <reg>^(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) -</reg>
spoil+++

---
Dans notre programme python, nous souhaitons maintenant 'extraire' l'adresse IP de chacunes des lignes du fichiers. L'extraction permet de capturer l'information qui nous intéresse et nous pouvons la traiter par la suite comme bon nous semble (l'afficher, la stocker, effectuer des opérations avec, etc...)

Voici un exemple de code python permettant d'afficher seulement les adresses IP contenues dans le fichier `extrait_access.log`

```python
import re

fichier = open("extrait_access.log", "r")

regex = r"^(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) -"

for ligne in fichier:
    matches = re.search(regex, ligne)
    print(matches.groups(1))
```

+++bulle matthieu
  prenons quelques instants pour exécuter et analyser ensemble ce programme
+++

---

+++bulle matthieu
  dans la mise en pratique qui suit, je vous conseille de travailler d'abord sur un extrait des 20 premières lignes du fichier <code>extrait_access.log</code>, cela facilitera votre travail et vos sessions debogages
+++

L'équipe des administrateurs systèmes suspecte que des attaques malveillantes ont lieux lorsque des clients sollicitent la ressource serveur dont le chemin commence par `/template`

+++question "Améliorez le pattern REGEX et le programme python afin de n'afficher que les IP qui ont sollicité une ressource serveur située dans le chemin <b>commençant</b> par <code>/template/</code>"

+++spoil "Aide"
  voici un exemple de programme écrit sous la forme d'algorithme : 
  
```md
importer la bibliothèque python 're'
fichier <- contenu du fichier de log en mode lecture
regex <- r"^(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) -.*\/templates.*"

POUR CHAQUE ligne DANS fichier
  matches <- regexRecherche(regex, ligne)
  SI matches != None ALORS
    afficher(matches.groups(1)[0])
  FIN SI
FIN POUR
```
spoil+++

---

Finalement, il ne s'agit pas d'attaques malveillantes volontaires mais d'un bug déclenché lorsqu'un client tente d'accéder à une ressource serveur située dans le chemin <b>commençant</b> par <code>/template/</code>. Des développeurs pensent qu'il s'agit d'un bug provoqué lorsque le client utilise un OS spécifique.

+++question "Améliorez votre REGEX afin de capturer, en plus de l'IP, le nom de l'OS (toujours pour des requêtes sollicitant une ressource serveur située dans /templates). Afficher ensuite pour chaque IP l'OS correspondant correspondant"

+++question "Affichez en pourcentage le ratio du nombre de requêtes qui ont abouties à une 200 par rapport au nombre de requêtes qui ont abouties à une erreur 500"

+++question "Toujours en améliorant votre code Python : dans les requêtes relevées par votre REGEX, quel est l'OS qui a rencontré le plus d'erreur 500 lors d'une requête vers le serveur ?"

## Tableau récapitulatif sur les REGEX

<div class="mb-3 col-10 mx-auto">
<table class="table table-sm">
  <thead>
    <tr class="text-center">
      <th></th>
      <th>description</th>
      <th>équivalent à</th>
      <th>exemple de pattern</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td colspan="4" class="text-center" style="background-color:lightblue">Les ensembles préconçus</td>
    </tr>
    <tr>
      <td class="text-center fw-bold align-middle"><reg>.</reg></td>
      <td class="text-center align-middle">représente n'importe quel caractère</td>
      <td class="bg-lightgrey"></td>
      <td><reg><b>L..t</b></reg><br>reconnaît : <b class="text-green">Leet</b>, <b class="text-green">L33t</b> et <b class="text-green">L$$t</b></td>
    </tr>
    <tr>
      <td class="text-center fw-bold align-middle"><reg>\w</reg></td>
      <td class="text-center align-middle">représente un caractère alpha-numérique <b>et</b> '_'</td>
      <td class="text-center align-middle"><reg>[a-zA-Z0-9_]</reg></td>
      <td><reg>L<b>\w\w</b>t</reg><br>reconnaît : <b class="text-green">Leet</b>, <b class="text-green">L33t</b> et <b class="text-green">L3_t</b><br>ne reconnait pas : <b class="text-red">L$$t</b></td>
    </tr>
    <tr>
      <td class="text-center fw-bold align-middle"><reg>\d</reg></td>
      <td class="text-center align-middle">représente un caractère numérique</td>
      <td class="text-center align-middle"><reg>[0-9]</reg></td>
      <td><reg>L<b>\d\d</b>t</reg><br>reconnaît : <b class="text-green">L33t</b><br>ne reconnait pas : <b class="text-red">Leet</b> et <b class="text-red">L$$t</b></td>
    </tr>
    <tr>
      <td class="text-center fw-bold align-middle"><reg>\n</reg></td>
      <td class="text-center align-middle">représente un saut à la ligne</td>
      <td class="bg-lightgrey"></td>
      <td></td>
    </tr>
    <tr>
      <td class="text-center fw-bold align-middle"><reg>\t</reg></td>
      <td class="text-center align-middle">représente une tabulation</td>
      <td class="bg-lightgrey"></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="4" class="text-center" style="background-color:lightblue">Les quantificateurs</td>
    </tr>
    <tr>
      <td class="text-center fw-bold align-middle"><reg>*</reg></td>
      <td class="text-center align-middle">0 à plusieurs répétitions</td>
      <td class="align-middle regex text-center"><reg>{0,}</reg></td>
      <td><reg>ch<b>[u]*</b>t</reg><br>
      reconnait : <b class="text-green">cht</b>, <b class="text-green">chut</b>,<b class="text-green">chuuuuut</b><br>
      ne reconnait pas : <b class="text-red">chuuuaut</b><br>
      </td>
    </tr>
    <tr>
      <td class="text-center fw-bold align-middle"><reg>+</reg></td>
      <td class="text-center align-middle">1 à plusieurs répétitions</td>
      <td class="align-middle text-center"><reg>{1,}</reg></td>
      <td><reg>ch<b>[u]+</b>t</reg><br>
      reconnait : <b class="text-green">chut</b>,<b class="text-green">chuuuuut</b><br>
      ne reconnait pas : <b class="text-red">cht</b>, <b class="text-red">chuuuaut</b><br>
      </td>
    </tr>
    <tr>
      <td class="text-center fw-bold align-middle"><reg>?</reg></td>
      <td class="text-center align-middle">0 à 1 répétition</td>
      <td class="align-middle text-center"><reg>{,1}</reg></td>
      <td><reg>ch<b>[u]?</b>t</reg><br>
      reconnait : <b class="text-green">cht</b>, <b class="text-green">chut</b><br>
      ne reconnait pas : <b class="text-red">chuut</b><br>
      </td>
    </tr>
    <tr>
      <td colspan="4" class="text-center" style="background-color:lightblue">Les groupes de capture</td>
    </tr>
    <tr>
      <td class="text-center fw-bold align-middle text-nowrap"><reg>(</reg>...<reg>)</reg></td>
      <td class="text-center align-middle">Détecter un groupe <b>et</b> le capturer</td>
      <td class="align-middle text-center bg-lightgrey"></td>
      <td></td>
    </tr>
    <tr>
      <td class="text-center fw-bold align-middle text-nowrap"><reg>(?:</reg>...<reg>)</reg></td>
      <td class="text-center align-middle">Détecter un groupe <b>mais ne pas</b> le capturer</td>
      <td class="align-middle text-center bg-lightgrey"></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="4" class="text-center" style="background-color:lightblue">Divers</td>
    </tr>
    <tr>
      <td class="text-center fw-bold align-middle text-nowrap"><reg>\ </reg></td>
      <td class="text-center align-middle">Échaper un caractère</td>
      <td class="align-middle text-center bg-lightgrey"></td>
      <td><reg>L<b>\..</b>t</reg><br>reconnaît : <b class="text-green">L.3t</b><br>ne reconnait pas : <b class="text-red">Leet</b> et <b class="text-red">L$$t</b></td>
    </tr>
    <tr>
      <td class="text-center fw-bold align-middle text-nowrap"><reg>\\ </reg></td>
      <td class="text-center align-middle">Détecter un anti-slash</td>
      <td class="align-middle text-center bg-lightgrey"></td>
      <td><reg>L<b>\\.</b>t</reg><br>reconnaît : <b class="text-green">L\$t</b>, <b class="text-green">L\3t</b></td>
    </tr>
  </tbody>
</table>
</div>

## Quelques informations supplémentaires sur les REGEX

- Nous avons exploré ici les principales fonctionnalités de recherche utilisables avec REGEX, il en existe beaucoup d'autres qui peuvent être très utiles dans des cas de recherche plus rares 
- Il est possible, toujours en couplant une REGEX avec un langage de programmation, de rechercher <b>mais ensuite aussi de remplacer</b> un groupe capturé par une autre information
- Utiliser des REGEX peut être une opération gourmande en terme de ressources machine (surtout pour le processeur..), il existe un bon nombre d'articles sur Internet qui listent et illustrent les bonnes et les mauvaises pratiques lorsqu'on écrit une REGEX

## Amélioration des projets

Reprenez un des deux projets proposés à l'issue de la séance dédiée aux interfaces graphiques et apportez y les améliorations suivantes : 

### Sujet 0 : les calculatrices

- le programme doit maintenant afficher une erreur lorsque l'utilisateur saisit une opération mal formée, voici des exemples d'opérations mal formées : 
    - `4 ++ 7`
    - `* 8`
    - `12 -`
    - `/ 45 7`

+++bulle matthieu
  si vos calculatrices sont capables d'effectuer deux opérations à la fois, ajoutez le code adéquat afin de vérifier la bonne construction des opérations
+++

+++spoil "Aide"
+++bulle matthieu
  posez vous la question, est-ce qu'il vaut mieux créer une/des REGEX pour détecter si l'opération est mal construite ou une/des REGEX pour détecter si l'opération est bien construite
+++
spoil+++

- si vos calculatrices sont capables d'effectuer une ou deux opérations (dans le cas contraire, implémentez d'abord cette fonctionnalité), améliorez votre code afin que les calculatrices puissent effectuer un nombre quelconques d'opérations

+++spoil "Aide"
+++bulle matthieu
  vous aurez bien sur besoin des REGEX pour extraire les opérations et les valeurs afin, ensuite, de pouvoir les exécuter et ainsi afficher le résultat
+++
spoil+++

### Sujet 1 : POO-RPG enhanced edition

- ajoutez un champs `description` à votre personnage si ce n'est déjà fait. Le joueur pourra renseigner une description à son personnage lors de sa création : 
    - En partant du principe que le jeu se déroule dans une période médiévale, définissez un ensemble de mots interdits tels que : technologie, voiture, ordinateur, ...
    - Lorsque le joueur renseigne un de ses mots, l'interface graphique doit signaler en temps réel qu'il s'agit d'un mot interdit (et donc la validation de la description est impossible tant que le mot reste présent)
    - La description doit bien sur être visible pendant la partie

+++spoil "Aide"
  +++bulle matthieu
    une première étape pourrait consister à afficher simplement dans un print() que le mot est interdit
  +++
  +++bulle matthieu droite
    ensuite, ajoutez plutot une popup pour afficher le message, et pour terminer une première version satisfaisante, affichez l'erreurs dans un label devenant rouge situé dans l'interface graphique
  +++
  +++bulle matthieu
    bref ! procédez étapes par étapes !
  +++
spoil+++

- ajoutez un filtre d'obscénité si le personnage créé à moins de 15 ans, les insultes devront alors être automatiquement remplacées par des caractères générés aléatoirements (ressemblant par exemple à "@#$$@ !!") où le nombre de caractères avant les !! doit être égal au nombre de caractères de l'insulte originale
