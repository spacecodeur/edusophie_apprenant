# Base de données 1

+++programme
  utiliser une base de données avec Spring Boot
  
  concevoir une base de données
  créer une base de données, créer des tables, créer des colonnes
  insérer et extraire des valeurs depuis la base de données
+++

+++sommaire

## Introduction 

Dans la vie quotidienne, il est fréquent d'utiliser des informations provenant de bases de données : 
- lorsque nous consultons le catalogue d'une bibliothèque
- lorsque nous effectuons une réservation dans un hotel
- lorsque nous écoutons notre playliste de musique favorite
- ...

+++bulle matthieu
  comment définir ce qu'est une base de données ? quelle(s) différences y-a-t'il entre une base de données et un fichier texte ?
+++

+++spoil
  L'objectif premier d'une base de données est d'organiser un ensemble de données selon une structure qui va optimiser le temps nécessaire pour les stocker et pour les extraire
spoil+++

Nous utilisons aujourd'hui des **SGBD** (**S**ystème de **G**estion de **B**ase de **D**onnées) qui permettent d'interfacer la base de données et les utilisateurs qui ont besoin d;utiliser cette dernière.

+++bulle matthieu
  connaissez vous des SGBD ?
+++

Il existe plusieurs types de bases de données qui répondent à des besoins différents (souvent liés aux structures des données à conserver). Nous nous focalisons ici sur les bases de données relationnelles qui sont majoritairement utilisées aujourd'hui.

## Modélisation de base de données 

+++image "/images/md/E3FDH667994H4E862C2BB9C4A4CE47" col-6 mx-auto

<p></p>

Les bases de données sont composées d'un ensemble de tables qui peuvent contenir un ensemble d'entités **partageant les mêmes propriétés**. Chaque entité dans une table **doit être identifiable de manière unique**. Des relations **peuvent lier** certaines tables entre elles. 

+++bulle matthieu
  cette organisation ressemble furieusement aux exercices de modélisation POO ! 
+++

La modélisation des bases de données se fait en trois principales étapes qui s'effectuent dans cet ordre : 
0. la modélisation **conceptuelle** : représentée par un schéma nommé **MCD** (**M**odèle de **C**onception de **D**onnées)
1. **Puis**, la modélisation **logique** : représentée par un schéma **MLD** (**M**odèle de **L**ogique de **D**onnées)
2. **Puis**, la modélisation **physique** : représentée par un schéma **MPD** (**M**odèle **P**hysique de **D**onnées)

+++bulle matthieu
  tous ensemble, modélisons puis créons une base de données utilisée par un système de gestion simple d'emprunt de livre dans une bibliothèque française
+++

### Créer un dictionnaire de données

+++bulle matthieu 
  déterminons les principales fonctionnalités du système de gestion d'emprunt 
+++

+++spoil
  <ul>
    <li>Une personne inscrite dans la bibliothèque peut emprunter un livre</li>
    <li>Chaque livre peut être emprunté pendant 30 jours</li>
    <li>Si une personne ne rend pas un livre avant sa date de fin d'emprunt, nous la relançons par téléphone</li>
  </ul>
spoil+++

+++bulle matthieu droite
  ensuite, quelles données doit conserver la bibliothèque pour pouvoir fonctionner ?
+++

<div class="col-8 mx-auto">
  <table class="table table-sm">
    <thead>
      <tr class="text-center" style="background-color:lightblue">
        <th>Donnée</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Donnée 1</td>
        <td>description de la donnée 1</td>
      </tr>
      <tr>
        <td>Donnée 2</td>
        <td>description de la donnée 2</td>
      </tr>
      <tr>
        <td>...</td>
        <td>...</td>
      </tr>
    </tbody>
  </table>
</div>

+++spoil
  <div class="col-8 mx-auto">
  <table class="table table-sm">
    <thead>
      <tr class="text-center" style="background-color:lightblue">
        <th>Donnée</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><b>numéro de sécurité sociale NIR</b></td>
        <td>Permet d'identifier de manière unique une personne</td>
      </tr>
      <tr>
        <td><b>nom</b></td>
        <td>nom d'une personne</td>
      </tr>
      <tr>
        <td><b>prénom</b></td>
        <td>prénom d'une personne</td>
      </tr>
      <tr>
        <td><b>numéro de téléphone</b></td>
        <td>numéro de téléphone d'une personne</td>
      </tr>
      <tr>
        <td><b>ISBN-13</b></td>
        <td>Permet d'identifier de manière unique un livre (<b>I</b>nternational <b>S</b>tandard <b>B</b>ook <b>N</b>umber)</td>
      </tr>
      <tr>
        <td><b>date d'emprunt</b></td>
        <td>La date où le livre a été emprunté</td>
      </tr>
    </tbody>
  </table>
</div>
spoil+++

+++bulle matthieu droite
  pour terminer, précisons la nature des données et regroupons les en groupe
+++

<div class="col-8 mx-auto">
  <table class="table table-sm">
    <thead>
      <tr class="text-center" style="background-color:lightblue">
        <th>Donnée</th>
        <th>Description</th>
        <th>Type de donnée</th>
        <th>taille min et max de la donnée</th>
        <th>Groupe</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Donnée 1</td>
        <td>description de la donnée 1</td>
        <td>type de la donnée 1 (booléen, entier, chaîne de caractère, date, ...)</td>
        <td>de X à Y</td>
        <td>groupe A</td>
      </tr>
      <tr>
        <td>Donnée 2</td>
        <td>description de la donnée 2</td>
        <td>type de la donnée 2 (booléen, entier, chaîne de caractère, date, ...)</td>
        <td>de X à Y</td>
        <td>groupe B</td>
      </tr>
      <tr>
        <td>...</td>
        <td>...</td>
      </tr>
    </tbody>
  </table>
</div>

+++spoil
  <div class="col-8 mx-auto">
  <table class="table table-sm">
    <thead>
      <tr class="text-center" style="background-color:lightblue">
        <th>Donnée</th>
        <th>Description</th>
        <th>Type de donnée</th>
        <th>taille min et max de la donnée</th>
        <th>Groupe</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><b>numéro de sécurité sociale NIR</b></td>
        <td>Permet d'identifier de manière unique une personne</td>
        <td>entier</td>
        <td>15 chiffres</td>
        <td>Personne</td>
      </tr>
      <tr>
        <td><b>nom</b></td>
        <td>nom d'une personne</td>
        <td>chaîne de caractères</td>
        <td>de 3 à 20 caractères</td>
        <td>Personne</td>
      </tr>
      <tr>
        <td><b>prénom</b></td>
        <td>prénom d'une personne</td>
        <td>chaîne de caractères</td>
        <td>de 3 à 20 caractères</td>
        <td>Personne</td>
      </tr>
      <tr>
        <td><b>numéro de téléphone</b></td>
        <td>numéro de téléphone d'une personne</td>
        <td>chaîne de caractères</td>
        <td>10 chiffres</td>
        <td>Personne</td>
      </tr>
      <tr>
        <td><b>ISBN-13</b></td>
        <td>Permet d'identifier de manière unique un livre (<b>I</b>nternational <b>S</b>tandard <b>B</b>ook <b>N</b>umber)</td>
        <td>chaîne de caractères</td>
        <td>13 caractères</td>
        <td>Livre</td>
      </tr>
      <tr>
        <td><b>date d'emprunt</b></td>
        <td>La date où le livre a été emprunté</td>
        <td>chaîne de caractères</td>
        <td>10 caractères au format YYYY-MM-DD</td>
        <td>Livre</td>
      </tr>
    </tbody>
  </table>
</div>
spoil+++

### Créer un MCD

Un **MCD** suit les règles principales suivantes :
- Le schéma ne tient pas aucunément compte des technologies informatiques qui seront utilisées pour mettre en place la base de données
- Le schéma représente de manière abstraite à travers des entités et des relations
    - Une entité est seulement représentée par son nom
    - Une relation est symbolisée par <b class="text-red">un sens</b> (pour faciliter la lecture), <b class="text-green">une description</b> et un <b class="text-blue">couple de cardinalitées (x,y)</b> 
    - Une <b class="text-blue">cardinalité</b> peut avoir comme valeur :
        - <b class="text-blue">(0,1)</b> : par exemple, un produit a été commandé par <b class="text-blue">0</b> à <b class="text-blue">1</b> personne
        - <b class="text-blue">(1,1)</b> : par exemple, une personne est originaire de <b class="text-blue">1</b> et <b class="text-blue">1</b> seul pays
        - <b class="text-blue">(0,n)</b> : par exemple, une résidence est habitée par <b class="text-blue">0</b> à <b class="text-blue">n</b> habitant(s)
        - <b class="text-blue">(1,n)</b> : par exemple, un livre a été écrit par <b class="text-blue">1</b> à <b class="text-blue">n</b> auteur(s)

+++bulle matthieu
  rédigés dans les toutes premières phases d'un projet, un MCD doit pouvoir être autant compris par l'équipe MOA (Maitrîse d'ouvrage) que par l'équipe MOE (Maitrîse d'oeuvre) 
+++

#### Exercez vous ! 

Voici un exemple de MCD entre les deux entités 'livre' et 'éditeur'

+++image "/images/md/H428843AF42G8DF5B6A7AF3ABG3E5C" col-10 mx-auto

<p></p>

+++question "Dessinez les MCD pour les entités suivantes"

- L'oeuf et la poule
- Une maison et des pièces
- Un citoyen et un maire (un citoyen peut être maire et un maire correspond est un citoyen)
- Un article Web et un tag (exemples de tag : cuisine, jeuxvideo, sortie, danse, échecs, ...)
- Un acteur et un film
- Un train, un wagon, un passager
- Un voyageur, un pays, une ville

+++bulle matthieu
  revenons-en à notre bibliothèque, comment représenter en MCD les entités personne et livre ?
+++

+++spoil
  +++image "/images/md/8349E25HE3BAG9BA35AGFDG7GBB372" col-10 mx-auto
spoil+++

### Créer un MLD

Le **MLD** fait suite au **MCD**, il suit les règles principales suivantes : 
- Les entités deviennent des **tables** : un table possède le nom de l'entité et les noms des attributs propres à l'entité
    - Les attributs qui permettent d'identifier de manière unique chaque instance d'une table sont appelés **clé primaire** (+++flag-us **primary key**) et sont <u>surlignés</u>
- Les relations, décrites de manières abstraites dans le MCD sont **transformées** pour se rapprocher de l'état final de l'implémentation informatique de la base de données

+++bulle matthieu
  les transformations lors de cette étape sont une opération délicate qu'il faut suivre avec rigueur, vous vous rendrez compte un peu plus loin dans ce support de l'importance de ces opérations de transformations
+++

+++bulle matthieu droite
  voici les principales transformations à connaître et à savoir appliquer
+++

<br>

<p class=" mb-0 h4 text-center text-cyan">Règle de transformation MCD->MLD <b><u>n°1</u></b></p>
<div class="h4 mt-4 mb-4 bg-white text-center"><p class="border border-2 rounded d-inline p-3"><span class="p-2 border border-dark rounded-3" style="background-color:#cccccc;">entité 1</span> <sub><b class="text-blue">(0,1)</b> ou <b class="text-blue">(1,1)</b></sub> <b class="text-red"><=== </b><b class="bg-dark p-1 rounded" style="color:#00e400">association</b><b class="text-red"> ===></b> <sub><b class="text-blue">(0,n)</b> ou <b class="text-blue">(1,n)</b></sub> <span class="p-2 border border-dark rounded-3" style="background-color:#cccccc;">entité 2</span></p></div>

- Dans la liste des attributs de la table représentant l'entité 1, il faut ajouter la clé primaire de la table représentant l'entité 2

<p class=" mb-0 h4 text-center text-cyan">Règle de transformation MCD->MLD <b><u>n°2</u></b></p>
<div class="h4 mt-4 mb-4 bg-white text-center"><p class="border border-2 rounded d-inline p-3"><span class="p-2 border border-dark rounded-3" style="background-color:#cccccc;">entité 1</span> <sub><b class="text-blue">(0,1)</b> ou <b class="text-blue">(1,1)</b></sub> <b class="text-red"><=== </b><b class="bg-dark p-1 rounded" style="color:#00e400">association</b><b class="text-red"> ===></b> <sub><b class="text-blue">(0,1)</b> ou <b class="text-blue">(1,1)</b></sub> <span class="p-2 border border-dark rounded-3" style="background-color:#cccccc;">entité 2</span></p></div>

- On ajoute dans la table représentant l'entité 1 la clé primaire de l'entité 2
- On ajoute dans la table représentant l'entité 2 la clé primaire de l'entité 1

<p class=" mb-0 h4 text-center text-cyan">Règle de transformation MCD->MLD <b><u>n°3</u></b></p>
<div class="h4 mt-4 mb-4 bg-white text-center"><p class="border border-2 rounded d-inline p-3"><span class="p-2 border border-dark rounded-3" style="background-color:#cccccc;">entité 1</span> <sub><b class="text-blue">(0,n)</b> ou <b class="text-blue">(1,n)</b></sub> <b class="text-red"><=== </b><b class="bg-dark p-1 rounded" style="color:#00e400">association</b><b class="text-red"> ===></b> <sub><b class="text-blue">(0,n)</b> ou <b class="text-blue">(1,n)</b></sub> <span class="p-2 border border-dark rounded-3" style="background-color:#cccccc;">entité 2</span></p></div>

- Une **table de correspondance** doit être ajoutée entre la table représentant l'entité 1 et la table de l'entité 2
- La table de correspondance doit contenir les clés primaires des tables des entités 1 et 2 
- Le nom de la table de correspondance peut être la concaténation entre le nom de la table de l'entité 1 et le nom de la table de l'entité 2



---

#### Entraînons nous ensemble pour passer d'un MCD à un MLD !

+++bulle matthieu droite
  reprenons le <b>MCD</b> des entités voyageur, pays et ville
+++

+++image "/images/md/8BA7H5C9DG28HCG3CH7CF2G6D46DB2" col-6 mx-auto

+++bulle matthieu
  il y a deux relations en tout, et la relation la plus facile à transformer est la relation qui lie les entités Pays et Ville
+++

+++bulle matthieu droite
  nous devons appliquer la <span class="text-cyan">règle de transformation <b><u>n°1</u></b></span>, la relation <b>MCD</b>...
+++

+++image "/images/md/6GA89HHC6C8GC25G2DE2DAF7CAEBF4" col-8 mx-auto

+++bulle matthieu
  ...devient donc en <b>MLD</b>...
+++

+++image "/images/md/5D426ED976BFGFG6GGGCAAD9475DH9" col-8 mx-auto bg-white

+++bulle matthieu
  occupons nous maintenant de la relation entre voyageur et ville, nous devons appliquer la <span class="text-cyan">règle de transformation <b><u>n°3</u></b></span>, la relation <b>MCD</b>...
+++

+++image "/images/md/29B756G24HB7GD38622F3464GG73EE" col-8 mx-auto

+++bulle matthieu
  ...devient donc en <b>MLD</b>...
+++

+++image "/images/md/DC8AGB89AAG9H34C3H55AH7BBCB232" col-8 mx-auto bg-white

--- 

Voici donc le passage de la représentation de la base de données depuis le **MCD** vers le **MLD**

<div class="d-flex justify-content-between align-items-end bg-white">
  <div class="col-5 text-center">
    +++image "/images/md/8BA7H5C9DG28HCG3CH7CF2G6D46DB2"
    <p><b>M</b>odèle <b>C</b>onceptuel des <b>D</b>onnées</p>
  </div>
  <div class="col-6 text-center">
    +++image "/images/md/3FDEA67438B7F5BA5D3C9DDFBG5F4C"
    <p><b>M</b>odèle <b>L</b>ogique des <b>D</b>onnées</p>
  </div>
</div>

+++bulle matthieu
  dans la table Ville se trouve la propriété <b class="text-cyan">nomPays</b> qui fait référence à la <b>clé primaire</b> d'une autre table (nomPays), on dit alors que la propriété <b class="text-cyan">nomPays</b> de la table 'Ville' est <b>une clé étrangère</b>
+++

+++bulle matthieu droite
  ce qui identifie de manière unique chaque instance de la <b>table de correspondance</b> <b class="text-cyan">VoyageurVille</b> est la combinaison des <b>deux clés étrangères</b> <b class="text-cyan">idVoyageur</b> et <b class="text-cyan">nomVille</b>
+++

#### Exercez vous !

+++question "Dessinez les représentations MLD des MCD suivants"

- L'oeuf et la poule
- Une maison et des pièces
- Un citoyen et un maire (un citoyen peut être maire et un maire correspond est un citoyen)
- Un article Web et un tag (exemples de tag : cuisine, jeuxvideo, sortie, danse, échecs, ...)
- Un train, un wagon, un passager

+++bulle matthieu
  revenons denouveau à notre bibliothèque, comment représenter en MLD la base de données de notre bibliothèque ?
+++

+++spoil
  +++image "/images/md/9ECCCDCB396473A7CBC2E3H5795B84" col-8 mx-auto bg-white
spoil+++

### Créer un MPD

Le **MPD** est la dernière étape pour schématiser la base de donnée. Le but ici est de compléter le **MLD** afin de s'approcher le plus possible de l'état réel / physique de la base de données, nous prenons alors en compte la stack technique que nous utiliserons côté BDD/SGBD. Le **MPD** suit les principales suivantes : 

- Une table n'est plus composée de propriétés mais de champs
- Les noms des tables et des champs sont normalisés / informatisés : on ne garde des majuscules que lorsque c'est justifié/utile, on enlève les accents, on enlève les espaces, on utilise que des caractères alphabétiques (avec, plus rarement, des caractères numériques)
- Nous précisons pour chacun des champs leur type (utilisé en fonction du SGBD choisi) et (bonus) si ils peuvent être NULL

+++bulle matthieu
  le <b>MLD</b> représentant la base de données d'emprunts de livre...
+++

+++image "/images/md/9ECCCDCB396473A7CBC2E3H5795B84" col-8 mx-auto bg-white m-3

+++bulle matthieu
  ...devient alors en <b>MPD</b> (sachant que nous utilisons le <b>SGBD</b> MySQL)
+++

+++image "/images/md/8C352GE2B4G754B43GAF8CED8BC5G5" col-8 mx-auto bg-white

#### Exercez vous !

+++question "Reprenez le <b>MLD</b> sur les tables Voyageur, VoyageurVille, Ville, Pays et transcrivez le en <b>MPD</b>"

---

+++bulle matthieu
  vous savez donc maintenant comment définir les données nécessaires au bon fonctionnement d'une application (voir, d'un <b>S</b>ystème d'<b>I</b>nformations)
+++

+++bulle matthieu droite
  et à partir de là, comment obtenir un schéma représentant l'état le plus proche de l'implémentation réelle de la base de données.
+++

## Configuration d'une base de données avec Spring Boot

Comme la plupart des langages, JAVA permet de discuter avec une base de données. Cependant, un développement 'from scratch' présente plusieurs défauts : 

- Le temps de développement est conséquent 
- Si demain nous changeons de **SGBD**, le temps pris pour modifier le code peut être très important
- Utiliser du code spécifique peut nous éloigner des standards, cela peut alors impacter le travail en équipe 

+++bulle matthieu
  nous allons alors utiliser un <b>ORM</b> (<b>O</b>bject-<b>R</b>elational <b>M</b>apping)
+++

+++bulle matthieu droite
  et un des plus connus dans la stack technique graviant autour de Java et Spring Boot est l'ORM <b>Hibernate</b>
+++

**Hibernate** permets alors : 

- de créer facilement des tables/champs/relations/... avec le système d'annotation de Java
- de passer éventuellement d'un SGBD à un autre à moindre frais
- de centraliser le travail de base de données et applicatif au sein d'un même écosystème technique (Java ici)

+++bulle matthieu
  quel(s) avantage(s) et inconvénient(s) apporte ce dernier point ?
+++

### Installation des packages nécessaires

Pour rappel, notre projet utilise le gestionnaire de package **MAVEN**. Lorsque nous souhaitons utiliser un package JAVA extérieur, il faut alors modifier le fichier `pom.xml` du projet en y ajoutant dedans une nouvelle dépendance. 

Par exemple, l'ORM **Hibernate** a besoin que le package **JPA**  (<b>J</b>ava <b>P</b>ersistence <b>A</b>PI) soit installé dans le projet. Il suffit alors d'ajouter le bloc de ligne suivant dans le fichier 'pom.xml'

<pre><code class="xml"><​dependencies>
...
  <​dependency>
    <​groupId>org.springframework.boot<​/groupId>
    <​artifactId>spring-boot-starter-data-jpa<​/artifactId>
  <​/dependency>
...
<​/dependencies></code></pre>

#### Le driver SGBD

Dans le cadre de cette formation, nous nous tournerons vers le **SGBD** **H2**. Ce SGBG, relationnel, à l'avantage de demander peut de manipulations pour fonctionner avec le framework Spring Boot.

**Hibernate** n'embarque pas par défaut de drivers pour pouvoir fonctionner avec un SGBD spécifique, il va donc falloir ajouter une nouvelle dépendance dans notre `pom.xml`.

+++question "En vous aidant de recherches sur Internet, ajoutez le driver permettant de gérer une base de donnée type H2"

+++spoil
  Il faut ajouter dans le fichier <code>pom.xml</code> le bloc <code>dependency</code> suivant : 

  <pre><code><​dependencies>
...
  <​dependency>
    <​groupId>com.h2database<​/groupId>
    <​artifactId>h2<​/artifactId>
  <​/dependency>
...
<​/dependencies></code></pre>

  +++bulle matthieu
    ces lignes peuvent être trouvées dans divers sites sur Internet, cependant, il est possible de tomber sur de vieux tutoriaux utilisants des packages obsolètes ( +++flag-us deprecated )
  +++
  
  +++bulle matthieu droite
    je vous conseille alors de vérifier si le package est toujours maintenu en consultant +++lien "https://search.maven.org" "le moteur de recherche de package de <b>MAVEN</b>"
  +++

spoil+++

### Consulter sa base de données depuis une interface graphique

Le package `com.h2database` embarque avec lui une application Web locale permettant de consulter l'état de sa base de données. Une fois le server Spring démarré, il suffit d'ouvrir votree navigateur Web sur l'url <a href="localhost:8080/h2-console/">localhost:8080/h2-console/</a>

+++image "/images/md/BHG79DAC6E72698BBEH5CC39HEH749" col-6 mx-auto
<div class="text-center">le <span class="text-blue">JDBC URL</span> est une URL au format JDBC qui correspond à l'adresse de votre base de données</div>

### Utiliser une base de données enregistrée dans un fichier

Par défault, une nouvelle base de donnée est créée en mémoire chaque fois que le serveur Spring est redémarré. Une nouvelle URL (au format JDBC) est alors générée et est accessible dans les logs du serveur Spring lorsqu'il démarre.

+++question "En vous aidant de recherches sur Internet, trouvez le moyen d'enregistrer la base de données dans un fichier afin que les données puissent persister suite au redémarrage du serveur"

+++spoil
  <p>Le projet utilise un fichier de configuration central nommé 'application.properties', ce fichier est contenu dans le chemin 'nomDuProjetSprinBoot\src\main\resources\application.properties'</p>
  
  <p>Ce fichier permet entre autre de préciser la configuration utilisée avec une base de données. Donc pour utiliser une base de données stockée dans un fichier, il suffit d'ajouter la ligne suivante dans le fichier 'application.properties' :</p> 
  
  <pre><code class="language-properties">spring.datasource.url=jdbc:h2:file:./mabasededonnees</code></pre>
  
  <p>Ainsi, une base de données sera automatiquement créée (si elle n'existe pas) à la racine du dossier de votre projet</p>
  
  +++bulle matthieu
    avez vous trouvez d'autres propriétés intéressantes à utiliser dans le fichier <code>application.properties</code> ?
  +++
  
  +++bulle matthieu droite
    pour la suite, nous utiliserons dans le fichier 'application.properties' les paramètres suivants
  +++
  
  <pre><code class="language-properties">spring.datasource.url=jdbc:h2:file:./mabasededonnees

spring.jpa.database-platform=org.hibernate.dialect.H2Dialect # nécessaire pour que l'ORM hibernate utilise le 'langage' d'une BDD type H2
spring.jpa.hibernate.ddl-auto=create # la BDD est créée si elle n'existe pas lorsque le serveur Spring Boot démarre
logging.level.org.hibernate.SQL=DEBUG # affiche les requêtes H2 exécutées dans les logs du serveur Spring Boot</code></pre>
  
spoil+++

## Conception et manipulation d'une base de données avec Spring Boot 

Pour la suite, nous allons nous baser sur la création d'un site WEB de type CV, très utile pour partager son savoir faire à travers les réseaux et/ou pendant un entretien d'embauche ! 

Voici les actions que pourra faire un visiteur (un éventuel recruteur ?) sur le site : 

- Il pourra consulter les informations relatives au créateur du site
- Il pourra consulter les informations de chaque projets professionnels ou personnels du créateur du site
- Il pourra effectuer une recherche "par tag" pour consulter uniquement les projets qui possèdent le tag recherché. Voici quelques exemples de tags possibles : "télétravail", "stage", "Spring Boot", "Laravel", "projet-personnel", "projet-professionnel", ...

+++question "Rédigez un dictionnaire de données listant les données nécessaires pour le bon fonctionnement du site"

+++question "Dessinez un MCD, puis un MLD, puis un MPD en se basant sur votre dictionnaire de données"

+++bulle matthieu
  <b>attention</b>, pour rappel le MPD se rapproche le plus possible du SGBD utilisé 
+++

+++bulle matthieu droite
  étant donné que la BDD sera gérée via Hibernate, le type de données contenu dans le MPD correspond au types de variables utilisés en JAVA (cela facilitera le travail par la suite)
+++

+++spoil
  Voici un exemple de MPD possible : 
  
  +++image "/images/md/D29A666G365CBDBG8B873459CB4FEF" col-10 mx-auto
  
  
spoil+++

### Création d'entités


#### petit point vocabulaire...

<table class="table table-sm text-center">
    <tbody><tr>
      <th style="background-color:#bbdddd">Framework applicatif<br>(Spring Boot, Laravel, Symfony, ...)</th>
      <th style="background-color:#bbdddd">POO<br>(Programmation Orientée Objet)</th>
      <th style="background-color:#bbdddd">Base de données</th>
    </tr>
  
  </tbody><tbody>
    <tr>
      <td style="background-color:#ffff00"><b>entité (ou modèle)</b></td>
      <td style="background-color:#ffffaa">classe</td>
      <td style="background-color:#ffffaa">table</td>
    </tr>
    <tr>
      <td></td>
      <td>attribut d'une classe</td>
      <td>champ d'une table</td>
    </tr>
    <tr>
      <td style="background-color:#ffff00"><b>instance d'une entité</b></td>
      <td style="background-color:#ffffaa">objet (instance d'une classe)</td>
      <td style="background-color:#ffffaa">entrée d'une table</td>
    </tr>
    <tr>
      <td></td>
      <td>attribut d'un objet</td>
      <td>contenu d'un champ d'une table</td>
    </tr>
  </tbody>
</table>
  
+++bulle matthieu
  en résumé, si je veux créer une <b class="rounded px-1" style="background-color:#ffff00">entité</b> basée sur une <span class="rounded px-1" style="background-color:#ffffaa">table</span> de ma base de données, alors je dois créer une <span class="rounded px-1" style="background-color:#ffffaa">classe</span> JAVA
+++
  
+++question "Créez un nouveau fichier nommé 'Utilisateur.java' dans le dossier '/src/main/java/com/formation/sandbox/entities'"

+++question "Ajoutez dans le fichier 'Utilisateur.java' le contenu suivant"

```java
// src/main/java/com/formation/sandbox/entities/Utilisateur.java
package com.formation.sandbox.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Utilisateur {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
}
```

+++question "Que fait selon vous la ligne 14 ?"

+++spoil
  <p>La ligne 14 permet de définir la stratégie employée pour générer automatiquement le champ 'id' lorsqu'un nouvel utilisateur est inscrit dans la table 'Utilisateur'</p>
  <p>La stratégie <code>IDENTITY</code> signale à Hibernate que le champs ID doit être un identitifant dans la table (une clé primaire du coup). Donc lorsque Hibernate créé physiquement la table, il consulte le driver H2 afin de savoir comment le champ dans la table doit être crée pour être unique.</p>
spoil+++

+++question "Ajoutez dans la classe 'Utilisateur' les attributs suivants : nom, prenom, email, dateDeNaissance"

+++spoil

```java
// src/main/java/com/formation/sandbox/entities/Utilisateur.java
package com.formation.sandbox.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Utilisateur {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
 
    private String prenom;
    private String nom;
    private String email;
    private Date dateDeNaissance;
}
```

spoil+++
  
+++question "Démarrez le serveur Spring boot et vérifiez les points suivants :"

0. aucune erreur n'apparait lorsque le serveur démarre
1. un fichier de base de données nommé 'mabasededonnees' est créé à la racine du projet Spring Boot
2. on peut consulter le contenu de la table 'Utilisateur' à travers le client Web H2

+++spoil
  si tout se passe bien, le client Web H2 devrait vous afficher le contenu de la table 'Utilisateur'
  
  +++image "/images/md/42AHE358E23AF8B552H33A8777GHB4" col-6 mx-auto
spoil+++

---

+++bulle matthieu
  d'après vous, comment Spring Boot sait qu'il doit créer une entité à partir du fichier `Utilisateur.java`  ?
+++

+++spoil
  
Lorsque le serveur Spring Boot démarre, il exécute au début la méthode `main` du fichier java principal

<pre><code class="language-java">// src/main/java/com/formation/sandbox/SandboxApplication.java
public static void main(String[] args) {
	SpringApplication.run(SandboxApplication.class, args);
}</code></pre>

<ol start="0">
  <li>Le contenu actuel de cette fonction main exécute toute la machinerie qui se trouve derrière Spring Boot.</li>
  <li>Une des étapes de Spring Boot est d'analyser tous les fichiers contenus dans le projet et dont le namespace (qui est <code>com.formation.sandbox</code>) de la classe <code>SandboxApplication</code>.</li>
  <li>Le fichier 'Utilisateur.java' a pour namespace <code>com.formation.sandbox.entities</code>, il s'agit donc d'un sous-namespace du namespace <code>com.formation.sandbox</code>. Le fichier est donc analysé par Spring Boot.</li>
  <li>Pour terminer, le fichier 'Utilisateur.java' contient une classe précédée de l'annotation <code>@Entity</code>. Donc, Spring Boot, génère une table en base de données à partir de cette classe</li>
</ol>
  
spoil+++

+++question "Créez une entité nommée 'projet', . Nous ne nous occupons pas pour l'instant de la relation entre les entités 'User' et 'Emploi'"

+++spoil

<pre><code class="language-java">// src/main/java/com/formation/sandbox/entities/Projet.java
package com.formation.sandbox.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Projet {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String nom;
    private Integer nbrJours;
}</code></pre>

spoil+++

#### Insérer et extraire des données

+++bulle matthieu
  dans cette partie, je vous invite à télécharger le projet disponible +++lien "/d/formations/website_sandbox2.zip" "dans ce lien", il s'agit du projet actuel mais avec quelques améliorations supplémentaires
+++

+++bulle matthieu droite
  démarrez le serveur spring boot de ce projet, et ouvrez le fichier 'frontend/index.html' depuis un navigateur (<b>/!\ <a href="./01-__-_initiation-_Spring-_Boot.md#travailctfrontend">avec les protection liées aux CORS désactivées</a> /!\ </b>)
+++

+++question "Prenez quelques minutes pour analyser le code et tenter d'élucider l'origine de la donnée 'age' affichée sur le navigateur"

## À vous de jouer ! 

+++bulle matthieu
  le reste de la séance est à vous ! explorez/creusez/expérimentez Spring Boot à travers un projet
+++

- Recherchez/explorez des plugins communautaires consacrés à Spring Boot avec votre IDE, partagez vos trouvailles
- Améliorez, au choix, l'application Web pour photographe ou l'application Web type CV. Ou bien, lancez vous sur un projet Spring Boot perso : 
    - ajoutez de nouvelles routes 
    - complexifiez votre base de données
    
+++bulle matthieu
  voici quelques exemples de points non abordés que vous pourriez explorer 
+++

+++bulle matthieu droite
  vous pouvez au choix vous avancer pour la prochaine séance et/ou aborder des points qui vont au delà du cadre de cette formation
+++

### La prochaine séance 

- Comment prendre en compte, côté Spring Boot, les relations entre les tables ? (1<=>1, 0<=>N, ...)
- Actuellement, vous pouvez extraire et insérer des données en base, mais comment extraire/insérer/modifier/supprimer des données depuis l'application Web cliente ? 

### Aller plus loin

- Si le côté front vous intéresse d'avantage : 
    - explorer le framework CSS bootstrap qui vous permettra d'élaborer de belles interfaces graphiques Web 
    - utilisez, à la place du code Front actuel (il n'est pas nécessaire de modifier le code côté Back), un framework JS front connu tel que Angular, React, ... 

- Si le côté back vous intéresse d'avantage : 
    - trouvez et installez un package JAVA permettant de générer automatiquement du JSON à partir d'une entité
    - intéressez vous au stockage d'image côté serveur/backend (au lieu d'utiliser des images provenant d'url extérieures)