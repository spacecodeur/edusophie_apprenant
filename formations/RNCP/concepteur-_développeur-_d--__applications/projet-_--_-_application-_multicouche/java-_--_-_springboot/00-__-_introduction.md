# Introduction

+++programme
  construire une application organisée en couches
  
  concevoir une application multicouche
  installer un environnement de travail Spring Boot sous Linux
  déployer une application Spring Boot simple depuis un serveur de développement
+++

+++sommaire

## Introduction

Le but de ce module est de travailler les compétences énoncées plus haut à travers le développement d'une plateforme Web qui repose sur une architecture organisée en couche : 

- **dans un premier temps**, nous avancerons ensemble sur la partie conception, la configuration de vos postes de travail et le développement d'une application simple
- **dams un second temps**, vous travaillerez sur un projet individuel ou en binôme
- **pour terminer**, vous présenterez vos travaux (slides, démo, ...). L'évaluation du module se fera à travers cette présentation et votre rendu

## Choix des technologies

### La stack technique de base

- Le backend est écrit à l'aide d'une **framework java**

+++bulle matthieu
  nous apprendrons à utiliser dans cette formation un framework java aujourd'hui très connu et très utilisé qui est +++lien "https://spring.io/projects/spring-boot" "Spring Boot"
+++

- Le frontend est, par défaut, écrit en **HTML/CSS/Javascript natif**
- une **API REST** servira de canal de communication entre le **backend** et le **frontend**
- la persistance des données est gérée via le **SGBD MySQL**

+++bulle matthieu
  qu'est-ce qu'un framework ?
+++

+++bulle matthieu droite
  qu'est-ce qu'une API REST ?
+++

#### Amélioration possible côté frontend

+++bulle matthieu
  le frontend peut être entièrement développé à l'aide d'un framework spécialisé dans le développement d'application frontend
+++

- Depuis quelques années, les framework frontend dans le développement Web se sont multipliés. Les principaux aujourd'hui utilisent le langage javascript, en voici quelques uns : 
    - **Angular** (Google/communauté) : très complet mais demandant un temps d'apprentissage important
    - **Vuejs** : plus léger et plus rapide à prendre en main qu'Angular
    - **React** (facebook) : à mi-chemin entre Angular et Vuejs en terme de courbe d'apprentissage

### Architecture applicative générale 

Dans le cadre de ce module, vous développerez un projet en utilisant l'architecture applicative **MVC**

+++bulle matthieu
  qu'est-ce qu'une architecture applicative ? qu'est-ce qu'une architecture MVC ?
+++

+++spoil nolabel
  <p>Une architecture applicative désigne la manière dont une application <b>est découpée</b> et comment les différentes **couches**, ainsi découpées, <b>communiquent entre elles</b>. Avec l'explosion de l'industrie informatique dans les années 2000 et, grâce en partie à Internet, les pratiques de développement informatique se sont de plus en plus harmonisées. L'utilisation d'une architecture reconnue participe alors à l'harmonisation des pratiques tout en apportant des bases simples et puissantes visant à améliorer la maintenabilité d'une application. (sécurité, modularité, ...)</p>
  
  <p><b>MVC</b> est un exemple d'architecture applicative et l'acronyme signifie : </p>
  <ul>
    <li><b>Modèle</b> : cette partie représente la structuration et la persistence des données utilisées par l'application</li>
    <li><b>Vue</b> : cette partie représente l'affichage des informations de notre application (le contenu visuel d'une page Web par exemple) </li>
    <li><b>Controlleur</b> : cette partie fait office d'intermédiaire entre la vue et le modèle en s'assurant de la cohérence des données qui transitent entre les deux</li>
  </ul>
spoil+++

---

Voici ci-dessous un schéma vide représentant la future plateforme Web du module qui repose sur une architecture **MVC** : 

+++image "/images/md/265FEBCBE86A9B2C8HCCH8438EDC3B" mx-auto

+++bulle matthieu
  compte tenu des informations que vous avez lues/entendues depuis le début de cette séance, où se placeraient selon vous ces mots-clés dans le schéma ? 
+++

<div class="d-flex justify-content-center flex-wrap align-items-start">
  +++draggableTextBloc "java" m-2
  +++draggableTextBloc "HTML" m-2
  +++draggableTextBloc "CSS" m-2
  +++draggableTextBloc "navigateur Web" m-2
  +++draggableTextBloc "vue" m-2
  +++draggableTextBloc "modèle" m-2
  +++draggableTextBloc "controlleur" m-2
  +++draggableTextBloc "frontend" m-2
  +++draggableTextBloc "backend" m-2
  +++draggableTextBloc "javascript" m-2
  +++draggableTextBloc "mysql" m-2
  +++draggableTextBloc "API REST" m-2
  +++draggableTextBloc "HTTP(S)" m-2
</div>

+++spoil
  +++image "/images/md/C7GHE7FA7GEF32EH52HBHG9D26B458" col-10 mx-auto nolabel
spoil+++

## Installation de son environnement de travail

+++bulle matthieu
  avant de se lancer dans le code il est important de parcourir les premières étapes de la documentation <b>officielle</b> de ou des outils dont nous allons nous serveur
+++

+++bulle matthieu droite
  dans le cas de Spring Boot, +++lien "https://spring.io/quickstart" "la page quick start" nous indique les étapes à suivre pas-à-pas qui sont reprises dans les grandes lignes ci-dessous
+++

### Prérequis

#### Concernant Spring Boot : 

0. La documentation de Spring Boot recommande d'installer le JDK +++lien "https://bell-sw.com/pages/downloads" "BellSoft Liberica". **Assurez vous** que la version de "BellSoft Liberica JDK" soit une version supportée par +++lien "https://start.spring.io/" "le générateur de projet Spring Boot"
1. Après installation du JDK, vérifiez sa bonne installation en ouvrant un terminal et en saisissant la commande `java --version`, la commande ne doit pas retourner d'erreur et vous afficher la vesion de java que vous venez d'installer

{{{switch
[Un problème ? (OS windows)]
<ul>
  <li>La commande <code>java --version</code> n'est pas reconnue<ul>
    <li>Il est possible qu'il faille ajouter la <b>variable d'environnement</b> <code>JAVA_HOME</code> dans les <b>paramètres de Windows</b>. Une petite recherche sur Internet vous montrera comment faire</li>
  </ul></li>
[Un problème ? (OS débian)]
<ul>
  <li>La version de java qui est affichée via <code>java --version</code> n'est pas celle que je viens d'installer<ul>
    <li>Il est possible que plusieurs versions de JAVA cohexiste dans votre OS, vous pouvez alterner d'une version à l'autre avec la commande <code>sudo update-alternatives --config java</code></li>
  </ul>
</li></ul>
switch}}}

#### Concernant le développement

+++bulle matthieu
  le choix de l'<b>IDE</b> (ou des <b>IDE</b>s) est libre, dans les supports de cette formation, l'<b>IDE</b> VScode sera utilisé
+++

- (optionnel) +++checkbox "Installer VScode"--- (puis l'extension : Spring Boot Tools)
- +++checkbox "Installer +++lien "https://www.postman.com/downloads/" "Postman""--- (outil facilitant les tests de l'<b>API REST</b>)

### Générer un squelette de projet grâce au générateur de projet Spring Boot

+++bulle matthieu
  la plupart des frameworks mettent à disposition des outils permettant de générer des squelettes de projets
+++

+++bulle matthieu droite
  c'est le cas avec Spring Boot qui fournis +++lien "https://start.spring.io/" "une interface Web" pour générer rapidement un projet vide, générez alors un nouveau squelette de projet en respectant les configurations ci-dessous
+++

- Utilisez un projet de type **MAVEN**
- Utilisez 'com.formation' comme **nom de groupe** et 'sandbox' comme nom d'**artefact**
- Assurez vous que la **version JAVA** du squelette est la même que celle de '**BellSoft Liberica**' que vous avez auparavant installée
- Ajoutez les dépendances '**Spring Web**' (qui permettra par la suite de déployer un server Web) et '**devtools**' (qui permettra de recharger automatiquement votre serveur en cas de modification)
- Vous pouvez laisser le reste par défaut

## Sa première page 'Hello World'

### Démarrer le serveur de développement fournis par Spring Boot

Par le passé, et dans le développement Web notamment, il était nécessaire d'installer et de configurer un ordinateur pour le passer en mode serveur (et ainsi pouvoir tester son application pendant son développement). Depuis quelques années, la plupart des frameworks applicatifs embarquent leur propre <b>serveur de développement</b>. 

Spring Boot ne fait pas exception ! pour démarrer le serveur applicatif, il sufft de se rendre dans le dossier de son projet avec un terminal et de saisir la commande `./mvnw spring-boot:run`

+++bulle matthieu
  attention, je parle bien de <b>serveur de développement</b>, ce type de serveur n'est pas du tout calibré pour un environnement de production (sécurité, performance, ...)
+++

Une fois la commande `./mvnw spring-boot:run` exécutée et si tout se passe bien (= pas d'erreur en exécutant la commande), votre terminal vous afichera fièrement que le serveur est démarré tout en précisant le port utilisé (8080 par défaut). Ouvrons donc notre navigateur Web préféré sur l'url <a href="http://127.0.0.1:8080">http://127.0.0.1:8080</a> et voici ce qu'il devrait afficher : 

+++image "/images/md/8CFAD88BGC3H9D8E9E4H57B45438AF" col-8 mx-auto

+++bulle matthieu
  damned ! une erreur 404 ! quelle peut être la raison de cette erreur ?
+++

+++spoil
  Le serveur a bien reçu la requette HTTP de notre navigateur. Cette requête HTTP demande au serveur une ressource qui se trouve à la racine de l'application, hors, nous n'avons pas encore expliqué à notre application quelle ressource il faut délivrer au client lorsque ce dernier souhaite accéder à l'url <a href="http://127.0.0.1:8080">http://127.0.0.1:8080</a> (qui correspond à la racine du site)
spoil+++

+++bulle matthieu
  prenez quelques minutes pour suivre la documentation de Spring Boot et trouvez le moyen d'afficher une page HTML sans erreur lorsqu'un utilisateur souhaite accéder à la racine du site (c'est à dire l'url : <a href="http://127.0.0.1:8080">http://127.0.0.1:8080</a> )
+++

+++spoil
  La documentation montre un exemple qui permet d'afficher une page Web (sans erreur) lorsqu'on souhaite accéder à la ressource '/hello'. Il suffit alors de reprendre le code et de le modifier très légèrement afin de gérer la ressource '<a href="http://127.0.0.1:8080">http://127.0.0.1:8080</a>' (et non la ressource <a href="http://127.0.0.1:8080">http://127.0.0.1:8080/hello</a>). Voici le code que dois contenir le fichier 'src/main/java/com/formation/sandbox/SandboxApplication.java' : <br><br>
  
  ```java
package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SandboxApplication {

	public static void main(String[] args) {
		SpringApplication.run(SandboxApplication.class, args);
	}

	@GetMapping("/")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
		return String.format("Hello %s!", name);
	}
}
  ```
  
spoil+++

+++bulle matthieu
  comment peut on vérifier depuis son navigateur que le status HTTP de la page <a href="http://127.0.0.1:8080">http://127.0.0.1:8080</a> est le status 200 ? ( = tout s'est bien passé)
+++

+++spoil "indice" nolabel d-print-none
  La réponse se trouve dans l'inspecteur de code de votre navigateur...
spoil+++

+++spoil
  La plupart des navigateurs Web sur ordinateur proposent un outil permettant d'inspecter le code de la page, outil naturellement nommé 'inspecteur de code'. Dans cet inspecteur de code, nous pouvons trouver le code HTTP renvoyé par le serveur lorsque le client sollicite une ressource serveur spécifique. 
  
  Par exemple, pour Firefox et Chrome, il faut appuyer sur la touche f12 pour ouvrir l'inspecteur et naviguer dans l'onglet 'réseau' (+++flag-us network) afin de trouver le code HTTP renvoyé par le serveur.
spoil+++
</ul>