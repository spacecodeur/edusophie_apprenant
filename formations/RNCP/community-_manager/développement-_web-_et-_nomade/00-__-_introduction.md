# Introduction

+++programme
  Les différents types de terminaux
  Les plateformes et le marché mobile
  Les standards HTML, HTML 5, CSS3 (API, sélecteurs….)
  Contraintes spécifiques du mobile
  
  Contextualiser l´état du parc informatique grand public actuel
  Définir des termes techniques importants relatifs aux ordinateurs et au Web
  Analyser le temps d'exécution d'une page Web et son affichage sous différentes résolutions
+++

+++sommaire

## Quelques dates historiques 

**Le premier ordinateur avec moniteur ?**

+++spoil nolabel
  +++image "/images/md/H6A6D6HC9F376H46D6E3G4EF865C8E" col-6 mx-auto
  <p class="text-center">1973 : Xerox Alto computer (606 × 808)</p>
spoil+++

**Le premier ordinateur portable ?**

+++spoil nolabel
  +++image "/images/md/DAE7B3998D2F2FGACG688E524AFH5F" col-8 mx-auto
  <p class="text-center">1982 : Grid Compass (320 × 240)</p>
spoil+++

**Le premier téléphone portable ?**

+++spoil nolabel
  +++image "/images/md/6D34DB54978H47EF8H2399GBEHE24F" col-8 mx-auto
  <p class="text-center">1985 : Siemens Mobiltelefon C1 (?)</p>
spoil+++

**Le premier smartphone Apple ? le premier smartphone Android ?**

+++spoil nolabel

  <div class="col-10 mx-auto d-flex align-items-stretch">
    <div>
      +++image "/images/md/F23F8G8G9E2592FB63BHBFB7845AE8" col-10 mx-auto p-2
      <p class="text-center">2007 : Iphone 1 (320 x 480)</p>
    </div>
    <div style="background-color:black;width:2px;"></div>
    <div>
      +++image "/images/md/A27G59CB33CF8BC52BD6AG8A42CG53" col-10 mx-auto p-2
      <p class="text-center">2008 : HTC Dream (320 x 480)</p>
    </div>
  </div>
  
spoil+++

**Une des premières tablettes Android ? La première tablette iPad ?**

+++spoil nolabel

  <div class="col-10 mx-auto d-flex align-items-stretch">
    <div>
      +++image "/images/md/G4A846ED4DCE7EEG7FAH9C58F3B74H" col-10 mx-auto p-2
      <p class="text-center">2009 : Archos 5 (800 x 480)</p>
    </div>
    <div style="background-color:black;width:2px;"></div>
        <div>
      +++image "/images/md/7C2622C3G97HCCHC5AG72D688EH229" col-10 mx-auto p-2
      <p class="text-center">2010 : iPad 1 (768 x 1024)</p>
    </div>
  </div>

spoil+++

## La profusion actuelle des écrans et les problématiques soulevées

<style>
.bgr{
    background-color:#ff8f6c;
}
.bgg{
    background-color:#76f07e;
}
.bgb{
    background-color:#94dbd4;
}
</style>

<div class="d-flex flex-wrap justify-content-evenly col-10 mx-auto border border-dark rounded mb-3">
    <div class="p-2 m-2 border bgg rounded"><i class="fas fa-5x fa-laptop"></i></div>
    <div class="p-2 m-2 border bgg rounded"><i class="fas fa-5x fa-tablet-alt"></i></div>
    <div class="p-2 m-2 border bgg rounded"><i class="fas fa-5x fa-mobile-alt"></i></div>
    <div class="p-2 m-2 border bgg rounded"><i class="fas fa-5x fa-tv"></i></div>
    <div class="p-2 m-2 border bgg rounded"><i class="fas fa-5x fa-desktop"></i></div>
    <div class="p-2 m-2 border bgg rounded"><i class="far fa-5x fa-clock"></i></div>
</div>

<div class="d-flex flex-wrap justify-content-evenly col-10 mx-auto border border-dark rounded mb-3">
    <div class="p-2 m-2 border bgb rounded"><i class="fas fa-7x fa-car-side"></i></div>
    <div class="p-2 m-2 border bgb rounded"><i class="fas fa-7x fa-home"></i></div>
    <div class="p-2 m-2 border bgb rounded"><i class="fas fa-7x fa-city"></i></div>
</div>

Derrière ces écrans sont utilisées une multitudes d'applications, reposant sur une multitude de systèmes d'exploitations (+++flag-us <b>O</b>perating <b>S</b>ystem) possibles

+++bulle matthieu 
  petit rappel, qu'est-ce qu'un système d'exploitation ?
+++

+++spoil nolabel
  <ul>
    <li>Un système d'exploitation est un logiciel qui fait la passerelle entre les applications utilisées par un utilisateur (software : client email, logiciel de messagerie, navigateur web, ...) et le matériel de la machine hôte (le hardware : souris, clavier, écran tactile, puce de géolocalisation, ...). </li>
    <li>Un système d'exploitation offre à l'utilisateur un environnement qui peut être graphique (ou non) facilitant l'utilisation de l'ordinateur</li>
  </ul>
spoil+++

+++bulle matthieu droite
  saurez vous reconnaître ces logos ? navigateur web ou OS ?
+++

<div class="d-flex justify-content-center flex-wrap">
  <img class="m-2" style="max-height:80px" src="/images/md/7D29DHF24B3BDCGA5478A454F7624C">
  <img class="m-2" style="max-height:80px" src="/images/md/C97EBA4CGGE89E2G8H2D7FDE4F49D3">
  <img class="m-2" style="max-height:80px" src="/images/md/25B24HB4HEAG6GHEE33D4B3GG8FF72">
  <img class="m-2" style="max-height:80px" src="/images/md/7H423DEE5ECFAHF9456964A4DDC5A6">
  <img class="m-2" style="max-height:80px" src="/images/md/A7495A24E9646BA85AGE3B86C9HC59">
  <img class="m-2" style="max-height:80px" src="/images/md/A57F35DDD38622B2226ECFDHBGAC6G">
  <img class="m-2" style="max-height:80px" src="/images/md/5563AAAGFAE29BF56HG24CBB333D9B">
  <img class="m-2" style="max-height:80px" src="/images/md/64C68FCGAD99B8BE9C4D8H5HHG439G">
  <img class="m-2" style="max-height:80px" src="/images/md/B88D6BH43794G68HG57B5FB37EDC58">
</div>

---

+++bulle matthieu
  vous allez procédez à une petite expérience de pensée pendant 15 minutes
+++

- **le sujet** : vous devez rédiger les principales étapes permettant de créer un jeux vidéo de type "jeu de plateau" (jeux dchec ou jeu de dames par exemple) et de le rendre accessible au plus grand nombre (de la conception, la création du jeux à la mise en place et son maintien dans le temps)
- **les consignes** :
    - vous avez 15 minutes
    - vous travaillez en binóme ou trinôme
    - n'hésitez pas à détailler la partie concernant celle du métier de community manager
- **le rendu** :
    - des groupes seront désignés pour venir présenter au tableau le résultat de leurs réflexions (environ 2 minutes par groupe), nous procéderons à un vote collégial pour le groupe qui a présenté le projet semblant être **le plus viable**
    - les groupes devront également proposer un chiffrage du projet entre la phase de conception et la phase de production 

---

+++spoil "la question bonus (après la présentation des groupes)" nolabel text-center
  
  +++bulle matthieu
    et si cette réflexion avait été faite dans les années 1990 ?
  +++
  
  +++spoil "Réponse" text-start
    Le coût de la production du jeu aurait tout simplement explosé, d'une part parce que les moyens de diffusions sont plus onnéreux mais aussi parce qu'il faudrait un développement spécifique pour chaque plateforme ciblée
  spoil+++
  
spoil+++

### L'émergence de standards majeurs avec la venue d'Internet : l'HTML et le CSS

+++bulle matthieu
  qui peut nous rappeler ce que sont les langages HTML et CSS ? qu'elles sont les différences entre les deux ?
+++

+++spoil nolabel
  <ul>
    <li>HTML (+++flag-us <b>H</b>yper<b>T</b>ext <b>M</b>arkup <b>L</b>anguage) : langage de balisage dont la première version a été publiée en <b>1993</b>. L'HTML, en version 5 actuellememt, permet de <b>créer la structure et d'ajouter du contenu enrichi</b> dans une page Web</li>
    <li>CSS (+++flag-us <b>C</b>ascading <b>S</b>tyle <b>S</b>heets) : langage de style utilisant un système de cascade (héritage + surcharge) dont la première version est apparue fin 1996. Le CSS, en version 3 actuellement, permet d'apporter des décorations visuelles à une page Web</li>
  </ul>
spoil+++

+++bulle matthieu droite
  quelles ont été les premières fonctions principales d'un navigateur Web ? 
+++

+++spoil nolabel
  Le navigateur Web permet de communiquer avec le réseau Internet via le protocole HTTP (et HTTPS). Il permet également de traduire du code HTML, CSS et Javascript en une interface graphique.
spoil+++

---

L'arrivée d'Internet et du Web dans les années 1990, offrant un canal de communication mondial, ont grandement contribué au développement de normes et à la démocratisation de l'informatique.


## Focus sur les smartphones

+++citation
  Global smartphone shipments grew 4% YoY to reach <b>1.39 billion units</b> in 2021. Q4 2021 shipments declined 6% YoY to reach 371 million units.
+++site web counterpointresearch https://www.counterpointresearch.com/global-smartphone-share/

+++citation
  A strong end to 2021 saw global PC shiments grow 15% <b>to reach 341 million</b> units, the highest number <b>since 2012</b>
+++site web canalys https://www.canalys.com/newsroom/global-pc-market-Q4-2021

+++bulle matthieu
  petite mise en situation, en tant que community manager, vous devez créer un nouvel article sur un agriculteur récemment devenu fournisseur de la biocop pour laquelle vous travaillez
+++

+++bulle matthieu droite
  quels points d'attentions devriez vous avoir vis à vis des utiliseurs lecteurs qui utilisent leur smartphone ? quels outils peuvent nous aider ?
+++

{{textarea "Nos réponses..." 4 "w-100 px-1"}}