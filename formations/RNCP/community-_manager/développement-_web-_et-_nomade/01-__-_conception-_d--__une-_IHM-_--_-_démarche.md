# Conception d'une IHM : démarche

+++programme
  Concept de Marcotte, Mobile First
  Séparation contenu/contenant
  Approche paysage/portrait, tactile, impact sur l’ergonomie
  Plan de tests
  Différence entre design web et design mobile
  
  Définir des termes techniques importants relatifs au design d'une page Web
  Créer des maquettes responsives
  Créer et manipuler un plan de tests
+++

+++sommaire

## Le concept de Responsive Web Design

Nous avons vu que les langages HTML et CSS ont permis depuis les années 2000 de créer des sites Web accessibles depuis n'importe quel navigateurs. Hors, cela n'a plus suffit avec l'explosion du marché des smartphones qui ont introduit une multitude de tailles d'écrans différents.

<div class="text-center">
  <img src="/images/md/F3BC936GD43BCHH3BC4DCF5323CGH3" alt="photo ethan marcotte" class="">
  <p>ethan marcotte</p>
</div>

En 2011, Ethan Marcotte (designer et wev développeur) introduit le concept de **R**esponsive **W**eb **D**esign. Le principe ? adapter le contenu d'un site Web en fonction de la résolution de l'écran qui le consulte :

<div class="col-10 mx-auto d-flex align-items-center justify-content-evenly">
  <div class="col-4">
    +++image "/images/md/DBB3528CFFB74EC5498A94F52EFH99" col-10 mx-auto p-2
  </div>
  <div class="col-6">
    +++image "/images/md/G72DF9H7ED4BH7GEBH3H33C7HCDAGH" col-10 mx-auto p-2
  </div>
</div>

Le langage CSS version 3 (utilisé depuis 1999) permet l'ajout de modules. Un nouveau module dédié au Responsive Web Design fait son arrivée en 2012 et permet alors de décorer une page Web avec du CSS tout en prenant en compte la résolution de l'écran. Voici un exemple : 

+++image "/images/md/2E62E9FHF2FA2GF254GD452E9CH8G6" col-6 mx-auto

+++spoil "Explication du code" nolabel
  Ce qui suit l'instruction <code>@media</code> est une requête (ligne 5). On apelle ce type de ligne une <b>media query</b>. Si les conditions de cette <b>media query</b> sont remplies (si l'apparail à un écran et si sa largeur n'exède pas les 320 pixels), alors tous les titres feront une taille de <code>3em</code>. 
spoil+++

+++bulle matthieu
  que fait le code CSS suivant ?
+++

+++image "/images/md/8C4F843C4B5A9FHBD3DE6D5737F769" col-6 mx-auto

### Le mobile first

L'ordinateur dominait le marché avant l'arrivée des smartphones. L'arrivée de ces derniers a bouleversé la manière de consevoir un site Web ou une application. Le contenu est alors d'abord pensé pour le mobile en priorité et les <b>media queries</b> permettent d'enrichir le contenu pour les tablettes puis pour les ordinateurs classiques (+++flag-us desktop).

Par conséquent, la plupart des frameworks CSS aujourd'hui tels que +++lien "https://getbootstrap.com/docs/5.2/examples" "bootstrap" ou encore +++lien "https://tailwindcss.com/" "tailwindcss" suivent l'approche mobile first. 

+++bulle matthieu
  dans les langages informatiques, qu'est-ce qu'une bibliothèque ? et qu'est-ce qu'un framework ? quelle est la différence entre les deux ?
+++

+++spoil nolabel
  <p>Dans les langages informatiques, une bibliothèque contient un ensemble de fonctions pré-faîtes permettant d'accélérer le travail du développeur. En CSS, il s'agirait d'un fichier contenant des règles CSS déjà écrites et prêtes à l'emploi.</p>
  
  <p>Un framework ( +++flag-fr cadre de travail ) est une bibliothèque ou un ensemble de bibliothèques qui, en plus, force le développeur à concevoir son application d'une manière contrainte par le framework. Par exemple, +++lien "https://getbootstrap.com/docs/5.2/examples" "bootstrap" découpe par défaut une page web en douze colonnes invisibles, et le framework permet de placer facilement des objets graphiques par rapport à ces 12 colonnes </p>
  
  +++image "/images/md/AG743ECC3A4B88958CDDH2G59ECA4H" col-10 mx-auto
spoil+++

---

+++image "/images/md/2367H653DF5785FBC38E27E2F8C6H9" col-10 mx-auto

Ici, 'content' est traduit par 'contenu'. Suivre le concept du Responsive Web Design revient à obtenir un contenu qui s'adapte quelque soit son contenant. Un <b>site responsive</b> est un site Web dont le contenu s'adapte en fonction du contenant. 

## L'ergonomie Web aujourd'hui

Étant donné :

- qu'il est possible d'accéder à un site Web depuis différents appareils : ordinateurs, tablettes, smartphones, ...
- que les smartphones et tablettes utilisent une multitude de résolutions possibles, en plus du mode 'portait' ou 'paysage'

**Comment garantir que son site Web est ergonomique ?**

+++bulle matthieu
  commençons déjà par définir ce qu'est l'ergonomie, et plus précisemment, qu'est-ce qu'un site Web ergonomique ?
+++

+++spoil nolabel
  L'ergonomie englobe deux points principaux : l'<b>utilité</b> et l'<b>utilisabilité</b>
  
  <ul>
    <li>L'<b>utilité</b> : est-ce que le site Web est utile ? est-ce qu'il répond à un besoin ?</li>
    <li>L'<b>utilisabilité</b> : est-ce que l'utilisateur peut, à travers le site, répondre au besoin recherché ?</li>
  </ul>
  
  Le premier point ne concerne par le Responsive Web Design, mais le second si !
spoil+++

---

L'UX design est un domaine dont le but est de répondre au mieux aux enjeux d'utilisabilités dans une application informatique ou dans un site Web. L'UX design peut être découpée en trois grands points : 

- L'**efficacité** : est-ce que l'utilisateur peut arriver à ses fins ?
- L'**efficience** : est-ce que le parcours de l'utilisateur est rapide et sans erreur ?
- La **satisfaction** : est-ce que le parcours de l'utilisateur est agréable ? est-ce que l'utilisateur peut découvrir de nouvelles choses suceptibles de l'intéresser ?

Dans le cas de <b>sites responsives</b>, nous parlons alors d'<b>UX design responsive</b>.

En tant que futurs community managers, le concept d'<b>UX design responsive</b> est important !

+++bulle matthieu
  à vos claviers ! prenez 15 minutes pour vous renseigner sur l'<b>UX design responsive</b> et relever les points qui seront importants dans votre futur emploi, quels outils peuvent vous aider à concevoir une interface graphique responsive ?
+++

{{textarea "Liste des points d'attention à avoir entre le métier de Community Manager et l'UX design responsive" 4 "w-100 px-1"}}

--- 

+++bulle matthieu
  l'exercice suivant consiste à prendre en main un outil permettant de créer des maquettes responsives pour une page Web 
+++

- **le sujet** : vous travaillez pour un restaurant qui souhaite mettre en valeur sa nouvelle carte des menus. Cette nouvelle carte sera affichée sur le site du restaurant. Le responsable informatique vous informe que les apparails les plus utilisés par les visiteurs du site sont la tablette "Samsung Galaxy Tab S7+" et le smartphone "iPhone 6"
- **les consignes** :
    - vous avez 60 minutes
    - vous travaillez en binóme ou trinôme 
    - votre maquette doit prendre en compte l'affichage via la tablette "Samsung Galaxy Tab S7+" (portrait et paysage) et via le smartphone "iPhone 6" (portrait et paysage)
- **le rendu** :
    des groupes seront désignés pour venir présenter au tableau le résultat de leurs travaux et leurs réflexions sur la disposition graphique des éléments en fonction du contenant (environ 5 minutes par groupe)

## Plan de tests

Vous pouvez être amenés à concevoir des maquettes, mais également à tester le résultat du rendu final ! Hors : 

- il se peut qu'entre le moment de la conception et le moment du test, plusieurs semaines se soient écoulées. Vous avez donc oublié en partie les points d'attentions importants concernant les objectifs des maquettes.
- il se peut que le nombre de devices à tester soit conséquent et, que donc, le temps total de test exède plusieurs jours. Il y aura alors possiblement plusieurs personnes désignées pour effecter les tests. 

Il faut rédiger au plus tôt un scénario de tests afin d'éviter les mauvaises surprises par la suite.

+++bulle matthieu
  rédiger des tests avant le développement d'un contenu peut également être très utile pour la ou les personnes qui se chargeront de créer le contenu
+++

+++bulle matthieu droite
  Il s'agit d'une excellente pratique en informatique et qui s'inscrit dans la démarche du <b>TDD</b> (+++flag-us <b>T</b>est <b>D</b>riven <b>D</b>evelopment)
+++

Dans +++lien "/d/formations/cahier_de_tests.ods" "ce lien" se trouve un cahier de tests généralistes.

+++bulle matthieu
  adaptez/modifiez/supprimez/ajoutez pendant 10mn des tests afin de répondre au mieux aux problèmes qui pourraient être rencontrés avec la maquette que vous avez créé dans l'exercice précédent
+++

+++bulle matthieu droite
  ajouter deux nouveaux appareils dans le cahier des tests
+++

## Designer un site Web ou une application mobile, les différences

+++bulle matthieu
  prenez quelques minutes pour lire cet +++lien "https://curiouscore.com/ux-design-jobs/differences-between-designing-for-mobile-and-web" "excellent article" et notez les points qui vous semblent importants
+++

+++bulle matthieu droite
  si vous êtes en avance, vous pouvez compléter votre lecture par d'autres recherches sur le Web
+++

{{textarea "Les principales différences de design entre le Web et les applications mobiles " 4 "w-100 px-1"}}