# Les médiaqueries

## Introduction

La démocratisation d'Internet dans les années 2000 a radicalement changé nos habitudes et le **rapport entre l'Informatique et le grand public**. Cette dernière, plus accessible, s'adapte aux usages quotidiens et il en résulte alors depuis quelques années une véritable profusion d'appareils en tout genre en plus du classique ordinateur : 

- les tablettes 
- les smartphones 
- les montres connectées 
- et bien plus encore : les appareils électroménagers connectés, les terminaux interactifs de villes connectées, les télévisions, ...

+++bulle matthieu
  les services d'un développeur informatique, ce n'est pas donné ! 
+++

+++bulle matthieu droite
  la plupart des entreprises n'ont pas les moyens de recruter un développeur par type d'appareil 
+++

D'une manière générale et pour répondre à ce genre de problématique, des outils sont développés pour permettre avec un seul et même programme de produire des applications adaptatives. Dans le cadre du développement Web, l'outils fondamental utilisé est un module intégré à la version 3 du langage CSS en 2012.

## Quelques exemples avec le CSS

<pre>\<!-- dans une balise body depuis un fichier HTML -->

\<div class="rectangle">
  ceci est un rectangle
\</div>
</pre>

<pre>\<!-- dans un fichier CSS -->

.rectangle{
  background-color:lightgreen;
}
</pre>

Le résultat : 

<style>
.rectangle{
  background-color:lightgreen;
}
</style>

<div class="rectangle">
  ceci est un rectangle
</div>

<br>

+++bulle matthieu
  nous souhaitons maintenant que le rectangle change de couleur lorsque la taille de l'écran est celle d'un téléphone portable 
+++

+++bulle matthieu droite
  il faut alors légèrement modifier le fichier CSS comme suit
+++

<pre>\<!-- dans un fichier CSS -->

\<!-- le rectangle doit toujours être vert... -->
.rectangle{
  background-color:lightgreen;
}

\<!-- ...sauf lorsqu'on visualise la page Web dans une fenêtre faisant moins de 500 pixel, dans ce cas il doit être bleu -->
@media(max-width: 600px) {
  .rectangle{
    background-color:lightblue;
  }
}
</pre>

Testons le résultat...le rectangle devient bleu lorsque vous retrecissez la fenêtre ! 

<style>
.rectangle2{
  background-color:lightgreen;
}

@media(max-width: 600px) {
  .rectangle2{
    background-color:lightblue;
  }
}
</style>

<div class="rectangle2">
  ceci est un rectangle
</div>

## Utilisation de media query avec bootstrap 

Bootstrap fait parti des frameworks CSS les plus connus. Ce dernier mets à disposition des classes CSS prêtes à l'emploi et qui s'adaptent en fonction de la largeur de l'écran utilisé.

Les classes Bootstrap qui peuvent s'adapter en fonction de la largeur de l'écran utilisent +++lien "https://getbootstrap.com/docs/5.0/layout/breakpoints/" "les infixes indiqués dans ce lien".

Testons ensemble quelques exemples : 

- +++lien "https://getbootstrap.com/docs/5.0/layout/grid/#responsive-classes" "faire varier la largeur des plusieurs blocs en fonction de la taille de l'écran"
- +++lien "https://getbootstrap.com/docs/5.0/utilities/spacing/" "gérer les espaces entre des blocs en fonction de la taille de l'écran"
- +++lien "https://getbootstrap.com/docs/5.0/utilities/display/" "afficher ou cacher des éléments en fonction de la taille de l'écran"
