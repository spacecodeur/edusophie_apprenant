# Pages web simples

Ci-dessous sont listées 3 pages web au code épuré. Ces exemples sont utiles pour montrer des exemples simples de page à de jeunes développeurs web en herbe.

**Les 3 pages :**

- [page html simple](/pageSimple/simple.html)
- [page html contenant du css](/pageSimple/avecCSS.html)
- [page html contenant du css et du javascript](/pageSimple/avecCSSetJavascript.html)